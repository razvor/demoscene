#include "LightMesh.h"

LightMesh::LightMesh(glm::vec3 pos, glm::vec3 color){
	mLightType = LightType::Point;
	mColor = color;
	setPosition(pos);
}

LightMesh::LightMesh(SphereLight *sl){
	mLightType = LightType::Sphere;
	mColor = sl->color;
	setPosition(sl->position);
	mSl = sl;
}

LightMesh::LightMesh(RectangleLight *rl){
	mLightType = LightType::Rectangle;
	mColor = rl->color;
	setPosition(rl->position);
	mRl = rl;
}

LightMesh::LightMesh(SpotLight *sp) {
	mLightType = LightType::Spot;
	mColor = sp->color;
	setPosition(sp->position);
	mSp = sp;
}

LightMesh::~LightMesh(){

}

void LightMesh::setGDepth(Texture *t){
	mGDepth = t;
}

void LightMesh::init(){

	switch(mLightType){
		case LightType::Point:
			initGeometry();
			break;
		case LightType::Sphere:
			initSphereGeometry(mSl->radius);
			//initGeometry();
			break;
		case LightType::Rectangle:
			initRectGeometry();
			break;
		case LightType::Spot:
			initSpotGeometry();
			break;
		default:
			break;
	}

	setTangents(std::vector<glm::vec3>(1000));
	setBiTangents(std::vector<glm::vec3>(1000));

	setType(1);

	//initGeometry();
	//initProgram();
	RenderMesh::init();
}

void LightMesh::initGeometry(){
  std::vector<glm::vec3> verts = {
		//front
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0),

		//back
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(-1.0, -1.0, 1.0),

		//top
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(-1.0, 1.0, -1.0),

		//bottom
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0),

		//right
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, -1.0, -1.0),

		//left
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0)

	};

	std::vector<int> inds;
	for(int i=0; i<36; ++i){
		inds.push_back(i);
	}

	glm::vec3 nds[] = {
		glm::vec3(0, 0, -1.0),
		glm::vec3(0, 0, 1.0),
		glm::vec3(0, 1.0, 0),
		glm::vec3(0, -1.0, 0),
		glm::vec3(1.0, 0, 0),
		glm::vec3(-1.0, 0, 0)
	};

	std::vector<glm::vec3> norms;
	for(int i=0; i<6; ++i){
		for(int j=0; j<6; ++j){
			norms.push_back(nds[i]);
		}
	}

	std::vector<glm::vec2> texCords;
	for(int i=0; i<6; ++i){
		texCords.push_back(glm::vec2(0, 0));
		texCords.push_back(glm::vec2(0, 1));
		texCords.push_back(glm::vec2(1, 1));
		texCords.push_back(glm::vec2(1, 1));
		texCords.push_back(glm::vec2(1, 0));
		texCords.push_back(glm::vec2(0, 0));
	}

	setVertices(verts);
	setNormals(norms);
	setIndices(inds);
	setTexCords(texCords);

	setRotation(glm::vec3(0));
	setScale(glm::vec3(0.5));
}

void LightMesh::initSphereGeometry(float radius){
	const int density = 32;
	//const float radius =  //mSl->radius;

	std::vector<glm::vec3> vertices, normals;
	std::vector<glm::vec2> texCords; 
	std::vector<int> indices;

	float S = 2.f * M_PI / (float)(density-1);
	for(int i=0; i<density; ++i){
		for(int j=0; j<density; ++j){
			float r = cosf( i * S - M_PI / 2.f ) * radius;
			float y = sinf( i * S - M_PI / 2.f ) * radius;
			float x = cosf( j * S ) * r;
			float z = sinf( j * S ) * r;

			vertices.push_back(glm::vec3(x, y, z));
			normals.push_back(glm::normalize(glm::vec3(x, y, z)));
			texCords.push_back(glm::vec3(0));

			int a,b,c,d;
			a = i * density + j;
			b = i * density + j + 1;
			c = (i+1) * density + j + 1;
			d = (i+1) * density + j;

			indices.push_back( a );
			indices.push_back( b );
			indices.push_back( c );
			indices.push_back( c );
			indices.push_back( d );
			indices.push_back( a );
		}
	}

	setVertices(vertices);
	setNormals(normals);
	setIndices(indices);
	setTexCords(texCords);

	setRotation(glm::vec3(0));
	setScale(glm::vec3(1.0));

}

void LightMesh::initSpotGeometry() {
	initSphereGeometry(0.1);
}

void LightMesh::initRectGeometry(){
	
	const float width = mRl->width;
	const float height = mRl->height;
	const glm::mat4 matrix = mRl->matrix; //glm::rotate( glm::mat4(1), 45.f, glm::vec3(1, 0, 0) );

	std::vector<glm::vec3> vertices, normals;
	std::vector<glm::vec2> texCords; 
	std::vector<int> indices;

	float d = min(width, height) / 3.f;

	vertices = {
		glm::vec3(-0.5 * width, 0.0, -0.5 * height),
		glm::vec3(-0.5 * width, 0.0, 0.5 * height),
		glm::vec3(0.5 * width, 0.0, 0.5 * height),
		glm::vec3(0.5 * width, 0.0, -0.5 * height),
		glm::vec3(0, 0.5 * d, 0)
	};
	for(int i=0; i<5; ++i){
		vertices[i] = glm::mat3(matrix) * vertices[i];
	}

	normals = {
		glm::vec3(0, 1, 0),
		glm::vec3(0, 1, 0),
		glm::vec3(0, 1, 0),
		glm::vec3(0, 1, 0),
		glm::vec3(0)
	};

	texCords.resize(4, glm::vec3(0));

	indices = {0, 1, 4, 1, 2, 4, 2, 3, 4, 3, 0, 4};

	setVertices(vertices);
	setNormals(normals);
	setIndices(indices);
	setTexCords(texCords);

	setRotation(glm::vec3(0));
	setScale(glm::vec3( 1 ));
}

void LightMesh::initProgram(){
	mProgram = Program("light", "light");
	if(!mProgram.init()){
		std::cerr << "Program init ERROR " << std::endl;
	}
}

void LightMesh::initLocations(){
	RenderMesh::initLocations();
	unifLocations.color = mProgram.uniform("color");
}

void LightMesh::pushUniforms(){
	//setGridSize(0);
	//setLights(NULL);

	RenderMesh::pushUniforms();
	glUniform3f( unifLocations.color, mColor.x, mColor.y, mColor.z );
}
