#ifndef IMAGE_LOADER
#define IMAGE_LOADER

#include "includer.h"
#include "stb/stb_image.h"
#include <algorithm>
#include <numeric>
#include <future>

using callback = std::function<void(unsigned char *, int, int)>;

class ImageLoader{
private:

	//std::vector<std::function<void()>> onLoad;
	//std::vector<std::string> mFileNames;
	//static ImageLoader* instance;
	//static ImageLoader* getInstance();

	std::string mFileName;

	std::thread mThread;
	callback mCallback;
	void loadAsync();


public:

	ImageLoader(std::string fl, callback fn);
	~ImageLoader();

};

#endif
