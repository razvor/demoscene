#ifndef LIGHT
#define LIGHT

#include "includer.h"
#include "Texture.h"


class Light{
private:
	

	int mIndex;


public:
	Program program;
	std::string makePath(std::string n);
	//const char* makePath(std::string n);

	std::string mUniformName;

	virtual void init(Program program, int ind);
	virtual void pushToShader();

	Light();
	~Light();

};

class DirectionalLight : Light{
private:

	std::string mDirectionName = "direction";
	std::string mAmbientName   = "ambient";
	std::string mDiffuseName   = "diffuse";
	std::string mSpecularName  = "specular";

	int mDirLoc, mAmbLoc, mDiffLoc, mSpecLoc;

public:

	glm::vec3 direction;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;

	void init(Program program, int ind);
	void pushToShader();

	DirectionalLight();
	~DirectionalLight();

};

class PointLight : Light{
private:

	std::string mPositionName  = "position";
	std::string mAmbientName   = "ambient";
	std::string mDiffuseName   = "diffuse";
	std::string mSpecularName  = "specular";
	std::string mConstantName  = "constant";
	std::string mLinearName    = "linear";
	std::string mQuadraticName     = "quadratic";

	int mPosLoc, mAmbLoc, mDiffLoc, mSpecLoc, mConsLoc, mLinLoc, mQuadLoc;

public:

	glm::vec3 position;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;

	float constant,
		  linear,
		  quadratic;

	void init(Program program, int ind);
	void pushToShader();

	PointLight();
	~PointLight();
};

class SpotLight : Light {
private:
	std::string mPositionName = "position";
	std::string mRadiusName = "radius";
	std::string mIntensityName = "intensity";
	std::string mColorName = "color";
	std::string mDirectionName = "direction";
	std::string mFovName = "fov";

	int mPosLoc, mRadLoc, mIntLoc, mColLoc, mDirLoc, mFovLoc;

public:
	glm::vec3 position;
	float radius;
	float intensity;
	glm::vec3 color;
	glm::vec3 direction;
	float fov;

	void init(Program progam, int ind);
	void pushToShader();

	SpotLight();
	~SpotLight();

};

class SphereLight : Light{
private:
	std::string mPositionName  = "position";
	std::string mRadiusName = "radius";
	std::string mIntensityName = "intensity";
	std::string mColorName = "color";
	
	int mPosLoc, mRadLoc, mIntLoc, mColLoc;
	
public:
	
	glm::vec3 position;
	float radius;
	float intensity;
	glm::vec3 color;
	
	void init(Program progam, int ind);
	void pushToShader();

	SphereLight();
	~SphereLight();
	
};

class RectangleLight : Light{
private:

	std::string mPositionName  = "position";
	std::string mIntensityName = "intensity";
	std::string mWidthName     = "width";
	std::string mHeightName    = "height";
	std::string mColorName     = "color";
	std::string mMatrixName    = "matrix";
	//TODO: send to shader only p0, p1, p2, p3 light quad angle points

	int mPosLoc, mIntLoc, mWidLoc, mHeiLoc, mColLoc, mMatLoc;

public:
	
	glm::vec3 position;
	glm::mat4 poseMatrix;
	glm::vec3 color;
	float intensity;
	float width;
	float height;
	glm::mat4 matrix;
	
	void init(Program program, int ind);
	void pushToShader();

	RectangleLight();
	~RectangleLight();
	
};

struct Lights{
	DirectionalLight *directionalLight;
	std::vector<PointLight> pointLights;
	std::vector<SpotLight> spotLights;
	std::vector<SphereLight> sphereLights;
	std::vector<RectangleLight> rectangleLights;
	Texture *ltc1, *ltc2;

	int pointCountLoc, sphereCountLoc, spotCountLoc, rectangleCountLoc;
	Program program;


	void init(Program program);
	void pushToShader();

	Lights();
	~Lights();
};

#endif
