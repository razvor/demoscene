#include "RoomModel.h"

RoomModel::RoomModel() : Model("/erato/room3.obj") {}

RoomModel::~RoomModel() {

}

std::vector<int> RoomModel::getFaceNormalMeshes() {
	std::vector<int> r = { 0, 1, 2, 3, 38, 75, 76, 77 };
	for (int i = 87; i <= 466; ++i) r.push_back(i);
	return r;
}

void RoomModel::postProcess() {
	std::cout << "Post process" << std::endl;

	initMaterials();
}

void RoomModel::initMaterials() {
	int cnt = getMeshesCount();

	//Child 0 - walls
	//Child 1 - floor
	//Child 2 - celling
	//Child 3 - window frame
	//Child 4 - carpet
	//Child 4, 36, 37 - carpet
	//Child 5 - lamps on the celling
	//Child 6,7,8,9,10,11,12,13 - chair 1 
	//Child 14,15,16,17,18,19,20,21 - char 2
	//Child 22, 73, 74, 79, 80, 81, 82, 83, 84, 85, 86 - bottle
	//Child 23, 24 - plates
	//Child 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35 - palm
	//Child 38 - table
	//Child 39, 87, 88, 89, 90, 91-466 - books
	//Child 42, 43, 44 - lamp on floor
	//Child 67, 70, 71, 72 - poof
	//Child 68, 69 - bed base
	//Child 75(base), 76(doors bottom), 77(doors top), 78(grips) - closet
	//Child 467, 468, 469, 470, 471, 472, 473, 474 - tv

	setMaterial(new Material(glm::vec4(0.1, 0.1, 0.1, 1), 0.999, 0.0));

	vector<Material*> materials;
	materials.resize(475, NULL);

	//materials[0] = new Material("../models/room_materials/wall1");
	materials[0] = new Material(glm::vec4(0.31, 0.26, 0.26, 1.0), 1.0, 0);
	materials[1] = new Material("../models/room_materials/wood6");
	materials[2] = new Material(glm::vec4(0.9, 0.9, 0.9, 1.0), 0.999, 0);
	materials[3] = new Material(glm::vec4(0.1, 0.14, 0.1, 1.0), 0.999, 0);
	materials[4] = new Material("../models/room_materials/fabric5");
	materials[5] = new Material(glm::vec4(0.7, 0.7, 0.7, 1.0), 0.6, 0.3);

	//carpet
	auto carpetMaterial = new Material("../models/room_materials/fabric7");
	//auto carpetMaterial = new Material("../models/wool3");
	materials[36] = carpetMaterial;
	materials[37] = new Material(*carpetMaterial);
	//closet
	auto closetDoorMaterial = new Material("../models/room_materials/wood5");
	materials[77] = new Material(*closetDoorMaterial); //new Material(glm::vec4(0.77, 0.77, 0.77, 1.0), 0.5, 0.3);
	materials[76] = new Material(glm::vec4(0.77, 0.77, 0.77, 1.0), 0.3, 0.0); //new Material(*closetDoorMaterial);
	materials[75] = closetDoorMaterial; //new Material(glm::vec4(0.8, 0.8, 0.7, 1.0), 0.999, 0);

	//table
	materials[38] = new Material(*closetDoorMaterial); //new Material(glm::vec4(0.2, 0.2, 0.1, 1.0), 0.3, 0.1);

	//bottle
	auto bottleMaterial = new Material(glm::vec4(0.5, 0.5, 0.5, 0.1), 0.2, 0.05, 1.5);
	std::vector<int> bottle = { 22, 28, 73, 74, 79, 80, 81, 82, 83, 84, 85, 86, 23, 24 };
	for (auto b : bottle) materials[b] = new Material(*bottleMaterial);

	//tv
	auto tvMaterial = new Material(glm::vec4(0.05, 0.05, 0.05, 1.0), 0.3, 0.1);
	std::vector<int> tv = { 465, 466, 467, 468, 469, 470, 471, 472, 473, 474 };
	for (auto t : tv) materials[t] = new Material(*tvMaterial);
	materials[470] = new Material(glm::vec4(0.1, 0.1, 0.1, 1.0), 0.1, 0.6);

	//books
	//std::vector<int> books = { 467, 468, 469, 470, 471, 472, 473, 474 };
	auto booksMaterial = new Material("../models/room_materials/books");
	Material *mtl;
	srand(time(0));
	for (int i = 87; i <= 464; ++i) {
		/*int rnd = rand() % 3;
		if (rnd == 1) mtl = new Material(glm::vec4(0.67, 0.62, 0.54, 1.0), 1, 0);
		if (rnd == 2) mtl = new Material(glm::vec4(0.12, 0.1, 0.1, 1.0), 1, 0);
		else mtl = new Material(glm::vec4(0.2, 0.17, 0.13, 1.0), 1, 0);*/

		mtl = new Material(*booksMaterial);		
		materials[i] = mtl;
	}

	//poof and bed
	auto poofMaterial = (new Material("../models/room_materials/fabric5"));
	auto poofInds = {67, 72, 64, 65, 66, 70};
	for (int i : poofInds) {
		materials[i] = new Material(*poofMaterial);
	}
	materials[68] = new Material(glm::vec4(0.5,0.5,0.5,1.0), 0.5, 0.8);
	materials[69] = new Material(glm::vec4(0.5, 0.5, 0.5, 1.0), 0.5, 0.8);
	materials[71] = new Material(glm::vec4(1), 0.999, 0.0);

	//chair
	auto chairSitMaterial = new Material(glm::vec4(1.0), 0.05, 0.1); //new Material("../models/room_materials/leather1");
	auto chromeMaterial = new Material(glm::vec4(0.5, 0.5, 0.5, 1.0), 0.2, 0.8);
	materials[13] = chairSitMaterial;
	for (int i = 6; i < 13; ++i) {
		materials[i] = new Material(*chromeMaterial);
	}
	materials[21] = new Material(*chairSitMaterial);
	for (int i = 14; i < 21; ++i) {
		materials[i] = new Material(*chromeMaterial);
	}

	//lamp
	auto borderMaterial = new Material(glm::vec4(0.01, 0.01, 0.01, 1.0), 0.3, 0.3);
	materials[42] = new Material(*chromeMaterial);
	materials[43] = borderMaterial; //new Material(*chromeMaterial);
	materials[44] = new Material(*chromeMaterial);

	//palm
	auto potMaterial = new Material(*chromeMaterial);
	auto trunkMaterial = new Material("../models/room_materials/trunk1");
	auto leafMaterial = new Material("../models/room_materials/leaf1");
	materials[35] = potMaterial;
	for (int i = 25; i < 29; ++i) {
		materials[i] = new Material(*trunkMaterial);
	}
	for (int i = 29; i < 35; ++i) {
		materials[i] = new Material(*leafMaterial);
	}

	for (int i = 0; i < materials.size(); ++i) {
		if (materials[i] == NULL) continue;
		for (auto m : root->children[i]->meshes) {
			(m)->setMaterial(
				materials[i]
				//new Material(glm::vec4(1, 0, 0, 1), 0.6, 0.1)
			);
		}
	}

	

	//face normals
	/*for (int i = 0; i < 400; ++i) {
		for (auto m : root->children[0]->meshes) {
			m->computeFaceNormals();
			m->init();
		}
	}*/

	/*for (int i = 0; i < 1; ++i) {
		int e = 475 + i;
		for (auto m : root->children[e]->meshes) {
			(m)->setMaterial(new Material(glm::vec4(1, 0, 0, 1), 0.6, 0.1));
		}
		for (auto m : root->children[e+1]->meshes) {
			(m)->setMaterial(new Material(glm::vec4(0, 1, 0, 1), 0.6, 0.1));
		}
		for (auto m : root->children[e+2]->meshes) {
			(m)->setMaterial(new Material(glm::vec4(0, 0, 1, 1), 0.6, 0.1));
		}
	}*/

	//celling (200, 255)
	//3 walls and floor(225)

	//for (auto m : root->children[225]->meshes) {
		//(root->children[225]->meshes[0])->setMaterial(new Material(glm::vec4(0, 1, 1, 1), 0.6, 0.1));
	//}

	//((RenderMesh*)getMesh(50))->setMaterial(new Material(glm::vec4(1.0, 0, 0, 1), 0.6, 0.1));


}

void RoomModel::hightlight() {
	if (mHightlighten - 1 > 0) {
		for (auto m : root->children[mHightlighten - 1]->meshes) {
			m->setMaterial(new Material(glm::vec4(0, 0, 0, 1), 1, 0));
		}
	}
	for (auto m : root->children[mHightlighten]->meshes) {
		m->setMaterial(new Material(glm::vec4(1, 0, 0, 1), 1, 0));
	}
	std::cout << "Now hightlighten " << mHightlighten << std::endl;
	mHightlighten++;
}