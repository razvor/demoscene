#include "WallsMesh.h"

WallsMesh::WallsMesh(){

}

WallsMesh::~WallsMesh(){

}

void WallsMesh::initGeometry(){

	float xSize = 10.f, 
		  ySize = 3.f, 
		  zSize = 10.f;

	std::vector<glm::vec3> verts = {
		//front
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0),

		//back
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(-1.0, -1.0, 1.0),

		//right
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, -1.0, -1.0),

		//left
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0)

	};

	for(int i=0; i<24; ++i){
		verts[i].x *= xSize / 2.f;
		verts[i].y *= ySize / 2.f;
		verts[i].z *= zSize / 2.f;
	}

	std::vector<int> inds;
	for(int i=0; i<24; ++i){
		inds.push_back(i);
	}

	glm::vec3 nds[] = {
		glm::vec3(0, 0, 1.0),
		glm::vec3(0, 0, -1.0),
		glm::vec3(-1.0, 0, 0),
		glm::vec3(1.0, 0, 0.0)
	};

	std::vector<glm::vec3> norms;
	norms.clear();
	for(int i=0; i<4; ++i){
		for(int j=0; j<6; ++j){
			norms.push_back(nds[i]);
		}
	}

	std::vector<glm::vec2> texCords;
	for(int i=0; i<4; ++i){
		texCords.push_back(glm::vec2(0, 0));
		texCords.push_back(glm::vec2(0, 1));
		texCords.push_back(glm::vec2(1, 1));
		texCords.push_back(glm::vec2(1, 1));
		texCords.push_back(glm::vec2(1, 0));
		texCords.push_back(glm::vec2(0, 0));
	}
	float uvScale = 0.2f;
	for(int i=0; i<12; ++i){
		texCords[i].y *= ySize * uvScale;
		texCords[i].x *= zSize * uvScale;
	}
	for(int i=12; i<24; ++i){
		texCords[i].x *= ySize * uvScale;
		texCords[i].y *= zSize * uvScale;
	}

	setVertices(verts);
	setNormals(norms);
	setIndices(inds);
	setTexCords(texCords);

	setPosition(glm::vec3(0, ySize/2.f, 0));
	setRotation(glm::vec3(0));
	setScale(glm::vec3(1));
}

void WallsMesh::initMaterial(){
	Material *material = new Material("../models/wall5");
	setMaterial(material);
}

void WallsMesh::initProgram(){
	mProgram = Program("mainVertex", "firstPass");
	if(!mProgram.init()){
		std::cerr << "Program init ERROR " << std::endl;
	}
}

void WallsMesh::init(){
	initGeometry();
	initMaterial();
	RenderMesh::init();
}
