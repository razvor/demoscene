#ifndef FLOOR_MESH
#define FLOOR_MESH

#include "includer.h"
#include "RenderMesh.h"

class FloorMesh : public RenderMesh{
private:
	struct UniformLocations{
	  int viewMatrix,
		  projectionMatrix,
		  modelMatrix,
		  viewPosition;
	} unifLocations;

	void initGeometry();
public:
	void initLocations();
	void pushUniforms();
	void initProgram();

	void init();

	FloorMesh();
	~FloorMesh();
};

#endif
