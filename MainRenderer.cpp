#include "MainRenderer.h"
#include "LightMesh.h"
#include "WallsMesh.h"
#include "CellingMesh.h"
#include "ObjMesh.h"
#include "RoomModel.h"

//#include "glMessageCallback.h"

void MainRenderer::initGL(){
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	//glEnable(GL_DEPTH_TEST);
	//glEnable(GL_MULTISAMPLE);
	//glDisable(GL_DEPTH_CLAMP);
	
	//glEnable(GL_LESS);
	glEnable(GL_DEPTH_TEST);

	//glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	//glEnable(GL_FRAMEBUFFER_SRGB);
	//glEnable(GL_CULL_FACE);

	//glDebugMessageCallback(GLDebugMessageCallback, NULL);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
}

void MainRenderer::initDifferredRenderer(){
	mDifferredRenderer = new DifferredRenderer();
	mDifferredRenderer->setMatrices(mProjectionMatrix, mViewMatrix);
	mDifferredRenderer->setViewportSize(viewportWidth, viewportHeight);
	mDifferredRenderer->setLights(lights);
	mDifferredRenderer->setViewPosition(mViewPosition);
	for(auto oi : meshes){
		mDifferredRenderer->addMesh(oi);
	}
	for(auto mi : models){
		mDifferredRenderer->addModel(mi);
	}

	mDifferredRenderer->init();

	mVoxelizator = new Voxelizator();
	mVoxelizator->setMeshes(meshes);
	mVoxelizator->setModels(models);
	mVoxelizator->init();

	mAmbientOcclusionVolumes = new AmbientOcclusionVolumes();
	mAmbientOcclusionVolumes->setMeshes(meshes);
	mAmbientOcclusionVolumes->setModels(models);
	mAmbientOcclusionVolumes->setSize(viewportWidth, viewportHeight);
	mAmbientOcclusionVolumes->init();
}

void MainRenderer::initFrameBuffers(){
	/*glGenFramebuffers(1, &framebuffer);	
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer); 

	glGenTextures(1, &bufferTexture);
	glBindTexture(GL_TEXTURE_2D, bufferTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, viewportWidth, viewportHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, bufferTexture, 0);  

	//glGenRenderbuffers(1, &renderbuffer);
	//glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 512, 512);
	//glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderbuffer);

	GLuint depthTex;
	glGenTextures(1, &depthTex);
	glBindTexture(GL_TEXTURE_2D, depthTex);
	glTexImage2D(
		GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 
		viewportWidth, viewportHeight, 0, 
		GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0
	);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depthTex, 0);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, bufferTexture, 0);
	GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1, DrawBuffers);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
		std::cout << "-------00000------" << std::endl;
		std::cout << "Framebuffer is not complete!" << std::endl;
		std::cout << "-------00000------" << std::endl;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);*/

	/*Texture tex("../models/wall.jpg");

	tex.init(onScreen.mProgram);
	//onScreen.addTexture(tex.mId);*/
	
	/*onScreen.setShaderFile("screen");
	onScreen.addTexture(bufferTexture);
	onScreen.init();*/
}

void MainRenderer::initCamera(){
	float aspect = (float)viewportWidth / (float)viewportHeight;

	mProjectionMatrix = glm::perspective( glm::radians(-30.f * 2), aspect, 0.1f, 100.0f);
	/*mViewMatrix       = glm::lookAt(
			mViewPosition, 
			glm::vec3(0.0, 0.0, 0), 
			glm::vec3(0.0, -1.0, 0.0)
	);*/

	camera = Camera();
	camera.setPosition(mViewPosition);
	camera.setAngle(glm::vec2(0, 0));
	mViewMatrix = camera.getViewMatrix();
	
}

void MainRenderer::initObjects(){
	BoxMesh *bm = new BoxMesh();
	bm->init();
	meshes.push_back(bm);
	bm->setScale(glm::vec3(4.8));
	bm->setPosition(glm::vec3(0));

	/*FloorMesh *fm = new FloorMesh();
	fm->init();
	meshes.push_back(fm);

	WallsMesh *wm = new WallsMesh();
	wm->init();
	meshes.push_back(wm);

	CellingMesh *cm = new CellingMesh();
	cm->init();
	meshes.push_back(cm);*/

	/*Model *testModel = new Model("/viper_SRT/viper.obj");
	testModel->init();
	testModel->setScale(glm::vec3(0.09));
	testModel->setPosition(glm::vec3(3.0, 0.6, 0.0));
	testModel->setRotation(glm::vec3(-M_PI/2.0, 0.0, 0.0));
	testModel->setMaterial(new Material(glm::vec4(0.8), 0.8, 0.6));
	models.push_back(testModel);*/

	/*Model *testModel2 = new Model("/erato/dragon.obj");
	testModel2->init();
	testModel2->setScale(glm::vec3(1.0f));
	testModel2->setPosition(glm::vec3(0.0, 1.1, -0.0));
	//testModel2->setMaterial(new Material("../models/wall"));
	testModel2->setMaterial(new Material(glm::vec4(0.8, 0.3, 0.1, 1.0), 0.3, 0.6));
	models.push_back(testModel2);*/

	/*Model *testModel2 = new Model("/erato/head.OBJ");
	testModel2->init();
	testModel2->setScale(glm::vec3(5.0f));
	testModel2->setPosition(glm::vec3(2.0));
	testModel2->setRotation(glm::vec3(0.0, -M_PI/2.0, 0.0));
	testModel2->setMaterial(new Material(glm::vec4(0.8, 0.3, 0.1, 1.0), 0.3, 0.6));
	models.push_back(testModel2);*/

	/*Model *testModel3 = new Model("/erato/Room.obj");
	testModel3->init();
	testModel3->setScale(glm::vec3(0.013f));
	testModel3->setRotation(glm::vec3(M_PI/2.f*0, 0, 0.f));
	testModel3->setPosition(glm::vec3(0.0, 1.3, -0.0));
	testModel3->setMaterial(new Material(glm::vec4(0.8, 0.8, 0.8, 1.0), 0.9, 0.9));
	models.push_back(testModel3);*/

	RoomModel *roomModel = new RoomModel();
	roomModel->init();
	roomModel->setScale(glm::vec3(0.013f));
	roomModel->setRotation(glm::vec3(M_PI/2.f*0, 0, 0.f));
	roomModel->setPosition(glm::vec3(-5.0, -0.6, 3.0));
	models.push_back(roomModel);
	mRoomModel = roomModel;

	/*ObjMesh *om = new ObjMesh("/Users/ivan/programming/c++/demoScene/models/erato/testObj.obj");
	om->init();
	meshes.push_back(om);*/

	for (auto *m : meshes) {
		m->setLights(lights);
	}

	for (auto *m : models) {
		for (auto *mh : m->mMeshes) {
			mh->setLights(lights);
		}
	}

}

void MainRenderer::initParams(){

	int *size = mWindow.getSize();
	viewportWidth = size[0];
	viewportHeight = size[1];
	
	DirectionalLight *dl = new DirectionalLight();
	dl->direction = glm::vec3(1, -1, -1);
	dl->ambient = glm::vec3(0.03, 0.03, 0.03);
	dl->diffuse = glm::vec3(0.5, 0.5, 0.5);
	dl->specular = glm::vec3(0.5, 0.5, 0.5);

	
	
	//SpotLight *sp1, *sp2, *sp3, *sp4;
	const int spotCount = 6;
	SpotLight* spotLights[spotCount];
	//Initing lamps on celling
	{

		glm::vec3 color = glm::vec3(1, 1, 0.8);
		glm::vec3 bottom = glm::vec3(0, -1, 0);
		const float height = 3.1;
		const float radius = 7.0;
		const float fov = 0.1;
		const float intensity = 14.0;
		
		SpotLight *sp = new SpotLight();
		sp->position = glm::vec3(0, height, -2.0);
		sp->radius = radius;
		sp->direction = bottom;
		sp->fov = fov;
		sp->color = color;
		sp->intensity = intensity;

		spotLights[0] = new SpotLight(*sp);
		spotLights[0]->position = glm::vec3(-0.5, height, -1.2);

		spotLights[1] = new SpotLight(*sp);
		spotLights[1]->position = glm::vec3(-2.5, height, -1.2);

		spotLights[2] = new SpotLight(*sp);
		spotLights[2]->position = glm::vec3(2.0, height, -1.2);

		spotLights[3] = new SpotLight(*sp);
		spotLights[3]->position = glm::vec3(-0.5, height, 1.5);

		spotLights[4] = new SpotLight(*sp);
		spotLights[4]->position = glm::vec3(2.0, height, 1.5);

		spotLights[5] = new SpotLight(*sp);
		spotLights[5]->position = glm::vec3(-2.5, height, 1.5);

	}

	/*PointLight *pl1 = new PointLight();
	pl1->position = glm::vec3(-2, 1, 2);
	pl1->ambient   = glm::vec3(0.1, 0.1, 0.1);
	pl1->diffuse   = glm::vec3(1.0);
	pl1->specular  = glm::vec3(1.0);
	pl1->constant  = 1.0;
	pl1->linear    = 0.1;
	pl1->quadratic = 0.032;

	PointLight *pl2 = new PointLight();
	pl2->position = glm::vec3(-2, 3, -2);
	pl2->ambient   = glm::vec3(0.1, 0.1, 0.1);
	pl2->diffuse   = glm::vec3(1.0);
	pl2->specular  = glm::vec3(1.0, 0.1, 0.1);*/

	/*SphereLight *sl1 = new SphereLight();
	sl1->position = glm::vec3(-4.2, 1.3, 1.4);
	sl1->color = glm::vec3(1);
	sl1->radius = 0.1;
	sl1->intensity = 1;
	
	SphereLight *sl2 = new SphereLight();
	sl2->position = glm::vec3(4.0, 2.0, 1.4);
	sl2->color = glm::vec3(1,1,0.8);
	sl2->radius = 0.1;
	sl2->intensity = 1;

	RectangleLight *rl1 = new RectangleLight();
	rl1->position = glm::vec3(0, 3.0, -1);
	rl1->color = glm::vec3(1.0, 1, 0.8);
	rl1->width = 0.5f;
	rl1->height = 0.5f;
	rl1->matrix = glm::rotate(glm::mat4(1.0), (float)M_PI*1.0f, glm::vec3(1, 0, 0));
	rl1->intensity = 3;

	RectangleLight *rl2 = new RectangleLight();
	rl2->position = glm::vec3(-3, 3.0, -3);
	rl2->color = glm::vec3(1, 1, 0.8);
	rl2->width = 0.5f;
	rl2->height = 0.5f;
	rl2->matrix = glm::rotate(glm::mat4(1.0), (float)M_PI*1.0f, glm::vec3(1, 0, 0));
	rl2->intensity = 3;

	RectangleLight *rl3 = new RectangleLight();
	rl3->position = glm::vec3(3, 3.0, -2);
	rl3->color = glm::vec3(1, 1, 0.8);
	rl3->width = 0.5f;
	rl3->height = 0.5f;
	rl3->matrix = glm::rotate(glm::mat4(1.0), (float)M_PI*1.0f, glm::vec3(1, 0, 0));
	rl3->intensity = 3;

	RectangleLight *rl4 = new RectangleLight();
	rl4->position = glm::vec3(-3, 3.0, 1);
	rl4->color = glm::vec3(1, 1, 0.8);
	rl4->width = 0.5f;
	rl4->height = 0.5f;
	rl4->matrix = glm::rotate(glm::mat4(1.0), (float)M_PI*1.0f, glm::vec3(1, 0, 0));
	rl4->intensity = 3;

	RectangleLight *rl5 = new RectangleLight();
	rl5->position = glm::vec3(3, 3.0, 2);
	rl5->color = glm::vec3(1, 1, 0.8);
	rl5->width = 0.5f;
	rl5->height = 0.2f;
	rl5->matrix = glm::rotate(glm::mat4(1.0), (float)M_PI*1.0f, glm::vec3(1, 0, 0));
	rl5->intensity = 3;*/


	lights = new Lights();
	lights->directionalLight = dl;
	
	//lights->pointLights.push_back(pl1);
	//lights->pointLights.push_back(pl2);
	//lights->sphereLights.push_back(*sl1);
	//lights->sphereLights.push_back(*sl2);
	/*lights->rectangleLights.push_back(*rl1);
	lights->rectangleLights.push_back(*rl2);
	lights->rectangleLights.push_back(*rl3);
	lights->rectangleLights.push_back(*rl4);
	lights->rectangleLights.push_back(*rl5);*/

	for (int i = 0; i < spotCount; ++i) {
		lights->spotLights.push_back(*spotLights[i]);
		/*LightMesh *sLm = new LightMesh(spotLights[i]);
		sLm->init();
		meshes.push_back((RenderMesh*)sLm);*/
	}


	/*LightMesh *lm1 = new LightMesh(sl1);
	lm1->init();
	meshes.push_back((RenderMesh *)lm1);

	LightMesh *lm2 = new LightMesh(sl2);
	lm2->init();
	meshes.push_back((RenderMesh *)lm2);*/

	/*LightMesh *lm3 = new LightMesh(rl1);
	lm3->init();
	meshes.push_back((RenderMesh *)lm3);

	LightMesh *lm4 = new LightMesh(rl2);
	lm4->init();
	meshes.push_back((RenderMesh *)lm4);

	LightMesh *lm5 = new LightMesh(rl3);
	lm5->init();
	meshes.push_back((RenderMesh *)lm5);

	LightMesh *lm6 = new LightMesh(rl4);
	lm6->init();
	meshes.push_back((RenderMesh *)lm6);*/

} 

void MainRenderer::initLocalCubemaps(){
	/*auto lcm1 = new LocalCubemap();
	lcm1->setPosition(glm::vec3( 2.0, 1.0, 0.0 ));
	lcm1->init();*/

	/*auto lcm2 = new LocalCubemap();
	lcm2->setPosition(glm::vec3( -2, 1, -2 ));
	lcm2->init();

	auto lcm3 = new LocalCubemap();
	lcm3->setPosition(glm::vec3( -2, 1, 0 ));
	lcm3->init();*/

	//localCubemaps.push_back(lcm1);
	/*localCubemaps.push_back(lcm2);
	localCubemaps.push_back(lcm3);*/

	//mDifferredRenderer->addLocalCubemap(lcm1);
	/*mDifferredRenderer->addLocalCubemap(lcm2);
	mDifferredRenderer->addLocalCubemap(lcm3);*/

}

void MainRenderer::clear(){
	glClearColor( 0.0, 0.0, 0.0, 1 );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	//glClear( GL_COLOR_BUFFER_BIT );
}

void MainRenderer::renderShadows(){

}
void MainRenderer::renderOclusion(){
	
}
void MainRenderer::renderObjects(){
	for(auto oi : meshes){
		oi->setMode(mMode);
		oi->setProjectionMatrix(mProjectionMatrix);
		oi->setViewMatrix(mViewMatrix);
		oi->setViewPosition(mViewPosition);
		oi->render();
	}
	for(auto mi : models){
		mi->setMatrices(mViewMatrix, mProjectionMatrix);
		mi->setViewPosition(mViewPosition);
		mi->render();
	}
}

void MainRenderer::setWindow(Window &w){
	mWindow = w;
}

void MainRenderer::setViewportSize(int w, int h){
	viewportWidth = w;
	viewportHeight = h;
}

void MainRenderer::setViewPosition(glm::vec3 vp){
	mViewPosition = vp;
}

void MainRenderer::init(){
	initParams();
	DebugHelper::getInstance()->printError("par");
	initGL();
	DebugHelper::getInstance()->printError("gl");
	initFrameBuffers();
	DebugHelper::getInstance()->printError("fbos");
	initCamera();
	DebugHelper::getInstance()->printError("cam");
	initObjects();
	DebugHelper::getInstance()->printError("obj 1");
	DebugHelper::getInstance()->printError("obj 2");
	initDifferredRenderer();
	DebugHelper::getInstance()->printError("diff rend 1");
	initLocalCubemaps();

	DebugHelper::getInstance()->printError("cbm 1");
	DebugHelper::getInstance()->printError("cbm 2");
	//glCheckError();
	

	

	glEnable(GL_MULTISAMPLE);
	//glEnable(GL_DEBUG_OUTPUT);
	//glEnable(GL_LESS);
	

}


void MainRenderer::renderToCubemap(glm::mat4 matr, float dir, Texture *t){
	/*mMode = 1;
	mViewMatrix = matr;
	clear();
	//renderObjects();
	mDifferredRenderer->setViewPosition(mViewPosition);
	mDifferredRenderer->renderFirstLight(glm::mat4(0));
	mViewMatrix = camera.getViewMatrix();*/

	mDifferredRenderer->setState(1);
	mDifferredRenderer->setTargetTexture(t);
	mDifferredRenderer->setViewPosition(mViewPosition);
	for(auto oi : meshes){
		//glm::mat4 pm = glm::perspective(45.f, -1.f, 0.1f, 30.0f);
		oi->setProjectionMatrix(mProjectionMatrix);
		//oi->setProjectionMatrix(pm);
		oi->setViewMatrix(matr);
		oi->setViewPosition(mViewPosition);
		oi->setDirection(dir);
	}
	mDifferredRenderer->renderFirstLight(glm::mat4(0));
}

void MainRenderer::renderLocalCubemaps(){
	/*for(int i=0; i<localCubemaps.size(); ++i){
		localCubemaps[i]->capture( std::bind( &MainRenderer::renderToCubemap, this, 
			  std::placeholders::_1, std::placeholders::_2, std::placeholders::_3 ) );
	}*/

	mDifferredRenderer->setState(1);
	mDifferredRenderer->captureEnvMaps();
}

void MainRenderer::renderLocalCubemapsMeshes(){
	for(int i=0; i<localCubemaps.size(); ++i){
		localCubemaps[i]->vm = mViewMatrix;	
		localCubemaps[i]->pm = mProjectionMatrix;
		localCubemaps[i]->renderMesh();
	}
}

void MainRenderer::render(){

	clear();

	/*Rid of old monster bug*/

	//renderObjects();
	
	/*Voxelization test*/

	for(auto oi : meshes){
		oi->setProjectionMatrix(mProjectionMatrix);
		oi->setViewMatrix(mViewMatrix);
		oi->setViewPosition(mViewPosition);
	}
	for(auto *mi : models){
		mi->setMatrices(mViewMatrix, mProjectionMatrix);
	}

	mVoxelizator->render();

	glViewport(0, 0, viewportWidth, viewportHeight);

	/*End of voxelization test*/

	/*clear();
	mVoxelizator->show(mViewMatrix, mProjectionMatrix, mViewPosition);*/

	for(auto oi : meshes){
		oi->setGridSize(0);
	}
	for(auto mi : models){
		for (auto mo : mi->mMeshes) {
				mo->setGridSize(0);
		}
	}

	mAmbientOcclusionVolumes->render();
	Texture *aoTexture = mAmbientOcclusionVolumes->getOutTexture();

	clear();

	GLuint voxTexId = mVoxelizator->getVoxelTextureId();
	GLuint SVOChildTex = mVoxelizator->getSVOChildId();
	GLuint SVODiffuseTex = mVoxelizator->getSVODiffuseId();
	float voxGridSize = mVoxelizator->getVoxelGridSize();
	mDifferredRenderer->setVoxelTextureId(voxTexId);
	mDifferredRenderer->mVoxelConeTracingComponent->setSVOChildId(SVOChildTex);
	mDifferredRenderer->mVoxelConeTracingComponent->setSVODiffuseId(SVODiffuseTex);
	mDifferredRenderer->mVoxelConeTracingComponent->setVoxelGridSize(voxGridSize);
	mDifferredRenderer->mVoxelConeTracingComponent->setViewPosition(mViewPosition);
	mDifferredRenderer->mVoxelConeTracingComponent->setAOTexture(aoTexture);
	mDifferredRenderer->setViewPosition(mViewPosition);
	mDifferredRenderer->setVoxelWorldSize(voxGridSize);
	mDifferredRenderer->renderVoxelGI();
	mDifferredRenderer->renderAA();

	/*New differred*/

	/*float s = 256;
	float fov = 2.0 * atan(s / (s - 0.5));
	glm::mat4 pm = glm::perspective(fov, 1.f, 0.1f, 10.f);
	for(auto oi : meshes){
		oi->setProjectionMatrix(pm);
	}
	renderLocalCubemaps();

	mDifferredRenderer->setState(2);
	mDifferredRenderer->setViewPosition(mViewPosition);
	for(auto oi : meshes){
		oi->setProjectionMatrix(mProjectionMatrix);
		oi->setViewMatrix(mViewMatrix);
		oi->setViewPosition(mViewPosition);
	}
	for(auto mi : models){
		mi->setMatrices(mViewMatrix, mProjectionMatrix);
		mi->setViewPosition(mViewPosition);
	}
	for(int i=0; i<localCubemaps.size(); ++i){
		localCubemaps[i]->vm = mViewMatrix;	
		localCubemaps[i]->pm = mProjectionMatrix;
	}
	mDifferredRenderer->renderFinal();*/

	/* Old farward
	 * renderLocalCubemaps();
	
	mMode = 2;
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	glViewport(0, 0, viewportWidth, viewportHeight);
	clear();
	renderObjects();

	for(auto *mi : meshes){
		mi->addLCTextureId( localCubemaps[0]->getTextureId() );
	}
	
	renderLocalCubemapsMeshes();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	glViewport(0, 0, viewportWidth, viewportHeight);
	clear();
	onScreen.render();*/

	/*mMode = 2;
	clear();
	glViewport(viewportWidth * 0, viewportHeight * 0, viewportWidth/1, viewportHeight/1);
	renderObjects();*/

}

void MainRenderer::event(int key, int act){

	const float moveDist = 0.2;
	const float rotDist = M_PI / 20.f;
	
	if(key == GLFW_KEY_W){
		camera.move(glm::vec3(moveDist, 0, 0));
	}
	if(key == GLFW_KEY_S){
		camera.move(glm::vec3(-moveDist, 0, 0));
	}
	if(key == GLFW_KEY_A){
		camera.move(glm::vec3(0, 0, -moveDist));
	}
	if(key == GLFW_KEY_D){
		camera.move(glm::vec3(0, 0, moveDist));
	}
	if(key == GLFW_KEY_R){
		camera.move(glm::vec3(0, moveDist, 0));
	}
	if(key == GLFW_KEY_F){
		camera.move(glm::vec3(0, -moveDist, 0));
	}
	if(key == GLFW_KEY_UP){
		camera.rotate(glm::vec2(0, rotDist));
	}
	if(key == GLFW_KEY_DOWN){
		camera.rotate(glm::vec2(0, -rotDist));
	}
	if(key == GLFW_KEY_LEFT){
		camera.rotate(glm::vec2(-rotDist, 0));
	}
	if(key == GLFW_KEY_RIGHT){
		camera.rotate(glm::vec2(rotDist, 0));
	}
	if (key == GLFW_KEY_Y) {
		mRoomModel->hightlight();
	}

	mViewMatrix = camera.getViewMatrix();
	setViewPosition(camera.getPosition());
}

MainRenderer::MainRenderer(){
	
}

MainRenderer::~MainRenderer(){

}
