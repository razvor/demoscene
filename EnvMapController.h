#ifndef ENV_MAP_CONTROLLER
#define ENV_MAP_CONTROLLER

#include "LocalCubemap.h"
#include "RenderMehs.h"

#define BINDING_COUNT 3

struct CubemapBinding{
	LocalCubemap *bined[3];
}

class EnvMapController{
private:

	std::vector<LocalCubemap *> mEnvMaps;
	
public:

	void add(glm::vec3 pos);

	void init();

	int getCount();
	LocalCubemap *get(int ind);

	void bindToMesh(RenderMesh *mesh);

	void pushToShader(RenderMesh *mesh, int margin);

	EnvMapController();
	~EnvMapController();

};

#endif
