#ifndef BOX_MESH
#define BOX_MESH

#include "RenderMesh.h"

class BoxMesh : public RenderMesh{
private:

	struct UniformLocations{
	  int viewMatrix,
		  projectionMatrix,
		  modelMatrix,
		  viewPosition;
	} unifLocations;

	void initGeometry();
	void initLights();

	float r = 0.0;


public:
	
	void initLocations();
	void pushUniforms();
	void initProgram();

	void init();
	void render();

	void setTTT(GLuint a);

	//void setLightPos(glm::vec3 lp);

	BoxMesh();
	~BoxMesh();
};

#endif
