#pragma once

#include "DifferredComponent.h"
#include "Light.h"

class VoxelConeTracingComponent : public DifferredComponent{
private:
	
	GLuint mVoxelTextureId, mSVOChildId, mSVODiffuseId;
	float mVoxelWorldSize;
	int mVoxelGridSize;
	glm::vec3 mViewPosition;

	Texture *mAOTexture;

	Lights *mLights;

	int mViewPositionLocation,
		mVoxelTextureLocation,
		mVoxelWorldSizeLocation,
		mVoxelGridSizeLocation;

public:

	void setVoxelTextureId(int id);
	void setVoxelWorldSize(float size);
	void setVoxelGridSize(int size);

	void setSVOChildId(GLuint id);
	void setSVODiffuseId(GLuint id);

	void setViewPosition(glm::vec3 vp);

	void init();
	void initProgram();
	void initLocations();
	void pushAdditional();

	void setLights(Lights *l);
	void setAOTexture(Texture *t);
	
	VoxelConeTracingComponent();
	~VoxelConeTracingComponent();
	
};