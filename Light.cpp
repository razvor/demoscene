#include "Light.h"
//float *_ltc_mag = 
#include "ltc_mag.h"

std::string Light::makePath(std::string n){

	std::string indc = "[" + std::to_string(mIndex)+ "]"; 
	std::string r = mUniformName + indc + std::string(".") + n;

	//const char *ret = r.c_str();

	return r;
}

void Light::init(Program program, int ind){
	mIndex = ind;
	this->program = program;
}

void Light::pushToShader(){
}

Light::Light(){}
Light::~Light(){}

void DirectionalLight::init(Program program, int ind){
	Light::init(program, ind);
	mUniformName = "directionalLights";

	mDirLoc  = program.uniform(makePath(mDirectionName));	
	mAmbLoc  = program.uniform(makePath(mAmbientName));	
	mDiffLoc = program.uniform(makePath(mDiffuseName));	
	mSpecLoc = program.uniform(makePath(mSpecularName));

	/*GLuint inds[1];;
	const char* unifName = "aa";
	glGetUniformIndices(program.getInstance(), 1, &unifName, inds);
	mDirLoc = inds[0];
	std::cout << "jjjj " << mDirLoc << std::endl;*/
	
}

void DirectionalLight::pushToShader(){
	glUniform3f(mDirLoc, direction.x, direction.y, direction.z);
	glUniform3f(mAmbLoc, ambient.x, ambient.y, ambient.z);
	glUniform3f(mDiffLoc, diffuse.x, diffuse.y, diffuse.z);
	glUniform3f(mSpecLoc, specular.x, specular.y, specular.z);
}

DirectionalLight::DirectionalLight(){}
DirectionalLight::~DirectionalLight(){}

void PointLight::init(Program program, int ind){
	Light::init(program, ind);
	mUniformName = "pointLights";

	mPosLoc = program.uniform(makePath(mPositionName));
	mAmbLoc  = program.uniform(makePath(mAmbientName));	
	mDiffLoc = program.uniform(makePath(mDiffuseName));	
	mSpecLoc = program.uniform(makePath(mSpecularName));	
	mConsLoc = program.uniform(makePath(mConstantName));	
	mLinLoc = program.uniform(makePath(mLinearName));	
	mQuadLoc = program.uniform(makePath(mQuadraticName));	
}

void PointLight::pushToShader(){
	glUniform3f(mPosLoc, position.x, position.y, position.z);
	glUniform3f(mAmbLoc, ambient.x, ambient.y, ambient.z);
	glUniform3f(mDiffLoc, diffuse.x, diffuse.y, diffuse.z);
	glUniform3f(mSpecLoc, specular.x, specular.y, specular.z);
	glUniform1f(mConsLoc, constant);
	glUniform1f(mLinLoc, linear);
	glUniform1f(mQuadLoc, quadratic);
}

PointLight::PointLight(){}
PointLight::~PointLight(){}

void SpotLight::init(Program program, int ind) {
	Light::init(program, ind);
	mUniformName = "spotLights";

	mPosLoc = program.uniform(makePath(mPositionName));
	mIntLoc = program.uniform(makePath(mIntensityName));
	mRadLoc = program.uniform(makePath(mRadiusName));
	mColLoc = program.uniform(makePath(mColorName));
	mDirLoc = program.uniform(makePath(mDirectionName));
	mFovLoc = program.uniform(makePath(mFovName));
}

void SpotLight::pushToShader() {
	glUniform3f(mPosLoc, position.x, position.y, position.z);
	glUniform3f(mColLoc, color.x, color.y, color.z);
	glUniform1f(mIntLoc, intensity);
	glUniform1f(mRadLoc, radius);
	glUniform3f(mDirLoc, direction.x, direction.y, direction.z);
	glUniform1f(mFovLoc, fov);
}

SpotLight::SpotLight(){}
SpotLight::~SpotLight(){}

void SphereLight::init(Program program, int ind){
	Light::init(program, ind);
	mUniformName = "sphereLights";

	mPosLoc = program.uniform(makePath(mPositionName));
	mIntLoc = program.uniform(makePath(mIntensityName));
	mRadLoc = program.uniform(makePath(mRadiusName));
	mColLoc = program.uniform(makePath(mColorName));
}

void SphereLight::pushToShader(){
	//std::cout << "ii " << mPosLoc << std::endl;
	glUniform3f(mPosLoc, position.x, position.y, position.z);
	glUniform3f(mColLoc, color.x, color.y, color.z);
	glUniform1f(mIntLoc, intensity);
	glUniform1f(mRadLoc, radius);
}

SphereLight::SphereLight(){}
SphereLight::~SphereLight(){}

void RectangleLight::init(Program program, int ind){
	Light::init(program, ind);
	this->mUniformName = "rectangleLights"; 
	

	mPosLoc = program.uniform(makePath(mPositionName));
	mIntLoc = program.uniform(makePath(mIntensityName));
	mColLoc = program.uniform(makePath(mColorName));
	mWidLoc = program.uniform(makePath(mHeightName));
	mHeiLoc = program.uniform(makePath(mWidthName));
	mMatLoc = program.uniform(makePath(mMatrixName));

	/*std::cout << std::string(makePath(mMatrixName)) << std::endl;
	std::cout << "a " << mMatrixName << " " << mUniformName << std::endl;*/

	//std::cout << "init rect light" << std::endl;
}

void RectangleLight::pushToShader(){
	glUniform3f(mPosLoc, position.x, position.y, position.z);
	//mColLoc = 73;
	//std::cout << "R lig " << mColLoc << std::endl;

	//std::cout << "push rect light " << mColLoc << " " << color.x << " " << color.y << " " << color.z << std::endl;

	glUniform3f(mColLoc, color.x, color.y, color.z);
	glUniform1f(mIntLoc, intensity);
	glUniform1f(mWidLoc, width);
	glUniform1f(mHeiLoc, height);
	glUniformMatrix4fv(mMatLoc, 1, false, &matrix[0][0]);
}

RectangleLight::RectangleLight(){}
RectangleLight::~RectangleLight(){}

void Lights::init(Program program){

	//std::cout << "Light init begin" << std::endl;

	program.use();

	int i;
	this->program = program;

	/*if(directionalLight)
		directionalLight->init(program, 0);*/

	for(i=0; i<pointLights.size(); ++i)
	  pointLights[i].init(program, i);

	for(i=0; i<sphereLights.size(); ++i)
	  sphereLights[i].init(program, i);

	for(i=0; i<rectangleLights.size(); ++i)
	  rectangleLights[i].init(program, i);

	for (i = 0; i<spotLights.size(); ++i)
		spotLights[i].init(program, i);

	if(pointLights.size() > 0)
		pointCountLoc = program.uniform("pointLightCount");

	sphereCountLoc = program.uniform("sphereLightCount");
	rectangleCountLoc = program.uniform("rectangleLightCount");
	spotCountLoc = program.uniform("spotLightCount");

	//Init rect brdf textures
	
	/*ltc1 = new Texture(64, 64);
	ltc1->setUniformName("ltcMat");
	ltc1->init(program);*/

	/*ltc2 = new Texture("../models/amp.png");
	ltc2->setUniformName("ltcAmp");
	ltc2->init(program);*/
	
	/*GLuint aa;
	glGenTextures(1, &aa);
	glBindTexture(GL_TEXTURE_2D, aa);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, viewportWidth, viewportHeight, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//glTexData()
	//extern float __ltc_mag[4096];
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 64, 64, 0, GL_RGBA, GL_FLOAT, &__ltc_mag[0]);
	glBindTexture(GL_TEXTURE_2D, 0);

	ltc2->mId = aa;*/


	/*GLuint ab;
	glGenTextures(1, &ab);
	glBindTexture(GL_TEXTURE_2D, ab);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, viewportWidth, viewportHeight, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//glTexData()
	//extern float __ltc_mag[4096];
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 64, 64, 0, GL_RGBA, GL_FLOAT, &__ltc_mat[0]);
	glBindTexture(GL_TEXTURE_2D, 0);

	ltc1->mId = ab;*/

	//std::cout << "Lights init end" << std::endl;

}

void Lights::pushToShader(){
	int i;
	//directionalLight->pushToShader();

	for(i=0; i<pointLights.size(); ++i)
	  pointLights[i].pushToShader();

	for(i=0; i<sphereLights.size(); ++i)
	  sphereLights[i].pushToShader();

	for(i=0; i<rectangleLights.size(); ++i)
	  rectangleLights[i].pushToShader();

	for (i = 0; i < spotLights.size(); ++i)
		spotLights[i].pushToShader();

	//countLoc = program.uniform("pointLightCount");

	if(pointLights.size() > 0)
		glUniform1i(pointCountLoc, pointLights.size());

	//std::cout << sphereLights.size() << endl;
	
	//countLoc = program.uniform("sphereLightCount");
	glUniform1i(sphereCountLoc, sphereLights.size());

	//countLoc = program.uniform("rectangleLightCount");
	glUniform1i(rectangleCountLoc, rectangleLights.size());

	glUniform1i(spotCountLoc, spotLights.size());

	//ltc1->pushToShader(0);

	//ltc2->pushToShader(13);
}

Lights::Lights(){}
Lights::~Lights(){
	if(directionalLight) delete directionalLight;
	
	/*for(int i=0; i<pointLights.size(); ++i)
	  if(pointLights[i]) delete pointLights[i];

	for(int i=0; i<sphereLights.size(); ++i)
	  if(sphereLights[i]) delete sphereLights[i];

	for(int i=0; i<rectangleLights.size(); ++i)
	  if(rectangleLights[i]) delete rectangleLights[i];*/
}
