#ifndef WINDOW
#define WINDOW

#include "includer.h"


class WindowStatic{
private:
	WindowStatic(){}
	~WindowStatic(){}
	WindowStatic(WindowStatic const&);
	WindowStatic& operator= (WindowStatic const&);

public:
	std::function<void(int, int)> mEventFunction;
	static WindowStatic& getInstance(){
		static WindowStatic inst;
		return inst;
	}
};

class Window{
private:

	std::function<void()> mDrawFunction;

	int mViewportWidth, mViewportHeight;
	GLFWwindow* mWindow;

	void closeWindow();
	void terminate();

	static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

  
public:
	Window();
	~Window();

	void setViewportSize(int w, int h);
	void setDrawFunction(std::function<void()>);
	int *getSize();
	//void setEndFunction(std::function<void()>);
	void setEventFunction(std::function<void(int, int)>);
	GLFWwindow *getWindow();

	bool init();
	void start();
	void end();

};

#endif
