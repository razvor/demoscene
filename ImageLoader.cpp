#include "ImageLoader.h"

ImageLoader::ImageLoader(std::string fl,callback fn){
	mFileName = fl;
	mCallback = fn;
	mThread = std::thread(std::bind(&ImageLoader::loadAsync, this));
	mThread.detach();
}

ImageLoader::~ImageLoader(){
	
}

void ImageLoader::loadAsync(){
	int comp;
	int width, height;
	unsigned char* mImage = stbi_load(mFileName.c_str(), &width, &height, &comp, STBI_rgb);

	mCallback(mImage, width, height);
}
