#ifndef MODEL
#define MODEL

#include "includer.h"
#include "RenderMesh.h"
#include "ObjMesh.h"

struct ModelNode{
	std::vector<ObjMesh*> meshes;
	std::vector<ModelNode*> children;
};

class Model{
private:

	std::string mPath;

	aiScene *mScene;

	glm::mat4 mModelMatr, mViewMatr, mProjMatr;
	glm::vec3 mViewPosition;


	void processNode(aiNode *node, ModelNode *modelNode);
	void processMesh(aiMesh *mesh, ModelNode *modelNode);

public:

	virtual std::vector<int> getFaceNormalMeshes();

	ModelNode *root;

	std::vector<ObjMesh*> mMeshes;

	Model(std::string path);
	~Model();

	void init();
	virtual void postProcess();
	void setMatrices(glm::mat4 view, glm::mat4 proj);
	void setViewPosition(glm::vec3 viewPos);

	void setPosition(glm::vec3 p);
	void setScale(glm::vec3 s);
	void setRotation(glm::vec3 r);

	int getMeshesCount();
	ObjMesh* getMesh(int ind);

	void setMaterial(Material *m);

	void render();
	void renderGeometry(Program &currentProgram);
};

#endif
