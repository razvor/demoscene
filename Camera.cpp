#include "Camera.h"

Camera::Camera(){}
Camera::~Camera(){}

glm::vec3 Camera::getPosition(){
	return mPosition;
}

glm::vec3 Camera::getViewPoint(){
	return mViewPoint;
}

glm::mat4 Camera::getViewMatrix(){
	return mViewMatrix;
}

void Camera::setPosition(glm::vec3 p){
	mPosition = p;
	compute();
}

void Camera::setAngle(glm::vec2 a){
	mAngles = a;
	compute();
}

void Camera::move(glm::vec3 dp){
	const glm::mat4 rx = glm::rotate(glm::mat4(1), -mAngles.x, glm::vec3(0, 1, 0));
	const glm::mat4 ry = glm::rotate(glm::mat4(1), -mAngles.y, glm::vec3(1, 0, 0));
	const glm::mat4 rf = rx;
	const glm::vec3 mt = glm::mat3(rf) * dp;
	setPosition(mPosition + mt);
}

void Camera::rotate(glm::vec2 dr){
	setAngle(mAngles + dr);
}

void Camera::computeViewPoint(){
	glm::vec3 viewPoint = mPosition;
	float x,y,z,r;

	y = viewDistance * sinf(mAngles.y);
	r = viewDistance * cosf(mAngles.y);
	x = r * cosf(mAngles.x);
	z = r * sinf(mAngles.x);

	viewPoint += glm::vec3(x,y,z);

	mViewPoint = viewPoint;
}

void Camera::computeViewMatrix(){
	mViewMatrix = glm::lookAt(
			mPosition, 
			mViewPoint,
			glm::vec3(0.0, -1.0, 0.0)
	);
}
void Camera::compute(){
	computeViewPoint();
	computeViewMatrix();
}
