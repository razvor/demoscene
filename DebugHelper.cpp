#include "DebugHelper.h"

DebugHelper::DebugHelper(){

}

DebugHelper::~DebugHelper(){

}

DebugHelper* DebugHelper::getInstance(){
	static DebugHelper *inst = new DebugHelper();
	return inst;
}

void DebugHelper::printError(std::string p){
	GLenum e = glGetError();
	if(e != GL_NO_ERROR)
		std::cout << "GL ERROR: " << p << "||  ";
	switch(e){
		case GL_NO_ERROR:
			//std::cout << "no error " << std::endl;
			break;
		case GL_INVALID_OPERATION:
			std::cout << "invalid operation " << std::endl;
			break;
		case GL_INVALID_ENUM:
			std::cout << "invalid enum" << std::endl;
			break;
		case GL_INVALID_VALUE:
			std::cout << "invalid value" << std::endl;
			break;
		case GL_OUT_OF_MEMORY:
			std::cout << "out of memory" << std::endl;
			break;

		default:
			std::cout << "another error" << e << std::endl;
			break;
	}
}
