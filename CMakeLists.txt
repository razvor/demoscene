cmake_minimum_required(VERSION 2.6)

project(demoSceneNew)

find_package(OpenGL REQUIRED)

option(GLFW_BUILD_DOCS OFF)
option(GLFW_BUILD_EXAMPLES OFF)
option(GLFW_BUILD_TESTS OFF)
add_subdirectory(glfw)

add_subdirectory(glew)

option(ASSIMP_BUILD_ASSIMP_TOOLS OFF)
option(ASSIMP_BUILD_SAMPLES OFF)
option(ASSIMP_BUILD_TESTS OFF)
add_subdirectory(assimp)

include_directories(glm)
include_directories(gli)
include_directories(glfw/include)
include_directories(glew/include)
include_directories(glew/src)
include_directories(assimp/include)
include_directories(stb/)

INCLUDE_DIRECTORIES(
  #assimp/code
  assimp
)

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -w -lSOIL -std=c++11 -Wall -g -lglfw3 -lglew -gdwarf-3 -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}" /W4)

set(SOURCE_FILES 
	main.cpp
	glad/glad.c
	Window.cpp
	MainRenderer.cpp
	RenderMesh.cpp
	BoxMesh.cpp
	Light.cpp
	FloorMesh.cpp
	LightMesh.cpp
	WallsMesh.cpp
	CellingMesh.cpp
	Camera.cpp
	Texture.cpp
	Material.cpp
	OnScreen.cpp
	LocalCubemap.cpp
	Framebuffer.cpp
	GBuffer.cpp
	DifferredRenderer.cpp
	DifferredComponent.cpp
	FirstLightComponent.cpp
	ReflectLightComponent.cpp
	ImageLoader.cpp
	DebugHelper.cpp
	TextureCube.cpp
	FramebufferCube.cpp
	ObjMesh.cpp
	Model.cpp
	Voxelizator.cpp
	VoxelConeTracingComponent.cpp
	RoomModel.cpp
	AmbientOcclusionVolumes.cpp
	AntiAliasingComponent.cpp
)

#add_library(GLAD "glad/glad.c")
set(LIBS 
	#glfw3
	#glew
	#SOIL
	#assimp
	#GLAD
)

#LINK_DIRECTORIES(
#	assimp
#	assimp/lib
#)

file(GLOB PROJECT_FILES
	"shaders/*.vsh"
	"shaders/*.fsh"
	"shaders/*.geom"
	"shaders/*/*.vsh"
	"shaders/*/*.fsh"
)

set(CMAKE_SUPPRESS_REGENERATION true)

add_definitions(-DGLEW_STATIC)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/shaders DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
#file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/models DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
#file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/models DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/models)

add_executable(demoScene ${SOURCE_FILES} ${PROJECT_FILES})

add_custom_command(TARGET demoScene PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                       ${CMAKE_SOURCE_DIR}/shaders ${CMAKE_CURRENT_BINARY_DIR})

target_link_libraries( demoScene ${LIBS} assimp glfw ${GLFW_LIBRARIES} libglew_static )
#set_target_properties(${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${PROJECT_NAME})

