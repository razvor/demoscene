#ifndef RENDER_MESH
#define RENDER_MESH

#include "includer.h"
#include "map"
#include "Light.h"
#include "Texture.h"
#include "Material.h"
//#include "LocalCubemap.h"

class RenderMesh{
private:

	int mMode;
	int mType; //1 - emissive(light), 2 - not transpresent object
	float mDirection; //for lcm

	glm::vec3 mRotation;
	glm::vec3 mPosition;
	glm::vec3 mScale;

	glm::vec3 mViewPosition;

	glm::mat4 mModelMatrix;
	glm::mat4 mViewMatrix;
	glm::mat4 mProjectionMatrix;

	std::vector<glm::vec3> mVertices;
	std::vector<glm::vec3> mNormals;
	std::vector<glm::vec2> mTexCords;
	std::vector<glm::vec3> mTangents;
	std::vector<glm::vec3> mBitangents;
	std::vector<unsigned int> mIndices;
	glm::vec3 mBoundingBox; //max size of object

	Lights *lights = NULL;
	Material *mMaterial = NULL;
	GLuint mLCTextureId = 0;
	GLuint mVoxelTextureId = 0;
	GLuint mVoxelKdTexId = 0;
	GLuint mVoxelPositionTexId = 0;
	int mGridSize = 0;
 	float mZoneSize;
	GLuint mVoxelAtomicBuffer;
	int mVoxelState;
	//std::vector<LocalCubemap*> mLocalCubemaps; 

	VertexBuffer *mVerticesBuffer;
	VertexBuffer *mNormalsBuffer;
	VertexBuffer *mTexCordsBuffer;
	VertexBuffer *mTangentsBuffer;
	VertexBuffer *mBitangentsBuffer;
	VertexBuffer *mIndicesBuffer;
	GLuint vertArr;

	struct AttributeLocations{
		int vertices,
			normals,
			texCords,
			tangents,
			bitangents,
			indices;
	} attrLocations;

	struct UniformLocations{
	  int viewMatrix,
		  projectionMatrix,
		  modelMatrix,
		  viewPosition,
			viewDirection,
		  state,
		  type,
		  direction,
			LCM,
		  color,
		  boundingBox;
	} unifLocations;

	std::map<string, int> mAttributesLocations;
	std::map<string, int> mUniformsLocations;

	void computeModelMatrix();
	void computeTangents();
	virtual void computeBoundingBox();
	void initBuffers();

	void pushMatrix(int location, glm::mat4 &matr);
	void pushBuffer(int location, VertexBuffer *buffer);

public:
	std::vector<Texture*> mTextures;

	Program mProgram;
	Program& getProgram();

	virtual void prepare();
	virtual void draw();
	virtual void initProgram();
	virtual void initLocations();
	void initMinLocations();
	virtual void initTextures();
	virtual void pushUniforms();
	void pushMinUniforms();
	virtual void pushAttributes();
	//virtual void pushLights();
	//virtual void pushTextures();

	size_t getIndicesCount();
	size_t getVerticesCount();
	unsigned int getIndex(int ind);
	glm::vec3 getVertex(int ind);
	glm::vec3 getNormal(int ind);
	glm::vec2 getTexCoords(int ind);

	void setVertices(std::vector<glm::vec3> &vertices);
	void setNormals(std::vector<glm::vec3> &normals);
	void setTexCords(std::vector<glm::vec2> &texCords);
	void setTangents(std::vector<glm::vec3> &tn);
	void setBiTangents(std::vector<glm::vec3> &tn);
	void setIndices(std::vector<unsigned int> &indices);
	void setIndices(std::vector<int> &indices);
	void setPosition(glm::vec3 pos);
	void setRotation(glm::vec3 rot);
	void setScale(glm::vec3 scale);

	void setMode(int mode);
	void setType(int type);
	void setDirection(float dir);
	void setViewMatrix(glm::mat4 vm);
	void setProjectionMatrix(glm::mat4 pm);
	void setViewPosition(glm::vec3 vp);
	void setLights(Lights *l);
	void addTexture(Texture *texture);
	void setMaterial(Material *material);
	void addLCTextureId(GLuint id);
	void setZoneSize(float size);
	void setVoxelAtomicBuffer(GLuint buf);
	void setVoxelState(int s);
	void setVoxelPosTexId(GLuint id);
	void setVoxelKdTexId(GLuint id);
	void setGridSize(int size);
	void setVoxelTextureId(GLuint id);

	void setProgram(Program &p);

	virtual void init();
	virtual void render();
	virtual void renderGeometry(Program &currentProgram);

	RenderMesh();
	~RenderMesh();
};

#endif
