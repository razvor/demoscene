#ifndef MATERIAL
#define MATERIAL

#include "includer.h"
#include "Texture.h"
//#include "Program.cpp"

#define DEFAULT_IOR 1.001

class Material{
	private:

	std::string mPath;

	Texture *mAlbedo;
	Texture *mNormals;
	Texture *mRoughness;
	Texture *mMetallness;
	Texture *mHeight;
	Texture *mAO;
	Texture *mOcclusion;
	//Texture *mIOR;
	float mIOR;

	void load();
	
public:

	void init(Program program);
	void pushToShader(int i);

	Material(std::string p, float IOR = DEFAULT_IOR);
	Material(glm::vec4 color, float rough, float metal, float IOR = DEFAULT_IOR);
	~Material();


};

#endif
