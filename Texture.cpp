#include "Texture.h"
//#include "soil/SOIL.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

void Texture::load(std::string file){
	mWidth = 0;
	mHeight = 0;
	this->file = file;
	//std::cout << "Image loading " << file << endl;
	//mImage = SOIL_load_image(file.c_str(), &mWidth, &mHeight, 0, SOIL_LOAD_RGB);
	///std::cout << "Image loaded " << file << " w: " << mWidth << endl;
	//assert(mWidth != 0);
	
	int comp;
	mImage = stbi_load(file.c_str(), &mWidth, &mHeight, &comp, STBI_rgb);
	//mImage = NULL;

	if(mImage == NULL){
		std::cout << "field load image " << file << std::endl;
	}

	std::cout << "Loaded image " << file << " " << mWidth << " " << mHeight << std::endl;

	
}

void Texture::onLoad(unsigned char* img, int w, int h){

	mImage = img;
	if(mImage == NULL){
		std::cout << "field load image " << file << std::endl;
	}
	mWidth = w;
	mHeight = h;

	//create();

}

void Texture::initLocations(Program &program){
	mUnifLocation = program.uniform(mUnifName.c_str());
}

void Texture::create(){
	//std::cout << "Creating texture " << file << std::endl;
	glGenTextures(1, &mId);
	glBindTexture(GL_TEXTURE_2D, mId);
	
	if(isGeneric){
		//glTexImage2D(GL_TEXTURE_2D, 0, mInternalFormat, 1, 1, 0, GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE);
		std::vector<GLfloat> d(1 * 4, 0);
		//d[0] = 0xFF000000 * ((GLubyte)mGen.r * 255 << 16) + 0x00FF0000 * ((GLubyte)mGen.g * 255 << 8) + 0x0000FF00 * ((GLuint)mGen.b * 255 << 4) + 0x000000FF;
		d[0] = mGen.r;
		d[1] = mGen.g;
		d[2] = mGen.b;
		d[3] = mGen.a;
		//d[1] = 0xFFFFFFFF * mGen.g; //(GLubyte)((pixel >> 16) & 0xFF);
		//d[2] = 0xFFFFFFFF * mGen.b; //(GLubyte)((pixel >> 16) & 0xFF);
		//d[3] = 0xFFFFFFFF * mGen.a; //(GLubyte)((pixel >> 16) & 0xFF);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 1, 1, 0, GL_RGBA, GL_FLOAT, &d[0]);
	}else {
		if (file.size() > 0)
			glTexImage2D(GL_TEXTURE_2D, 0, mInternalFormat, mWidth, mHeight, 0, mFormat, mDataType, mImage);
		else
			glTexImage2D(GL_TEXTURE_2D, 0, mInternalFormat, mWidth, mHeight, 0, mFormat, mDataType, NULL);
	}

	glGenerateMipmap(GL_TEXTURE_2D);
	if(mSizeMode == CLAMP_EDGE){
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}else{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	}

	/*glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );*/

	///glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	float aniso = 0.f;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &aniso);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, aniso);

	//SOIL_free_image_data(mImage);
	glBindTexture(GL_TEXTURE_2D, 0); 

}

void Texture::init(){
	if(!isInited){
		isInited = true;

		/*ImageLoader il(file, std::bind(&Texture::onLoad, this, 
			  std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));*/

		create();
	}
}

void Texture::init(Program &program){
	init();
	initLocations(program); 
}

void Texture::setProgram(Program &program){
	initLocations(program);
}
	
void Texture::pushToShader(int ind){
	glActiveTexture(GL_TEXTURE0 + ind);
	glBindTexture(GL_TEXTURE_2D, mId);
	glUniform1i(mUnifLocation, ind);
}

void Texture::setUniformName(std::string un){
	mUnifName = un;
}

GLuint Texture::getId(){
	return mId;
}

glm::vec2 Texture::getSize(){
	return glm::vec2(mWidth, mHeight);
}

void Texture::setSizeMode(SizeMode sm){
	mSizeMode = sm;
}

void Texture::setInternalFormat(GLint format){
	mInternalFormat = format;
}
void Texture::setFormat(GLenum format){
	mFormat = format;
}
void Texture::setDataType(GLenum type){
	mDataType = type;
}

Texture::Texture(std::string file){
	setInternalFormat(GL_RGB);
	setFormat(GL_RGB);
	setDataType(GL_UNSIGNED_BYTE);
	load(file);	
	setSizeMode(CLAMP_EDGE);
}

Texture::Texture(std::string file, SizeMode sm){
	setInternalFormat(GL_RGB);
	setFormat(GL_RGB);
	setDataType(GL_UNSIGNED_BYTE);
	load(file);	
	setSizeMode(sm);
}

Texture::Texture(int width, int height){
	setInternalFormat(GL_RGB);
	setFormat(GL_RGB);
	setDataType(GL_UNSIGNED_BYTE);
	mWidth = width;
	mHeight = height;
	setSizeMode(CLAMP_EDGE);
}

Texture::Texture(glm::vec4 c) {
	mGen = c;
	isGeneric = true;
}

Texture::~Texture(){
	//delete mImage;
}
