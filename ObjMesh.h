#include "RenderMesh.h"
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

#ifndef OBJ_MESH
#define OBJ_MESH

class ObjMesh : public RenderMesh{
private:
	std::string mPath;
	aiMesh *mMesh;
	bool mFaceNormals = false;

	void initGeometry();
	void initMaterials();

public:

	void init();
	void initProgram();
	
	void setFaceNormals();

	void computeFaceNormals();

	//ObjMesh(std::string path);
	ObjMesh(aiMesh *mesh, aiMaterial *material);
	~ObjMesh();


};

#endif


