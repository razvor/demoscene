#include "ReflectLightComponent.h"

ReflectLightComponent::ReflectLightComponent(){

}


ReflectLightComponent::~ReflectLightComponent(){

}

void ReflectLightComponent::addLocalCubemap(LocalCubemap *l){
	mLocalCubemaps.push_back(l);
}

void ReflectLightComponent::setViewPosition(glm::vec3 vp){
	mViewPosition = vp;
}

void ReflectLightComponent::init(){
	this->initProgram();
	DifferredComponent::init();
}

void ReflectLightComponent::initProgram(){
	setProgram(Program("lightReflectPass", "lightReflectPass"));
	//setProgram(Program("lightFirstPass", "lightFirstPass"));
}

void ReflectLightComponent::initLocations(){
	DifferredComponent::initLocations();
	mViewPositionLocation = getProgram().uniform("viewPosition");
}

void ReflectLightComponent::pushAdditional(){
	glUniform3f(mViewPositionLocation, 
		mViewPosition.x, mViewPosition.y, mViewPosition.z);

	int i = 0;
	for(auto l : mLocalCubemaps){
		l->pushToComponentShader(getProgram(), i, i + getGBuffer()->getPushCount() + 1 );
		++i;
	}

	int envCountLocation = getProgram().uniform("envMapCount");
	glUniform1f(envCountLocation, mLocalCubemaps.size());

	DifferredComponent::pushAdditional();
}
