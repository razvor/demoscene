#include "ObjMesh.h"
#include "includer.h"
#include <glm/gtc/matrix_access.hpp>

ObjMesh::ObjMesh(aiMesh *m, aiMaterial *material){
	//mPath = op;
	mMesh = m;

}

ObjMesh::~ObjMesh(){

}

void ObjMesh::init(){
	//std::cout << "initing obj" << std::endl;
	initGeometry();
	initMaterials();
	RenderMesh::init();
}

void ObjMesh::initGeometry(){


	aiMesh *mesh = mMesh; //scene->mMeshes[1];

	std::vector<glm::vec3> verts, norms, tans, bitans;
	std::vector<glm::vec2> texCoords;
	std::vector<unsigned int> indices;

	verts.resize(mesh->mNumVertices);
	//std::cout << "1" << std::endl;
	norms.resize(mesh->mNumVertices);
	//std::cout << "2" << std::endl;
	tans.resize(mesh->mNumVertices);
	//std::cout << "3" << std::endl;
	bitans.resize(mesh->mNumVertices);
	texCoords.resize(norms.size());

	//std::cout << "size of mesh " << mesh->mNumVertices << std::endl;
	for(unsigned int i=0; i<mesh->mNumVertices; ++i){
		verts[i] = (glm::vec3(mesh->mVertices[i].x / 1.f, mesh->mVertices[i].y / 1.f, mesh->mVertices[i].z / 1.f));

		if(mesh->HasNormals()){
			norms[i] = (glm::vec3(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z));
		}else{
			norms[i] = (glm::vec3(0));
		}

		if(mesh->HasTangentsAndBitangents()){
			tans[i] = (glm::vec3(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z));
			bitans[i] = (glm::vec3(mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z));
		}else{
			tans[i] = (glm::vec3(0));
			bitans[i] = (glm::vec3(0));
		}

		if(mesh->HasTextureCoords(0)){
			//std::cout << "got texcoords" << std::endl;
			texCoords[i] = (glm::vec2(mesh->mTextureCoords[0][i].x * 1.0, mesh->mTextureCoords[0][i].y * 1.0));
			//std::cout << mesh->mTextureCoords[0][i].x << " ";
		}else{
			texCoords[i] = (glm::vec2(0));
		}
	}

	for(unsigned int i = 0; i < mesh->mNumFaces; i++){
		aiFace face = mesh->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++) {
			indices.push_back(face.mIndices[j]);
		}

		/*int ind1 = face.mIndices[0];
		int ind2 = *(face.mIndices+1);
		int ind3 = face.mIndices[2];
		glm::vec3 e1 = verts[ind2] - verts[ind1];
		glm::vec3 e2 = verts[ind3] - verts[ind1];
		glm::vec3 n = glm::normalize(glm::cross(e1, e2));
		norms[ind1] = n;
		norms[ind2] = n;
		norms[ind3] = n;*/
	}

	//std::cout << "setting mesh: " << indices.size() << " " << verts.size() << std::endl;
	//std::cout << "setting mesh: " << indices[100] << std::endl;

	//std::vector<glm::vec3> averageNormals(norms.size());
	/*std::vector<glm::vec1> indc(indices.size(), glm::vec1(0));
	int max = 0;
	int maxn = 0;
	for (unsigned int i = 0; i < indices.size(); ++i) {
		int ind = indices[i];
		indc[ind].x++;
		if (indc[ind].x > max) {
			++max;
			maxn = ind;
		}
	}
	std::cout << "max " << max << std::endl;
	std::cout << "man " << maxn << std::endl;
	std::cin >> max;*/

	if (mFaceNormals) {
		for (size_t i = 0; i < indices.size(); i += 3) {
			int ind1 = indices[i];
			int ind2 = indices[i+1];
			int ind3 = indices[i+2];
			glm::vec3 e1 = verts[ind2] - verts[ind1];
			glm::vec3 e2 = verts[ind3] - verts[ind1];
			glm::vec3 n = glm::normalize(glm::cross(e1, e2));

			norms[ind1] = norms[ind2] = norms[ind3] = n;

			if (mesh->HasTextureCoords(0)) {
				//std::cout << "to get" << std::endl;
				glm::vec2 w0 = texCoords[ind1];
				glm::vec2 w1 = texCoords[ind2];
				glm::vec2 w2 = texCoords[ind3];
				//std::cout << "to compute" << std::endl;
				float s1 = w1.x - w0.x; 
				float s2 = w2.x - w0.x; 
				float t1 = w1.y - w0.y;
				float t2 = w2.y - w0.y;
				glm::vec2 st1 = glm::vec2(w1.x - w0.x, w1.y - w0.y);
				glm::vec2 st2 = glm::vec2(w2.x - w0.x, w2.y - w0.y);

				/*glm::mat3x2 tb = 1.f / (st1.x * st2.y - st2.x * st1.y) *
					glm::mat2(st2.y, -st1.x, st2.y, st1.x) *
					glm::mat3x2(glm::transpose(glm::mat2x3(e1, e2)));
				glm::vec3 pt = glm::row(tb, 0);
				glm::vec3 pb = glm::row(tb, 1);*/

				float r = 1.0F / (s1 * t2 - s2 * t1);

				glm::vec3 sdir((t2 * e1.x - t1 * e2.x) * r, (t2 * e1.y - t1 * e2.y) * r,
					(t2 * e1.z - t1 * e2.z) * r);

				glm::vec3 tdir((s1 * e2.x - s2 * e1.x) * r, (s1 * e2.y - s2 * e1.y) * r,
					(s1 * e2.z - s2 * e1.z) * r);

				glm::vec3 pt = sdir;
				glm::vec3 pb = tdir;
				glm::vec3 t = pt - glm::dot(n, pt) * n;
				 //pb - glm::dot(n, pb) * n - dot(t, pb) * t / glm::dot(t, t);
				glm::vec3 b = glm::cross(n, t);

				t = normalize(t);
				
				b = glm::normalize(b);

				//std::cout << "to write" << std::endl;
				tans[ind1] = tans[ind2] = tans[ind3] = t;
				bitans[ind1] = bitans[ind2] = bitans[ind3] = b;
			}

		}
	}

	setIndices(indices);
	setVertices(verts);
	setNormals(norms);
	setTexCords(texCoords);
	setTangents(tans);
	setBiTangents(bitans);

	setPosition(glm::vec3(0, 0, 0));
	setRotation(glm::vec3(0));
	setScale(glm::vec3(1.f));

	//std::cout <
}

void ObjMesh::initMaterials(){
	Material *material = new Material(glm::vec4(0.1), 0.9, 0.5); //new Material("../models/wall");
	setMaterial(material);
}

void ObjMesh::initProgram(){
	mProgram = Program("mainVertex", "firstPass");
	if(!mProgram.init()){
		std::cerr << "Program init ERROR " << std::endl;
	}
}

void ObjMesh::computeFaceNormals() {

	std::vector<glm::vec3> ns(getVerticesCount());
	std::vector<glm::vec3> ts(getVerticesCount());
	std::vector<glm::vec3> bs(getVerticesCount());

	for (size_t i = 0; i < getIndicesCount(); i+=3) {
		int ind1 = getIndex(i);
		int ind2 = getIndex(i+1);
		int ind3 = getIndex(i+2);
		glm::vec3 e1 = getVertex(ind2) - getVertex(ind1);
		glm::vec3 e2 = getVertex(ind3) - getVertex(ind1);
		glm::vec3 n = glm::normalize(glm::cross(e1, e2));

		ns[ind1] = ns[ind2] = ns[ind3] = n;
		//ns[ind1] = 

		//glm::vec3 t =  

		glm::vec2 w0 = getTexCoords(i);
		glm::vec2 w1 = getTexCoords(i+1);
		glm::vec2 w2 = getTexCoords(i+2);
		glm::vec2 st1 = glm::vec2(w1.x - w0.x, w1.y - w0.y);
		glm::vec2 st2 = glm::vec2(w2.x - w0.x, w2.y - w0.y);
		glm::mat3x2 tb = 1.f / (st1.x * st2.y - st2.x * st1.y) * 
			glm::mat2(st2.y, -st1.x, st2.y, st1.x) * 
			glm::mat3x2(glm::transpose(glm::mat2x3(e1, e2)));
		glm::vec3 pt = glm::row(tb, 0);
		glm::vec3 pb = glm::row(tb, 1);

		glm::vec3 t = pt - glm::dot(n, pt) * n;
		glm::vec3 b = pb - glm::dot(n, pb) * n - dot(t, pb) * t / glm::dot(t, t);

		ts[ind1] = ts[ind2] = ts[ind3] = t;
		bs[ind1] = bs[ind2] = bs[ind3] = b;

	}

	setNormals(ns);
	setTangents(ns);
	setBiTangents(bs);
}

void ObjMesh::setFaceNormals() {
	mFaceNormals = true;
}