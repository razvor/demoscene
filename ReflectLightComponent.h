#ifndef REFLECT_LIGHT
#define REFLECT_LIGHT

#include "includer.h"
#include "DifferredComponent.h"
#include "LocalCubemap.h"

class ReflectLightComponent : public DifferredComponent{
private:
	std::vector<LocalCubemap*> mLocalCubemaps;
	glm::vec3 mViewPosition;

	int mViewPositionLocation;


public:

	void setViewPosition(glm::vec3 vp);

	void init();

	void initProgram();
	void initLocations();
	void pushAdditional();
	
	void addLocalCubemap(LocalCubemap *lc);

	ReflectLightComponent();
	~ReflectLightComponent();

};

#endif
