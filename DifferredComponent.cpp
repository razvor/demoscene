#include "DifferredComponent.h"

DifferredComponent::DifferredComponent(){

}

DifferredComponent::~DifferredComponent(){

}

void DifferredComponent::setGBuffer(GBuffer *gb){
	mGBuffer = gb;
}

void DifferredComponent::setState(int state){
	mState = state;
}

void DifferredComponent::setRenderToBuffer(bool rtb) {
	mRenderToBuffer = rtb;
}

void DifferredComponent::setProgram(Program p){
	mProgram = p;
}

GBuffer* DifferredComponent::getGBuffer(){
	return mGBuffer;
}

Program& DifferredComponent::getProgram(){
	return mProgram;
}

void DifferredComponent::init(){
	initGeometry();
	initProgram();
	initLocations();
}

void DifferredComponent::initGeometry(){
	//std::cout << "init gem" << std::endl;
	std::vector<glm::vec3> mVertices = {
		glm::vec3(-1, -1, 0), 
		glm::vec3(-1, 1, 0), 
		glm::vec3(1, 1, 0), 
		glm::vec3(1,-1, 0)
	};
	std::vector<unsigned int> indices = {
		0, 1, 2, 2, 0, 3
	};

	mVerticesBuffer = new VertexBuffer(1, mVertices);
	mIndicesBuffer = new VertexBuffer(2, indices);

	glGenVertexArrays(1, &mVAO);
	glBindVertexArray(mVAO);
	mIndicesBuffer->render(0);
	mVerticesBuffer->render(0);
	glBindVertexArray(0);
}

void DifferredComponent::initProgram(){
	//mProgram = Program("lightFirstPass", "lightFirstPass");
	mProgram.init();

}

void DifferredComponent::initLocations(){
	mStateLocation = mProgram.uniform("state");
}

void DifferredComponent::pushAdditional(){
	glUniform1i(mStateLocation, mState);
}

void DifferredComponent::render(){
	//std::cout << mRenderToBuffer << std::endl;
	
	
	//glClearColor(0, 0, 0, 1);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mProgram.use();
	glViewport(0, 0, mGBuffer->getSize().x, mGBuffer->getSize().y);

	if (mRenderToBuffer) mGBuffer->beginFinalCapture();

	//glViewport(0, 0, 500, 500);

	mVerticesBuffer->render(0);

	pushAdditional();

	mGBuffer->pushToShader(mProgram, 1);

	glBindVertexArray(mVAO);
	glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL );
	glBindVertexArray(0);

	if (mRenderToBuffer) mGBuffer->endFinalCapture();

}

//Debug only
void DifferredComponent::render(int w, int h){
	mProgram.use();
	glViewport(0, 0, w, h);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	pushAdditional();
	mVerticesBuffer->render(0);
	mGBuffer->pushToShader(mProgram, 1);
	//mTex->setProgram(mProgram);
	//mTex->pushToShader(0);
	/*glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mIndicesBuffer->getInstance() );
	glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL );
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);*/

	glBindVertexArray(mVAO);
	glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL );
	glBindVertexArray(0);

}
