#include "includer.h"
#include "RenderMesh.h"
#include "Model.h"
#include "RenderMesh.h"

class Voxelizator{
private:

	unsigned int gridSize = 128 * 2;
	float zoneSize = 10.f;
	float cellSize = zoneSize / gridSize;

	GLuint mVoxelTex, mVoxelTex0;
	GLuint
		mVoxelKdTex,
		mVoxelKdTexBuf,
		mVoxelPosTex,
		mVoxelPosTexBuf,
		mOctreeChildTex,
		mOctreeChildBuf,
		mOctreeDiffuseTex,
		mOctreeDiffuseBuf,
		mAtomicReadBuffer;
	GLuint mDebugVAO;
	GLuint mCountAtomicBuffer;

	std::vector<RenderMesh*> mMeshes;
	std::vector<Model*> mModels;
	Program mProgram;
	Program mFirstPassProgram; //original program
	Program mDebugProgram;
	Program mOctreeFlagProgram;
	Program mOctreeAllocProgram;
	Program mOctreeInitProgram;
	Program mOctreeLeafProgram;
	Program mOctreeMipmapProgram;
	Program mDownSampleProgram;

	void initTexture();
	void initShaders();
	void initBuffers();

	void genLinearBuffer(unsigned int size, GLenum format, GLuint *tex, GLuint *buf);
	void genAtomicBuffer(GLuint *buf);
	void genFastAtomicBuffer(GLuint *buf, GLuint *bufRead);
	
	void buildSVO(int voxelsCount);

	void prepare();

	void turnBackState();

public:

	void setMeshes(std::vector<RenderMesh*> &m);
	void setModels(std::vector<Model*> &m);

	GLuint getVoxelTextureId();
	GLuint getSVOChildId();
	GLuint getSVODiffuseId();
	int getVoxelGridSize();

	void init();
	void render();
	void debugShow();
	void show(glm::mat4 viewMatrix, glm::mat4 projectionMatrix, glm::vec3 viewPosition);

	Voxelizator();
	~Voxelizator();

};
