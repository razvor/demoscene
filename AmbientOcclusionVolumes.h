#include "includer.h"
#include "Framebuffer.h"
#include "GBuffer.h"
#include "RenderMesh.h"
#include "Model.h"

class AmbientOcclusionVolumes {
private:
	
	std::vector<RenderMesh*> mMeshes;
	std::vector<Model*> mModels;

	Program mAOProgram;
	Program mFirstPassProgram;
	GBuffer *mGBuffer;
	Texture *mOutTexture;
	Framebuffer *mOutFramebuffer;

	VertexBuffer *mVerticesBuffer;

	GLuint mQuadVAO;

	int mWidth, mHeight;
	float mFocalLen[2];
	float mInvFocalLen[2];
	float mUVToViewA[2];
	float mUVToViewB[2];
	
	void initConsts();
	void initPrograms();
	void initQuad();
	void initFramebuffers();

	void firstPass();
	void aoPass();

public:
	void init();
	void render();

	void setSize(int w, int h);
	void setMeshes(std::vector<RenderMesh*> ms);
	void setModels(std::vector<Model*> ms);

	Texture* getOutTexture();

	AmbientOcclusionVolumes();
	~AmbientOcclusionVolumes();
};