#ifndef ROOM_MODEL
#define ROOM_MODEL

#include "Model.h"
#include "Material.h"

class RoomModel : public Model{
private:
	
	int mHightlighten = 0;

	void initMaterials();


public:

	std::vector<int> getFaceNormalMeshes();

	virtual void postProcess();

	void hightlight();

	RoomModel();
	~RoomModel();
};

#endif