#include "Window.h"

Window::Window(){
	//init();
}

Window::~Window(){
	
}

bool Window::init(){
	if (!glfwInit()){
	  std::cerr << "Error initing GLFW" << std::endl;
	}
	glfwWindowHint(GLFW_SAMPLES, 8);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	
	mWindow = glfwCreateWindow(mViewportWidth, mViewportHeight, "Demo scene", NULL, NULL);
	glfwMakeContextCurrent(mWindow);
	if(mWindow == NULL){
		std::cerr << "Error creating window" << std::endl;
		getchar();
		glfwTerminate();
		return false;
	}

	glfwMakeContextCurrent(mWindow);
	glfwGetFramebufferSize(mWindow, &mViewportWidth, &mViewportHeight );

	//glewExperimental = GL_TRUE;
	//glGetError();
	
	/*if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return false;
	}*/

	if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)){
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

	std::cout << "Versions glfw: " << glfwGetVersionString() << std::endl;
	std::cout << "Versions gl: " << glGetString(GL_VERSION) << std::endl;

	std::cout << "gl version " << glGetString(GL_VERSION) << std::endl;

	glfwSetInputMode(mWindow, GLFW_STICKY_KEYS, GL_TRUE);

	glfwSetKeyCallback(mWindow, Window::keyCallback);

	return true;
}

void Window::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods){
  WindowStatic::getInstance().mEventFunction(key, action);
}

void Window::closeWindow(){
	glfwDestroyWindow(mWindow);
}

void Window::terminate(){
	glfwTerminate();
}

void Window::start(){
	while ( !glfwWindowShouldClose(mWindow) 
		&& glfwGetKey(mWindow, GLFW_KEY_ESCAPE ) != GLFW_PRESS ){
		mDrawFunction();
		glfwPollEvents();
		glfwSwapBuffers(mWindow);
	}
}

void Window::end(){
  closeWindow();
  terminate();
}

void Window::setViewportSize(int w, int h){
	mViewportWidth = w;
	mViewportHeight = h;
}

void Window::setDrawFunction(std::function<void()> df){
	mDrawFunction = df;
}

int* Window::getSize(){
	int *s = new int[2];
	glfwGetFramebufferSize(mWindow, &s[0], &s[1]);
	return s;
}

void Window::setEventFunction(std::function<void(int, int)> ef){
  WindowStatic::getInstance().mEventFunction = ef;
}
