#include "AntiAliasingComponent.h"

void AntiAliasingComponent::init(){
	this->initProgram();
	DifferredComponent::init();
	//mLights->init(getProgram());
}

void AntiAliasingComponent::initLocations() {
	DifferredComponent::initLocations();
}

void AntiAliasingComponent::initProgram() {
	setProgram(Program("fxaaPass", "fxaaPass"));
}

void AntiAliasingComponent::pushAdditional() {
	//TODO
	DifferredComponent::pushAdditional();
}