# Graphics demoscene 

Indor demoscene(render) implemented from scratch on C++ and OpenGL

![](https://pbs.twimg.com/media/DVM2TywWkAcuxJH.jpg)
![](https://pbs.twimg.com/media/DVM2Y5rWkAAJcpd.jpg)

Graphic things implemented:

+ BRDF models
+ Voxel GI
+ HBAO
+ FXAA
+ PBR metallness workflow

Other things:

- Models and materials loader
- Crossplatform CMake

Controls:

W, A, S, D - forward, left, right, back

R, F - top, bottom

Compiling:
Just configure using cmake in build dir and compile.