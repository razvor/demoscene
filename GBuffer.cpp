#include "GBuffer.h"

GBuffer::GBuffer(int w, int h) {

	setSize(w, h);
}

GBuffer::~GBuffer(){}

glm::uvec2 GBuffer::getSize(){
	return glm::uvec2(mWidth, mHeight);
}

void GBuffer::setSize(int w, int h){
	mWidth = w;
	mHeight = h;
	if(mFramebuffer != NULL)
		mFramebuffer->setSize(w, h);
}

void GBuffer::init(){

	std::string mTextureUnifNames[] = {
		"albedos",
		"normals",
		"positions",
		"boundingBoxes",
		"types",
		//"occlusions",
		"depths",
		"finals"
	};

	mFramebuffer = new Framebuffer(mWidth, mHeight);
	mFinalFramebuffer = new Framebuffer(mWidth, mHeight);

	for(int i=0; i<mTexturesCount; ++i){
		mFramebuffer->addTexture(new Texture(mWidth, mHeight));
		mFramebuffer->getTexture(i)->setUniformName(mTextureUnifNames[i]);
		mFramebuffer->getTexture(i)->setInternalFormat(GL_RGBA32F);
		mFramebuffer->getTexture(i)->setFormat(GL_RGBA);
		mFramebuffer->getTexture(i)->setDataType(GL_FLOAT);
	}

	mFinalFramebuffer->addTexture(new Texture(mWidth, mHeight));
	mFinalFramebuffer->getTexture(0)->setUniformName(mTextureUnifNames[6]);
	mFinalFramebuffer->getTexture(0)->setInternalFormat(GL_RGBA32F);
	mFinalFramebuffer->getTexture(0)->setFormat(GL_RGBA);
	mFinalFramebuffer->getTexture(0)->setDataType(GL_FLOAT);

	/*mFramebuffer->getTexture(3)->setInternalFormat(GL_RGBA32UI);
	mFramebuffer->getTexture(3)->setDataType(GL_UNSIGNED_INT);
	mFramebuffer->getTexture(3)->setFormat(GL_RGBA_INTEGER);*/

	for(int i=0; i<mTexturesCount; ++i){
		mFramebuffer->getTexture(i)->init();
	}
	mFramebuffer->init();

	mFinalFramebuffer->getTexture(0)->init();
	mFinalFramebuffer->init();

	mFramebuffer->getDepthTexture()->setUniformName(mTextureUnifNames[6]);

}

void GBuffer::beginCapture(){
	mFramebuffer->beginCapture();
}

void GBuffer::endCapture(){
	mFramebuffer->endCapture();
}

void GBuffer::beginFinalCapture(){
	mFinalFramebuffer->beginCapture();
}

void GBuffer::endFinalCapture(){
	mFinalFramebuffer->endCapture();
}

void GBuffer::show(){
	mFramebuffer->show(0, glm::vec4( 0, 0, mWidth/4, mHeight/4 ));
}

void GBuffer::show(int n){
	mFramebuffer->show(n, glm::vec4( 0, 0, mWidth/4, mHeight/4 ));
}

void GBuffer::showFinal() {
	mFinalFramebuffer->show(0, glm::vec4(0, 0, mWidth / 4, mHeight / 4));
}

int GBuffer::getPushCount(){
	return mTexturesCount + 2; // + depth and final buffers
}

void GBuffer::pushToShader(Program &program, int ind){
	//mFramebuffer->bindForReading();
	//const int textureCount = 4;
	int i;
	for(i=0; i< mTexturesCount; ++i){
		mFramebuffer->getTexture(i)->setProgram(program);
		mFramebuffer->getTexture(i)->pushToShader(i+ind);
	}
	mFramebuffer->getDepthTexture()->setProgram(program);
	mFramebuffer->getDepthTexture()->pushToShader(++i);

	mFinalFramebuffer->getTexture(0)->setProgram(program);
	mFinalFramebuffer->getTexture(0)->pushToShader(++i);
}


