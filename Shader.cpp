#include "Shader.h"

Shader::Shader( string vertex, string fragment ){
	mProgram = Program(vertex, fragment);
}

Shader::~Shader(){

}

void Shader::init(){
	mProgram.init();
}
