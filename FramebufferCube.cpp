#include "FramebufferCube.h"

FramebufferCube::FramebufferCube(TextureCube *tc){
	mTextureCube = tc;
	mWidth = tc->getSize().x;
	mHeight = tc->getSize().y;
}

FramebufferCube::FramebufferCube(int w, int h){
	mWidth = w;
	mHeight = h;
}

FramebufferCube::~FramebufferCube(){

}

GLint FramebufferCube::getId(){
	return mId;
}

void FramebufferCube::setLevel(int l){
	mLevel = l;
}

void FramebufferCube::swapTexture(TextureCube *ntc){
	mTextureCube = ntc;
}

void FramebufferCube::init(){
	mDepth = new Texture(mWidth, mHeight);
	mDepth->setInternalFormat(GL_DEPTH24_STENCIL8);
	mDepth->setFormat(GL_DEPTH_STENCIL);
	mDepth->setDataType(GL_UNSIGNED_INT_24_8);
	mDepth->init();
	
	glGenFramebuffers(1, &mId);
	glBindFramebuffer(GL_FRAMEBUFFER, mId);

	//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT, GL_TEXTURE_2D, mDepth->getId(), 0);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, mDepth->getId(), 0);

	GLenum drawBuffers[1];
	drawBuffers[0] = GL_COLOR_ATTACHMENT0;
	glDrawBuffers(6, drawBuffers);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE){
		std::cout << "FRAMEBUFFER ERROR: Framebuffer creating error" << status << std::endl;

		switch(status){
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
				std::cout << "Inc att" << std::endl;
				break;
			case GL_FRAMEBUFFER_UNSUPPORTED:
				std::cout << "Format" << std::endl;
				break;
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FramebufferCube::initDepth(){

}

void FramebufferCube::initBuffer(){

}

void FramebufferCube::prepare(int i){
	glBindTexture(GL_TEXTURE_CUBE_MAP, mTextureCube->getId());
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
		GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, mTextureCube->getId(), mLevel);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, mDepth->getId(), 0);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE){
		std::cout << "FRAMEBUFFER ERROR: Framebuffer creating error" << status << std::endl;

		switch(status){
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
				std::cout << "Inc att" << std::endl;
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
				std::cout << "layer targets" << std::endl;
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
				std::cout << "missing att" << std::endl;
				break;
			case GL_FRAMEBUFFER_UNSUPPORTED:
				std::cout << "Format" << std::endl;
				break;
		}
	}
}

void FramebufferCube::beginCapture(int i){
	glBindFramebuffer(GL_FRAMEBUFFER, mId);
	prepare(i);
	glViewport(0, 0, mWidth / pow(2, mLevel), mHeight / pow(2, mLevel));
	glClearColor(0.0, 0, 0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void FramebufferCube::endCapture(){
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
