#include <iostream>
#include <fstream>
//#include <OpenGL/gl.h>
//#include <GLUT/glut.h>
#include <streambuf>
#include <filesystem>

namespace fs = std::experimental::filesystem;

using namespace std;

class Program{
	
	string 
		vShaderExt = "vsh",
		fShaderExt = "fsh",
		gShaderExt = "geom",
		cShaderExt = "comp",
		shaderFileRoot = "../shaders/";

	string
		vShaderFile,
		fShaderFile,
		gShaderFile,
		vShaderSource,
		fShaderSource,
		gShaderSource;

	GLuint vShader, fShader, gShader, program;

	bool mIsComputeShader = false;
	bool mIsGeometryShader = false;

	int k1 = 0, k2 = 0;

public:

	Program(){
		//std::cout << "PROGRMM INITED WIHTOUT CONSTRUCTOR" << std::endl;
	}

	string loadFile(string file){

		

		ifstream shaderFile(file);
		if( shaderFile.fail() ){ 
			std::cout << "Fieled to load shader " << file << std::endl;
		}
		string shaderContent = string(
			istreambuf_iterator<char>(shaderFile),
			istreambuf_iterator<char>()
		);
		shaderFile.close();
		return shaderContent;
	}

	bool loadShader( int type ){
		string fl, src;
		
		if( type == 1 ){
			fl = this -> vShaderFile + "." + this -> vShaderExt;
		}else if(type == 2) {
			fl = this->fShaderFile + "." + this->fShaderExt;
		}else if(type == 3) {
			fl = this->fShaderFile + "." + this->cShaderExt;
		}else if(type == 4){
			fl = this -> gShaderFile + "." + this->gShaderExt;
		}

		fl = this -> shaderFileRoot + fl;

		ifstream shaderFile(fl);
		if( shaderFile.fail() ){ 
			std::cout << "Fieled to load shader " << type << " " << fl << std::endl;
			return false;
		}
		string shaderContent = string(
			istreambuf_iterator<char>(shaderFile),
			istreambuf_iterator<char>()
		);
		shaderFile.close();

		if (type == 1)
			vShaderSource = shaderContent;
		else if (type == 2)
			fShaderSource = shaderContent;
		else if (type == 3)
			fShaderSource = shaderContent;
		else if (type == 4)
			gShaderSource = shaderContent;

		return true;
	}

	bool compileShader( int type ){
		//cout << "Compile Shader" << endl;

		GLuint shader;
		int sourceCount;
		GLint compiled;
		string *src;
		const char **sr;
		bool res = true;

		if( type == 1 ){
			shader = glCreateShader(GL_VERTEX_SHADER);
			sourceCount = 1;
			//src = new string{ vShaderSource }; 
			sr = new const char*[sourceCount]; 
			sr[0] = vShaderSource.c_str();
		}else if(type == 2){
			shader = glCreateShader(GL_FRAGMENT_SHADER);
			/*if(fShaderSource[0] != '#'){
				string fShaderBRDF = loadFile("../shaders/common/BRDF.fsh");
				sourceCount = 2;
				sr = new const char*[sourceCount];
				sr[0] = fShaderBRDF.c_str();
				sr[1] = fShaderSource.c_str();
			}else{
				sourceCount = 1;
				sr = new const char*[sourceCount];
				sr[0] = fShaderSource.c_str();
			}*/
			
			sourceCount = 1;
			sr = new const char*[sourceCount];
			sr[0] = fShaderSource.c_str();

			//src = new ; 
		}else if(type == 3){
			shader = glCreateShader(GL_COMPUTE_SHADER);
			sourceCount = 1;
			sr = new const char*[sourceCount]; 
			sr[0] = fShaderSource.c_str();
		}
		else if (type == 4) {
			shader = glCreateShader(GL_GEOMETRY_SHADER);
			sourceCount = 1;
			sr = new const char*[sourceCount];
			sr[0] = gShaderSource.c_str();
		}

		/*char *sh[sourceCount];
		for(int i = 0; i < sourceCount; ++i)
			sh[i] = src*/

		//const char *sr = src -> c_str();
		
		glShaderSource( shader, sourceCount, sr, 0 );
		
		std::cout << "Compiling " << vShaderFile << " " << type << " " << shader << std::endl;
		
		glCompileShader( shader );
		
		//const GLchar *path = "aa";
		//glNamedStringARB();

		//glNamedStringARB(GL_SHADER_INCLUDE_ARB, -1, "/lights.fsh", -1, "../shaders/lights.fsh");
		//std::cout << 0 << std::endl;
		//auto path = "/";
		//glCompileShaderIncludeARB(shader, 1, &path, nullptr);
		//glCompileShaderIncludeARB(shader, 0, NULL, NULL);
		//  glad_glCompileShaderIncludeARB(shader, 1, &path, nullptr);

		//std::cout << 1 << std::endl;

		glGetShaderiv( shader, GL_COMPILE_STATUS, &compiled );

		std::cout << 2 << std::endl;

		if(!compiled){

			GLint blen = 0;
			GLsizei slen = 0;
				
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH , &blen);     

			GLchar* compilerLog = (GLchar*)malloc(blen);
			glGetShaderInfoLog(shader, 1000, &blen, compilerLog);
			cout << "Shader " + fShaderFile + " type " + std::to_string(type) + " Compilation error: " << compilerLog << endl;

			getchar();

			res = false;
		}

		if (type == 1)
			vShader = shader;
		else if (type == 2)
			fShader = shader;
		else if (type == 3)
			fShader = shader;
		else if (type == 4)
			gShader = shader;

		return res;

	}
	
	bool createProgram(){
		bool res = true;
		GLuint program = glCreateProgram();

		if(mIsComputeShader){
			glAttachShader( program, fShader );
		}else{
			glAttachShader( program, vShader );	
			glAttachShader( program, fShader );
			if (mIsGeometryShader) glAttachShader( program, gShader );
		}

		glLinkProgram( program );

		GLint linked;
		glGetProgramiv(program, GL_LINK_STATUS, &linked);
		if(!linked){
			GLint len = 0;
			glGetProgramiv( program,	GL_INFO_LOG_LENGTH, &len );
			GLchar *linkLog = (GLchar*)malloc(len);
			glGetProgramInfoLog(program, len, &len, &linkLog[0]);
			cout << "Link error in " << vShaderFile << " + " << fShaderFile << " : " << linkLog << endl;
			res = false;

			getchar();
		}

		//glBindFragDataLocation( program, 0, "fragColor" );

		this -> program = program;

		//std::cout << "Program created " << std::endl;

		return res;

	}

	bool init(){
		if(mIsComputeShader){
			std::cout << "compute shader" << std::endl;
			return loadShader(3) && compileShader(3) && createProgram();
		}else{
			return 
				loadShader(1) &&
				loadShader(2) &&
				(mIsGeometryShader ? loadShader(4) : true) &&
				compileShader(1) &&
				compileShader(2) &&
				(mIsGeometryShader ? compileShader(4) : true) &&
				createProgram();
		}
	}

	GLint getInstance(){
		return program;
	}

	void use(){
		glUseProgram( this -> getInstance() );
	}

	void unuse(){
		glUseProgram( 0 );
	}

	int attribute( const char *name ){
		int r = glGetAttribLocation( this -> getInstance(), name );
		if (r == -1 && k1 < 5) { cerr << "No attr found for " << name << endl; k1=0; }
		return r;
	}

	int uniform( const char *name ){
		int r = glGetUniformLocation( this -> getInstance(), name );
		if (r == -1) { cerr << "No unif in " << fShaderFile << " found for " << std::string(name) << endl; }
		return r;
	}

	int uniform( std::string name ){
		int r = glGetUniformLocation( this -> getInstance(), name.c_str() );
		if (r == -1 && k2 < 5) { cerr << "No unif in " << fShaderFile << " found for " << std::string(name) << endl; k2=0; }
		return r;
	}

	Program( string vertex, string fragment ){
		vShaderFile = vertex;
		fShaderFile = fragment;
	};

	Program( string vertex, string fragment, string geometry ){
		vShaderFile = vertex;
		fShaderFile = fragment;
		gShaderFile = geometry;
		mIsGeometryShader = true;
	};

	Program(string compute){
		fShaderFile = compute;
		mIsComputeShader = true;
	}

	~Program(){
		glUseProgram(0);
		glDetachShader( getInstance(), vShader );
		glDetachShader( getInstance(), fShader );
		glDeleteShader( vShader );
		glDeleteShader( fShader );
	}
};
