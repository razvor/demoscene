#ifndef FRAMEBUFFER
#define FRAMEBUFFER

#include "includer.h"
#include "Texture.h"

class Framebuffer{
private:

	GLint mInternalFormat;
	GLenum mFormat, mDataType;
	
	int mChannel;
	int mWidth;
	int mHeight;

	GLuint mId;
	
	std::vector<Texture*> mTextures;
	Texture *mDepth;

	void attachTexture(int ind);


public:

	/*void setInternalFormat(GLint format);
	void setFormat(GLenum format);
	void setDataType(GLenum type);
	void setCannel(int c);*/
	
	void setSize(int width, int height);

	void addTexture(Texture *texture);
	void swapTexture(int ind, Texture *t);
	void swapDepth(Texture *t);
	Texture *getTexture(int ind);
	Texture *getDepthTexture();

	void init();

	void bindForReading();
	void bindForWriting();

	void beginCapture();
	void endCapture();

	void show(int c, glm::vec4 zone);

	Framebuffer(int width, int height);
	~Framebuffer();

};

#endif
