#version 440

layout (points) in;
layout (triangle_strip, max_vertices=6) out;

in vec3 vPos[];

out vec4 vColor;
out vec3 vGeomNormal;
out vec2 vUV;

uniform layout(binding=0, r32ui) uimageBuffer octreeDiffuse;
uniform layout(binding=1, r32ui) uimageBuffer octreeChild;

uniform layout(binding=5, rgba8) readonly image3D voxelTexture;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform int gridSize;
uniform int levels;
uniform float zoneSize;

vec3 cube[] = {
		vec3(-1.0, -1.0, -1.0),
		vec3(-1.0, 1.0, -1.0),
		vec3(1.0, 1.0, -1.0),
		vec3(1.0, 1.0, -1.0),
		vec3(1.0, -1.0, -1.0),
		vec3(-1.0, -1.0, -1.0),

		//back
		vec3(-1.0, -1.0, 1.0),
		vec3(-1.0, 1.0, 1.0),
		vec3(1.0, 1.0, 1.0),
		vec3(1.0, 1.0, 1.0),
		vec3(1.0, -1.0, 1.0),
		vec3(-1.0, -1.0, 1.0),

		//top
		vec3(-1.0, 1.0, -1.0),
		vec3(-1.0, 1.0, 1.0),
		vec3(1.0, 1.0, 1.0),
		vec3(1.0, 1.0, 1.0),
		vec3(1.0, 1.0, -1.0),
		vec3(-1.0, 1.0, -1.0),

		//bottom
		vec3(-1.0, -1.0, -1.0),
		vec3(-1.0, -1.0, 1.0),
		vec3(1.0, -1.0, 1.0),
		vec3(1.0, -1.0, 1.0),
		vec3(1.0, -1.0, -1.0),
		vec3(-1.0, -1.0, -1.0),

		//right
		vec3(1.0, -1.0, -1.0),
		vec3(1.0, -1.0, 1.0),
		vec3(1.0, 1.0, 1.0),
		vec3(1.0, 1.0, 1.0),
		vec3(1.0, 1.0, -1.0),
		vec3(1.0, -1.0, -1.0),

		//left
		vec3(-1.0, -1.0, -1.0),
		vec3(-1.0, -1.0, 1.0),
		vec3(-1.0, 1.0, 1.0),
		vec3(-1.0, 1.0, 1.0),
		vec3(-1.0, 1.0, -1.0),
		vec3(-1.0, -1.0, -1.0)
};

vec4 convRGBA8ToVec4( uint val ){
    return vec4( float( (val&0x000000FF) ), float( (val&0x0000FF00)>>8U),
	             float( (val&0x00FF0000)>>16U), float( (val&0xFF000000)>>24U) );
}

bool nodeOccupied( in uvec3 loc, out int leafIdx ){
    int voxelDim = gridSize;

	//decide max and min coord for the root node
	uvec3 umin = uvec3(0,0,0);

    bool bOccupied = true;
	uint idx = 0;
	uint subnode;
	uvec3 offset;
	int depth;
	
    for( depth = 0; depth <= levels; ++depth ){
	    leafIdx = int(idx);
	    idx = imageLoad( octreeChild, int(idx) ).r;
		if( (idx & 0x80000000) == 0 ){
		    bOccupied= false;
		    break;
		}
		else if( depth == levels ){
			//return true;
		    break;
		}

		idx &= 0x7FFFFFFF;  //mask out flag bit to get child pointer
		if( idx == 0 ){
		    bOccupied = false;
		    break;
		}
		voxelDim /= 2;
		/*offset = clamp( ivec3( 1 + loc - umin - voxelDim ), 0, 1 );*
	    idx += offset.x + 4*offset.y + 2*offset.z;*/


		subnode = (loc.x > voxelDim + umin.x - 1) ? 1 : 0;
		subnode += (loc.y > voxelDim + umin.y - 1) ? 2 : 0;
		subnode += (loc.z > voxelDim + umin.z - 1) ? 4 : 0;
	    idx += int(subnode);
	    umin.x += voxelDim * ((loc.x > voxelDim + umin.x - 1) ? 1 : 0);
	    umin.y += voxelDim * ((loc.y > voxelDim + umin.y - 1) ? 1 : 0);
	    umin.z += voxelDim * ((loc.z > voxelDim + umin.z - 1) ? 1 : 0);

	    //umin += voxelDim * offset;
	}

	return bOccupied;
}

/*void main(){
	
	mat4 mm = projectionMatrix * viewMatrix; /// float(gridSize) * 10.0;

	vec3 p = gl_in[0].gl_Position.xyz;
	uvec3 ip = uvec3( p );

	vec4 pos, vox;
	int leafIdx;
	if( nodeOccupied( ip, leafIdx )  ){
	    uint val = imageLoad( octreeDiffuse, leafIdx ).r;
	    vox = convRGBA8ToVec4(val).rgba / 255.0; 
	}
	else return;

	if(true){
		for(int f=0; f<12; ++f){
			for(int v=0; v<3; ++v){

				gl_Position = mm * vec4( ((p / float(gridSize) ) - vec3(0.5)) * zoneSize + cube[f*3+v] / float(gridSize) * zoneSize, 1.0 );

				vGeomNormal = normalize(cross( cube[f*3+1] - cube[f*3], cube[f*3+2] - cube[f*3] ));
				vColor = vec4(vox);
				EmitVertex();
			}
			EndPrimitive();
		}
	}

}*/

void main(){
	gl_Position = vec4(-1.0, -1.0, 0.0, 1.0);
	vUV = vec2(1.0, 1.0);
	EmitVertex();
	gl_Position = vec4(-1.0, 1.0, 0.0, 1.0);
	vUV = vec2(1.0, 0.0);
	EmitVertex();
	gl_Position = vec4(1.0, 1.0, 0.0, 1.0);
	vUV = vec2(0.0, 0.0);
	EmitVertex();
	EndPrimitive();

	gl_Position = vec4(1.0, 1.0, 0.0, 1.0);
	vUV = vec2(0.0, 0.0);
	EmitVertex();
	gl_Position = vec4(1.0, -1.0, 0.0, 1.0);
	vUV = vec2(0.0, 1.0);
	EmitVertex();
	gl_Position = vec4(-1.0, -1.0, 0.0, 1.0);
	vUV = vec2(1.0);
	EmitVertex();
	EndPrimitive();
}