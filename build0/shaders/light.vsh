#version 330

layout(location=0) in vec3 aPosition;
layout(location=1) in vec3 aNormal;
layout(location=2) in vec2 aTexCord;
layout(location=3) in vec3 aTangent;
layout(location=4) in vec3 aBitangent;

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform int state;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCord;

vec4 getParabaloid(vec4 inp){
	float L = length(inp);
	inp = inp / L;
	inp.z += 1.0;
	inp.x /= inp.z;
	inp.y /= inp.z;

	inp.z = (L - 0.1) / (30.0);
	inp.w = 1;
	
	return inp;
}

void main(){
	vNormal = aNormal;
	vPosition = aPosition;
	vTexCord = aTexCord;

	vec3 t, b;
	t = aTangent;
	b = aBitangent;

	vec4 oPos =  projectionMatrix * viewMatrix * modelMatrix * vec4( aPosition, 1.0 );

	if(state == 1){
		oPos = getParabaloid(oPos);
	}

	/*if(state == 1){

		oPos = normalize(oPos);

		float L = length(oPos);
		oPos = oPos / L;
		oPos.z += 1;
		oPos.x /= oPos.z;
		oPos.y /= oPos.z;

		oPos.z = (L - 0.1) / (30.0 - 0.1);
		oPos.w = 1;
	}*/

	gl_Position = oPos;

}
