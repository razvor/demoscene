//#version 410

uniform sampler2D albedos;
uniform sampler2D normals;
uniform sampler2D positions;
uniform sampler2D boundingBoxes;
uniform sampler2D types;
uniform sampler2D depths;
uniform sampler2D finals;
uniform vec3 viewPosition;
uniform int state;
uniform samplerCube envMaps[20];
uniform vec3 envMapPosition[20];
uniform mat4 envMapMatrices[20];
uniform mat4 irradiance[60];
uniform int envMapCount;
uniform uvec2 Dimensions = uvec2(256, 256);

in vec2 cords;

layout(location=0) out vec4 FragColor;

/* Prefiltering */

//const float PI = 3.1415926535807932384;

float FSchlick1(float VoH){
  float Kmetallic = globalData.metallness;
  return Kmetallic + (1.0 - Kmetallic) * pow(1.0 - VoH, 5.0);
}

float compute_lod(uint NumSamples, float NoH){
	return 0.5 * (log2(float(Dimensions.x * Dimensions.y) / NumSamples) - log2(D1(NoH, 0.1)));
}

vec2 hammersley(uint i, uint N) {
	return vec2(float(i)/float(N), radicalInverse_VdC(i));
}

vec3 parallaxReflection(vec3 V){
	vec3 normal = globalData.normal;
	vec3 origPos = vec3(0, 1, 0);
	vec3 cubemapPos = envMapPosition[0]; 
	//cubemapPos = vec3(2.0, 1.0, -0.0);
	vec3 bBox = vec3( 10, 2.0, 10 );
	vec3 bBoxMin = origPos - bBox/2;
	vec3 bBoxMax = origPos + bBox/2;
	vec3 viewVec = V;
	vec3 vertPos = globalData.fragPosition;

	vec3 refDir = reflect(normalize(-viewVec), normal);
	vec3 invRef = 1.0 / refDir;
	vec3 intMin = (bBoxMin - vertPos) / refDir;
	vec3 intMax = (bBoxMax - vertPos) / refDir;
	vec3 invLar = max(intMin, intMax);
	float dist  = min((invLar.x, invLar.y), invLar.z);

	vec3 intPos = vertPos + refDir * dist;	
	vec3 locVec = intPos - cubemapPos;

	return locVec;
}

vec3 parallaxOBB(int ind, vec3 V){

	mat4 envMat = inverse(envMapMatrices[ind]);
	vec3 envPos = envMapPosition[ind];

	vec3 reflectionWS = reflect(-V, globalData.normal);

	vec3 rayLS = vec3(envMat * vec4(reflectionWS, 0.0));
	vec3 positionLS =  vec3(envMat * vec4(globalData.fragPosition, 1.0));

	vec3 unitary = vec3(1.0, 1.0, 1.0);
	vec3 firstPlaneIntersect  = (unitary - positionLS) / rayLS;
	vec3 secondPlaneIntersect = (-unitary - positionLS) / rayLS;
	vec3 furthestPlane = max(firstPlaneIntersect, secondPlaneIntersect);
	float dist = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));

	// Use Distance in WS directly to recover intersection
	vec3 intersectPositionWS = globalData.fragPosition + reflectionWS * dist;
	vec3 res = intersectPositionWS - envPos;

	return res * vec3(1);
}

/*samplerCube getEnvMap(int ind){
	if(ind == 0) return envMaps[0];
	if(ind == 1) return envMaps[1];
	if(ind == 2) return envMaps[2];
	if(ind == 3) return envMaps[3];
}*/

vec3 ImportanceSampleGGX(vec2 Xi, float Roughness, vec3 N){
	float a = Roughness * Roughness;

	float M_PI = PI;
	float Phi = 2 * M_PI * Xi.x;
	float CosTheta = sqrt((1 - Xi.y) / (1 + (a*a - 1) * Xi.y));
	float SinTheta = sqrt(1 - CosTheta * CosTheta);

	vec3 H;
	H.x = SinTheta * cos(Phi);
	H.y = SinTheta * sin(Phi);
	H.z = CosTheta;

	vec3 UpVector = abs(N.z) < 0.999 ? vec3(0, 0, 1) : vec3(1, 0, 0);
	vec3 TangentX = normalize(cross(UpVector, N));
	vec3 TangentY = cross(N, TangentX);

	// Tangent to world space
	return TangentX * H.x + TangentY * H.y + N * H.z;
}

vec3 computeIbl(vec3 localRefVec){
	vec3 normal = globalData.normal;
	vec3 view = -normalize(globalData.viewPosition - globalData.fragPosition);
	float roughness = 0.1; //globalData.roughness;
	vec3 upVector = abs(normal.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
    vec3 tangentX = normalize(cross(upVector, normal));
    vec3 tangentY = cross(normal, tangentX);
	
	float NoV = abs(dot(normal, view));

	vec3 fColor = vec3(0.0);
	const uint NumSamples = 128;
	for (uint i = 0; i < NumSamples; ++i){
		vec2 Xi = hammersley(i, NumSamples);
		//vec3 Li = S(Xi); 
		vec3 Li = ImportanceSampleGGX(Xi, roughness, normal);
		vec3 H  = normalize(Li.x * tangentX + Li.y * tangentY + Li.z * normal);
		vec3 L  = localRefVec; //normalize(-reflect(view, normal));

		float NoL = abs(dot(normal, L));
		float NoH = abs(dot(normal, H));
		float VoH = abs(dot(view, H));
		float lod = compute_lod(NumSamples, NoH);

		float F_ = FSchlick1(VoH);
		float G_ = G1(NoL, NoV, NoH, VoH, roughness);
		//G(NoL, NoV, NoH, VoH); 
		//L *= vec3( -1, 1, -1 );
		vec3 LColor = textureLod( envMaps[0], L, lod ).rgb; 
		//textureSphereLod(environment, rEnv(L), lod).rgb; 
		fColor += F_ * G_ * LColor * VoH / (NoH * NoV);
		//fColor += LColor;;
	}
	fColor /= float(NumSamples);

	return fColor;
}

vec3 SpecularIBL( vec3 SpecularColor, float Roughness, vec3 N, vec3 V, vec3 locVec ) {
	vec3 SpecularLighting = vec3(0);
	const uint NumSamples = 256;
	for( uint i = 0; i < NumSamples; i++ ) {
		vec2 Xi = hammersley( i, NumSamples );
		vec3 H = ImportanceSampleGGX( Xi, Roughness, N ); 
		vec3 L = 2 * dot( V, H ) * H - V;
		//vec3 L = reflect(-V, H);
		//v
		vec3 R = parallaxReflection(V);
		L = reflect(-N, R);

		float NoV = saturate( dot( N, V ) ); 
		float NoL = saturate( dot( N, L ) ); 
		float NoH = saturate( dot( N, H ) ); 
		float VoH = saturate( dot( V, H ) );
		
		if( NoL > 0 ) {
			float lod = compute_lod(NumSamples, NoH);
			vec3 SampleColor = texture( envMaps[0], L, lod ).rgb;
			float G = GSmithGgx_(NoV, Roughness);
			float Fc = pow( 1 - VoH, 5 );
			vec3 F = (1 - Fc) * SpecularColor + Fc;
			// Incident light = SampleColor * NoL
			// Microfacet specular = D*G*F / (4*NoL*NoV)
			// pdf = D * NoH / (4 * VoH)
			SpecularLighting += SampleColor * F * G * VoH / (NoH * NoV);
        }
	}

	return SpecularLighting / NumSamples; 
}

/*End prefiltering*/

vec3 getIrradiance(){
	vec4 nn = vec4(-globalData.normal, 1.0);
	vec3 irr;
	for(int i=0; i<3; ++i){
		irr[i] = dot(nn, irradiance[i] * nn);
	}
	return irr;
}

vec3 getIrradiance(vec3 vec){
	vec4 nn = vec4(vec, 1.0);
	vec3 irr;
	for(int i=0; i<3; ++i){
		irr[i] = dot(nn, irradiance[i] * nn);
	}
	return irr;
}

float GGX1(float nDotV, float a){
	float aa = a*a;
	float oneMinusAa = 1 - aa;
	float nDotV2 = 2 * nDotV;
	float root = aa + oneMinusAa * nDotV * nDotV;
	return nDotV2 / (nDotV + sqrt(root));
}
float G_Smith(float a, float nDotV, float nDotL){
	return GGX1(nDotL, a) * GGX1(nDotV, a);
}

vec3 pem( float Roughness, vec3 R ){
	vec3 N = R;
	vec3 V = R;
	vec3 PrefilteredColor = vec3(0);
	const uint NumSamples = 1024;
	float TotalWeight = 0.0;

	for( uint i = 0; i < NumSamples; i++ ){
		vec2 Xi = hammersley( i, NumSamples );
		vec3 H = ImportanceSampleGGX( Xi, Roughness, N );
		vec3 L = 2 * dot( V, H ) * H - V;
		float NoL = saturate( dot( N, L ) );   
		if( NoL > 0 ){
			PrefilteredColor += texture(envMaps[0], L).rgb * NoL;
			TotalWeight += NoL;
		}
	}
	return PrefilteredColor / TotalWeight;
}
vec2 IntegrateBRDF( float Roughness, float NoV ){
	vec3 V;
	V.x = sqrt( 1.0f - NoV * NoV ); // sin
	V.y = 0;
	V.z = NoV;
	// cos
	float A = 0.0;
	float B = 0.0;
	const uint NumSamples = 32;
	for( uint i = 0; i < NumSamples; i++ ){
		vec2 Xi = hammersley( i, NumSamples );
		vec3 H = ImportanceSampleGGX( Xi, Roughness, vec3(0,0,1) );
		vec3 L = 2.0 * dot( V, H ) * H - V;
		float NoL = saturate( L.z );
		float NoH = saturate( H.z );
		float VoH = saturate( dot( V, H ) );
		if( NoL > 0 ){
			float G = G_Smith( Roughness, NoV, NoL );
			float G_Vis = G * VoH / (NoH * NoV);
			float Fc = pow( 1.0 - VoH, 5.0 );
			A += (1.0 - Fc) * G_Vis;
			B += Fc * G_Vis;
		}
	}
	return vec2( A, B ) / NumSamples;
}


float correctParallax(int ind, vec3 vector, out float factor){
	mat4 tmat = inverse(envMapMatrices[ind]);
	//samplerCube env = getEnvMap(ind);
	//vec3 position = envMapPosition[ind];

	vec3 rayLs = (tmat * vec4(vector, 0)).xyz;
    vec3 positionLs = (tmat * vec4(globalData.fragPosition, 1)).xyz;

	vec3 localV = abs(positionLs);
	factor = max(localV.x, max(localV.y, localV.z));

	vec3 firstPlane   = (1.0 - positionLs) / rayLs;
	vec3 secondPlane  = (-1.0 - positionLs) / rayLs;
	vec3 furthestPlane = max(firstPlane, secondPlane);

	return min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
}

vec3 getCubemapVector(int ind, vec3 vector, out float factor, out float dist){
	dist = correctParallax(ind, vector, factor);
	vec3 mapCenter = envMapPosition[ind];
	mat4 tmat = inverse(envMapMatrices[ind]);

	vec3 intersectionPos = mix(
        globalData.fragPosition + vector * dist,
        mapCenter + vector,
        globalData.roughness);

    return (tmat * vec4(intersectionPos, 1)).xyz;
}

vec3 getReflectionVector(int ind, out float factor, out float dist){
	//vec3 mapCenter = envMapPosition[ind];
	//mat4 tmat = envMapMatrices[ind];
	
	vec3 viewVec = normalize(globalData.fragPosition - globalData.viewPosition);
	vec3 reflected = reflect(viewVec, globalData.normal);

	return getCubemapVector(ind, reflected, factor, dist);
}

vec3 getDiffuseVector(int ind){
	vec3 mapCenter = envMapPosition[ind];
	mat4 tmat = envMapMatrices[ind];

	vec4 transformed = tmat * vec4(fma(globalData.normal, vec3(1e5),
		mapCenter), 1);

	return transformed.xyz;
}

void applyCubemap(int ind, out vec3 diffuse, out vec3 specular, inout float totalWeight, inout float totalBlend){
	float roughness = globalData.roughness;
	vec3 mapCenter = envMapPosition[ind];
	float mapRadius = 3.0;

	float factor = 0.0;
    float mipmap = roughness * 10.0;
	float intersectionDistance = 1.0;

	//samplerCube cubemap = envMaps[0]; //getEnvMap(ind);

	vec3 direction = getReflectionVector(ind, factor, intersectionDistance);
	vec3 diffuseDirection = getDiffuseVector(ind);

	vec3 vectorToSource = normalize(mapCenter - globalData.fragPosition);

	float blend = 1.0;

	float localDistance = intersectionDistance / mapRadius;

	if(ind == 0)
		specular = textureLod(envMaps[0], normalize(direction), clamp(mipmap, 0.0, 7.0)).rgb;
	if(ind == 1)
		specular = textureLod(envMaps[1], normalize(direction), clamp(mipmap, 0.0, 7.0)).rgb;
	if(ind == 2)
		specular = textureLod(envMaps[2], normalize(direction), clamp(mipmap, 0.0, 7.0)).rgb;

    diffuse = /*textureLod(cubemap,
        vec4(diffuse_direction), 0);*/ getIrradiance(normalize(diffuseDirection));

	float weight = exp(-0.05 * mapRadius);
    weight *= factor >= 1.0 ? 0.0 : 1.0;

    // Apply clip factors
    specular *= weight * blend;
    diffuse *= weight * blend;

    totalWeight += weight * blend;
    totalBlend += blend;

}

void env(out vec3 diffuse, out vec3 specular){

    float totalBlend = 0;
    float totalWeight = 0;
	vec3 totalDiffuse = vec3(0);
    vec3 totalSpecular = vec3(0);

	int processed_probes = 0;
	for (int i = 0; i < 1; ++i) {
		vec3 diff, spec;

		processed_probes += 1;
		applyCubemap(i, diff, spec, totalWeight, totalBlend);
		totalDiffuse += diff;
		totalSpecular += spec;
	}

	float scale = 1.0 / max(1e-9, totalWeight) * min(1.0, totalBlend);

	diffuse = totalDiffuse * scale;
	specular = totalSpecular * scale;

	//applyCubemap(0, diffuse, specular, totalWeight, totalWeight);
}

vec3 ApproximateSpecularIBL(vec3 V){
	float roughness = globalData.roughness;
	vec3 N = globalData.normal;
	float NoV = saturate( dot( N, V ) );
	vec3 R = normalize(parallaxOBB(0, V));
	float mip = clamp(roughness * 7.0, 0.0, 6.0);
	vec3 PrefilteredColor = textureLod( envMaps[0], R, mip).rgb;
	vec2 envBRDF = IntegrateBRDF( roughness, NoV );

	return PrefilteredColor * ( globalData.color * envBRDF.x + envBRDF.y );
}

void main(){

	vec4 al = texture(albedos, cords);
	vec4 nr = texture(normals, cords);
	vec4 ps = texture(positions, cords);
	vec4 bb = texture(boundingBoxes, cords);
	vec4 ts = texture(types, cords);
	vec4 dp = texture(depths, cords);
	vec4 fn = texture(finals, cords);

	vec3 albedo = al.rgb;
	vec3 normal = nr.xyz;
	vec3 position = ps.xyz;
	vec3 bBox = bb.xyz;
	float roughness = nr.w;
	float metalness = ps.w;
	float depth = dp.r;
	float type = ts.r;
	int s = state;

	globalData.normal = normalize(normal);
	globalData.fragPosition = position;
	globalData.metallness = metalness;
	globalData.roughness = roughness;
	globalData.color = albedo;
	globalData.viewPosition = viewPosition;

	int k = envMapCount;
	vec3 p = envMapPosition[0];

	vec3 viewVec = viewPosition - position;
	vec3 N = normal;
	vec3 V = normalize(viewVec);

	//vec4 refl = vec4(SpecularIBL( albedo, roughness, N, V, vec3(0) ), 1.0); 
	//vec4( PrefilterEnvMap( 0.2, N, V ), 1.0); 
	//vec4(texture( envMaps[0], locVec ).rgb, 1.0);
	//vec4 alb = vec4(albedo, 1.0);

	//FragColor = refl; ///mix( refl, alb, roughness + 0.5 );
	//vec4 diff = vec4(albedo * getIrradiance(), 1.0);

	//vec4 refl = vec4(ApproximateSpecularIBL( albedo, roughness, N, V ), 1.0);
	
	//FragColor = vec4(blendMaterial(vec3(diff), vec3(refl), albedo, metalness), 1.0);
	
	//vec3 refVec = normalize(parallaxReflection(V));
	//vec4 refl = vec4(pem(roughness, normalize(parallaxReflection(V))), 1.0);

	//FragColor = vec4(1.0, 0.0, 0.0, 0.0);

	//FragColor = vec4(albedo * textureLod( envMaps[0], refVec * vec3(1.0, 1.0, 1.0), roughness * 6.0 ).rgb, 1.0);

	//vec3 refVec2 = normalize(parallaxOBB(0, V));

	//float mip = clamp(roughness * 8.0, 0.0, 6.0);

	vec4 spec = vec4(ApproximateSpecularIBL(V), 1.0);
	vec4 diff = vec4(getIrradiance() * albedo / PI, 1.0);
	  //vec4( albedo * textureLod( envMaps[0], refVec2 * vec3(1.0, 1.0, 1.0), mip ).rgb, 1.0);

	FragColor = diff + spec; //mix( diff, spec, 0.5 );

	if(type == 2.0){
		FragColor = vec4(albedo, 1.0);
	}

	//vec3 diff, spec = vec3(0.0);

	//env(diff, spec);

	//FragColor = vec4(albedo * diff + albedo * spec, 1.0);
}
