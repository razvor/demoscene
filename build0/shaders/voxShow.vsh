#version 440

//layout(location=0) in vec3 pos;

uniform int gridSize;

out vec3 vPos;

void main(){
	//gl_Position = vec4(1.0); //vec4(pos, 1.0);

	vec3 pos;
	pos.x = gl_VertexID % gridSize; //i
	pos.z = (gl_VertexID / gridSize) % gridSize; //j
	pos.y = gl_VertexID / (gridSize*gridSize); //k

	gl_Position = vec4(pos, 1.0);
	vPos = pos;
}
