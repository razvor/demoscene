#version 440

uniform sampler2D albedos;
uniform sampler2D normals;
uniform sampler2D positions;
uniform sampler2D types;
uniform sampler2D boundingBoxes;
uniform sampler2D depths;
uniform vec3 viewPosition;
uniform int state;
uniform int gridSize;
uniform int voxelWorldSize;

//uniform layout(binding=5, rgba8) image3D voxelTexture0;
uniform sampler3D voxelTexture;
layout(binding=0, r32ui) uniform uimageBuffer octreeChild;
layout(binding=1, r32ui) uniform uimageBuffer octreeDiffuse;

in vec2 cords;

struct PointLight{
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float constant;
	float linear;
	float quadratic;
};

struct SphereLight{
	vec3 position;
	vec3 color;
	float intensity;
	float radius;
};

struct RectangleLight{
	vec3 position;
	vec3 color;
	float width;
	float height;
	float intensity;
	mat4 matrix;
};

uniform PointLight pointLights[10];
uniform SphereLight sphereLights[10];
uniform RectangleLight rectangleLights[10];
uniform int pointLightCount;
uniform int sphereLightCount;
uniform int rectangleLightCount;

layout(location=0) out vec4 FragColor;

#define SQRT2 1.414213

#define M_PI 3.1428

float saturate(float v){
	return min(1.0, max(0.0, v));
}

float smoothDistanceAtt(float squaredDistance, float invSqrAttRadius){
	float factor = squaredDistance * invSqrAttRadius;
	float smoothFactor = saturate(1.0 - factor * factor);
	return smoothFactor * smoothFactor;
}

float getDistanceAtt(float sqrDist, float invSqrAttRadius){
	float attenuation = 1.0 / max( sqrDist , 0.0001);
	attenuation *= smoothDistanceAtt(sqrDist, invSqrAttRadius);
	return attenuation;
}

vec4 convRGBA8ToVec4( in uint val ){
    return vec4( float( (val&0x000000FF) ), float( (val&0x0000FF00)>>8U),
	             float( (val&0x00FF0000)>>16U), float( (val&0xFF000000)>>24U) );
}

bool nodeOccupied( in uvec3 loc, int level, out int leafIdx ){
    int voxelDim = gridSize;
	uvec3 umin = uvec3(0,0,0);
    bool bOccupied = true;
	uint idx = 0;
	uint subnode;
	uvec3 offset;
	int depth;

	int levels = level;
	
    for( depth = 0; depth <= levels; ++depth ){
	    leafIdx = int(idx);
	    idx = imageLoad( octreeChild, int(idx) ).r;
		if( (idx & 0x80000000) == 0 ){
		    bOccupied= false;
		    break;
		}
		else if( depth == levels ){
		    break;
		}
		idx &= 0x7FFFFFFF;  //mask out flag bit to get child pointer
		if( idx == 0 ){
		    bOccupied = false;
		    break;
		}
		voxelDim /= 2;

		subnode = (loc.x > voxelDim + umin.x - 1) ? 1 : 0;
		subnode += (loc.y > voxelDim + umin.y - 1) ? 2 : 0;
		subnode += (loc.z > voxelDim + umin.z - 1) ? 4 : 0;
	    idx += int(subnode);
	    umin.x += voxelDim * ((loc.x > voxelDim + umin.x - 1) ? 1 : 0);
	    umin.y += voxelDim * ((loc.y > voxelDim + umin.y - 1) ? 1 : 0);
	    umin.z += voxelDim * ((loc.z > voxelDim + umin.z - 1) ? 1 : 0);
	}
	return bOccupied;
}

bool SVONode(uvec3 loc, int level, out int outInd ){
	uint node = 0, childInd = 0;
	uint subnode;
	uvec3 umin = uvec3(0);
	int voxelDim = gridSize;
	bool available = true;

	//node = imageLoad(octreeChild, int(childInd)).r;

	level = level - 1;

    for( int i = 0; i <= level; ++i ){

		outInd = int(childInd);
		
	    childInd = imageLoad( octreeChild, int(childInd) ).r;
		if( (childInd & 0x80000000) == 0 ){
		    available = false;
		    break;
		}
		else if( i == level ){
		    break;
		}

		childInd &= 0x7FFFFFFF;  //mask out flag bit to get child pointer
		if( childInd == 0 ){
		    available = false;
		    break;
		}
		voxelDim /= 2;

		subnode = (loc.x > voxelDim + umin.x - 1) ? 1 : 0;
		subnode += (loc.y > voxelDim + umin.y - 1) ? 2 : 0;
		subnode += (loc.z > voxelDim + umin.z - 1) ? 4 : 0;
	    childInd += int(subnode);
	    umin.x += voxelDim * ((loc.x > voxelDim + umin.x - 1) ? 1 : 0);
	    umin.y += voxelDim * ((loc.y > voxelDim + umin.y - 1) ? 1 : 0);
	    umin.z += voxelDim * ((loc.z > voxelDim + umin.z - 1) ? 1 : 0);
	}

	return available;

	/*for(int i=0; i<=level; ++i){
		if((node & 0x80000000) == 0){
			available = false;
			break;
		}else if(i == level){
			break;
		}

		voxelDim /= 2;

		childInd = (node & 0x7FFFFFFF);
		
		subnode = (loc.x > umin.x + voxelDim + 1) ? 1 : 0;
		subnode += (loc.y > umin.y + voxelDim + 1) ? 2 : 0;
		subnode += (loc.z > umin.z + voxelDim + 1) ? 4 : 0;

	    umin.x += voxelDim * ((loc.x > voxelDim + umin.x - 1) ? 1 : 0);
	    umin.y += voxelDim * ((loc.y > voxelDim + umin.y - 1) ? 1 : 0);
	    umin.z += voxelDim * ((loc.z > voxelDim + umin.z - 1) ? 1 : 0);

		node = imageLoad( octreeChild, int(childInd) ).r;
	}

	ind = int(childInd);

	return available;*/
}

vec4 SVOLod(in layout(r32ui) uimageBuffer buf, in vec4 pos){
	
	vec3 fLoc = float(gridSize) * (vec3(pos)+1.0)/2.0;
	uvec3 loc1 = uvec3( floor(fLoc) );
	uvec3 loc2 = uvec3( ceil(fLoc) );
	uvec3 loc = uvec3( float(gridSize) * (vec3(pos)+1.0)/2.0 );
	float lev = float(log2(gridSize)) - pos.w;
	int minLevel = int(floor(lev));
	int maxLevel = int(ceil(lev));
	
	int minInd, minInd1, minInd2, 
		maxInd, maxInd1, maxInd2;
	vec4 minColor = vec4(0), minColor1 = vec4(0), minColor2 = vec4(0),
		 maxColor = vec4(0), maxColor1 = vec4(0), maxColor2 = vec4(0);
	vec4 res;
	
	/*if(nodeOccupied(loc, minLevel, minInd)) minColor = convRGBA8ToVec4(imageLoad(buf, int(minInd)).r).rgba / 255.0;
	if(nodeOccupied(loc, maxLevel, maxInd)) maxColor = convRGBA8ToVec4(imageLoad(buf, maxInd).r).rgba / 255.0;*/

	const float SQRT3 = 1.732;

	float mix12 = 0.5; //length(loc2 - vec3(loc)) / SQRT3;

	if(nodeOccupied(loc1, minLevel, minInd)) minColor1 = convRGBA8ToVec4(imageLoad(buf, int(minInd)).r).rgba / 255.0;
	if(nodeOccupied(loc2, minLevel, minInd)) minColor2 = convRGBA8ToVec4(imageLoad(buf, minInd).r).rgba / 255.0;
	minColor = mix( minColor1, minColor2, mix12 );
	if(nodeOccupied(loc1, maxLevel, minInd)) maxColor1 = convRGBA8ToVec4(imageLoad(buf, int(minInd)).r).rgba / 255.0;
	if(nodeOccupied(loc2, maxLevel, minInd)) maxColor2 = convRGBA8ToVec4(imageLoad(buf, minInd).r).rgba / 255.0;
	maxColor = mix( maxColor1, maxColor2, mix12 );

	res = mix( minColor, maxColor, 1.0 - (lev - float(minLevel)) );

	return res;
}

vec3 scaleAndBias(const vec3 p) { return 0.5f * p + vec3(0.5f); }
bool isInsideCube(const vec3 p, float e) { return abs(p.x) < 1 + e && abs(p.y) < 1 + e && abs(p.z) < 1 + e; }

vec3 traceDiffuseCone(vec3 from, vec3 direction, vec3 normal){

	float VOXEL_SIZE = 1.0 / float(gridSize);
	float MAX_DISTANCE = 3.0;
	float OFFSET = 8 * VOXEL_SIZE;
	float STEP = VOXEL_SIZE;
	const vec3 N_OFFSET = normal * (1 + 4 * 1.0 / SQRT2) * VOXEL_SIZE;
	const float CONE_SPREAD = 0.0325;
	
	from /= (5.35);
	from += vec3(0, 0.0355, 0);
	from += N_OFFSET;
	direction = normalize(direction);
	vec4 acc = vec4(0.0f);
	float dist = OFFSET;

	while(dist < SQRT2 && acc.a < 1){ 
		vec3 c = from + dist * direction;
		vec3 e = scaleAndBias(c); 
		if(!isInsideCube(e, 0.1)) break;
		float l = (1 + CONE_SPREAD * dist / VOXEL_SIZE);
		float level = log2(l);
		float ll = (level + 1) * (level + 1);
		vec4 voxel = textureLod(voxelTexture, e, min(7.0, ll));
		if(voxel.a <= 1.0) acc += 0.075 * ll * voxel * pow(1 - voxel.a, 2);
		dist += ll * VOXEL_SIZE * 2.0;
	}

	acc = pow(acc, vec4(0.8));

	return acc.rgb; 
	//* abs(dot(normal, direction));

}

float traceAOCone(vec3 from, vec3 direction, vec3 normal){

	float VOXEL_SIZE = 1.0 / float(gridSize);
	float MAX_DISTANCE = 3.0;
	float OFFSET = 2.0 * VOXEL_SIZE;
	float STEP = VOXEL_SIZE;
	const vec3 N_OFFSET = normal * (1 + 4 * 1.0 / SQRT2) * VOXEL_SIZE;
	const float CONE_SPREAD = 0.0325 * 2.0;

	//from /= (5.5);
	from /= 5.1;
	from += vec3(0, 0.045/2.0, 0);
	//from += normal * OFFSET;
	direction = normalize(direction);
	float dist = OFFSET;

	float ao = 0;
	float fo = 1500.0;
	while(dist < SQRT2 * 0.7 && ao < 1.0){ 
		vec3 c = from + dist * direction;
		vec3 e = scaleAndBias(c); 
		if(!isInsideCube(e, 0.1)) break;
		float l = (1 + 1.0 * CONE_SPREAD * dist / VOXEL_SIZE);
		float level = log2(l);
		float ll = (level + 1) * (level + 1);
		float va = textureLod(voxelTexture, e, min(7.0, ll)).a;
		va *= 0.7;
		ao += (1-ao)*va/(1+dist*fo);
		dist += ll * VOXEL_SIZE * 1.0;
	}

	float SHADOW_COEF = 0.08;
	float FALLOFF = 2.0;

	/*while(dist < SQRT2 && ao < 1.0){
		vec3 c = from + dist * direction;
		if(!isInsideCube(c, 0.1)) break;
		vec3 e = scaleAndBias(c); 

		float l = pow(dist, FALLOFF); // Experimenting with inverse square falloff for shadows.
		float s1 = SHADOW_COEF * 0.062 * textureLod(voxelTexture, e, 1 + 0.75 * l).a;
		float s2 = SHADOW_COEF * 0.3 * 0.135 * textureLod(voxelTexture, e, 4.5 * l).a;
		float s = s1 + s2;
		ao += (1.0 - ao) * s / (1+dist*3.0);
		dist += 0.9 * VOXEL_SIZE * (1 + 0.2 * l) * 2.0;
	}*/
	
	/*float aperture = 0.25;
	float falloff = 0.5 * 1.0;

	while(dist < SQRT2 && ao < 1.0){
		vec3 c = from + dist * direction;
		if(!isInsideCube(c, 0.1)) break;
		vec3 e = scaleAndBias(c); 

		float diameter = 1.0 + 2.0f * aperture * dist;
        float mipLevel = log2(2.0 * diameter / VOXEL_SIZE);
		float va = textureLod(voxelTexture, e, min(7.0, mipLevel)).a;

		ao = (1.0 - ao) * va / (1.0 + dist * falloff);

		dist += diameter * 0.5;
	}*/

	//ao *= dot(normal, direction);

	//ao = 1.0 - ao;

	return ao;
}

vec3 traceSpecularCone(vec3 from, vec3 direction, vec3 normal, float roughness){

	float VOXEL_SIZE = 1.0 / float(gridSize);
	float MAX_DISTANCE = 3.0;
	float OFFSET = 8 * VOXEL_SIZE;
	float STEP = VOXEL_SIZE;
	const vec3 N_OFFSET = normal * (1 + 4 * 1.0 / SQRT2) * VOXEL_SIZE;
	const float CONE_SPREAD = 0.0225;
	
	from += vec3(0, 0, 0);
	from /= (5.0 + 0.01);
	from += N_OFFSET;
	roughness *= 6.0;
	direction = normalize(direction);
	vec4 acc = vec4(0.0f);
	float dist = OFFSET;

	while(dist < SQRT2 && acc.a < 1){ 
		vec3 c = from + dist * direction;
		vec3 e = scaleAndBias(c); 
		if(!isInsideCube(e, 0.1)) break;

		float level = 0.1 * roughness * log2(1 + dist / VOXEL_SIZE);
		vec4 voxel = textureLod(voxelTexture, e, min(level, 7.0));
		float f = 1 - acc.a;
		acc.rgb += 0.25 * (1 + roughness) * voxel.rgb * voxel.a * f;
		acc.a += 0.25 * voxel.a * f;
		dist += STEP * (1.0f + 0.125f * level);
	}


	return acc.rgb;
}

vec3 basisVec(vec3 u){
	u = normalize(u);
	vec3 v = vec3(0.99146, 0.11664, 0.05832); // Pick any normalized vector.
	return abs(dot(u, v)) > 0.99999f ? cross(u, vec3(0, 1, 0)) : cross(u, v);
}

float radicalInverse_VdC(uint bits) {
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

vec2 hammersley(uint i, uint N) {
	return vec2( float(i) / float(N), radicalInverse_VdC(i) );
}

vec4 inderectIllumination(vec3 position, vec3 normal){
	vec3 ort1 = basisVec(normal);
	vec3 ort2 = normalize(cross(normal, ort1));
	vec3 cor1 = (ort1 + ort2) / 2.0;
	vec3 cor2 = (ort1 - ort2) / 2.0;

	vec3 ang1 = (normal + ort1) / 2.0;
	vec3 ang2 = (normal + ort2) / 2.0;
	vec3 ang3 = (normal - ort1) / 2.0;
	vec3 ang4 = (normal - ort2) / 2.0;

	vec3 s1 = (normal + cor1) / 2.0;
	vec3 s2 = (normal + cor2) / 2.0;
	vec3 s3 = (normal - cor1) / 2.0;
	vec3 s4 = (normal - cor2) / 2.0;

	vec4 res = vec4(0);

	res += traceDiffuseCone(position, normal, normal);

	res += traceDiffuseCone(position, s1, normal);
	res += traceDiffuseCone(position, s2, normal);
	res += traceDiffuseCone(position, s3, normal);
	res += traceDiffuseCone(position, s4, normal);

	res += traceDiffuseCone(position, ang1, normal);
	res += traceDiffuseCone(position, ang2, normal);
	res += traceDiffuseCone(position, ang3, normal);
	res += traceDiffuseCone(position, ang4, normal);

	//res /= 3.14 * 9.0;

	//res = pow(res, vec4(1.5));

	//res /= 3.1428;

	return res;

}

float ambientOcclusion(vec3 position, vec3 normal){
	vec3 ort1 = basisVec(normal);
	vec3 ort2 = normalize(cross(normal, ort1));
	vec3 cor1 = (ort1 + ort2) / 2.0;
	vec3 cor2 = (ort1 - ort2) / 2.0;

	vec3 ang1 = (normal + ort1) / 2.0;
	vec3 ang2 = (normal + ort2) / 2.0;
	vec3 ang3 = (normal - ort1) / 2.0;
	vec3 ang4 = (normal - ort2) / 2.0;

	vec3 s1 = (normal + cor1) / 2.0;
	vec3 s2 = (normal + cor2) / 2.0;
	vec3 s3 = (normal - cor1) / 2.0;
	vec3 s4 = (normal - cor2) / 2.0;

	float res = 0; //vec4(0);

	res += traceAOCone(position, normal, normal);

	res += traceAOCone(position, s1, normal);
	res += traceAOCone(position, s2, normal);
	res += traceAOCone(position, s3, normal);
	res += traceAOCone(position, s4, normal);

	res += traceAOCone(position, ang1, normal);
	res += traceAOCone(position, ang2, normal);
	res += traceAOCone(position, ang3, normal);
	res += traceAOCone(position, ang4, normal);

	res = saturate(1.0 - res) * 1.8;

	return res;
}

vec4 reflectionIllumination(vec3 position, vec3 normal, vec3 view, float roughness){
	//vec3 ort1 = basisVec(normal);
	//vec3 ort2 = normalize(cross(normal, ort1));

	vec3 dir = reflect(-view, normal);

	return vec4(traceSpecularCone(position, dir, normal, roughness), 1.0);
}

vec3 hardShadows(vec3 position, vec3 normal){
	vec3 ort1 = basisVec(normal);
	vec3 ort2 = normalize(cross(normal, ort1));

	vec3 cor1 = (ort1 + ort2) / 2.0;
	vec3 cor2 = (ort1 - ort2) / 2.0;
	
	float VOXEL_SIZE = 1.0 / float(gridSize);
	float FALLOFF = 0.6;
	float SHADOW_COEF = 0.5;

	vec3 res = vec3(0.0);
	float ao = 0.0;

	position /= 5.0;
	position += normal * VOXEL_SIZE * 2.0;

	float divideCoef = float(rectangleLightCount);

	float sumLumiance = 0.0001;

	for(int i=0; i<rectangleLightCount; ++i){
		RectangleLight light = rectangleLights[i];
		sumLumiance += light.intensity / (light.width * light.height * M_PI);
	}

	for(int i=0; i<sphereLightCount; ++i){
		//trace 1 ray(while better solution not found)
		SphereLight light = sphereLights[i];
		sumLumiance += light.intensity / (2.0 * light.radius * M_PI);
	}

	for(int i=0; i<rectangleLightCount; ++i){
		//trace 1 ray(while better solution not found)
		RectangleLight light = rectangleLights[i];

		float maxDist = length(light.position/5.0 - position) - 16 * VOXEL_SIZE;
		float lumiance = light.intensity / (light.width * light.height * M_PI);
		float lumCoef = 1.0 / sumLumiance * lumiance;

		const int NUM_SAMPLES = 3;
		const int SAMPLE_SCALE = 2;
		for(int j=0; j<NUM_SAMPLES; ++j){
			vec2 pn = hammersley(j*SAMPLE_SCALE, NUM_SAMPLES*SAMPLE_SCALE);
			pn -= 0.5;
			vec3 pp = light.position + vec3(light.matrix * vec4(light.width * pn.x, 0, light.height * pn.y, 1.0));
			vec3 endPos = pp / 5.0;
			vec3 dir = normalize(endPos - position);

			float dist = VOXEL_SIZE * 3.0;
			while(dist < maxDist){
				vec3 c = position + dist * dir;
				if(!isInsideCube(c, 0.1)) break;
				vec3 e = scaleAndBias(c); 

				float l = pow(dist, FALLOFF); // Experimenting with inverse square falloff for shadows.
				float a1 = textureLod(voxelTexture, e, 1 + 0.75 * l).a;
				float a2 = textureLod(voxelTexture, e, 1 + 0.75 * l).a;
				float s1 = SHADOW_COEF * 0.062 * textureLod(voxelTexture, e, 1 + 0.75 * l).a / float(NUM_SAMPLES) * lumCoef;
				float s2 = SHADOW_COEF * 0.135 * textureLod(voxelTexture, e, 4.5 * l).a / float(NUM_SAMPLES) * lumCoef;
				float s = s1 + s2;
				ao += (1.0 - ao) * s;
				dist += 0.9 * VOXEL_SIZE * (1 + 0.05 * l);
			}
		}
	}

	for(int i=0; i<sphereLightCount; ++i){
		//trace 1 ray(while better solution not found)
		SphereLight light = sphereLights[i];
		vec3 dir = normalize(light.position/5.0 - position);

		float maxDist = length(light.position/5.0 - position) - 16 * VOXEL_SIZE;
		float dist = VOXEL_SIZE * 3.0;

		float lumiance = light.intensity / (2.0 * light.radius * M_PI);
		float lumCoef = 1.0 / sumLumiance * lumiance;

		while(dist < maxDist){
			vec3 c = position + dist * dir;
			if(!isInsideCube(c, 0.1)) break;
			vec3 e = scaleAndBias(c); 

			float l = pow(dist, FALLOFF); // Experimenting with inverse square falloff for shadows.
			float a1 = textureLod(voxelTexture, e, 1 + 0.75 * l).a;
			float a2 = textureLod(voxelTexture, e, 1 + 0.75 * l).a;
			float s1 = SHADOW_COEF * 0.062 * textureLod(voxelTexture, e, 1 + 0.75 * l).a * lumCoef;
			float s2 = SHADOW_COEF * 0.135 * textureLod(voxelTexture, e, 4.5 * l).a * lumCoef;
			float s = s1 + s2;
			ao += (1.0 - ao) * s;
			dist += 0.9 * VOXEL_SIZE * (1 + 0.05 * l);
		}

	}

	//ao /= 1.1;

	ao = 1.0 - ao;
	//ao = ao > 0.15 ? ao : ao / 5.0;
	//if(ao > 0.05 && ao < 0.2) ao /= 3.0;
	ao = saturate(ao);

	res = vec3(pow(ao, 1.0));

	return res;
}

vec3 rayPlaneIntersect(vec3 planeNormal, vec3 planePosition, vec3 rayDir, vec3 rayPosition){
	float k = (dot(planeNormal, planePosition - rayPosition)) / dot(planeNormal, rayDir);
	return rayPosition + rayDir * k;
}

vec3 closestPoint(vec3 point, vec3 lightPos, vec3 lightUp, vec3 lightRight, float halfWidth, float halfHeight){
	vec3 dir = point - lightPos;
	vec2 dist2D = vec2( dot(dir, lightRight), dot(dir, lightUp));
	vec2 rectHalfSize = vec2(halfWidth, halfHeight);
	dist2D = clamp( dist2D, -rectHalfSize, rectHalfSize );
	return lightPos + dist2D.x * lightRight + dist2D.y * lightUp;
}

float rectangleSolidAngle(vec3 worldPos, vec3 p0, vec3 p1, vec3 p2, vec3 p3){
	vec3 v0 = p0 - worldPos;
	vec3 v1 = p1 - worldPos;
	vec3 v2 = p2 - worldPos;
	vec3 v3 = p3 - worldPos;
	vec3 n0 = normalize(cross(v0 , v1));
	vec3 n1 = normalize(cross(v1 , v2));
	vec3 n2 = normalize(cross(v2 , v3));
	vec3 n3 = normalize(cross(v3 , v0));

	float g0 = acos(dot(-n0 , n1));
	float g1 = acos(dot(-n1 , n2));
	float g2 = acos(dot(-n2 , n3));
	float g3 = acos(dot(-n3 , n0));

	return g0 + g1 + g2 + g3 - 2 * M_PI ;
}
vec3 computeRectangleLight2(vec3 fragPos, vec3 normal, vec3 albedo, RectangleLight light){

	float lumiance = light.intensity / (light.width * light.height * M_PI);
	vec3 lightNormal = normalize(vec3(light.matrix * vec4(0, 1, 0, 1)));
	vec3 lightUp = normalize(vec3(light.matrix * vec4(0, 0, 1, 1)));
	vec3 lightRight = normalize(vec3(light.matrix * vec4(1, 0, 0, 1)));
	float halfWidth = light.width / 2;
	float halfHeight = light.height / 2;
	float lightDist = length(light.position - fragPos);
	
	if(dot(fragPos - light.position, lightNormal) > 0){
		float clampCosAngle = 0.001 + saturate(dot(normal, lightNormal));
		vec3 d0 = normalize(-lightNormal + normal * clampCosAngle);
		vec3 d1 = normalize(normal - normal * clampCosAngle);
		vec3 dh = normalize(d0 + d1);
		vec3 ph = rayPlaneIntersect(lightNormal, light.position, dh, fragPos);
		ph = closestPoint(ph, light.position, lightUp, lightRight, halfWidth, halfHeight);

		vec3 pph = normalize(ph - fragPos);

		vec3 p0 = light.position + lightRight * halfWidth + lightUp * halfHeight;
		vec3 p1 = light.position + lightRight * halfWidth - lightUp * halfHeight;
		vec3 p2 = light.position - lightRight * halfWidth - lightUp * halfHeight;
		vec3 p3 = light.position - lightRight * halfWidth + lightUp * halfHeight;

		float solidAngle = rectangleSolidAngle(fragPos, p0, p1, p2, p3);

		float illumiance = solidAngle * max(0.0, dot(normal, pph)) * lumiance;
		illumiance *= getDistanceAtt(lightDist * lightDist, 1.0 / light.intensity );

		return albedo * vec4(light.color, 1.0) * illumiance;

	}

	return vec3(0);
}

vec4 computeSphereLight(vec3 fragPos, vec3 normal, vec4 albedo, SphereLight light){

	vec4 res;

	float lumiance = light.intensity / (2.0 * M_PI * light.radius);

	vec3 lightVec = (light.position - fragPos);
	float lightDist = length(lightVec);
	vec3 lightDir = lightVec / lightDist;
	float sqrLightDist = lightDist * lightDist;
	float invSqrAtt = 1.0 / max(sqrLightDist, 0.001);

	float attenuation = 1.0 / pow( lightDist / light.radius + 1.0, 1.2 ); 
	//saturate(light.radius / lightDist) / (sqrLightDist + 1.0); //pow(saturate(1.0 - pow(lightDist / light.radius, 4.0)), 2.0) / (sqrLightDist + 1.0);

	float Beta = acos(dot(normal, lightDir));
    float H = lightDist;
    float h = H / light.radius;
    float x = sqrt(h * h - 1.0);
    float y = -x / tan(Beta);

    float illuminance = 0;
    if (h * cos(Beta) > 1){
		illuminance = cos(Beta) / (h * h);
    }
    else{
		illuminance = (1.0 / (M_PI * h * h)) *
		(cos(Beta) * acos(y) - x * sin(Beta) * sqrt(1.0 - y * y)) +
		atan(sin(Beta) * sqrt(1.0 - y * y) / x) / M_PI;
    }
    illuminance *= M_PI;

	res = vec4(light.color, 1.0) * albedo * lumiance * max(illuminance * attenuation, 0.0);

	return res;
}

vec3 lightTex(vec2 c){
	if(texture(types, c).y == 1.0){
		vec3 a = texture(albedos, c).rgb;
		return a;
	}else{
		return vec3(0);
	}
}

vec3 bloom(vec4 type){
	//if(type.y != 1.0) return vec3(0);
	//float weights[] = float[](0, 0, 0, 0, 0, 0.000003, 0.000229, 0.005977, 0.060598, 0.24173, 0.382925, 0.24173, 0.060598, 0.005977, 0.000229, 0.000003, 0, 0, 0, 0,	0);
	float weights[5];
	// = float[] (0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);
	weights[0] = 0.0702703f;
	weights[1] = 0.316216f;
	weights[2] = 0.227027f;
	weights[3] = 0.316216f;
	weights[4] = 0.0702703f;

	float offsetH[10], offsetW[10];
	offsetH[0] = -3.23077f;
	offsetH[1] = 0f;
	offsetH[2] = -1.38462f;
	offsetH[3] = 0f;
	offsetH[4] = 0f;
	offsetH[5] = 0f;
	offsetH[6] = 1.38462f;
	offsetH[7] = 0f;
	offsetH[8] = 3.23077f;
	offsetH[9] = 0f;

	offsetW[0] = 0f;
	offsetW[1] = -3.23077f;
	offsetW[2] = 0f;
	offsetW[3] = -1.38462f;
	offsetW[4] = 0f;
	offsetW[5] = 0f;
	offsetW[6] = 0f;
	offsetW[7] = 1.38462f;
	offsetW[8] = 0f;
	offsetW[9] = 3.23077f;

	vec2 texOffset = 1.0 / textureSize(albedos, 0) * 1.0;
	vec3 res = lightTex(cords).rgb * weights[0]; 

	for(int i=1; i<5; ++i){
		res += lightTex(cords + vec2(texOffset.x * i, 0.0)).rgb * weights[i];
		res += lightTex(cords - vec2(texOffset.x * i, 0.0)).rgb * weights[i];
		res += lightTex(cords + vec2(0.0, texOffset.y * i)).rgb * weights[i];
		res += lightTex(cords - vec2(0.0, texOffset.y * i)).rgb * weights[i];
	}

	return res * 1.3;
}

void main(){
	
	vec4 albedo = texture(albedos, cords);
	vec3 normal = normalize(texture(normals, cords).rgb);
	float roughness = texture(normals, cords).a;
	vec3 position = texture(positions, cords).rgb;
	float metallness = texture(positions, cords).a;
	vec4 type = texture(types, cords);
	vec4 boundingBox = texture(boundingBoxes, cords);
	float depth = texture(depths, cords).r;
	vec3 vp = viewPosition;
	int s = state;
	int vws = voxelWorldSize;

	vec3 view = normalize(viewPosition - position);

	if(type.y == 2.0){

		vec4 diffuse = vec4(0.4); //albedo * vec4(inderectIllumination(position, normal));
		//vec4 specular = albedo * vec4(reflectionIllumination(position, normal, view, roughness));
		float ao = ambientOcclusion(position, normal);
		
		for(int i=0; i<rectangleLightCount; ++i)
		   diffuse += vec4(computeRectangleLight2(position, normal, vec3(albedo), rectangleLights[i]), 1.0);

		for(int i=0; i<sphereLightCount; ++i)
			diffuse += computeSphereLight(position, normal, vec4(albedo), sphereLights[i]);
		
		//FragColor = mix(diffuse, specular, 0.0);

		//FragColor = albedo * 0.3;
		FragColor = diffuse;

		//FragColor = mix(diffuse, specular, 0.5);
		//FragColor = vec4(normal, 1.0);
		//FragColor *= (ao);
		//FragColor *= vec4(hardShadows(position, normal), 1.0);
		//FragColor = diffuse;
	}else{
		FragColor = vec4(albedo);
	}

	//FragColor += bloom(type);

	//FragColor = FragColor / (FragColor + 1.0);
	FragColor = pow(FragColor, vec4(1.0/2.2));

}