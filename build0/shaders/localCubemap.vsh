#version 330

layout(location=0) in vec3 aPosition;
layout(location=1) in vec3 aNormal;
layout(location=2) in vec2 aTexCord;
layout(location=3) in vec3 aTangent;
layout(location=4) in vec3 aBitangent;

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

out vec3 vPosition;
out vec3 vNormal;
out vec3 vTexCord;

void main(){
	vNormal = aNormal;
	vPosition = aPosition;
	vec2 vtc = aTexCord;
	vTexCord = aPosition; 
	//vec3((aPosition.xy + vec2(1.0)) / 2.0, 1.0);
	//vTexCord.y = -vTexCord.y;
	//vTexCord.x = -vTexCord.x;

	vec3 t, b;
	t = aTangent;
	b = aBitangent;

	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4( aPosition, 1.0 );
	//gl_Position.z = 0.0;
}
