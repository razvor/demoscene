#version 440

layout (triangles) in; 
layout (triangle_strip, max_vertices = 3) out;

in vec2 vTexCord[];
in vec3 vNormal[];
in vec3 vPosition[];

out vec2 vTexCordG;
out vec3 vNormalG;
out vec3 vPositionG;

uniform mat4 projX;
uniform mat4 projY;
uniform mat4 projZ;

void main(){
	for(int i=0; i<gl_in.length(); ++i){
		vTexCordG = vTexCord[i];
		vNormalG = vNormal[i];
		gl_Position = gl_in[i].gl_Position;
		EmitVertex();
	}

	EndPrimitive();
}
