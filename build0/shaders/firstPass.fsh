#version 410

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform vec3 viewPosition;
uniform vec3 boundingBox;
uniform int state;
uniform int type;
uniform vec3 color;
//uniform float direction;

uniform sampler2D albedo;
uniform sampler2D normals;
uniform sampler2D roughness;
uniform sampler2D metallness;

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCord;
in mat3 vTBN;

layout(location=0) out vec4 oAlbedo;
layout(location=1) out vec4 oNormal;
layout(location=2) out vec4 oPosition;
layout(location=3) out vec4 oBoundingBox;
layout(location=4) out vec4 oType;

vec4 getParabaloid(vec4 inp){
	float L = length(inp);
	//inp.z *= direction;
	inp = inp / L;
	inp.z += 1.0;
	inp.x /= inp.z;
	inp.y /= inp.z;

	inp.z = (L - 0.1) / (30.0);
	inp.w = 1;
	
	return inp;
}

void main(){
	vec3 vp = viewPosition;
	int s = state;

	vec3 pos = vec3( modelMatrix * vec4(vPosition, 1.0) );
	//vec3 spos = vec3(  );

	vec3 norm = texture(normals, vTexCord).rgb;
	norm = normalize(norm * 2.0 - 1.0);
	norm = vTBN * norm;

	//if((vTBN) == mat3(0.0))

	norm = normalize(mat3(transpose(inverse(modelMatrix))) * vNormal);
	norm = vec3(1.0);
	
	//norm = normalize(mat3(transpose(inverse(modelMatrix))) * vNormal);

	/*if(pos.z == 1.0)
	  pos = vec3(0);*/

	vec4 oPos = vec4(pos, 1.0);

	//oPos.w = texture(metallness, vTexCord).r;

	oAlbedo = vec4(texture(albedo, vTexCord));
	//oAlbedo = vec4(vec3(0.7, 0.8, 0.6), 1.0);
	if(type == 1) oAlbedo = vec4(color, 1.0);
	oNormal = vec4(norm, texture(roughness, vTexCord).r);
	oPosition = vec4(pos, texture(metallness, vTexCord).r);
	oType = vec4(1.0, type, 0.0, 0.0);
	oBoundingBox = vec4( boundingBox, 0.0 );

	//if(oAlbedo == vec4(0)) oAlbedo = vec4(0,1,1,1);

}
