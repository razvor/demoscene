#version 440

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform int type;

uniform sampler2D albedos;
uniform sampler2D normals;
uniform sampler2D positions;
uniform sampler2D depths;
uniform sampler2D types;

layout(location=0) out vec4 FragColor;

void main(){
	
	vec4 albedo = texture(albedos, vec2(0));
	
	FragColor = vec4(0.2);
}