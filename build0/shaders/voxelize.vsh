#version 440

layout(location=0) in vec3 aPosition;
layout(location=1) in vec3 aNormal;
layout(location=2) in vec2 aTexCord;
layout(location=3) in vec3 aTangent;
layout(location=4) in vec3 aBitangent;

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform int state;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCord;
out mat3 vTBN;

void main(){
	vNormal = aNormal;
	vPosition = aPosition;
	vTexCord = aTexCord;

	vec3 T = normalize(vec3(modelMatrix * vec4(aTangent,   0.0)));
	vec3 N = normalize(vec3(modelMatrix * vec4(aNormal,    0.0)));
	vec3 B = cross(N, T);
	vTBN = mat3(T, B, N);

	if(aTangent == vec3(0.0) && aBitangent == vec3(0.0)){
		vTBN = mat3(0.0);
	}

	//vec4 oPos = projectionMatrix * viewMatrix * modelMatrix * vec4( aPosition, 1.0 );
	vec4 oPos = modelMatrix * vec4( aPosition, 1.0 );

	gl_Position = oPos;
}
