#version 440

layout (triangles) in; 
layout (triangle_strip, max_vertices = 3) out;

in vec2 vTexCord[];
in vec3 vNormal[];
in vec3 vPosition[];

out vec2 vTexCordG;
out vec3 vNormalG;
out vec3 vPositionG;
out flat int vAxis;
out mat4 vProjAxis;

uniform mat4 projX;
uniform mat4 projY;
uniform mat4 projZ;

void main(){
	vec3 p1 = gl_in[0].gl_Position.xyz;
	vec3 p2 = gl_in[1].gl_Position.xyz;
	vec3 p3 = gl_in[2].gl_Position.xyz;

	vec3 N = cross( p2 - p1, p3 - p1 );
	N = normalize(N);
	N = abs(N);

	vAxis = 0;

	if(N.x > N.y && N.x > N.z){
		vAxis = 1;
		vProjAxis = projX;
	}else if(N.y > N.x && N.y > N.z){
		vAxis = 2;
		vProjAxis = projY;
	}else{
		vAxis = 3;	
		vProjAxis = projZ;
	}

	for(int i=0; i<gl_in.length(); ++i){
		vTexCordG = vTexCord[i];
		vNormalG = vNormal[i];
		gl_Position = vProjAxis * (gl_in[i].gl_Position + 0.02);
		vPositionG = vPosition[i];
		EmitVertex();

	}

	EndPrimitive();
	
}