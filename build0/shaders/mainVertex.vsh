#version 330

layout(location=0) in vec3 aPosition;
layout(location=1) in vec3 aNormal;
layout(location=2) in vec2 aTexCord;
layout(location=3) in vec3 aTangent;
layout(location=4) in vec3 aBitangent;

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform int state;
uniform float direction;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCord;
out mat3 vTBN;

vec4 getParabaloid(vec4 inp){
	float L = length(inp.xyz);
	inp.z *= direction;
	inp = inp / L;
	inp.z += 1.0;
	inp.x /= inp.z;
	inp.y /= inp.z;

	inp.z = (L - 0.1) / (30.0 - 0.1);
	inp.w = 1.0;
	
	return inp;
}

void main(){
	vNormal = aNormal;
	vPosition = aPosition;
	vTexCord = aTexCord;

	vec3 T = normalize(vec3(modelMatrix * vec4(aTangent,   0.0)));
	vec3 N = normalize(vec3(modelMatrix * vec4(aNormal,    0.0)));
	vec3 B = cross(N, T);
	vTBN = mat3(T, B, N);

	if(aTangent == vec3(0.0) && aBitangent == vec3(0.0)){
		vTBN = mat3(0.0);
	}

	vec4 oPos = projectionMatrix * viewMatrix * modelMatrix * vec4( aPosition, 1.0 );

	if(state == 1){
		//oPos = getParabaloid(oPos);
	}

	gl_Position = oPos;
}
