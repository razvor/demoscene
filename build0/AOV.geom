#version 440

layout (triangles) in; 
layout (triangle_strip, max_vertices = 3) out;

in vec3 vNormal[];
in vec3 vPosition[];

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

out flat vec4 sourceVertex0, sourceVertex1, sourceVertex2;
out flat vec3 vFaceNormal;
out flat vec3 m0, m1, m2;
out flat int isBase;
out flat float area;

float obscurance = 0.01;

vec3 edgeNormals[4];
vec4 p[3];

void emit(vec4 pos){
	gl_Position = vec4(pos);
	vFaceNormal = edgeNormals[3];
	sourceVertex0 = p[0]; //gl_in[0].gl_Position;
	sourceVertex1 = p[1]; //gl_in[1].gl_Position;
	sourceVertex2 = p[2]; //gl_in[2].gl_Position;
	m0 = edgeNormals[0];
	m1 = edgeNormals[1];
	m2 = edgeNormals[2];
	EmitVertex();
}

void main(){

	for(int i=0; i<3; ++i) p[i] = gl_in[i].gl_Position;
	mat4 screenMatrix = projectionMatrix * viewMatrix;

	/*vec3 faceNormal0 = -normalize((cross( p[1].xyz - p[0].xyz, p[2].xyz - p[0].xyz )));
	vec3 faceNormal = -(cross( p[1].xyz - p[0].xyz, p[2].xyz - p[0].xyz ));
	if(dot(faceNormal, vNormal[0]) > 0.0) faceNormal = -faceNormal;
	faceNormal = normalize(faceNormal);*/

	vec3 faceNormal = normalize((cross( p[1].xyz - p[0].xyz, p[2].xyz - p[0].xyz )));
	/*if(dot(faceNormal, vNormal[0]) > 0.0){ 
		faceNormal = -faceNormal; 
		p[2] = p[0];
		p[0] = p[2];
	}*/
	/*faceNormal = -faceNormal;

	vec3 m3 = faceNormal; 

	for(int i=0; i<3; ++i){
		edgeNormals[i] = normalize(cross( vec3( p[(i+1) % 3] - p[i] ), faceNormal ));
	}
	edgeNormals[3] = m3;

	vec3 m[4];
	vec3 offsets[6];
	vec4 v[6];

	for(int i=0; i<3; ++i){
		offsets[i] = vec3(-obscurance, -obscurance, 0) * inverse(mat3( edgeNormals[i], edgeNormals[(i-1+3)%3], m3 ));

		float len = dot(offsets[i], offsets[i]);
		if(len > obscurance * 2.0) offsets[i] *= obscurance * 2.0 * inversesqrt(len);

		//if(length(offsets[i]) > 2.0 * obscurance) offsets[i] = normalize(offsets[i]) * 2.0 * obscurance;

		offsets[i+3] = offsets[i] - obscurance * m3;

		v[i] = p[i] + vec4(offsets[i], 1.0);
		v[i+3] = p[i] + vec4(offsets[i+3], 1.0);

		v[i] = screenMatrix * vec4( vec3(v[i]), 1.0 );
		v[i+3] = screenMatrix * vec4( vec3(v[i+3]), 1.0 );

	}*/

	vec3 negFaceNormal = -cross(p[1].xyz - p[0].xyz, p[2].xyz - p[0].xyz);

	/*if(dot(faceNormal, vNormal[0]) > 0.0){ 
		negFaceNormal = -negFaceNormal; 
		vec4 tmp = p[2];
		p[2] = p[0];
		p[0] = tmp;
	}*/
	/*{
		negFaceNormal = -negFaceNormal; 
		vec4 tmp = p[2];
		p[2] = p[0];
		p[0] = tmp;
	}*/

    /*float sharedArea = length(negFaceNormal);
	area = sharedArea;
    negFaceNormal = normalize(negFaceNormal);
	edgeNormals[3] = negFaceNormal;

    for (int i = 2, j = 0; j < 3; i = j++) {
        edgeNormals[i] = normalize(cross(p[j].xyz - p[i].xyz, negFaceNormal));
    }

    // bias is used to force low mip level, which gives a useful average across the triangle.
    const float bias = 4.0;
    //vec4 LC = texture2D(lambertianCoverageMap, (texCoord[0] + texCoord[1] + texCoord[2]) / 3.0, bias) * lambertianCoverageConstant;
    //sharedMeanCoverage = LC.a;
    vec4 v[6];

    // Largest possible extrusion
    float maxLen = obscurance * 2.0;

    // Tracks whether the camera is inside the occlusion volume.  This is an incremental point-in-polyhedron test.
    // If any one plane returns false, inside will become false.  We begin by testing the top and bottom planes.  

    float maxLen2 = maxLen * maxLen;
    for (int i = 2, j = 0; j < 3; i = j++) {
        // OpenGL matrix constructor takes columns for initialization
        mat3 M = mat3(edgeNormals[i], edgeNormals[j], negFaceNormal);

        // Outward offset from the vertex, in the plane
        vec3 offset = vec3(-obscurance, -obscurance, 0.0) * inverse(M);

        // Extrusion clamp
        float len2 = dot(offset, offset);

        // Clamp to the maximum length
        if (len2 > maxLen2) {
            offset *= maxLen * inversesqrt(len2);
        }

        // Extend base vertex outwards
        v[j].xyz = p[j].xyz + offset;

        // Test the plane through this face

        // Extend upwards and project
        v[j + 3] = (projectionMatrix * viewMatrix) * vec4(negFaceNormal * -obscurance + v[j].xyz, 1.0);

        // Project the lower vertex as well
        v[j] = (projectionMatrix * viewMatrix) * vec4(v[j].xyz, 1.0);
    }


	//outputing

	{
		emit(v[3]);
		emit(v[4]);
		emit(v[5]);
		
		emit(v[1]);
		emit(v[2]);

		emit(v[0]);
	}
	EndPrimitive();

	{
		emit(v[5]);
		emit(v[2]);
		emit(v[3]);
		emit(v[0]);

		emit(v[4]);
		emit(v[1]);
	}
	EndPrimitive();*/

	emit(screenMatrix * p[0]);
	emit(screenMatrix * p[1]);
	emit(screenMatrix * p[2]);
	EndPrimitive();

	/*emit(v[3]);
	emit(v[4]);
	emit(v[5]);
	EndPrimitive();*/

}
