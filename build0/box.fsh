#version 410

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform vec3 viewPosition;
uniform int state;

uniform sampler2D albedo;
uniform sampler2D normals;
uniform sampler2D roughness;

in vec3 vPosition;
layout(location=0) out vec4 FragColor;

void main(){

	vec3 ambient, diffuse, specular, final;

	//globalData.fragPosition = vec3(modelMatrix * vec4(vPosition, 1.0));
	/*globalData.sNormal = 
	normalize(mat3(transpose(inverse(modelMatrix))) * vNormal);*/
	//globalData.viewPosition = viewPosition;

	/*vec3 nm = texture(normals, vTexCord).rgb;
	nm = normalize(nm * 2.0 - 1.0);
	globalData.normal = vTBN * nm;
	globalData.color = vec3(texture(albedo, vTexCord));
	globalData.color = vec3(vPosition / 2.0 + vec3(0.5));
	globalData.uwPosition = (projectionMatrix * viewMatrix * modelMatrix * vec4(vPosition, 1.0)).xyz;
	globalData.color = vec3(texture(albedo, vTexCord));
	globalData.roughness = normalize(texture(roughness, vTexCord).rgb).x;
	globalData.metallness = 0.016;
	globalData.TBN = vTBN;

	if(state == 1)
		final = calcLights() + globalData.color * 0.1;
	else{
		vec3 origPos = vec3(0, 1.5, 0);
		vec3 cubemapPos = vec3(-0.0, 0.2, -0.0);
		vec3 bBox = vec3( 0.4 );
		vec3 bBoxMin = origPos - bBox/2;
		vec3 bBoxMax = origPos + bBox/2;
		vec3 viewVec = globalData.viewPosition - globalData.fragPosition;
		vec3 normal = normalize(globalData.normal);
		vec3 vertPos = globalData.fragPosition;

		vec3 refDir = reflect(normalize(viewVec), normal);
		vec3 invRef = 1.0 / refDir;
		vec3 intMin = (bBoxMin - vertPos) / refDir;
		vec3 intMax = (bBoxMax - vertPos) / refDir;
		vec3 invLar = max(intMin, intMax);
		float dist = min(invLar.x, min(invLar.y, invLar.z));

		vec3 intPos = vertPos + refDir * dist;	
		vec3 locVec = intPos - cubemapPos;
		locVec.y *= -1;

		//vec3 rc = texture(LCM, reflect(-normalize(viewVec), globalData.normal)).rgb;
		final = calcLights();
	
		//final = final * 0.6 + rc * 0.4;
	}

	FragColor = vec4(final, 1.0);*/

	FragColor = vec4((vPosition), 1.0);
}
