#version 410

uniform samplerCube envMap;
uniform float roughness;
//uniform mat4 trans;
uniform int face;
in vec3 vPosition;
//layout(binding=0, rgba32f) uniform image2D output;
layout(location=0) out vec4 FragColor;

const float PI = 3.1415926535807932384;

float saturate(float s){
	return clamp(s, 0.0, 1.0);
}

vec3 ImportanceSampleGGX(vec2 Xi, float Roughness, vec3 N){
	float a = Roughness * Roughness;

	float M_PI = 3.14;
	float Phi = 2 * M_PI * Xi.x;
	float CosTheta = sqrt((1 - Xi.y) / (1 + (a*a - 1) * Xi.y));
	float SinTheta = sqrt(1 - CosTheta * CosTheta);

	vec3 H;
	H.x = SinTheta * cos(Phi);
	H.y = SinTheta * sin(Phi);
	H.z = CosTheta;

	vec3 UpVector = abs(N.z) < 0.999 ? vec3(0, 0, 1) : vec3(1, 0, 0);
	vec3 TangentX = normalize(cross(UpVector, N));
	vec3 TangentY = cross(N, TangentX);

	// Tangent to world space
	return TangentX * H.x + TangentY * H.y + N * H.z;
}

float radicalInverse_VdC(uint bits) {
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

vec2 hammersley(uint i, uint N) {
	return vec2( float(i) / float(N), radicalInverse_VdC(i) );
}

vec3 pem( samplerCube envMap1, float Roughness, vec3 R ){
	vec3 N = R;
	vec3 V = R;
	vec3 PrefilteredColor = vec3(0);
	const uint NumSamples = 128;
	float TotalWeight = 0.0;

	for( uint i = 0; i < NumSamples; i++ ){
		vec2 Xi = hammersley( i, NumSamples );
		vec3 H = ImportanceSampleGGX( Xi, Roughness, N );
		vec3 L = 2 * dot( V, H ) * H - V;
		float NoL = saturate( dot( N, L ) );   
		if( NoL > 0 ){
			PrefilteredColor += textureLod(envMap, L, 0.0).rgb * NoL;
			TotalWeight += NoL;
		}
	}
	return PrefilteredColor / TotalWeight;
}

void main(){

	//mat3 m = mat3(vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, 1.0), vec3(1.0, 0.0, 0.0));

	vec2 vp = vPosition.xy;
	vec3 N = vec3(0.0);

	if(face == 0) N = vec3(1.0, -vp.y, -vp.x);
	if(face == 1) N = vec3(-1.0, -vp.y, vp.x);
	if(face == 2) N = vec3(vp.x, 1.0, vp.y);
	if(face == 3) N = vec3(vp.x, -1.0, -vp.y);
	if(face == 4) N = vec3(vp.x, -vp.y, 1.0);
	if(face == 5) N = vec3(-vp.x, -vp.y, -1.0);

	//N *= vec3(-1.0, 1.0, 1.0);

	//vec3 N = vec3(vPosition.x, -1.0, vPosition.y);

	//N = vec3( trans * vec4(N, 1.0) );
	//N = m * N;

	N = normalize(N);

	FragColor = /*textureLod(envMap, N, 0);*/ vec4(pem(envMap, roughness, N), 1.0);

}
