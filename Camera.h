#ifndef CAMERA
#define CAMERA

#include "includer.h"

class Camera{
private:

  glm::vec3 mPosition;
  glm::vec3 mViewPoint;
  glm::vec2 mAngles;

  glm::mat4 mViewMatrix;
  float viewDistance = 1.f;

  void computeViewPoint();
  void computeViewMatrix();
  void compute();

public:

  glm::vec3 getPosition();
  glm::vec3 getViewPoint();
  
  glm::mat4 getViewMatrix();

  void setPosition(glm::vec3 p);
  void setAngle(glm::vec2 a);

  void move(glm::vec3 dc);
  void rotate(glm::vec2 dr);

  Camera();
  ~Camera();
};

#endif
