#include "Framebuffer.h"

/*
void Framebuffer::setInternalFormat(GLint format){
	mInternalFormat = format;
}

void Framebuffer::setFormat(GLenum format){
	mFormat = format;
}

void Framebuffer::setDataType(GLenum type){
	mDataType = type;
}

void Framebuffer::setChannel(int c){
	mChannel = c;
}*/

void Framebuffer::setSize(int width, int height){
	mWidth = width;
	mHeight = height;
}

void Framebuffer::addTexture(Texture *texture){
	mTextures.push_back(texture);
}

void Framebuffer::swapTexture(int ind, Texture *tex){
	mTextures[ind] = tex;
	bindForWriting();
	attachTexture(ind);
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE){
		std::cout << "FRAMEBUFFER ERROR: Framebuffer creating error" << status << std::endl;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Framebuffer::swapDepth(Texture *t){

}

Texture* Framebuffer::getTexture(int ind){
	return mTextures[ind];
}

Texture* Framebuffer::getDepthTexture(){
	return mDepth;
}

void Framebuffer::attachTexture(int i){
	auto t = mTextures[i];
	//glBindTexture(GL_TEXTURE_2D, t->getId());
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, t->getId(), 0);
	//glBindTexture(GL_TEXTURE_2D, 0);
}

void Framebuffer::init(){

	mDepth = new Texture(mWidth, mHeight);
	mDepth->setInternalFormat(GL_DEPTH24_STENCIL8);
	mDepth->setFormat(GL_DEPTH_STENCIL);
	mDepth->setDataType(GL_UNSIGNED_INT_24_8);
	mDepth->init();
	
	glGenFramebuffers(1, &mId);
	glBindFramebuffer(GL_FRAMEBUFFER, mId);

	for(int i=0; i<mTextures.size(); ++i){
		//mTextures[i]->init();
		/*glBindTexture(GL_TEXTURE_2D, mTextures[i]->getId());
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, mTextures[i]->getId(), 0);
		glBindTexture(GL_TEXTURE_2D, 0);*/
		attachTexture(i);
	}
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, mDepth->getId(), 0);

	const size_t texSize = 7; //mTextures.size();
	GLenum drawBuffers[texSize+1];
	for(int i=0; i<texSize; ++i){
		drawBuffers[i] = GL_COLOR_ATTACHMENT0 + i;
	}
	drawBuffers[7] = GL_DEPTH_ATTACHMENT;
	glDrawBuffers(texSize, drawBuffers);
	//glDrawBuffer(GL_NONE);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE){
		std::cout << "FRAMEBUFFER ERROR: Framebuffer creating error" << status << std::endl;

		switch(status){
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
				std::cout << "Inc att" << std::endl;
				break;
			case GL_FRAMEBUFFER_UNSUPPORTED:
				std::cout << "Format" << std::endl;
				break;
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void Framebuffer::bindForReading(){
	glBindFramebuffer(GL_READ_FRAMEBUFFER, mId);
}

void Framebuffer::bindForWriting(){
	glBindFramebuffer(GL_FRAMEBUFFER, mId);
}

void Framebuffer::beginCapture(){
	bindForWriting();
	glViewport(0, 0, mWidth, mHeight);
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Framebuffer::endCapture(){
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

void Framebuffer::show(int c, glm::vec4 zone){
	//glDrawBuffer();
	bindForReading();
	glReadBuffer(GL_DEPTH_ATTACHMENT);
	glBlitFramebuffer(
			0, 0, 
			mWidth, 
			mHeight,
			/*(int)zone.x*0,
			(int)zone.y*0, 
			(int)zone.z*0+100, 
			(int)zone.w*0+100, */
			0, 0,
			mWidth, mHeight,
			GL_COLOR_BUFFER_BIT, 
			GL_LINEAR
	);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
	
}

Framebuffer::Framebuffer(int width, int height){
	setSize(width, height);
}

Framebuffer::~Framebuffer(){
	//delete mTexture;
	delete mDepth;
}
