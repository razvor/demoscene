#include "RenderMesh.h"
#include <glm/gtc/type_ptr.hpp>

RenderMesh::RenderMesh(){
	setType(2);
}

RenderMesh::~RenderMesh(){

}

Program& RenderMesh::getProgram(){
	return mProgram;
}

void RenderMesh::setVertices(std::vector<glm::vec3> &verts){
	mVertices = verts;
}
void RenderMesh::setNormals(std::vector<glm::vec3> &normals){
	mNormals = normals;
}
void RenderMesh::setTexCords(std::vector<glm::vec2> &texCords){
	mTexCords = texCords;
}
void RenderMesh::setTangents(std::vector<glm::vec3> &ts){
	mTangents = ts;
}
void RenderMesh::setBiTangents(std::vector<glm::vec3> &ts){
	mBitangents = ts;
}
void RenderMesh::setIndices(std::vector<unsigned int> &indices){
	mIndices = indices;
}
void RenderMesh::setIndices(std::vector<int> &indices){
	vector<unsigned int> vv;
	vv.resize(indices.size());
	for(size_t i=0; i<indices.size();++i){
		vv[i] = (unsigned int)indices[i];
	}
	mIndices = vv;
}
void RenderMesh::setMode(int mode){
	mMode = mode;
}
void RenderMesh::setType(int type) {
	mType = type;
}
void RenderMesh::setDirection(float dir){
	mDirection = dir;
}
void RenderMesh::setViewMatrix(glm::mat4 vm){
	mViewMatrix = vm;
}
void RenderMesh::setProjectionMatrix(glm::mat4 pm){
	mProjectionMatrix = pm;
}
void RenderMesh::setViewPosition(glm::vec3 vp){
	mViewPosition = vp;
}

void RenderMesh::setLights(Lights *l){
	if(l != NULL)
		lights = new Lights(*l);
}

void RenderMesh::addTexture(Texture *texture){
	mTextures.push_back(texture);
}

void RenderMesh::setMaterial(Material *material){
	mMaterial = material;
}

void RenderMesh::setPosition(glm::vec3 pos){
	mPosition = pos;
	computeModelMatrix();
}
void RenderMesh::setRotation(glm::vec3 rot){
	mRotation = rot;
	computeModelMatrix();
}
void RenderMesh::setScale(glm::vec3 scale){
	mScale = scale;
	computeModelMatrix();
}
void RenderMesh::addLCTextureId(GLuint id){
	mLCTextureId = id;
}
void RenderMesh::setZoneSize(float size){
	mZoneSize = size;
}
void RenderMesh::setGridSize(int size){
	mGridSize = size;
}
void RenderMesh::setVoxelTextureId(GLuint id){
	mVoxelTextureId = id;
}
void RenderMesh::setVoxelKdTexId(GLuint id) {
	mVoxelKdTexId = id;
}
void RenderMesh::setVoxelPosTexId(GLuint id) {
	mVoxelPositionTexId = id;
}
void RenderMesh::setVoxelAtomicBuffer(GLuint buf) {
	mVoxelAtomicBuffer = buf;
}
void RenderMesh::setVoxelState(int s) {
	mVoxelState = s;
}

void RenderMesh::setProgram(Program &p){
	mProgram = p;
	initLocations();
	//initBuffers();
	initTextures();
}

void RenderMesh::init(){
	computeTangents();
	//computeBoundingBox();
	//initProgram();
	initLocations();
	initBuffers();
	initTextures();
}

void RenderMesh::render(){
	prepare();
	pushUniforms();
	pushAttributes();
	draw();
}

void RenderMesh::renderGeometry(Program &currentProgram) {

	//initLocations();

	unifLocations.viewMatrix = currentProgram.uniform("viewMatrix");
	unifLocations.projectionMatrix = currentProgram.uniform("projectionMatrix");
	unifLocations.modelMatrix = currentProgram.uniform("modelMatrix");
	unifLocations.viewPosition = currentProgram.uniform("viewPosition");
	unifLocations.type = currentProgram.uniform("type");

	pushMatrix(unifLocations.viewMatrix, mViewMatrix);
	pushMatrix(unifLocations.projectionMatrix, mProjectionMatrix);
	pushMatrix(unifLocations.modelMatrix, mModelMatrix);
	glUniform3f(unifLocations.viewPosition, mViewPosition.x, mViewPosition.y, mViewPosition.z);
	glUniform1i(unifLocations.type, mType);

	pushAttributes();
	draw();
}

void RenderMesh::computeModelMatrix(){
	glm::mat4 translate = glm::translate(glm::mat4(1), mPosition);
	glm::mat4 rotX = glm::rotate(glm::mat4(1), mRotation.x, glm::vec3(1, 0, 0));
	glm::mat4 rotY = glm::rotate(glm::mat4(1), mRotation.y, glm::vec3(0, 1, 0));
	glm::mat4 rotZ = glm::rotate(glm::mat4(1), mRotation.z, glm::vec3(0, 0, 1));
	glm::mat4 rot = rotX * rotY * rotZ;
	glm::mat4 scale = glm::scale(glm::mat4(1), mScale);
	mModelMatrix = translate * rot * scale;
}

void RenderMesh::computeTangents(){

	if(mTangents.size() > 0) return;

	int i,j, ind1, ind2, ind3;
	glm::vec3 v1, v2, v3, e1, e2;
	glm::vec2 uv1, uv2, uv3, duv1, duv2;
	mTangents.clear();
	mBitangents.clear();
	i=0;
	if(mIndices.size() % 3	!= 0){
		mTangents.resize(mIndices.size());
		mBitangents.resize(mIndices.size());
	}
	while(i<mIndices.size()){
		ind1 = mIndices[i++];
		ind2 = mIndices[i++];
		ind3 = mIndices[i++];
		v1 = mVertices[ind1];
		v2 = mVertices[ind2];
		v3 = mVertices[ind3];
		uv1 = mTexCords[ind1];
		uv2 = mTexCords[ind2];
		uv3 = mTexCords[ind3];
		e1 = v2 - v1;
		e2 = v3 - v1;
		duv1 = uv2 - uv1;
		duv2 = uv3 - uv1;
		/*GLfloat f = 1.0f / (duv1.x * duv2.y - duv2.x * duv1.y);
		float m1[4] = {duv2.y, -duv1.y, -duv2.x, duv1.x};
		float m2[6] = {e1.x, e1.y, e1.z, e2.x, e2.y, e2.z};
		glm::mat2x3 tb = f * glm::make_mat2(m1) * glm::make_mat3x2(m2);
		glm::vec3 tangent = glm::vec3(tb[0][0], tb[0][1], tb[0][2]);
		glm::vec3 bitangent = glm::vec3(tb[1][0], tb[1][1], tb[1][2]);*/

		glm::vec3 tangent, bitangent;

		GLfloat f = 1.0f / (duv1.x * duv2.y - duv2.x * duv1.y);
		tangent.x = f * (duv2.y * e1.x - duv1.y * e2.x);
		tangent.y = f * (duv2.y * e1.y - duv1.y * e2.y);
		tangent.z = f * (duv2.y * e1.z - duv1.y * e2.z);

		bitangent.x = f * (-duv2.x * e1.x + duv1.x * e2.x);
		bitangent.y = f * (-duv2.x * e1.y + duv1.x * e2.y);
		bitangent.z = f * (-duv2.x * e1.z + duv1.x * e2.z);

		for(j=0; j<3; ++j){
			mTangents.push_back(tangent);
			mBitangents.push_back(bitangent);
		}
	}
}

void RenderMesh::computeBoundingBox(){
	glm::vec3 minPoint, maxPoint;
	float min, max;

	if(!mVertices.size()) return;
	
	for(int i=0; i<3; ++i){
		min = mVertices[0][i];
		max = mVertices[0][i];
		for(auto v : mVertices){
			if(v[i] < min) min = v[i];
			if(v[i] > max) max = v[i];
		}
		minPoint[i] = min;
		maxPoint[i] = max;
	}

	//std::cout << "PM " << minPoint.y << " " << maxPoint.y << std::endl;

	mBoundingBox = glm::vec3(
		std::max( abs(minPoint.x), abs(maxPoint.x) ) * 2,
		std::max( abs(minPoint.y), abs(maxPoint.y) ) * 2,
		std::max( abs(minPoint.z), abs(maxPoint.z) ) * 2
	);

	//std::cout << "Bbox " << mBoundingBox.x << " " << mBoundingBox.y << " " << mBoundingBox.z << std::endl;

}

void RenderMesh::initBuffers(){

	mProgram.use();

	if (mVerticesBuffer != NULL) {
		/*delete mVerticesBuffer;
		delete mNormalsBuffer;
		delete mTexCordsBuffer;
		delete mTangentsBuffer;
		delete mBitangentsBuffer;
		delete mIndicesBuffer;*/
	}

	mVerticesBuffer   = new VertexBuffer(1, mVertices);
	mNormalsBuffer    = new VertexBuffer(1, mNormals);
	//std::cout << mTexCords[mTexCords.size()-1].x << std::endl;
	mTexCordsBuffer   = new VertexBuffer(1, mTexCords);
	mTangentsBuffer   = new VertexBuffer(1, mTangents);
	mBitangentsBuffer = new VertexBuffer(1, mBitangents);
	mIndicesBuffer    = new VertexBuffer(2, mIndices);


	/*std::vector<unsigned int> indices;
	for(size_t i=0;i<36;++i) indices.push_back(i);
	GLuint EBO;
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(size_t) * indices.size(), &indices[0], GL_STATIC_DRAW);*/

	/*GLuint handler;
	glGenBuffers(1, &handler);
	std::vector<size_t> ii; for(size_t i=0; i<36; ++i) ii.push_back(i);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handler);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(size_t) * ii.size(), &ii[0], GL_STATIC_DRAW);*/

	glGenVertexArrays(1, &vertArr);
	glBindVertexArray(vertArr);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

	pushBuffer(0, mIndicesBuffer);
	pushBuffer(attrLocations.vertices, mVerticesBuffer);
	pushBuffer(attrLocations.normals, mNormalsBuffer);
	pushBuffer(attrLocations.texCords, mTexCordsBuffer);
	pushBuffer(attrLocations.tangents, mTangentsBuffer);
	pushBuffer(attrLocations.bitangents, mBitangentsBuffer);


	//pushBuffer(0, mIndicesBuffer);

	glBindVertexArray(0);

	//glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

void RenderMesh::initProgram(){
	mProgram = Program("box", "box");
	if(!mProgram.init()){
		std::cerr << "Program init ERROR " << std::endl;
	}
}

void RenderMesh::initLocations(){

	//mProgram.use();

	//std::cout << "Init locations begin" << std::endl;

	attrLocations.vertices = 0;
	attrLocations.normals  = 1;
	attrLocations.texCords = 2;
	attrLocations.tangents = 3;
	attrLocations.bitangents = 4;
	//attrLocations.indices  = 3;

	unifLocations.viewMatrix          = mProgram.uniform(std::string("viewMatrix").c_str());
	unifLocations.projectionMatrix    = mProgram.uniform("projectionMatrix");
	unifLocations.modelMatrix         = mProgram.uniform("modelMatrix");
	unifLocations.viewPosition        = mProgram.uniform("viewPosition");
	//unifLocations.viewDirection     = mProgram.uniform("viewDirection");
	unifLocations.state               = mProgram.uniform("state");
	//unifLocations.direction         = mProgram.uniform("direction");
	unifLocations.boundingBox         = mProgram.uniform("boundingBox");
	//unifLocations.boundingBox         = mProgram.uniform("boundingBox");
	unifLocations.type                = mProgram.uniform("type");

	if(lights && mGridSize > 0){
		lights->init(mProgram);
		//unifLocations.LCM           = mProgram.uniform("LCM");
	}

	//std::cout << "Init locations end" << std::endl;
	
}

void RenderMesh::initMinLocations(){
	attrLocations.vertices   = 0;
	attrLocations.normals    = 1;
	attrLocations.texCords   = 2;
	attrLocations.tangents   = 3;
	attrLocations.bitangents = 4;
	//attrLocations.indices  = 3;

	unifLocations.viewMatrix       = mProgram.uniform("viewMatrix");
	unifLocations.projectionMatrix = mProgram.uniform("projectionMatrix");
	unifLocations.modelMatrix      = mProgram.uniform("modelMatrix");
	unifLocations.state            = mProgram.uniform("state");
	unifLocations.type             = mProgram.uniform("type");

}

void RenderMesh::initTextures(){
	for(int i=0; i<mTextures.size(); ++i)
		mTextures[i]->init(mProgram);

	if(mMaterial) mMaterial->init(mProgram);
}

void RenderMesh::pushMatrix(int location, glm::mat4 &matr){
	glUniformMatrix4fv( location, 1, GL_FALSE, &matr[0][0] );
}

void RenderMesh::pushBuffer(int location, VertexBuffer *buffer){
	//TODO different types of buffers	
	/*glBindBuffer( GL_ARRAY_BUFFER, buffer->getInstance() );
	glEnableVertexAttribArray( location );
	glVertexAttribPointer(
			location,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL
			);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );*/
	buffer->render(location);
}

void RenderMesh::prepare(){
	mProgram.use();
}

void RenderMesh::pushUniforms(){
	pushMatrix(unifLocations.viewMatrix, mViewMatrix);
	pushMatrix(unifLocations.projectionMatrix, mProjectionMatrix);
	pushMatrix(unifLocations.modelMatrix, mModelMatrix);

	glUniform3f(unifLocations.viewPosition, mViewPosition.x, mViewPosition.y, mViewPosition.z);
	glUniform1i(unifLocations.state, mMode);

	glUniform1i(unifLocations.type, mType);

	/*glUniform3f(unifLocations.boundingBox, 
		mBoundingBox.x, mBoundingBox.y, mBoundingBox.z);*/

	if(lights && mGridSize > 0 && mVoxelState == 1){ //lights only for voxelization
		lights->pushToShader();
	}
	
	if (mVoxelState == 1) {
		int i = 0;
		for (i = 0; i < mTextures.size(); ++i)
			mTextures[i]->pushToShader(i);

		if (mMaterial) mMaterial->pushToShader(i);
	}

	if(mGridSize > 0){

		float size = mZoneSize;
		glm::mat4
			projectionMatrix = glm::ortho(-size*0.5f, size*0.5f, -size*0.5f, size*0.5f, size*0.5f, size*1.5f),
			projX_ = projectionMatrix * glm::lookAt(glm::vec3(size, 0, 0), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)),
			projY_ = projectionMatrix * glm::lookAt(glm::vec3(0, size, 0), glm::vec3(0, 0, 0), glm::vec3(0, 0, -1)),
			projZ_ = projectionMatrix * glm::lookAt(glm::vec3(0, 0, size), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
		glUniformMatrix4fv(mProgram.uniform("projX"), 1, GL_FALSE, &projX_[0][0]);
		glUniformMatrix4fv(mProgram.uniform("projY"), 1, GL_FALSE, &projY_[0][0]);
		glUniformMatrix4fv(mProgram.uniform("projZ"), 1, GL_FALSE, &projZ_[0][0]);

		//glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, mVoxelAtomicBuffer);
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, mVoxelAtomicBuffer);

		if (mVoxelState == 1) {
			glUniform1i(mProgram.uniform("gridSize"), mGridSize);
			glUniform1f(mProgram.uniform("zoneSize"), mZoneSize);

			glUniform1i(mProgram.uniform("voxelState"), mVoxelState);

			//glBindImageTexture(0, mVoxelPositionTexId, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA16UI);
			//glBindImageTexture(1, mVoxelKdTexId, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA8);

			//std::cout << "voxel tex id " << mVoxelTextureId << std::endl;

			glBindImageTexture(5, mVoxelTextureId, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA16F);

		}
	}
}

void RenderMesh::pushAttributes(){
	/*pushBuffer(attrLocations.vertices, mVerticesBuffer);
	pushBuffer(attrLocations.normals, mNormalsBuffer);
	pushBuffer(attrLocations.texCords, mTexCordsBuffer);
	pushBuffer(attrLocations.tangents, mTangentsBuffer);
	pushBuffer(attrLocations.bitangents, mBitangentsBuffer);*/
}

void RenderMesh::draw(){
	/*glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mIndicesBuffer->getInstance() );
	glDrawElements( GL_TRIANGLES, mIndices.size(), GL_UNSIGNED_INT, 0 );
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);*/

	//glBindVertexArray(0);
	
	glBindVertexArray(vertArr);
	//std::cout << "ind size " << mIndices.size() << std::endl;
	//std::cout << "start drawing" << std::endl;
    glDrawElements(GL_TRIANGLES, mIndices.size(), GL_UNSIGNED_INT, 0);
	//std::cout << "end drawing" << std::endl;
	//glDrawArrays(GL_TRIANGLES, 0, 36);

	//if (mGridSize > 0) glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

    glBindVertexArray(0);
}

size_t RenderMesh::getIndicesCount() {
	return mIndices.size();
}

size_t RenderMesh::getVerticesCount() {
	return mVertices.size();
}

unsigned int RenderMesh::getIndex(int ind) {
	return mIndices[ind];
}
glm::vec3 RenderMesh::getVertex(int ind) {
	return mVertices[ind];
}
glm::vec3 RenderMesh::getNormal(int ind) {
	return mNormals[ind];
}
glm::vec2 RenderMesh::getTexCoords(int ind) {
	return mTexCords[ind];
}
