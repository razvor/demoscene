#version 440

//in vec3 vPos;
in vec3 vGeomNormal;
in vec4 vColor;
in vec2 vUV;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform vec3 viewPosition;
uniform int gridSize;
uniform int levels;
uniform float zoneSize;

uniform layout(binding=0, r32ui) uimageBuffer octreeDiffuse;
uniform layout(binding=1, r32ui) uimageBuffer octreeChild;

uniform layout(binding=5, rgba8) image3D voxelTexture;
uniform sampler3D voxelTexture1;

layout(location=0) out vec4 FragColor;

/*void main(){
	FragColor = vec4(vColor);
}*/

vec4 convRGBA8ToVec4( uint val ){
    return vec4( float( (val&0x000000FF) ), float( (val&0x0000FF00)>>8U),
	             float( (val&0x00FF0000)>>16U), float( (val&0xFF000000)>>24U) );
}

bool nodeOccupied( in uvec3 loc, out int leafIdx ){
    int voxelDim = gridSize;

	//decide max and min coord for the root node
	uvec3 umin = uvec3(0,0,0);

    bool bOccupied = true;
	uint idx = 0;
	uint subnode;
	uvec3 offset;
	int depth;
	
    for( depth = 0; depth <= levels; ++depth ){
	    leafIdx = int(idx);
	    idx = imageLoad( octreeChild, int(idx) ).r;
		if( (idx & 0x80000000) == 0 ){
		    bOccupied= false;
		    break;
		}
		else if( depth == levels ){
			//return true;
		    break;
		}

		idx &= 0x7FFFFFFF;  //mask out flag bit to get child pointer
		if( idx == 0 ){
		    bOccupied = false;
		    break;
		}
		voxelDim /= 2;
		/*offset = clamp( ivec3( 1 + loc - umin - voxelDim ), 0, 1 );*
	    idx += offset.x + 4*offset.y + 2*offset.z;*/


		subnode = (loc.x > voxelDim + umin.x - 1) ? 1 : 0;
		subnode += (loc.y > voxelDim + umin.y - 1) ? 2 : 0;
		subnode += (loc.z > voxelDim + umin.z - 1) ? 4 : 0;
	    idx += int(subnode);
	    umin.x += voxelDim * ((loc.x > voxelDim + umin.x - 1) ? 1 : 0);
	    umin.y += voxelDim * ((loc.y > voxelDim + umin.y - 1) ? 1 : 0);
	    umin.z += voxelDim * ((loc.z > voxelDim + umin.z - 1) ? 1 : 0);

	    //umin += voxelDim * offset;
	}

	return bOccupied;
}

bool isInsideCube(vec3 p, float e) { return abs(p.x) < 1 + e && abs(p.y) < 1 + e && abs(p.z) < 1 + e; }

void main(){

	vec4 fAxis = vec4(0, 0, -1, 0);
	vec4 rAxis = vec4(1, 0, 0, 0);
	vec4 tAxis = vec4(0, 1, 0, 0);

	vec3 forward = vec3( viewMatrix * fAxis );
	vec3 right = vec3( viewMatrix * rAxis );
	vec3 top = vec3( viewMatrix * tAxis );


	vec2 tUV = (vUV) * 2.0 - 1.0;
	vec3 castPoint = top * tUV.y + right * tUV.x;
	vec3 fromPoint = forward * -3.0;
	vec3 dir = normalize(castPoint - fromPoint);
	vec3 pos = viewPosition / zoneSize;

	float stepSize = 0.005;

	vec4 acc = vec4(0);
	vec3 currentPoint = pos;
	uvec3 ipos;

	uint li;

	int i = 0;

	while(acc.a < 1.0 && isInsideCube(currentPoint, 0.2)){
		currentPoint += stepSize * dir;

		ipos = uvec3( (currentPoint + 1.0) / 2.0 * gridSize );
		/*if( nodeOccupied(ipos, li) ){
			uint col = imageLoad(octreeDiffuse, int(li)).r;
			acc = convRGBA8ToVec4(col) / 255.0;
			break;
		}*/

		//acc = imageLoad(voxelTexture, ivec3(ipos)).rgba;

		acc += textureLod(voxelTexture1, vec3( vec3(ipos) / float(gridSize)), 0 );

		//acc = imageLoad(voxelTexture, ipos, );
	}
	
	FragColor = acc; //vec4(vUV, 1.0, 1.0);
}