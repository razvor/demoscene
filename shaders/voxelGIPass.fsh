#version 440

//#include "lights.fsh"

uniform sampler2D albedos;
uniform sampler2D normals;
uniform sampler2D positions;
uniform sampler2D types;
uniform sampler2D boundingBoxes;
uniform sampler2D depths;
uniform sampler2D finals;
uniform sampler2D aoTexture;
uniform vec3 viewPosition;
uniform int state;
uniform int gridSize;
uniform int voxelWorldSize;

//uniform layout(binding=5, rgba8) image3D voxelTexture0;
uniform sampler3D voxelTexture;
layout(binding=0, r32ui) uniform uimageBuffer octreeChild;
layout(binding=1, r32ui) uniform uimageBuffer octreeDiffuse;

in vec2 cords;

struct PointLight{
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float constant;
	float linear;
	float quadratic;
};

struct SphereLight{
	vec3 position;
	vec3 color;
	float intensity;
	float radius;
};

struct RectangleLight{
	vec3 position;
	vec3 color;
	float width;
	float height;
	float intensity;
	mat4 matrix;
};

struct SpotLight{
	vec3 position;
	vec3 color;
	float intensity;
	float fov;
	float radius;
	vec3 direction;
};

uniform PointLight pointLights[10];
uniform SphereLight sphereLights[10];
uniform RectangleLight rectangleLights[10];
uniform SpotLight spotLights[10];
uniform int pointLightCount;
uniform int sphereLightCount;
uniform int rectangleLightCount;
uniform int spotLightCount;


layout(location=0) out vec4 FragColor;

#define SQRT2 1.414213

#define M_PI 3.1428

float saturate(float v){
	return min(1.0, max(0.0, v));
}

float smoothDistanceAtt(float squaredDistance, float invSqrAttRadius){
	float factor = squaredDistance * invSqrAttRadius;
	float smoothFactor = saturate(1.0 - factor * factor);
	return smoothFactor * smoothFactor;
}

float getDistanceAtt(float sqrDist, float invSqrAttRadius){
	float attenuation = 1.0 / max( sqrDist , 0.0001);
	attenuation *= smoothDistanceAtt(sqrDist, invSqrAttRadius);
	return attenuation;
}

vec4 convRGBA8ToVec4( in uint val ){
    return vec4( float( (val&0x000000FF) ), float( (val&0x0000FF00)>>8U),
	             float( (val&0x00FF0000)>>16U), float( (val&0xFF000000)>>24U) );
}

bool nodeOccupied( in uvec3 loc, int level, out int leafIdx ){
    int voxelDim = gridSize;
	uvec3 umin = uvec3(0,0,0);
    bool bOccupied = true;
	uint idx = 0;
	uint subnode;
	uvec3 offset;
	int depth;

	int levels = level;
	
    for( depth = 0; depth <= levels; ++depth ){
	    leafIdx = int(idx);
	    idx = imageLoad( octreeChild, int(idx) ).r;
		if( (idx & 0x80000000) == 0 ){
		    bOccupied= false;
		    break;
		}
		else if( depth == levels ){
		    break;
		}
		idx &= 0x7FFFFFFF;  //mask out flag bit to get child pointer
		if( idx == 0 ){
		    bOccupied = false;
		    break;
		}
		voxelDim /= 2;

		subnode = (loc.x > voxelDim + umin.x - 1) ? 1 : 0;
		subnode += (loc.y > voxelDim + umin.y - 1) ? 2 : 0;
		subnode += (loc.z > voxelDim + umin.z - 1) ? 4 : 0;
	    idx += int(subnode);
	    umin.x += voxelDim * ((loc.x > voxelDim + umin.x - 1) ? 1 : 0);
	    umin.y += voxelDim * ((loc.y > voxelDim + umin.y - 1) ? 1 : 0);
	    umin.z += voxelDim * ((loc.z > voxelDim + umin.z - 1) ? 1 : 0);
	}
	return bOccupied;
}

bool SVONode(uvec3 loc, int level, out int outInd ){
	uint node = 0, childInd = 0;
	uint subnode;
	uvec3 umin = uvec3(0);
	int voxelDim = gridSize;
	bool available = true;

	//node = imageLoad(octreeChild, int(childInd)).r;

	level = level - 1;

    for( int i = 0; i <= level; ++i ){

		outInd = int(childInd);
		
	    childInd = imageLoad( octreeChild, int(childInd) ).r;
		if( (childInd & 0x80000000) == 0 ){
		    available = false;
		    break;
		}
		else if( i == level ){
		    break;
		}

		childInd &= 0x7FFFFFFF;  //mask out flag bit to get child pointer
		if( childInd == 0 ){
		    available = false;
		    break;
		}
		voxelDim /= 2;

		subnode = (loc.x > voxelDim + umin.x - 1) ? 1 : 0;
		subnode += (loc.y > voxelDim + umin.y - 1) ? 2 : 0;
		subnode += (loc.z > voxelDim + umin.z - 1) ? 4 : 0;
	    childInd += int(subnode);
	    umin.x += voxelDim * ((loc.x > voxelDim + umin.x - 1) ? 1 : 0);
	    umin.y += voxelDim * ((loc.y > voxelDim + umin.y - 1) ? 1 : 0);
	    umin.z += voxelDim * ((loc.z > voxelDim + umin.z - 1) ? 1 : 0);
	}

	return available;

	/*for(int i=0; i<=level; ++i){
		if((node & 0x80000000) == 0){
			available = false;
			break;
		}else if(i == level){
			break;
		}

		voxelDim /= 2;

		childInd = (node & 0x7FFFFFFF);
		
		subnode = (loc.x > umin.x + voxelDim + 1) ? 1 : 0;
		subnode += (loc.y > umin.y + voxelDim + 1) ? 2 : 0;
		subnode += (loc.z > umin.z + voxelDim + 1) ? 4 : 0;

	    umin.x += voxelDim * ((loc.x > voxelDim + umin.x - 1) ? 1 : 0);
	    umin.y += voxelDim * ((loc.y > voxelDim + umin.y - 1) ? 1 : 0);
	    umin.z += voxelDim * ((loc.z > voxelDim + umin.z - 1) ? 1 : 0);

		node = imageLoad( octreeChild, int(childInd) ).r;
	}

	ind = int(childInd);

	return available;*/
}

vec4 SVOLod(in layout(r32ui) uimageBuffer buf, in vec4 pos){
	
	vec3 fLoc = float(gridSize) * (vec3(pos)+1.0)/2.0;
	uvec3 loc1 = uvec3( floor(fLoc) );
	uvec3 loc2 = uvec3( ceil(fLoc) );
	uvec3 loc = uvec3( float(gridSize) * (vec3(pos)+1.0)/2.0 );
	float lev = float(log2(gridSize)) - pos.w;
	int minLevel = int(floor(lev));
	int maxLevel = int(ceil(lev));
	
	int minInd, minInd1, minInd2, 
		maxInd, maxInd1, maxInd2;
	vec4 minColor = vec4(0), minColor1 = vec4(0), minColor2 = vec4(0),
		 maxColor = vec4(0), maxColor1 = vec4(0), maxColor2 = vec4(0);
	vec4 res;
	
	/*if(nodeOccupied(loc, minLevel, minInd)) minColor = convRGBA8ToVec4(imageLoad(buf, int(minInd)).r).rgba / 255.0;
	if(nodeOccupied(loc, maxLevel, maxInd)) maxColor = convRGBA8ToVec4(imageLoad(buf, maxInd).r).rgba / 255.0;*/

	const float SQRT3 = 1.732;

	float mix12 = 0.5; //length(loc2 - vec3(loc)) / SQRT3;

	if(nodeOccupied(loc1, minLevel, minInd)) minColor1 = convRGBA8ToVec4(imageLoad(buf, int(minInd)).r).rgba / 255.0;
	if(nodeOccupied(loc2, minLevel, minInd)) minColor2 = convRGBA8ToVec4(imageLoad(buf, minInd).r).rgba / 255.0;
	minColor = mix( minColor1, minColor2, mix12 );
	if(nodeOccupied(loc1, maxLevel, minInd)) maxColor1 = convRGBA8ToVec4(imageLoad(buf, int(minInd)).r).rgba / 255.0;
	if(nodeOccupied(loc2, maxLevel, minInd)) maxColor2 = convRGBA8ToVec4(imageLoad(buf, minInd).r).rgba / 255.0;
	maxColor = mix( maxColor1, maxColor2, mix12 );

	res = mix( minColor, maxColor, 1.0 - (lev - float(minLevel)) );

	return res;
}

vec3 scaleAndBias(const vec3 p) { return 0.5f * p + vec3(0.5f); }
bool isInsideCube(const vec3 p, float e) { return abs(p.x) < 1 + e && abs(p.y) < 1 + e && abs(p.z) < 1 + e; }

float orenNayarDiffuse(
	vec3 lightDirection,
	vec3 viewDirection,
	vec3 surfaceNormal,
	float roughness) {

	float LdotV = dot(lightDirection, viewDirection);
	float LdotN = dot(surfaceNormal, lightDirection);
	float VdotN = dot(viewDirection, surfaceNormal);

	/*float s = LdotV - NdotL * NdotV;
	float t = mix(1.0, max(NdotL, NdotV), step(0.0, s));

	float sigma2 = roughness * roughness;
	float A = 1.0 + sigma2 * (1.0 / (sigma2 + 0.13) + 0.5 / (sigma2 + 0.33));
	float B = 0.45 * sigma2 / (sigma2 + 0.09);

	return max(0.0, NdotL) * (A + B * s / t) / M_PI;*/

	float result = saturate(LdotN);
	float soft_rim = saturate(1-VdotN/2);
	float fakey = pow(1-result*soft_rim,2);
	float fakey_magic = 0.62;
	fakey = fakey_magic - fakey*fakey_magic;
	return mix(result, fakey, roughness);
}

vec3 traceDiffuseCone(vec3 from, vec3 direction, vec3 normal, vec3 view, float roughness){

	float VOXEL_SIZE = 1.0 / float(gridSize);
	float MAX_DISTANCE = 3.0;
	float OFFSET = 8 * VOXEL_SIZE;
	float STEP = VOXEL_SIZE;
	const vec3 N_OFFSET = normal * (1 + 4 * 1.0 / SQRT2) * VOXEL_SIZE;
	const float CONE_SPREAD = 0.0325;
	
	from /= (5.0 + 0.3);
	from += 0.1;
	//from += vec3(0, 0.0355, 0);
	from += N_OFFSET;
	direction = normalize(direction);
	vec4 acc = vec4(0.0f);
	float dist = OFFSET;

	while(dist < 1.0 * SQRT2 && acc.a < 1){ 
		vec3 c = from + dist * direction;
		vec3 e = scaleAndBias(c); 
		if(!isInsideCube(e, 0.3)) break;
		float l = (1 + CONE_SPREAD * dist / VOXEL_SIZE);
		float level = log2(l);
		float ll = (level + 1) * (level + 1);
		vec4 voxel = textureLod(voxelTexture, e, min(7.0, ll));
		if(voxel.a <= 1.0) acc += 0.075 * ll * voxel * pow(1 - voxel.a, 2);
		dist += ll * VOXEL_SIZE * 2.0;
	}

	//acc *= abs(dot(normal, direction));

	//acc = pow(acc, vec4(1.3));

	acc *= orenNayarDiffuse(direction, -view, normal, roughness);
	//acc *= saturate(dot(normal, direction));

	return acc.rgb; //* abs(dot(normal, direction));

}

float traceAOCone(vec3 from, vec3 direction, vec3 normal){

	float VOXEL_SIZE = 1.0 / float(gridSize);
	float MAX_DISTANCE = 3.0;
	float OFFSET = 2.0 * VOXEL_SIZE;
	float STEP = VOXEL_SIZE;
	const vec3 N_OFFSET = normal * (1 + 4 * 1.0 / SQRT2) * VOXEL_SIZE;
	const float CONE_SPREAD = 0.0325 * 2.0;

	//from /= (5.5);
	//from /= 5.1;
	from /= (5.0 + 0.1);
	from += vec3(0, 0.045/2.0, 0);
	from += normal * OFFSET;
	direction = normalize(direction);
	float dist = OFFSET;

	float ao = 0;
	float fo = 1000; //1500.0;
	while(dist < SQRT2 * 0.7 && ao < 1.0){ 
		vec3 c = from + dist * direction;
		vec3 e = scaleAndBias(c); 
		if(!isInsideCube(e, 0.1)) break;
		float l = (1 + 1.0 * CONE_SPREAD * dist / VOXEL_SIZE);
		float level = log2(l);
		float ll = (level + 1) * (level + 1);
		float va = textureLod(voxelTexture, e, min(7.0, ll)).a;
		va *= 0.6;
		ao += (1-ao)*va/(1+dist*fo);
		dist += ll * VOXEL_SIZE * 1.0;
	}

	float SHADOW_COEF = 0.08;
	float FALLOFF = 2.0;

	/*while(dist < SQRT2 && ao < 1.0){
		vec3 c = from + dist * direction;
		if(!isInsideCube(c, 0.1)) break;
		vec3 e = scaleAndBias(c); 

		float l = pow(dist, FALLOFF); // Experimenting with inverse square falloff for shadows.
		float s1 = SHADOW_COEF * 0.062 * textureLod(voxelTexture, e, 1 + 0.75 * l).a;
		float s2 = SHADOW_COEF * 0.3 * 0.135 * textureLod(voxelTexture, e, 4.5 * l).a;
		float s = s1 + s2;
		ao += (1.0 - ao) * s / (1+dist*3.0);
		dist += 0.9 * VOXEL_SIZE * (1 + 0.2 * l) * 2.0;
	}*/
	
	/*float aperture = 0.25;
	float falloff = 0.5 * 1.0;

	while(dist < SQRT2 && ao < 1.0){
		vec3 c = from + dist * direction;
		if(!isInsideCube(c, 0.1)) break;
		vec3 e = scaleAndBias(c); 

		float diameter = 1.0 + 2.0f * aperture * dist;
        float mipLevel = log2(2.0 * diameter / VOXEL_SIZE);
		float va = textureLod(voxelTexture, e, min(7.0, mipLevel)).a;

		ao = (1.0 - ao) * va / (1.0 + dist * falloff);

		dist += diameter * 0.5;
	}*/

	//ao *= dot(normal, direction);

	//ao = 1.0 - ao;

	return ao;
}

vec3 traceSpecularCone(vec3 from, vec3 direction, vec3 normal, float roughness){

	//direction = normalize(vec3(0, 0, 1));

	float VOXEL_SIZE = 1.0 / float(gridSize);
	float MAX_DISTANCE = 3.0;
	float OFFSET = 4 * VOXEL_SIZE;
	float STEP = VOXEL_SIZE;
	float ACC_COEF = 0.4 * 0.3;
	float LEVEL_COEF = 0.1f;
	float LEVEL_STEP_COEF = 0.125f;
	float MAX_LEVEL = 7.0;
	float ROUGHNESS_COEF = 10.0;
	const vec3 N_OFFSET = normal * (1 + 2 * 1.0 / SQRT2) * VOXEL_SIZE;
	const float CONE_SPREAD = 0.0225;

	//roughness = 0.1;
	
	//first variant
	from += vec3(0, 0, 0);
	from /= (5.0 + 0.1);
	//from += 0.01;
	//from += N_OFFSET;
	//from /= 10.0;
	roughness *= 1.0;
	//roughness = 0.5;
	direction = normalize(direction);
	vec4 acc = vec4(0.0f);
	float dist = OFFSET;

	while(dist < 2.0 * SQRT2 && acc.a < 1){ 
		vec3 c = from + dist * direction;
		vec3 e = scaleAndBias(c); 
		if(!isInsideCube(e, 0.2)) break;

		float level = LEVEL_COEF * ROUGHNESS_COEF * roughness * log2(1 + dist / VOXEL_SIZE);
		vec4 voxel = textureLod(voxelTexture, e, min(level, MAX_LEVEL));
		float f = 1 - acc.a;
		acc.rgb += ACC_COEF * (1 + roughness) * voxel.rgb * voxel.a * f;
		acc.a += ACC_COEF * voxel.a * f;
		dist += STEP * (1.0f + LEVEL_STEP_COEF * level);
	}

	/*roughness = 0.1;
	float cs = roughness * 0.17;
	float cone_radius = (1.0 + 5.0 * cs) / 128.0;
	while(acc.a < 1) {
		vec3 e = scaleAndBias(from); 
		if(!isInsideCube(e, 0.2)) break;
		float mipmap = log2(cone_radius * 128.0 * 0.6) - 1;
        vec4 sampled = textureLod(voxelTexture, from, mipmap);
        sampled.w *= 2.0;
        sampled.w = saturate(sampled.w);
        acc += sampled * (1.0 - acc.w);
        from += direction * cone_radius / 2.25;
        cone_radius *= 1.01 + cs;
    }*/

	//second variant
	/*float apperture = 0.001;
	from /= (5.0 - 0.01);
	//from += N_OFFSET;
	direction = normalize(direction);
	vec4 acc = vec4(0);
	float diameter = 0;
	float dst = VOXEL_SIZE * (4 * SQRT2);
	float mipLevel = 0;
	vec3 pos = from;

	while(acc.a < 1.0){
		dst += max(diameter, VOXEL_SIZE * (4 * SQRT2)) * 0.1;
		diameter = dst * apperture;
		pos += dst * direction;

		mipLevel = max(0, log2( 1 + diameter / VOXEL_SIZE ));
		mipLevel = min(mipLevel, 7.0);

		vec3 e = scaleAndBias(pos); 
		if(!isInsideCube(e, 0.1)) break;

		vec4 voxelSample = textureLod( voxelTexture, e, mipLevel );
		
		//acc = acc * acc.a + (1) * voxelSample * voxelSample.a;
		acc += voxelSample * voxelSample.a * (1.0 - acc.a);

	}*/

	return acc.rgb;
}

vec3 basisVec(vec3 u){
	u = normalize(u);
	vec3 v = vec3(0.99146, 0.11664, 0.05832); // Pick any normalized vector.
	return abs(dot(u, v)) > 0.99999f ? cross(u, vec3(0, 1, 0)) : cross(u, v);
}

float radicalInverse_VdC(uint bits) {
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

vec2 hammersley(uint i, uint N) {
	return vec2( float(i) / float(N), radicalInverse_VdC(i) );
}


vec4 inderectIllumination(vec3 position, vec3 normal, vec3 view, float roughness){
	
	float w0 = M_PI / 2.0;
	float w1 = M_PI / 16.0;
	w0 = M_PI / 9.0;
	w1 = M_PI / 9.0;

	vec3 ort1 = basisVec(normal);
	vec3 ort2 = normalize(cross(normal, ort1));
	vec3 cor1 = (ort1 + ort2) / 2.0;
	vec3 cor2 = (ort1 - ort2) / 2.0;

	vec3 ang1 = (normal + ort1) / 2.0;
	vec3 ang2 = (normal + ort2) / 2.0;
	vec3 ang3 = (normal - ort1) / 2.0;
	vec3 ang4 = (normal - ort2) / 2.0;

	vec3 s1 = (normal + cor1) / 2.0;
	vec3 s2 = (normal + cor2) / 2.0;
	vec3 s3 = (normal - cor1) / 2.0;
	vec3 s4 = (normal - cor2) / 2.0;

	vec4 res = vec4(0);

	res += w0 * traceDiffuseCone(position, normal, normal, view, roughness);

	res += w1 * traceDiffuseCone(position, s1, normal, view, roughness);
	res += w1 * traceDiffuseCone(position, s2, normal, view, roughness);
	res += w1 * traceDiffuseCone(position, s3, normal, view, roughness);
	res += w1 * traceDiffuseCone(position, s4, normal, view, roughness);

	res += w1 * traceDiffuseCone(position, ang1, normal, view, roughness);
	res += w1 * traceDiffuseCone(position, ang2, normal, view, roughness);
	res += w1 * traceDiffuseCone(position, ang3, normal, view, roughness);
	res += w1 * traceDiffuseCone(position, ang4, normal, view, roughness);

	//res /= 3.1428;
	//res /= 2.0;

	//res *= 2.0;

	return res;

}

float ambientOcclusion(vec3 position, vec3 normal){
	vec3 ort1 = basisVec(normal);
	vec3 ort2 = normalize(cross(normal, ort1));
	vec3 cor1 = (ort1 + ort2) / 2.0;
	vec3 cor2 = (ort1 - ort2) / 2.0;

	vec3 ang1 = (normal + ort1) / 2.0;
	vec3 ang2 = (normal + ort2) / 2.0;
	vec3 ang3 = (normal - ort1) / 2.0;
	vec3 ang4 = (normal - ort2) / 2.0;

	vec3 s1 = (normal + cor1) / 2.0;
	vec3 s2 = (normal + cor2) / 2.0;
	vec3 s3 = (normal - cor1) / 2.0;
	vec3 s4 = (normal - cor2) / 2.0;

	float res = 0; //vec4(0);

	res += traceAOCone(position, normal, normal);

	res += traceAOCone(position, s1, normal);
	res += traceAOCone(position, s2, normal);
	res += traceAOCone(position, s3, normal);
	res += traceAOCone(position, s4, normal);

	res += traceAOCone(position, ang1, normal);
	res += traceAOCone(position, ang2, normal);
	res += traceAOCone(position, ang3, normal);
	res += traceAOCone(position, ang4, normal);

	res = saturate(1.0 - res) * 1.0;

	return res;
}

float FCookTorrance(float metallness, float VoH){
  float c = VoH;
  float Kmetallic = sqrt(metallness);
  float n = (1.0 + Kmetallic) / (1.0 - Kmetallic);
  float g = sqrt(n * n - 1.0 + c * c);

  float gMc  = g - c;
  float gPc  = g + c;
  float gMc1 = gMc * c + 1.0;
  float gPc1 = gPc * c - 1.0;
  float factor0 = (gMc * gMc) / (gPc * gPc);
  float factor1 = 1 + (gPc1 * gPc1) / (gMc1 * gMc1);

  return 0.5 * factor0 * factor1;
}

float FSphericalGaussian(float metallness, float VoH){
  return metallness + (1.0 - metallness) * pow(2.0, (-5.55473 * VoH - 6.98316) * VoH);
}

float F_Schlick(float f0, float f90, float LdotH){
	//f90 = 1.0 - f0;
	return saturate(f0 + (1.0 - f0) * pow(2, -5.55473 * LdotH - 6.98316 * LdotH ));
}

vec3 integrateApprox(vec3 specular, float roughness, float NoV){
	const vec4 c0 = vec4( -1, -0.0275, -0.572, 0.022 );
	const vec4 c1 = vec4( 1, -0.04, 1.04, -0.04 );
	vec4 r = roughness * c0 + c1;
	float a004 = min( r.x * r.x, exp2( -9.28 * NoV ) ) * r.x + r.y;
	vec2 AB = vec2( -1.04, 1.04 ) * a004 + r.zw;
	return specular * AB.x  + AB.y;
}

vec4 reflectionIllumination(vec3 position, vec3 normal, vec3 view, float roughness, float metallness, float rfr){
	//vec3 ort1 = basisVec(normal);
	//vec3 ort2 = normalize(cross(normal, ort1));

	vec3 dir = refract(-view, normal, rfr);
	if(rfr < 0) dir = reflect(-view, normal);
	vec3 h = normalize(dir + view);
	float VoH = dot(-view, h);
	float fresnel = FSphericalGaussian(metallness, VoH);
	fresnel += 0.01;
	fresnel = clamp(fresnel, 0.0, 1.0);
	
	vec4 reflectionColor = vec4(traceSpecularCone(position, dir, normal, roughness), 1.0);
	//vec3 envBRDF = integrateApprox(vec3(1.0), roughness, dot(normal, view));
	//reflectionColor *= vec4(envBRDF, 1.0);
	//reflectionColor *= fresnel;
	//reflectionColor *= (F_Schlick((metallness + 0.02),0.67, -VoH));

	return reflectionColor;
}

vec3 hardShadows(vec3 position, vec3 normal){
	vec3 ort1 = basisVec(normal);
	vec3 ort2 = normalize(cross(normal, ort1));

	vec3 cor1 = (ort1 + ort2) / 2.0;
	vec3 cor2 = (ort1 - ort2) / 2.0;
	
	float VOXEL_SIZE = 1.0 / float(gridSize);
	float FALLOFF = 0.6;
	float SHADOW_COEF = 0.5;

	vec3 res = vec3(0.0);
	float ao = 0.0;

	position /= 5.0;
	position += normal * VOXEL_SIZE * 2.0;

	float divideCoef = float(rectangleLightCount);

	float sumLumiance = 0.0001;

	for(int i=0; i<rectangleLightCount; ++i){
		RectangleLight light = rectangleLights[i];
		sumLumiance += light.intensity / (light.width * light.height * M_PI);
	}

	for(int i=0; i<sphereLightCount; ++i){
		//trace 1 ray(while better solution not found)
		SphereLight light = sphereLights[i];
		sumLumiance += light.intensity / (2.0 * light.radius * M_PI);
	}

	for(int i=0; i<rectangleLightCount; ++i){
		//trace 1 ray(while better solution not found)
		RectangleLight light = rectangleLights[i];

		float maxDist = length(light.position/5.0 - position) - 16 * VOXEL_SIZE;
		float lumiance = light.intensity / (light.width * light.height * M_PI);
		float lumCoef = 1.0 / sumLumiance * lumiance;

		const int NUM_SAMPLES = 3;
		const int SAMPLE_SCALE = 2;
		for(int j=0; j<NUM_SAMPLES; ++j){
			vec2 pn = hammersley(j*SAMPLE_SCALE, NUM_SAMPLES*SAMPLE_SCALE);
			pn -= 0.5;
			vec3 pp = light.position + vec3(light.matrix * vec4(light.width * pn.x, 0, light.height * pn.y, 1.0));
			vec3 endPos = pp / 5.0;
			vec3 dir = normalize(endPos - position);

			float dist = VOXEL_SIZE * 3.0;
			while(dist < maxDist){
				vec3 c = position + dist * dir;
				if(!isInsideCube(c, 0.1)) break;
				vec3 e = scaleAndBias(c); 

				float l = pow(dist, FALLOFF); // Experimenting with inverse square falloff for shadows.
				float a1 = textureLod(voxelTexture, e, 1 + 0.75 * l).a;
				float a2 = textureLod(voxelTexture, e, 1 + 0.75 * l).a;
				float s1 = SHADOW_COEF * 0.062 * textureLod(voxelTexture, e, 1 + 0.75 * l).a / float(NUM_SAMPLES) * lumCoef;
				float s2 = SHADOW_COEF * 0.135 * textureLod(voxelTexture, e, 4.5 * l).a / float(NUM_SAMPLES) * lumCoef;
				float s = s1 + s2;
				ao += (1.0 - ao) * s;
				dist += 0.9 * VOXEL_SIZE * (1 + 0.05 * l);
			}
		}
	}

	for(int i=0; i<sphereLightCount; ++i){
		//trace 1 ray(while better solution not found)
		SphereLight light = sphereLights[i];
		vec3 dir = normalize(light.position/5.0 - position);

		float maxDist = length(light.position/5.0 - position) - 16 * VOXEL_SIZE;
		float dist = VOXEL_SIZE * 3.0;

		float lumiance = light.intensity / (2.0 * light.radius * M_PI);
		float lumCoef = 1.0 / sumLumiance * lumiance;

		while(dist < maxDist){
			vec3 c = position + dist * dir;
			if(!isInsideCube(c, 0.1)) break;
			vec3 e = scaleAndBias(c); 

			float l = pow(dist, FALLOFF); // Experimenting with inverse square falloff for shadows.
			float a1 = textureLod(voxelTexture, e, 1 + 0.75 * l).a;
			float a2 = textureLod(voxelTexture, e, 1 + 0.75 * l).a;
			float s1 = SHADOW_COEF * 0.062 * textureLod(voxelTexture, e, 1 + 0.75 * l).a * lumCoef;
			float s2 = SHADOW_COEF * 0.135 * textureLod(voxelTexture, e, 4.5 * l).a * lumCoef;
			float s = s1 + s2;
			ao += (1.0 - ao) * s;
			dist += 0.9 * VOXEL_SIZE * (1 + 0.05 * l);
		}

	}

	//ao /= 1.1;

	ao = 1.0 - ao;
	//ao = ao > 0.15 ? ao : ao / 5.0;
	//if(ao > 0.05 && ao < 0.2) ao /= 3.0;
	ao = saturate(ao);

	res = vec3(pow(ao, 1.0));

	return res;
}

vec4 refractionIllumination(vec3 position, vec3 normal, vec3 view, float alpha, float ior){
	//view = -(refract(-view, normal, 0.85), normal);
	if(alpha == 1.0) return vec4(0);
	return (1.0 - alpha) * reflectionIllumination( position, view, normal, saturate(1.0 - alpha), 0.0, 1.01 / ior );
}

vec3 rayPlaneIntersect(vec3 planeNormal, vec3 planePosition, vec3 rayDir, vec3 rayPosition){
	float k = (dot(planeNormal, planePosition - rayPosition)) / dot(planeNormal, rayDir);
	return rayPosition + rayDir * k;
}

vec3 closestPoint(vec3 point, vec3 lightPos, vec3 lightUp, vec3 lightRight, float halfWidth, float halfHeight){
	vec3 dir = point - lightPos;
	vec2 dist2D = vec2( dot(dir, lightRight), dot(dir, lightUp));
	vec2 rectHalfSize = vec2(halfWidth, halfHeight);
	dist2D = clamp( dist2D, -rectHalfSize, rectHalfSize );
	return lightPos + dist2D.x * lightRight + dist2D.y * lightUp;
}

float rectangleSolidAngle(vec3 worldPos, vec3 p0, vec3 p1, vec3 p2, vec3 p3){
	vec3 v0 = p0 - worldPos;
	vec3 v1 = p1 - worldPos;
	vec3 v2 = p2 - worldPos;
	vec3 v3 = p3 - worldPos;
	vec3 n0 = normalize(cross(v0 , v1));
	vec3 n1 = normalize(cross(v1 , v2));
	vec3 n2 = normalize(cross(v2 , v3));
	vec3 n3 = normalize(cross(v3 , v0));

	float g0 = acos(dot(-n0 , n1));
	float g1 = acos(dot(-n1 , n2));
	float g2 = acos(dot(-n2 , n3));
	float g3 = acos(dot(-n3 , n0));

	return g0 + g1 + g2 + g3 - 2 * M_PI ;
}
vec3 computeRectangleLight2(vec3 fragPos, vec3 normal, vec3 albedo, RectangleLight light){

	float lumiance = light.intensity / (light.width * light.height * M_PI);
	vec3 lightNormal = normalize(vec3(light.matrix * vec4(0, 1, 0, 1)));
	vec3 lightUp = normalize(vec3(light.matrix * vec4(0, 0, 1, 1)));
	vec3 lightRight = normalize(vec3(light.matrix * vec4(1, 0, 0, 1)));
	float halfWidth = light.width / 2;
	float halfHeight = light.height / 2;
	float lightDist = length(light.position - fragPos);
	
	if(dot(fragPos - light.position, lightNormal) > 0){
		float clampCosAngle = 0.001 + saturate(dot(normal, lightNormal));
		vec3 d0 = normalize(-lightNormal + normal * clampCosAngle);
		vec3 d1 = normalize(normal - normal * clampCosAngle);
		vec3 dh = normalize(d0 + d1);
		vec3 ph = rayPlaneIntersect(lightNormal, light.position, dh, fragPos);
		ph = closestPoint(ph, light.position, lightUp, lightRight, halfWidth, halfHeight);

		vec3 pph = normalize(ph - fragPos);

		vec3 p0 = light.position + lightRight * halfWidth + lightUp * halfHeight;
		vec3 p1 = light.position + lightRight * halfWidth - lightUp * halfHeight;
		vec3 p2 = light.position - lightRight * halfWidth - lightUp * halfHeight;
		vec3 p3 = light.position - lightRight * halfWidth + lightUp * halfHeight;

		float solidAngle = rectangleSolidAngle(fragPos, p0, p1, p2, p3);

		float illumiance = solidAngle * max(0.0, dot(normal, pph)) * lumiance;
		illumiance *= getDistanceAtt(lightDist * lightDist, 1.0 / light.intensity );

		return albedo * vec4(light.color, 1.0) * illumiance;

	}

	return vec3(0);
}

vec4 computeSphereLight(vec3 fragPos, vec3 normal, vec4 albedo, SphereLight light){

	vec4 res;

	float lumiance = light.intensity / (2.0 * M_PI * light.radius);

	vec3 lightVec = (light.position - fragPos);
	float lightDist = length(lightVec);
	vec3 lightDir = lightVec / lightDist;
	float sqrLightDist = lightDist * lightDist;
	float invSqrAtt = 1.0 / max(sqrLightDist, 0.001);

	float attenuation = 1.0 / pow( lightDist / light.radius + 1.0, 1.2 ); 
	//saturate(light.radius / lightDist) / (sqrLightDist + 1.0); //pow(saturate(1.0 - pow(lightDist / light.radius, 4.0)), 2.0) / (sqrLightDist + 1.0);

	float Beta = acos(dot(normal, lightDir));
    float H = lightDist;
    float h = H / light.radius;
    float x = sqrt(h * h - 1.0);
    float y = -x / tan(Beta);

    float illuminance = 0;
    if (h * cos(Beta) > 1){
		illuminance = cos(Beta) / (h * h);
    }
    else{
		illuminance = (1.0 / (M_PI * h * h)) *
		(cos(Beta) * acos(y) - x * sin(Beta) * sqrt(1.0 - y * y)) +
		atan(sin(Beta) * sqrt(1.0 - y * y) / x) / M_PI;
    }
    illuminance *= M_PI;

	res = vec4(light.color, 1.0) * albedo * lumiance * max(illuminance * attenuation, 0.0);

	return res;
}

vec4 calcSpecularRectangleLight(RectangleLight l){
	return vec4(0);
}

float calcSpecularOcclustion(vec3 n, vec3 v, float ao, float roughness){
	float NdoV = dot(n, v);
	return saturate(pow( NdoV + ao , exp2(-16.0 * roughness - 1.0)) - 1.0 + ao);
}

float brdfOrenNayarDiffuse(
	float LoV, float LoN, float VoN,
	float roughness
){

	float result = saturate(LoN);
	float soft_rim = saturate(1-VoN/2);
	float fakey = pow(1-result*soft_rim,2);
	float fakey_magic = 0.62;
	fakey = fakey_magic - fakey*fakey_magic;
	return mix(result, fakey, roughness);
}

float brdfDiffuse(float LoV, float NoL, float NoV, float roughness){
	return brdfOrenNayarDiffuse(LoV, NoL, NoV, roughness);
}

float brdfFresnelSchlickF0(float LoH, float roughness, float f0) {
    return f0 + (1 - f0) * pow(2, (-5.55473 * LoH - 6.98316) * LoH);
}

float brdfDistributionGGX(float NoH, float alpha) {
    float rSquare = alpha * alpha;
    float f = (NoH * rSquare - NoH) * NoH + 1.0;
    return rSquare / (f * f);
}

float brdfDistributionTrowbridgeReitz(float NoH, float r){
	float a = r * r;
	return (a*a) / (M_PI * pow(NoH * NoH * (a * a -1) + 1, 2.0));
}

float brdfGeometryCookTorrance(float NoL, float NoV, float NoH, float VoH) {
    float nh_by_vh = 2.0 * NoH / VoH;
    float eq_nv = NoV * nh_by_vh;
    float eq_nl = NoL * nh_by_vh;
    return min(1.0, min(eq_nv, eq_nl));
}

float brdfGeometrySmithGGX(float NoL, float NoV, float roughness) {
    float alpha = roughness * roughness;
    float lambdaGGXV = NoL * sqrt((-NoV * alpha + NoV) * NoV + alpha);
    float lambdaGGXL = NoV * sqrt((-NoL * alpha + NoL) * NoL + alpha);
    return 0.5 / (lambdaGGXV + lambdaGGXL) * NoL;
}

float brdfGeometrySchlickG1(float NoX, float k){
	return (NoX) / (NoX * (1-k) + k);
}

float brdfGeometrySchlickG(float NoL, float NoV, float r){
	float k = pow(r + 1, 2.0) / 8.0;
	return brdfGeometrySchlickG1(NoL, k) * brdfGeometrySchlickG1(NoV, k);
}

//BRDF components
float brdfG(float NoL, float NoV, float roughness){
	//return brdfGeometrySmithGGX(NoL, NoV, roughness);
	return brdfGeometrySchlickG(NoL, NoV, roughness);
}

float brdfF(float LoH, float roughness, float f0){
	return brdfFresnelSchlickF0(LoH, roughness, f0);
}

float brdfD(float NoH, float roughness){
	NoH = max(1e-5, NoH);
    roughness = max(0.0019, roughness);
	//return brdfDistributionGGX(NoH, roughness);
	return brdfDistributionTrowbridgeReitz(NoH, roughness);
}

float getF0(float ior) {
	float AIR_IOR = 1.0;
	float f0 = (ior - AIR_IOR) / (ior + AIR_IOR);
    return (f0);
	//return mix(vec3(0), vec3(albedo), metallness);
}

//Common function for commpute whole simple lights

vec4 applyLight(vec4 albedo, vec3 normal, float roughness, float metallness, float ior,
	vec3 view, vec3 l, vec3 color, float attenuation
){
		vec3 nL = normalize(l);
		vec3 h = normalize(view + nL);
		float NoL = saturate(dot(normal, nL));
		float NoV = saturate(dot(normal, view));
		float NoH = saturate(dot(normal, h));
		float LoH = saturate(dot(nL, h));
		float LoV = saturate(dot(nL, view));
		float f0 = getF0(ior);

		NoL = max(1e-5, NoL);
		NoV = max(1e-5, NoV);

		vec4 res = brdfDiffuse(LoV, NoL, NoV, roughness) * albedo * (1.0 - metallness);

		res += brdfD(NoH, roughness) * brdfG(NoL, NoV, roughness) * brdfF(LoH, roughness, f0) / (4.0 * NoL * NoV) * vec4(1.0);
		
		res = res * vec4(color, 1.0) * attenuation * NoL;
		
		res = clamp(res, vec4(0), vec4(1));

		return res;
}

//Attenuation functions

// Computes the quadratic attenuation curve
float attenuationCurve(float distSquare, float radius) {
	float factor = distSquare / (radius * radius);
    float smoothFactor = saturate(1.0 - factor * factor);
    return smoothFactor * smoothFactor / max(0.01 * 0.01, distSquare);
}

// Computes the attenuation for a spot light
float getSpotLightAttenuation(vec3 l, vec3 lightDir, float fov, float radius, float distSq) {
    float distAttenuation = attenuationCurve(distSq, radius);
    float cosAngle = dot(l, -lightDir);
    float linearAngle = (cosAngle - fov) / (1 - fov);
    float angleAtt = saturate(linearAngle);
    return angleAtt * angleAtt * distAttenuation;
}

vec4 computeSpotLight(
	vec4 albedo, vec3 pos, vec3 normal, float roughness, float metallness, float ior,
	SpotLight light, vec3 view){

	vec3 position = light.position;
    float radius = light.radius;
    float fov = light.fov;
    vec3 direction = light.direction;
    vec3 l = light.position - pos; //pos - light.position;
	vec3 lNorm = normalize(l);
	vec3 color = light.color;

	float attenuation = 1.0 * getSpotLightAttenuation(
        lNorm, direction, fov, radius, dot(l, l));

	return applyLight(albedo, normal, roughness, metallness, ior, view, lNorm, color, attenuation) * light.intensity;

}

void main(){
	
	vec4 albedo = texture(albedos, cords);
	vec3 normal = normalize(texture(normals, cords).rgb * 2.0 - 1.0);
	normal *= vec3(1, 1, 1);
	float ao = texture(aoTexture, cords).a;
	float roughness = texture(normals, cords).a;
	vec3 position = texture(positions, cords).rgb;
	float metallness = texture(positions, cords).a;
	vec4 type = texture(types, cords);
	vec4 boundingBox = texture(boundingBoxes, cords);
	vec4 final = texture(finals, cords);
	float depth = texture(depths, cords).r;
	vec3 vp = viewPosition;
	int s = state;
	int vws = voxelWorldSize;
	float ior = type.z;

	vec3 view = normalize(viewPosition - position);
	//normal = aNorm(position, normal);

	if(type.y == 2.0){

		vec4 diffuse = albedo * vec4(inderectIllumination(position, normal, view, roughness));
		vec4 specular = albedo * vec4(reflectionIllumination(position, normal, view, roughness, metallness, -1.0));
		specular += refractionIllumination(position, normal, view, albedo.a, ior);
		float voxelAO = ambientOcclusion(position, normal);

		//diffuse *= 0.2;
		
		/*for(int i=0; i<rectangleLightCount; ++i)
		   diffuse += vec4(computeRectangleLight2(position, normal, vec3(albedo), rectangleLights[i]), 1.0) * 0.3;

		for(int i=0; i<sphereLightCount; ++i)
			diffuse += computeSphereLight(position, normal, vec4(albedo), sphereLights[i]) * 0.3;*/

		diffuse *= 1.5;

		//diffuse *= 1.0;
		specular *= 2.0; //* calcSpecularOcclustion(normal, -view, ao, roughness);

		//diffuse *= pow(ao, 4.0) * voxelAO;

		vec4 dielectric = diffuse + specular;
		vec4 metal = diffuse * specular;
		float blendCoef = smoothstep(0.15, 0.45, metallness);
		//vec4 final = mix(dielectric, metal, blendCoef);

		final = diffuse * (1.0 - metallness) + specular;

		//final = clamp(final, vec4(0), vec4(1));

		final *= 0.6;
		final = pow(final, vec4(1.2));
		final *= 1.0;
		
		for(int i=0; i<spotLightCount; ++i){
			final += computeSpotLight(albedo, position, normal, roughness, metallness, ior, spotLights[i], view);
		}
		//final = vec4(1.0);
		final *= pow(ao, 1.0) * pow(voxelAO, 1.0);
		FragColor = final;

		//FragColor = mix(diffuse, specular*3.0, smoothstep( 0.1, 0.9, (1.0-roughness) ));

		//FragColor = specular;

	}else{
		FragColor = vec4(albedo);
	}

	


	//FragColor = FragColor / (FragColor + 1.0);
	
	//FragColor = FragColor / (FragColor + 1.0);
	//FragColor = pow(FragColor, vec4(1.0/2.2));
	
	//FragColor = pow(FragColor, vec4(1.0/1.45));
	
	/*vec3 x = max(vec3(0.0), FragColor.xyz - 0.004);
    x = (x * (6.2 * x + 0.5)) / (x * (6.2 * x + 1.7) + 0.06);*/

	vec3 x = FragColor.rgb;
    x = exp(-1.0 / (2.72 * x + 0.15));
	x = pow(x, vec3(1.0 / 2.2));

	FragColor = vec4(x, 1.0);
	//FragColor += 10.0;

	//FragColor = albedo;

}