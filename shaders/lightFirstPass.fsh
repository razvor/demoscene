//#version 410

uniform sampler2D albedos;
uniform sampler2D normals;
uniform sampler2D positions;
uniform sampler2D types;
uniform sampler2D boundingBoxes;
uniform sampler2D depths;
uniform sampler2D finals;
uniform vec3 viewPosition;
uniform int state;
uniform int pointLightCount;
uniform int sphereLightCount;
uniform int rectangleLightCount;

uniform DirLight directionalLights[1];
uniform PointLight pointLights[10];
uniform SphereLight sphereLights[10];
uniform RectangleLight rectangleLights[10];

in vec2 cords;

layout(location=0) out vec4 FragColor;

vec3 calcLights(){
	vec3 r = vec3(0);

	r += calcDirectionalLight(directionalLights[0]) * 0.0;
	for(int i=0; i<pointLightCount; ++i){
		r += calcPointLight(pointLights[i]);
	}
	for(int i=0; i<sphereLightCount; ++i){
		r += calcSphereLight(sphereLights[i]) * 1.0;
		//vec3 ld = -normalize(globalData.fragPosition - sphereLights[i].position);
		//r += dot( ld, globalData.normal ) * globalData.color;
	}
	for(int i=0; i<rectangleLightCount; ++i){
		r += calcRectangleLight(rectangleLights[i])*1.0;
	}

	return r;
}

void main(){

	vec4 al = texture(albedos, cords);
	vec4 nr = texture(normals, cords);
	vec4 ps = texture(positions, cords);
	vec4 bb = texture(boundingBoxes, cords);
	vec4 ts = texture(types, cords);
	vec4 dp = texture(depths, cords);
	vec4 fn = texture(finals, cords);

	vec3 albedo = al.rgb;
	vec3 normal = nr.xyz;
	vec3 position = ps.xyz;
	vec3 bBox = bb.xyz;
	float roughness = nr.w;
	float metalness = ps.w;
	float depth = dp.r;
	float type = ts.r;

	globalData.normal = normalize(normal);
	globalData.fragPosition = position;
	globalData.metallness = metalness;
	globalData.roughness = roughness;
	globalData.color = albedo;
	globalData.viewPosition = viewPosition;
	//globalData.viewPosition = position + 1;

	vec3 f = vec3(0);

	if(type == 0){
		f = vec3(0.0);
	}else
	if(type==1){
		f = calcLights();
	}else
	if(type == 2){
		f = globalData.color;
	}
	//f = calcLights();

	FragColor = vec4(f * 1.0, 1.0);
	//:FragColor = vec4(albedo, 1.0);

	if(state == 2){
		//FragColor = vec4(albedo, 1.0);
	}

	if(state == 1){
		/*float xx = cords.x * 2.0 - 1.0;
		float yy = cords.y * 2.0 - 1.0;

		if(xx * xx + yy * yy > 1) 
			discard;*/

		//FragColor = vec4(0);
	}

}
