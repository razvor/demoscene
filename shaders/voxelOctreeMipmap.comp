#version 440

layout (local_size_x = 8, local_size_y = 8, local_size_z = 1 ) in;

uniform int voxelsCount;
uniform int mip;
uniform int gridSize;

layout(binding=0, rgba16ui) uniform uimageBuffer voxelPosTex;
layout(binding=1, r32ui) uniform uimageBuffer octreeChild;
layout(binding=2, r32ui) uniform uimageBuffer octreeDiffuse;

uint convVec4ToRGBA8( vec4 val );
vec4 convRGBA8ToVec4( uint val );

void main(){
	
	uint thxId = gl_GlobalInvocationID.y * 1024 + gl_GlobalInvocationID.x;
	if(thxId > voxelsCount) return;

	uvec3 loc = imageLoad(voxelPosTex, int(thxId)).xyz;
	uint voxelDim = gridSize;
	uint childInd = 0;
	int parInd, leafInd = 0;
	uint node, subnode;
	uvec3 umin = uvec3(0);
	uvec3 umax = uvec3(voxelDim);
	bool store = true;

	node = imageLoad(octreeChild, int(childInd)).r;

	for(int i=0; i<mip; ++i){
		if( (node & 0x80000000) == 0 ){
			store = false;
			break;
		}

		voxelDim /= 2;

		parInd = int(childInd);

		childInd = int(node & 0x7FFFFFFF);

		leafInd = int(childInd);

		subnode = (loc.x > voxelDim + umin.x - 1) ? 1 : 0;
		subnode += (loc.y > voxelDim + umin.y - 1) ? 2 : 0;
		subnode += (loc.z > voxelDim + umin.z - 1) ? 4 : 0;
	    childInd += int(subnode);
	    umin.x += voxelDim * ((loc.x > voxelDim + umin.x - 1) ? 1 : 0);
	    umin.y += voxelDim * ((loc.y > voxelDim + umin.y - 1) ? 1 : 0);
	    umin.z += voxelDim * ((loc.z > voxelDim + umin.z - 1) ? 1 : 0);

		node = imageLoad( octreeChild, int(childInd) ).r;
	}

	if(!store) return;

	//if(imageAtomicCompSwap(octreeDiffuse, parInd, 0, node) == node || true){

	vec4 newVal;
	for(int j=0; j<8; ++j){
		uint col = imageLoad(octreeDiffuse, int(leafInd + j)).r;
		if(col != 0){
			newVal += convRGBA8ToVec4(uint(col)) / 8.0;
		}
	}
	imageStore(octreeDiffuse, parInd, uvec4(convVec4ToRGBA8(newVal), 0, 0, 0));

	//}

}

vec4 convRGBA8ToVec4( in uint val ){
    return vec4( float( (val&0x000000FF) ), float( (val&0x0000FF00)>>8U),
	             float( (val&0x00FF0000)>>16U), float( (val&0xFF000000)>>24U) );
}

uint convVec4ToRGBA8( in vec4 val ){
    return ( uint(val.w)&0x000000FF)<<24U | (uint(val.z)&0x000000FF)<<16U | (uint(val.y)&0x000000FF)<<8U | (uint(val.x)&0x000000FF);
}