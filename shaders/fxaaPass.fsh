#version 440

uniform sampler2D albedos;
uniform sampler2D normals;
uniform sampler2D positions;
uniform sampler2D types;
uniform sampler2D boundingBoxes;
uniform sampler2D depths;
uniform sampler2D finals;
uniform vec3 viewPosition;
uniform int state;

in vec2 cords;

layout(location=0) out vec4 FragColor;

#ifndef FXAA_REDUCE_MIN
    #define FXAA_REDUCE_MIN   (1.0/ 128.0)
#endif
#ifndef FXAA_REDUCE_MUL
    #define FXAA_REDUCE_MUL   (1.0 / 8.0)
#endif
#ifndef FXAA_SPAN_MAX
    #define FXAA_SPAN_MAX     8.0
#endif

#ifndef FXAA_EDGE_THRESHOLD
	#define FXAA_EDGE_THRESHOLD (1.0 / 8.0)
#endif

#ifndef FXAA_EDGE_THRESHOLD_MIN
	#define FXAA_EDGE_THRESHOLD_MIN (1.0 / 32.0)
	#define FXAA_SUBPIX 2
	#define FXAA_SUBPIX_TRIM (1.0 / 8.0)
	#define FXAA_SUBPIX_CAP 0.01
	#define FXAA_SUBPIX_TRIM_SCALE 1
#endif

//optimized version for mobile, where dependent
//texture reads can be a bottleneck
vec4 fxaa(vec2 fragCoord, vec2 resolution,
            vec2 v_rgbNW, vec2 v_rgbNE, 
            vec2 v_rgbSW, vec2 v_rgbSE, 
            vec2 v_rgbM) {
    vec4 color;
    mediump vec2 inverseVP = vec2(1.0 / resolution.x, 1.0 / resolution.y);
    vec3 rgbNW = texture(finals, v_rgbNW).xyz;
    vec3 rgbNE = texture(finals, v_rgbNE).xyz;
    vec3 rgbSW = texture(finals, v_rgbSW).xyz;
    vec3 rgbSE = texture(finals, v_rgbSE).xyz;
    vec4 texColor = texture(finals, v_rgbM);
    vec3 rgbM  = texColor.xyz;
    vec3 luma = vec3(0.299, 0.587, 0.114);
    float lumaNW = dot(rgbNW, luma);
    float lumaNE = dot(rgbNE, luma);
    float lumaSW = dot(rgbSW, luma);
    float lumaSE = dot(rgbSE, luma);
    float lumaM  = dot(rgbM,  luma);
    float lumaMin = min(lumaM, min(min(lumaNW, lumaNE), min(lumaSW, lumaSE)));
    float lumaMax = max(lumaM, max(max(lumaNW, lumaNE), max(lumaSW, lumaSE)));
    
    mediump vec2 dir;
    dir.x = -((lumaNW + lumaNE) - (lumaSW + lumaSE));
    dir.y =  ((lumaNW + lumaSW) - (lumaNE + lumaSE));
    
    float dirReduce = max((lumaNW + lumaNE + lumaSW + lumaSE) *
                          (0.25 * FXAA_REDUCE_MUL), FXAA_REDUCE_MIN);
    
    float rcpDirMin = 1.0 / (min(abs(dir.x), abs(dir.y)) + dirReduce);
    dir = min(vec2(FXAA_SPAN_MAX, FXAA_SPAN_MAX),
              max(vec2(-FXAA_SPAN_MAX, -FXAA_SPAN_MAX),
              dir * rcpDirMin)) * inverseVP;
    
    vec3 rgbA = 0.5 * (
        texture(finals, fragCoord * inverseVP + dir * (1.0 / 3.0 - 0.5)).xyz +
        texture(finals, fragCoord * inverseVP + dir * (2.0 / 3.0 - 0.5)).xyz);
    vec3 rgbB = rgbA * 0.5 + 0.25 * (
        texture(finals, fragCoord * inverseVP + dir * -0.5).xyz +
        texture(finals, fragCoord * inverseVP + dir * 0.5).xyz);

    float lumaB = dot(rgbB, luma);
    if ((lumaB < lumaMin) || (lumaB > lumaMax))
        color = vec4(rgbA, texColor.a);
    else
        color = vec4(rgbB, texColor.a);
    return color;
}

//Take luminance by spectral and imperiacally proved
float fxaaLuma(vec2 rgb) {
	return rgb.y * (0.587/0.299) + rgb.x; 
}

vec4 fxaa2(vec2 texCoords, vec2 resolution){
	//1. Take a luma
	vec2 pixel = vec2(1.0) / resolution;
	vec2 rgbN, rgbS, rgbW, rgbM, rgbE;
	float lumaN, lumaS, lumaW, lumaE, lumaM, lumaL, rangeL, 
	blendL, rgbL, rgbNW, rgbNE, rgbSW, rgbSE;
	rgbN = texture(finals, texCoords + pixel * vec2(0,-1)).xy;
	rgbS = texture(finals, texCoords + pixel * vec2(0, 1)).xy;
	rgbW = texture(finals, texCoords + pixel * vec2(1,0)).xy;
	rgbE = texture(finals, texCoords + pixel * vec2(-1,0)).xy;
	rgbM = texture(finals, texCoords).xy;
	lumaN = fxaaLuma(rgbN);
	lumaS = fxaaLuma(rgbS);
	lumaW = fxaaLuma(rgbW);
	lumaE = fxaaLuma(rgbE);
	lumaM = fxaaLuma(rgbM);

	float rangeMin = min(lumaM, min(min(lumaN, lumaW), min(lumaS, lumaE)));
	float rangeMax = max(lumaM, max(max(lumaN, lumaW), max(lumaS, lumaE)));
	float range = rangeMax - rangeMin;
	if(range < max(FXAA_EDGE_THRESHOLD_MIN, rangeMax * FXAA_EDGE_THRESHOLD)){
		//No alias
		//return FxaaFilterReturn(rgbM);
	}

	lumaL = (lumaN + lumaW + lumaE + lumaS) * 0.25;
	rangeL = abs(lumaL - lumaM);
	blendL = max(0.0, (rangeL / range) - FXAA_SUBPIX_TRIM) * FXAA_SUBPIX_TRIM_SCALE;
	blendL = min(FXAA_SUBPIX_CAP, blendL);

	/*rgbL = rgbN + rgbW + rgbM + rgbE + rgbS;
	rgbNW = texture(finals, texCoords + pixel * vec2(-1,-1)).xy;
	rgbNE = texture(finals, texCoords + pixel * vec2(1,-1)).xy;
	rgbSW = texture(finals, texCoords + pixel * vec2(-1,1)).xy;
	rgbSE = texture(finals, texCoords + pixel * vec2(1,1)).xy;
	rgbL += (rgbNW + rgbNE + rgbSW + rgbSE);
	rgbL *= 1.0 / 9.0;*/
	
	return vec4(0);
}

void texcoords(vec2 fragCoord, vec2 resolution,
			out vec2 v_rgbNW, out vec2 v_rgbNE,
			out vec2 v_rgbSW, out vec2 v_rgbSE,
			out vec2 v_rgbM) {
	vec2 inverseVP = 1.0 / resolution.xy;
	v_rgbNW = (fragCoord + vec2(-1.0, -1.0)) * inverseVP;
	v_rgbNE = (fragCoord + vec2(1.0, -1.0)) * inverseVP;
	v_rgbSW = (fragCoord + vec2(-1.0, 1.0)) * inverseVP;
	v_rgbSE = (fragCoord + vec2(1.0, 1.0)) * inverseVP;
	v_rgbM = vec2(fragCoord * inverseVP);
}

void main(){

	vec4 albedo = texture(albedos, cords);
	vec4 normal = texture(normals, cords);
	vec4 position = texture(positions, cords);
	vec4 bbs = texture(boundingBoxes, cords);
	vec4 type = texture(types, cords);
	vec4 depth = texture(depths, cords);
	
	mediump vec2 v_rgbNW;
	mediump vec2 v_rgbNE;
	mediump vec2 v_rgbSW;
	mediump vec2 v_rgbSE;
	mediump vec2 v_rgbM;

	vec2 resolution = vec2(800*2, 700*2);

	texcoords(cords * resolution, resolution, v_rgbNW, v_rgbNE, v_rgbSW, v_rgbSE, v_rgbM);

	FragColor = fxaa(cords * resolution, resolution, v_rgbNW, v_rgbNE, v_rgbSW, v_rgbSE, v_rgbM); //texture(finals, cords) * vec4(0.1, 0.4, 0.5, 1.0);
	//FragColor = FragColor - 1;

}