#version 410

uniform sampler2D ltcMat;
//uniform samplerCube LCM;

struct DirLight{
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct PointLight{
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float constant;
	float linear;
	float quadratic;
};

struct SphereLight{
	vec3 position;
	vec3 color;
	float intensity;
	float radius;
};

struct RectangleLight{
	vec3 position;
	vec3 color;
	float width;
	float height;
	float intensity;
	mat4 matrix;
};

struct GlobData{
	vec3 color;
	float roughness;
	float metallness;
	vec3 fragPosition;
	vec3 normal;
	vec3 viewPosition;
	vec3 bBoxSize;
	vec3 centerPosition;
	int state;
};
GlobData globalData;

//float PI = 3.1415;
float PI = 3.1415926535807932384;

float saturate(float s){
	return clamp(s, 0.0, 1.0);
}

vec2 hammersley(uint originalSample){

	uint u_m = 6;
	float bff = 1.0 / pow(2.0, float(u_m));
	uint revertSample;

	// Revert bits by swapping blockwise. Lower bits are moved up and higher bits down.
	revertSample = (originalSample << 16u) | (originalSample >> 16u);
	revertSample = ((revertSample & 0x00ff00ffu) << 8u) | ((revertSample & 0xff00ff00u) >> 8u);
	revertSample = ((revertSample & 0x0f0f0f0fu) << 4u) | ((revertSample & 0xf0f0f0f0u) >> 4u);
	revertSample = ((revertSample & 0x33333333u) << 2u) | ((revertSample & 0xccccccccu) >> 2u);
	revertSample = ((revertSample & 0x55555555u) << 1u) | ((revertSample & 0xaaaaaaaau) >> 1u);

	// Shift back, as only m bits are used.
	revertSample = revertSample >> (32 - u_m);

	return vec2(float(revertSample) * bff, float(originalSample) * bff);

}

float radicalInverse_VdC(uint bits) {
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

vec2 hammersley2d(uint i, uint N) {
	return vec2( float(i) / float(N), radicalInverse_VdC(i) );
}

vec3 hemisphereSampleUniform(float u, float v) {
	float phi = v * 2.0 * PI;
	float cosTheta = 1.0 - u;
	float sinTheta = sqrt(1.0 - cosTheta * cosTheta);
	return vec3(cos(phi) * sinTheta, sin(phi) * sinTheta, cosTheta);
}

vec3 hemispherePoint(int i, int N){
	vec2 h = hammersley2d(i, N);
	return hemisphereSampleUniform(h.x, h.y);
}

vec3 FSchlick(vec3 f0, float f90, float u){
	return f0 + (f90 - f0) * pow(1.0 - u, 5.0);
}

float getDisneyDiffuse(
	float NdotV, 
	float NdotL, 
	float LdotH, 
	float linearRoughness,
	vec3 f0
){
	float energyBias = mix(0, 0.5, linearRoughness);
	float energyFactor = mix(1.0, 1.0 / 1.51, linearRoughness);
	float fd90 = energyBias + 2.0 * LdotH * LdotH * linearRoughness;
	float lightScatter = FSchlick(f0, fd90, NdotL).r;
	float viewScatter = FSchlick(f0, fd90, NdotV).r;

	return lightScatter * viewScatter * energyFactor / 3.14;
}

float getUESpecular(
	float NdotV,
	float NdotL,
	float NdotH,
	float VdotH,
	float roughness,
	float f0
){

	float D, G;
	float F;
	
	//D:
	float alpha  = pow( roughness, 2.0 );
	float alpha2 = pow(alpha, 2.0);
	float dlk = pow(pow(NdotH, 2.0) * (alpha2 - 1.0) + 1.0, 2.0);
	D = (alpha2)/(3.14 * dlk);

	//G:
	float k = pow(alpha + 1, 2.0) / (8.0);
	float g1l = (NdotL)/((NdotL)*(1.0 - k) + k);
	float g1v = (NdotV)/((NdotV)*(1.0 - k) + k);
	G = g1l * g1v;

	//F:
	float pow2 = (-5.5547 * VdotH - 6.9831) * VdotH;
	F = f0 + (1 - f0) * pow(2.0, pow2);

	return (D*G*F) / (3.14 * NdotL * NdotV);
}

float F1(float VoH, float m){
	//float Kmetallic = m;
	//return Kmetallic + (1.0 - Kmetallic) * pow(1.0 - VoH, 5.0);

	float c = VoH;
	float Kmetallic = sqrt(m);
	float n = (1.0 + Kmetallic) / (1.0 - Kmetallic);
	float g = sqrt(n * n - 1.0 + c * c);

	// Helpers
	float gMc  = g - c;
	float gPc  = g + c;
	float gMc1 = gMc * c + 1.0;
	float gPc1 = gPc * c - 1.0;
	float factor0 = (gMc * gMc) / (gPc * gPc);
	float factor1 = 1 + (gPc1 * gPc1) / (gMc1 * gMc1);

	// End Result
	return 0.5 * factor0 * factor1;
}

float GSmithGgx_(float NoV, float r){
	float numerator = 2.0 * NoV;
	float NoV2 = NoV * NoV;
	float Krough2 = r * r;
	float denominator = NoV + sqrt(NoV2 + Krough2 * (1.0 - NoV2));

	return numerator / denominator;
}

float G1(float NoL, float NoV, float NoH, float VoH, float r){
	return GSmithGgx_(NoL, r) * GSmithGgx_(NoV, r);
}

float D1(float NoH, float r){
	float Krough2 = r * r;
	float NoH2 = NoH * NoH;
	float denom = 1.0 + NoH2 * (Krough2 - 1.0);
	return Krough2 / (3.14 * denom * denom);
}

float Pdf(float NoL, float NoV){
	return (4.0 * NoL * NoV);
}

float getSpecularAA(float NoL, float NoV, float NoH, float VoH, float m, float r){
  return F1(VoH, m) * G1(NoL, NoV, NoH, VoH, r) * D1(NoH, r) / Pdf(NoL, NoV);

}

vec3 BlendDielectric(vec3 Kdiff, vec3 Kspec, vec3 Kbase){
	return Kdiff + Kspec;
}

vec3 BlendMetal(vec3 Kdiff, vec3 Kspec, vec3 Kbase){
	return Kspec * Kbase;
}

vec3 blendMaterial(vec3 Kdiff, vec3 Kspec, vec3 Kbase, float metallness){
	float scRange = smoothstep(0.25, 0.45, metallness);
	vec3  dielectric = BlendDielectric(Kdiff, Kspec, Kbase);
	vec3  metal = BlendMetal(Kdiff, Kspec, Kbase);
	return mix(dielectric, metal, scRange);
}

vec3 getSpecularDominantDirArea(vec3 N, vec3 R, float Krough){
	return normalize(mix(N, R, 1.0 - Krough));
}

float rectangleSolidAngle(vec3 viewPos, vec3 p0, vec3 p1, vec3 p2 , vec3 p3){
	vec3 v0 = p0 - viewPos;
	vec3 v1 = p1 - viewPos;
	vec3 v2 = p2 - viewPos;
	vec3 v3 = p3 - viewPos;

	vec3 n0 = normalize( cross(v0, v1 ) );
	vec3 n1 = normalize( cross(v1, v2 ) );
	vec3 n2 = normalize( cross(v2, v3 ) );
	vec3 n3 = normalize( cross(v3, v0 ) );

	float g0 = acos( dot(-n0, n1 ) );
	float g1 = acos( dot(-n1, n2 ) );
	float g2 = acos( dot(-n2, n3 ) );
	float g3 = acos( dot(-n3, n0 ) );

	return g0 + g1 + g2 + g3 - 2 * PI;
}

float smoothDistanceAtt(float squaredDistance, float invSqrAttRadius){
	float factor = squaredDistance * invSqrAttRadius;
	float smoothFactor = saturate(1.0 - factor * factor);
	return smoothFactor * smoothFactor;
}

float getDistanceAtt(float sqrDist, float invSqrAttRadius){
	float attenuation = 1.0 / max( sqrDist , 0.0001);
	attenuation *= smoothDistanceAtt(sqrDist, invSqrAttRadius);
	return attenuation;
}

vec3 rayPlaneIntersect(vec3 rayOrigin, vec3 rayDirection, vec3 planeOrigin, vec3 planeNormal){
	float dist = dot(planeNormal, planeOrigin - rayOrigin) /dot(planeNormal , rayDirection);
	return rayOrigin + rayDirection * dist;
}

vec3 closestPointRect(vec3 pos, vec3 planeOrigin, vec3 right, vec3 up, float hWidth, float hHeight){
	vec3 dir = pos - planeOrigin;
	vec2 dist2D = vec2(dot(dir, right), dot(dir, up));
	vec2 halfDim = vec2(hWidth, hHeight);
	dist2D = clamp(dist2D, -halfDim, halfDim);
	return planeOrigin + dist2D.x * right + dist2D.y * up;
}

vec3 smallestAnglePointRect(vec3 sNorm, vec3 pNorm, vec3 sPos, vec3 pRight, vec3 pUp, float hWidth, float hHeight, vec3 lightPos){
	float clampCosAngle = 0.001 + saturate(dot(sNorm, pNorm));
	// clamp d0 to the positive hemisphere of surface normal
	vec3 d0 = normalize(-pNorm + sNorm * clampCosAngle);
	// clamp d1 to the negative hemisphere of light plane normal
	vec3 d1 = normalize(sNorm - pNorm * clampCosAngle);
	vec3 dh = normalize(d0 + d1);
	vec3 ph = rayPlaneIntersect(sPos, dh, lightPos, pNorm) ;
	return closestPointRect(ph, lightPos, pRight, pUp, hWidth, hHeight);
}

float getPBRSpecularUnreal(
	vec3 lightDir, 
	vec3 eyeDire, 
	vec3 normal,
	float f0, 
	float roughness
){
	float final;
	float F, D, G;

	vec3 N = normalize(normal);
	vec3 V = normalize(eyeDire);
	vec3 L = normalize(lightDir);
	vec3 H = normalize(V + L);
	
	float NdotH = max( dot(N, H), 0.0 );
	float NdotV = max( dot(N, V), 0.0 );
	float NdotL = max( dot(N, L), 0.0 );
	float VdotH = max( dot(V, H), 0.0 );

	//D:
	float alpha  = pow( roughness, 2.0 );
	float alpha2 = pow(alpha, 2.0);
	float dlk = pow(pow(NdotH, 2.0) * (alpha2 - 1.0) + 1.0, 2.0);
	D = (alpha2)/(3.14 * dlk);

	//G:
	float k = pow(alpha + 1, 2.0) / (8.0);
	float g1l = (NdotL)/((NdotL)*(1.0 - k) + k);
	float g1v = (NdotV)/((NdotV)*(1.0 - k) + k);
	G = g1l * g1v;

	//F:
	float pow2 = (-5.5547 * VdotH - 6.9831) * VdotH;
	F = f0 + (1 - f0) * pow(2.0, pow2);

	final = (D*G*F) / (3.14 * NdotL * NdotV);

	return final;
}

float IntegrateEdge(vec3 v1, vec3 v2)
{
    float cosTheta = dot(v1, v2);
    cosTheta = clamp(cosTheta, -0.9999, 0.9999);

    float theta = acos(cosTheta);    
    float res = cross(v1, v2).z * theta / sin(theta);

    return res;
}

void ClipQuadToHorizon(inout vec3 L[5], out int n)
{
    // detect clipping config
    int config = 0;
    if (L[0].z > 0.0) config += 1;
    if (L[1].z > 0.0) config += 2;
    if (L[2].z > 0.0) config += 4;
    if (L[3].z > 0.0) config += 8;

    // clip
    n = 0;

    if (config == 0)
    {
        // clip all
    }
    else if (config == 1) // V1 clip V2 V3 V4
    {
        n = 3;
        L[1] = -L[1].z * L[0] + L[0].z * L[1];
        L[2] = -L[3].z * L[0] + L[0].z * L[3];
    }
    else if (config == 2) // V2 clip V1 V3 V4
    {
        n = 3;
        L[0] = -L[0].z * L[1] + L[1].z * L[0];
        L[2] = -L[2].z * L[1] + L[1].z * L[2];
    }
    else if (config == 3) // V1 V2 clip V3 V4
    {
        n = 4;
        L[2] = -L[2].z * L[1] + L[1].z * L[2];
        L[3] = -L[3].z * L[0] + L[0].z * L[3];
    }
    else if (config == 4) // V3 clip V1 V2 V4
    {
        n = 3;
        L[0] = -L[3].z * L[2] + L[2].z * L[3];
        L[1] = -L[1].z * L[2] + L[2].z * L[1];
    }
    else if (config == 5) // V1 V3 clip V2 V4) impossible
    {
        n = 0;
    }
    else if (config == 6) // V2 V3 clip V1 V4
    {
        n = 4;
        L[0] = -L[0].z * L[1] + L[1].z * L[0];
        L[3] = -L[3].z * L[2] + L[2].z * L[3];
    }
    else if (config == 7) // V1 V2 V3 clip V4
    {
        n = 5;
        L[4] = -L[3].z * L[0] + L[0].z * L[3];
        L[3] = -L[3].z * L[2] + L[2].z * L[3];
    }
    else if (config == 8) // V4 clip V1 V2 V3
    {
        n = 3;
        L[0] = -L[0].z * L[3] + L[3].z * L[0];
        L[1] = -L[2].z * L[3] + L[3].z * L[2];
        L[2] =  L[3];
    }
    else if (config == 9) // V1 V4 clip V2 V3
    {
        n = 4;
        L[1] = -L[1].z * L[0] + L[0].z * L[1];
        L[2] = -L[2].z * L[3] + L[3].z * L[2];
    }
    else if (config == 10) // V2 V4 clip V1 V3 impossible
    {
        n = 0;
    }
    else if (config == 11) // V1 V2 V4 clip V3
    {
        n = 5;
        L[4] = L[3];
        L[3] = -L[2].z * L[3] + L[3].z * L[2];
        L[2] = -L[2].z * L[1] + L[1].z * L[2];
    }
    else if (config == 12) // V3 V4 clip V1 V2
    {
        n = 4;
        L[1] = -L[1].z * L[2] + L[2].z * L[1];
        L[0] = -L[0].z * L[3] + L[3].z * L[0];
    }
    else if (config == 13) // V1 V3 V4 clip V2
    {
        n = 5;
        L[4] = L[3];
        L[3] = L[2];
        L[2] = -L[1].z * L[2] + L[2].z * L[1];
        L[1] = -L[1].z * L[0] + L[0].z * L[1];
    }
    else if (config == 14) // V2 V3 V4 clip V1
    {
        n = 5;
        L[4] = -L[0].z * L[3] + L[3].z * L[0];
        L[0] = -L[0].z * L[1] + L[1].z * L[0];
    }
    else if (config == 15) // V1 V2 V3 V4
    {
        n = 4;
    }
    
    if (n == 3)
        L[3] = L[0];
    if (n == 4)
        L[4] = L[0];
}

vec2 LTC_Coords(float cosTheta, float roughness){
    float theta = acos(cosTheta);
    vec2 coords = vec2(roughness, theta/(0.5*3.14159));

    const float LUT_SIZE = 64.0;
    // scale and bias coordinates, for correct filtered lookup
    coords = coords*(LUT_SIZE - 1.0)/LUT_SIZE + 0.5/LUT_SIZE;

    return coords;
}

mat3 LTC_Matrix(sampler2D texLSDMat, vec2 coord){
    // load inverse matrix
    vec4 t = texture(texLSDMat, coord);
    mat3 Minv = mat3(
        vec3(1,     0, t.y),
        vec3(  0, t.z,   0),
        vec3(t.w,   0, t.x)
    );
		Minv = transpose(Minv);

    return Minv;
}

vec3 LTC_Evaluate(
    vec3 N, vec3 V, vec3 P, mat3 Minv, vec4 points[4], bool twoSided)
{
    // construct orthonormal basis around N
    vec3 T1, T2;
    T1 = normalize(V - N*dot(V, N));
    T2 = cross(N, T1);

    // rotate area light in (T1, T2, R) basis
    //Minv = mul(Minv, mat3_from_rows(T1, T2, N));
		mat3 mm = transpose(mat3(T1, T2, N));
		//mm[0] = T1;
		//mm[1] = T2;
		//mm[2] = N;
		Minv = Minv * mm;

    // polygon (allocate 5 vertices for clipping)
    vec3 L[5];
    L[0] = Minv * (points[0].xyz - P);
    L[1] = Minv * (points[1].xyz - P);
    L[2] = Minv * (points[2].xyz - P);
    L[3] = Minv * (points[3].xyz - P);
    L[4] = L[3]; // avoid warning

    vec3 textureLight = vec3(1, 1, 1);
		//Not this time
		/*#if LTC_TEXTURED
    textureLight = FetchDiffuseFilteredTexture(texFilteredMap, L[0], L[1], L[2], L[3]);
		#endif*/

    int n;
    ClipQuadToHorizon(L, n);
    
    if (n == 0)
        return vec3(0, 0, 0);

    // project onto sphere
    L[0] = normalize(L[0]);
    L[1] = normalize(L[1]);
    L[2] = normalize(L[2]);
    L[3] = normalize(L[3]);
    L[4] = normalize(L[4]);

    // integrate
    float sum = 0.0;

    sum += IntegrateEdge(L[0], L[1]);
    sum += IntegrateEdge(L[1], L[2]);
    sum += IntegrateEdge(L[2], L[3]);
    if (n >= 4)
        sum += IntegrateEdge(L[3], L[4]);
    if (n == 5)
        sum += IntegrateEdge(L[4], L[0]);

    // note: negated due to winding order
    sum = twoSided ? abs(sum) : max(0.0, -sum);

    vec3 Lo_i = vec3(sum, sum, sum);

    // scale by filtered light color
    Lo_i *= textureLight;

    return Lo_i;
}

float getBRDFArea(vec3 V, vec3 L, float alpha, out float pdf){

	if(V.z <= 0)
	{
		pdf = 0;
		return 0;
	}

	// masking
	float a_V = 1.0 / alpha / tan(acos(V.z));
	float LambdaV = (V.z<1.0f) ? 0.5f * (-1.0f + sqrt(1.0f + 1.0f/a_V/a_V)) : 0.0f;
	float G1 = 1.0f / (1.0f + LambdaV);

	// shadowing
	float G2;
	if(L.z <= 0.0f)
		G2 = 0;
	else
	{
		float a_L = 1.0 / alpha / tan(acos(L.z));
		float LambdaL = (L.z<1.0) ? 0.5 * (-1.0 + sqrt(1.0 + 1.0/a_L/a_L)) : 0.0;
		G2 = 1.0 / (1.0 + LambdaV + LambdaL);
	}

	// D
	vec3 H = normalize(V+L);
	float slopex = H.x/H.z;
	float slopey = H.y/H.z;
	float D = 1.0 / (1.0 + (slopex*slopex+slopey*slopey)/alpha/alpha);
	D = D*D;
	D = D / (3.14159 * alpha * alpha * H.z*H.z*H.z*H.z);

	pdf = abs(D * H.z / 4.0 / dot(V,H));
	float res = D * G2 / 4.0 / V.z;

	return res;
}

bool QuadRayTest(vec4 q[4], vec3 pos, vec3 dir, out vec2 uv, bool twoSided){
    // compute plane normal and distance from origin
    vec3 xaxis = q[1].xyz - q[0].xyz;
    vec3 yaxis = q[3].xyz - q[0].xyz;

    float xlen = length(xaxis);
    float ylen = length(yaxis);
    xaxis = xaxis / xlen;
    yaxis = yaxis / ylen;

    vec3 zaxis = normalize(cross(xaxis, yaxis));
    float d = -dot(zaxis, q[0].xyz);

    float ndotz = -dot(dir, zaxis);
    if (twoSided)
        ndotz = abs(ndotz);

    if (ndotz < 0.00001)
        return false;

    // compute intersection point
    float t = -(dot(pos, zaxis) + d) / dot(dir, zaxis);

    if (t < 0.0)
        return false;

    vec3 projpt = pos + dir * t;

    // use intersection point to determine the UV
    uv = vec2(dot(xaxis, projpt - q[0].xyz),
              dot(yaxis, projpt - q[0].xyz)) / vec2(xlen, ylen);

    if (uv.x < 0.0 || uv.x > 1.0 || uv.y < 0.0 || uv.y > 1.0)
        return false;

    return true;
}

mat3 BasisFrisvad(vec3 v)
{
    vec3 x, y;

    if (v.z < -0.999999)
    {
        x = vec3( 0, -1, 0);
        y = vec3(-1,  0, 0);
    }
    else
    {
        float a = 1.0 / (1.0 + v.z);
        float b = -v.x*v.y*a;
        x = vec3(1.0 - v.x*v.x*a, b, -v.x);
        y = vec3(b, 1.0 - v.y*v.y*a, -v.y);
    }

    //return mat3_from_columns(x, y, v);
		mat3 res = mat3(x, y, v);
		return transpose(res);
}

vec3 CorrectNormal(vec3 n, vec3 v){
    if (dot(n, v) < 0.0)
        n = normalize(n - 1.01*v*dot(n, v));
    return n;
}

/*const int NSamples = 16;

vec3 brdfSample(vec3 V, float alpha, float U1, float U2){
	float phi = 2.0*3.14159 * U1;
	float r = alpha*sqrtf(U2/(1.0-U2));
	vec3 N = normalize(vec3(r*cosf(phi), r*sinf(phi), 1.0));
	vec3 L = -V + 2.0 * N * dot(N, V);
	return L;
}

vec3 calcLTCNorm(vec3 V, float alpha){
	float norm = 0.0;
	for(int i=0; i<NSamples; ++i){
		for(int j=0; j<NSamples; ++j){
			float U1 = (i+0.5)/(float)NSamples;
			float U2 = (j+0.5)/(float)NSamples;

			vec3 L = brdfSample(V, alpha, U1, U2);
			float pdf;
			float eval = getBRDFArea(V, L, alpha, pdf);

			norm += (pdf > 0) ? eval / pdf : 0.0;
		}
	}

	return norm / (float)(NSample*NSample);
}

vec3 calcAverDir(vec3 V, float alpha){
	vec3 averDir = vec3(0);
	for(int i=0; i<NSamples; ++i){
		for(int j=0; j<NSamples; ++j){
			float U1 = (i+0.5)/(float)NSamples;
			float U2 = (j+0.5)/(float)NSamples;

			vec3 L = brdfSample(V, alpha, U1, U2);
			float pdf;
			float eval = getBRDFArea(V, L, alpha, pdf);

			averDir += (pdf > 0) ? eval / pdf * L : vec3(0,0,0);
		}
	}
	averDir.y = 0.0;

	return averDir;
}

mat3 calcLTCMat( float roughness, float theta ){
	mat3 r;

	vec3 V = vec3(sin(theta), 0, cos(theta));
	float alpha = roughness * roughness;

	float amplitude = calcLTCNorm(V, alpha);
	vec3 averDir = computeAverDir(V, alpha);

	bool isotropic;

	vec3 X, Y, Z;
	float m11, m22, m13, m23;

	if(t==0){
		X = vec3(1,0,0);
		Y = vec3(0,1,0);
		Z = vec3(0,0,1);

		if(a == N-1) // roughness = 1
		{
			m11 = 1.0;
			m22 = 1.0;
		}
		else // init with roughness of previous fit
		{
			m11 = max(tab[a+1+t*N][0][0], MIN_ALPHA);
			m22 = max(tab[a+1+t*N][1][1], MIN_ALPHA);
		}
		
		m13 = 0;
		m23 = 0;
		update();

		isotropic = true;
	}else{
		vec3 L = normalize(averageDir);
		vec3 T1(L.z,0,-L.x);
		vec3 T2(0,1,0);
		X = T1;
		Y = T2;
		Z = L;

		update();

		isotropic = false;
	}

	return r;
}*/

vec3 getPBRLight(
	vec3 lightDir, 
	vec3 eyeDir, 
	vec3 normal, 
	vec3 albedo, 
	vec3 lightColor,
	float f0,
	float roughness,
	float metallness
){
	vec3 final;
	vec3 diffuse, specular;

	int stepsCount = 64;

	float dW = 1.0 / float(stepsCount);
	
	vec3 N = normalize(normal);
	vec3 L = normalize(lightDir);
	float NdotL = max(0.0, dot( N, L ) );

	specular = vec3(0);
	vec3 nl;

	for(int i=0; i<stepsCount; ++i){
		nl = hemispherePoint(i, stepsCount);
		specular += vec3(1.0) 
		  * getPBRSpecularUnreal(L, eyeDir, N, f0, roughness)
		  //* dot(N, nl)
		  //* dW;
		  ;
	}

	specular = lightColor * specular / float(stepsCount);

	diffuse = NdotL * albedo * vec3(1.0 / 3.14);

	final = diffuse * 1.0 + specular;

	return final;
}

vec3 calcDirectionalLight(DirLight dirLight){
	vec3 final, 
		 ambient,
		 color,
		 diffuse,
		 specular;
	float spec;

	color = globalData.color * 1.0;
	
	ambient = vec3(0.5) * color;

	vec3 L = -dirLight.direction;
	vec3 V = (globalData.viewPosition - globalData.fragPosition);
	vec3 N = globalData.normal;
	vec3 A = globalData.color;
	vec3 C = dirLight.specular;
	float r = globalData.roughness;
	float f0 = 0.02; //globalData.f0;
	float m = globalData.metallness;
	final = ambient + getPBRLight(L, V, N, A, C, r, f0, m);

	return final;
}

vec3 calcPointLight(PointLight pointLight){
	
	const float Intensity = 100.0;

	vec3 lightPos = pointLight.position;
	vec3 fragPos = globalData.fragPosition;
	vec3 viewPos = globalData.viewPosition;
	vec3 normal = normalize(globalData.normal);
	vec3 baseColor = globalData.color;
	vec3 lightColor = pointLight.specular;
	float metal = clamp(globalData.metallness, 0.01, 0.99);
	float roughness = clamp(globalData.roughness, 0.01, 0.99);
	
	vec3 lightVec = lightPos - fragPos;

	float sqrLightDist = dot(lightVec, lightVec);
	float lightDist   = sqrt(sqrLightDist);
	float lightRadius = Intensity;

	float attenuation = 1.0 / (max(sqrLightDist, 0.01*0.01));

	vec3 L = normalize(lightVec);
	vec3 V = normalize(viewPos - fragPos);
	vec3 H = normalize(L+V);
	vec3 N = normal;
	float NdotV = saturate(dot(N, V));
	float NdotL = saturate(dot(N, L));
	float LdotH = saturate(dot(L, H));
	float NdotH = saturate(dot(N, H));
	float VdotH = saturate(dot(V, H));
	
	vec3 Kdiff = NdotL * lightColor * baseColor * attenuation; ///saturate(getDisneyDiffuse( NdotV, NdotL, LdotH, roughness, vec3(metal) )) * lightColor;
	float specular = saturate(getSpecularAA(NdotL, NdotV, NdotH, VdotH, metal, roughness) * NdotL);
	vec3 Kspec = lightColor * specular * attenuation;
	vec3 Kbase = baseColor;

	return blendMaterial(Kdiff, Kspec, Kbase, metal);
	
}

vec3 calcSphereLight(SphereLight sphereLight){
	
	float Intensity = sphereLight.intensity;
	float radius = sphereLight.radius;
	vec3 lightPos = sphereLight.position;
	vec3 fragPos = globalData.fragPosition;
	vec3 viewPos = globalData.viewPosition;
	vec3 normal = normalize(globalData.normal);
	vec3 baseColor = globalData.color;
	vec3 lightColor = sphereLight.color;
	float metal = clamp(globalData.metallness, 0.00, 0.99);
	float roughness = clamp(globalData.roughness, 0.00, 0.99);
	
	vec3 lightVec = lightPos - fragPos;

	float sqrLightDist = dot(lightVec, lightVec);
	float lightDist   = sqrt(sqrLightDist);
	float lightRadius = radius + Intensity;
	float sqrLightRad = radius * radius;

	float luminance   = Intensity / (2 * PI * sqrLightRad * PI);
	float invSqrAtt   = 1.0 / max(sqrLightDist, 0.001);

	float attenuation = pow(saturate(1.0 - pow(lightDist / lightRadius, 4.0)), 2.0) / (sqrLightDist + 1.0);
	float surfaceLum  = luminance * pow(saturate(1.0 - pow(radius / lightRadius, 4.0)), 2.0) / (sqrLightDist + 1.0);

	vec3  viewDir     = -normalize( fragPos - viewPos );
    vec3  lightDir    = lightVec / lightDist;
    vec3  r           = normalize(reflect(viewDir, normal));

	float Beta = acos(dot(normal, lightDir));
    float H = lightDist;
    float h = H / radius;
    float x = sqrt(h * h - 1.0);
    float y = -x / tan(Beta);

    float illuminance = 0;
    if (h * cos(Beta) > 1){
    	illuminance = cos(Beta) / (h * h);
    }else{
		illuminance = (1.0 / (PI * h * h)) *
			(cos(Beta) * acos(y) - x * sin(Beta) * sqrt(1.0 - y * y)) +
			atan(sin(Beta) * sqrt(1.0 - y * y) / x) / PI;
	}
	illuminance *= PI;	

	vec3 Kdiff = lightColor * luminance * max(baseColor * illuminance * attenuation, 0.0);
	
	vec3 L = lightVec;
	vec3 centerToRay = dot(L, r) * r - L;
	vec3 closestPoint = L + centerToRay * saturate(radius/length(centerToRay));
	vec3 l = normalize(closestPoint);

	vec3  HH = normalize(l + viewDir);
	float NoL = saturate(dot(normal, l));
	float NoV = saturate(dot(normal, viewDir));
	float NoH = saturate(dot(normal, HH));
	float VoH = saturate(dot(viewDir, HH));
	float specular = saturate(getSpecularAA(NoL, NoV, NoH, VoH, metal, roughness) * NoL);

	//if(dot(L, normal) < 0.0) return vec3(0);

	vec3 KSpec = lightColor * max(specular * baseColor * surfaceLum, 0.0);
	return vec3(blendMaterial(Kdiff, KSpec, baseColor, metal));

}

vec3 calcRectangleLight(RectangleLight light){

	const int NUM_SAMPLES = 64;

	float Intensity = light.intensity;
	float width = light.width;
	float height = light.height;
	vec3 lightPos = light.position;
	vec3 fragPos = globalData.fragPosition;
	vec3 viewPos = globalData.viewPosition;
	vec3 normal = normalize(globalData.normal);
	vec3 baseColor = globalData.color;
	vec3 lightColor = light.color;
	float metal = clamp(globalData.metallness, 0.00, 0.99);
	float roughness = clamp(globalData.roughness, 0.00, 1.0);
	mat4 matrix = light.matrix;

	vec3 rightVec = mat3(matrix) * vec3(1,0,0);
	vec3 topVec = mat3(matrix) * vec3(0,0,1);
	vec3 norVec = normalize(mat3(matrix) * vec3(0,1,0));
	vec3 p0 = lightPos - rightVec * width / 2 - topVec * height / 2;
	vec3 p1 = lightPos - rightVec * width / 2 + topVec * height / 2;
	vec3 p2 = lightPos + rightVec * width / 2 + topVec * height / 2;
	vec3 p3 = lightPos + rightVec * width / 2 - topVec * height / 2;
	vec3 viewDir = normalize( viewPos - fragPos );

	vec4 PP[4]; //= {p0, p1, p2, p3};
	PP[0] = vec4(p0, 1);
	PP[1] = vec4(p1, 1);
	PP[2] = vec4(p2, 1);
	PP[3] = vec4(p3, 1);

	mat3 Minv = mat3(vec3(1, 0, 0), vec3(0, 1, 0), vec3(0, 0, 1));
	vec3 Lo_i = LTC_Evaluate(normal, viewDir, fragPos, Minv, PP, false);

	vec3 diff = Lo_i;
	diff *= Intensity / 9.0;
	diff *= lightColor * baseColor;
	diff /= 2.0 * PI;

	//Compute specular 

	normal = CorrectNormal(normal, viewDir);

	float F0 = 0.02;
	vec3 diffColor = baseColor*(1.0 - metal);
	vec3 specColor = mix(vec3(F0), baseColor, metal);
	
	vec2 coords = LTC_Coords(dot(normal, viewDir), roughness);

	Minv   = LTC_Matrix(ltcMat, coords);
	Lo_i   = LTC_Evaluate(normal, viewDir, fragPos, Minv, PP, false);

	Lo_i *= Intensity / (2 * PI * PI);
 	//vec2 schlick = texture(ltcAmp, coords).xy;
	//Lo_i *= texture(ltcAmp, coords).w;
	//Lo_i *= specColor*schlick.x + (1.0 - specColor)*schlick.y;
	Lo_i *= lightColor;
	Lo_i /= 2.0 * PI;
	
	vec3 spec = Lo_i;
		
	return blendMaterial( diff, spec, baseColor, metal );
}

vec3 IBL(){
	const int N = 64;

	return vec3(0);
}
