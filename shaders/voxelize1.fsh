#version 440
//#extension GL_ARB_shader_image_load_store : enable

#extension ARB_shading_language_include : enable

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform vec3 viewPosition;
uniform vec3 boundingBox;
uniform int state;
uniform int type;
uniform vec3 color;

uniform sampler2D albedo;
uniform sampler2D normals;
uniform sampler2D roughness;
uniform sampler2D metallness;
uniform float ior;

uniform int zoneSize;
uniform int gridSize;
uniform layout(binding=5, rgba16f) image3D voxelTexture;

uniform layout(binding=0, rgba16ui) uimageBuffer voxelPosTexture;
uniform layout(binding=1, rgba8) imageBuffer voxelKdTexture;

//layout(shared, binding=0) buffer atoms { uint counter; } atom;

uniform layout(binding=0) atomic_uint voxelsCount;

//uniform layout(binding=0) uniform {};

uniform int voxelState;

struct PointLight{
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float constant;
	float linear;
	float quadratic;
};

struct SphereLight{
	vec3 position;
	vec3 color;
	float intensity;
	float radius;
};

struct RectangleLight{
	vec3 position;
	vec3 color;
	float width;
	float height;
	float intensity;
	mat4 matrix;
};

struct SpotLight{
	vec3 position;
	vec3 color;
	float intensity;
	float fov;
	float radius;
	vec3 direction;
};

uniform PointLight pointLights[10];
uniform SphereLight sphereLights[10];
uniform RectangleLight rectangleLights[10];
uniform SpotLight spotLights[10];
uniform int pointLightCount;
uniform int sphereLightCount;
uniform int rectangleLightCount;
uniform int spotLightCount;

in vec3 vPositionG;
in vec3 vNormalG;
in vec2 vTexCordG;
in mat3 vTBN;
in flat int vAxis;
in mat4 vProjAxis;

layout(location=0) out vec4 FragColor;

#define M_PI 3.1428

float saturate(float v){
	return min(1.0, max(0.0, v));
}

float smoothDistanceAtt(float squaredDistance, float invSqrAttRadius){
	float factor = squaredDistance * invSqrAttRadius;
	float smoothFactor = saturate(1.0 - factor * factor);
	return smoothFactor * smoothFactor;
}

float getDistanceAtt(float sqrDist, float invSqrAttRadius){
	float attenuation = 1.0 / max( sqrDist , 0.0001);
	attenuation *= smoothDistanceAtt(sqrDist, invSqrAttRadius);
	return attenuation;
}

float rectangleSolidAngle(vec3 worldPos, vec3 p0, vec3 p1, vec3 p2, vec3 p3){
	vec3 v0 = p0 - worldPos;
	vec3 v1 = p1 - worldPos;
	vec3 v2 = p2 - worldPos;
	vec3 v3 = p3 - worldPos;
	vec3 n0 = normalize(cross(v0 , v1));
	vec3 n1 = normalize(cross(v1 , v2));
	vec3 n2 = normalize(cross(v2 , v3));
	vec3 n3 = normalize(cross(v3 , v0));

	float g0 = acos(dot(-n0 , n1));
	float g1 = acos(dot(-n1 , n2));
	float g2 = acos(dot(-n2 , n3));
	float g3 = acos(dot(-n3 , n0));

	return g0 + g1 + g2 + g3 - 2 * M_PI ;
}

vec3 computeRectangleLight(vec3 fragPos, vec3 normal, vec3 albedo, RectangleLight light){
	float luminance = light.intensity / (light.width * light.height * M_PI);
	vec3 lightPos = light.position;
	vec3 worldPos = fragPos; //lightPos;
	vec3 lightPlaneNormal = vec3(vec4(0.0, 1.0, 0.0, 0.0) * light.matrix);
	vec3 worldNormal = normal; //vec3(vec4(0.0, 1.0, 0.0, 0.0) * light.matrix);
	//vec3 worldNormal = normal;
	float lightWidth = light.width;
	float lightHeight = light.height;
	float distance = length(lightPos - fragPos);
	float sqrLightDist = distance * distance;
	float invSqrAtt = 1.0 / max(sqrLightDist, 0.001);
	float attenuation = invSqrAtt;

	if(dot( worldPos - lightPos , lightPlaneNormal ) > 0){
		float halfWidth = lightWidth * 0.5;
		float halfHeight = lightHeight * 0.5;

		vec3 p0 = light.position + vec3(light.matrix * vec4( lightWidth/2.0, 0, lightHeight/2.0, 0.0 ));
		vec3 p1 = light.position + vec3(light.matrix * vec4(  lightWidth/2.0, 0, -lightHeight/2.0, 0.0 ));
		vec3 p2 = light.position + vec3(light.matrix * vec4(  -lightWidth/2.0, 0,  -lightHeight/2.0, 0.0 ));
		vec3 p3 = light.position + vec3(light.matrix * vec4( -lightWidth/2.0, 0,  lightHeight/2.0, 0.0 ));

		float solidAngle = rectangleSolidAngle(worldPos, p0, p1, p2, p3);

		float illuminance = solidAngle * 0.2 * (
			saturate(dot(normalize(p0), worldNormal)) +
			saturate(dot(normalize(p1), worldNormal)) +
			saturate(dot(normalize(p2), worldNormal)) +
			saturate(dot(normalize(p3), worldNormal)) +
			saturate(dot(normalize(lightPos - worldPos), worldNormal))	
		);

		illuminance *= M_PI * illuminance;

		attenuation *= 1.0; //smoothDistanceAtt(distance * distance, invSqrAtt);
		
		vec3 res = light.color * albedo * max(0.0, illuminance * attenuation);

		return res;

	}

	return vec3(0);

}

vec3 rayPlaneIntersect(vec3 planeNormal, vec3 planePosition, vec3 rayDir, vec3 rayPosition){
	float k = (dot(planeNormal, planePosition - rayPosition)) / dot(planeNormal, rayDir);
	return rayPosition + rayDir * k;
}

vec3 closestPoint(vec3 point, vec3 lightPos, vec3 lightUp, vec3 lightRight, float halfWidth, float halfHeight){
	vec3 dir = point - lightPos;
	vec2 dist2D = vec2( dot(dir, lightRight), dot(dir, lightUp));
	vec2 rectHalfSize = vec2(halfWidth, halfHeight);
	dist2D = clamp( dist2D, -rectHalfSize, rectHalfSize );
	return lightPos + dist2D.x * lightRight + dist2D.y * lightUp;
}

vec3 computeRectangleLight2(vec3 fragPos, vec3 normal, vec3 albedo, RectangleLight light){

	float lumiance = light.intensity / (light.width * light.height * M_PI);
	vec3 lightNormal = normalize(vec3(light.matrix * vec4(0, 1, 0, 1)));
	vec3 lightUp = normalize(vec3(light.matrix * vec4(0, 0, 1, 1)));
	vec3 lightRight = normalize(vec3(light.matrix * vec4(1, 0, 0, 1)));
	float halfWidth = light.width / 2;
	float halfHeight = light.height / 2;
	float lightDist = length(light.position - fragPos);

	//return vec3(dot((fragPos - light.position), -lightNormal));
	//return vec3(fragPos - light.position);
	//return abs(lightNormal);
	//return vec3(-dot(normal, lightNormal));
	
	if(dot(fragPos - light.position, lightNormal) > 0){
		float clampCosAngle = 0.001 + saturate(dot(normal, lightNormal));
		vec3 d0 = normalize(-lightNormal + normal * clampCosAngle);
		vec3 d1 = normalize(normal - normal * clampCosAngle);
		vec3 dh = normalize(d0 + d1);
		vec3 ph = rayPlaneIntersect(lightNormal, light.position, dh, fragPos);
		ph = closestPoint(ph, light.position, lightUp, lightRight, halfWidth, halfHeight);

		vec3 pph = normalize(ph - fragPos);

		vec3 p0 = light.position + lightRight * halfWidth + lightUp * halfHeight;
		vec3 p1 = light.position + lightRight * halfWidth - lightUp * halfHeight;
		vec3 p2 = light.position - lightRight * halfWidth - lightUp * halfHeight;
		vec3 p3 = light.position - lightRight * halfWidth + lightUp * halfHeight;

		float solidAngle = rectangleSolidAngle(fragPos, p0, p1, p2, p3);

		float illumiance = solidAngle * max(0.0, dot(normal, pph)) * lumiance;
		//illumiance *= getDistanceAtt(lightDist * lightDist, 1.0 / light.intensity );

		return albedo * vec3(light.color) * illumiance;

	}

	return vec3(0.0);
}

void averageStore(uimage3D){}

vec4 computeSphereLight(vec3 fragPos, vec3 normal, vec4 albedo, SphereLight light){

	vec4 res;

	float lumiance = light.intensity / (2.0 * M_PI * light.radius);

	vec3 lightVec = (light.position - fragPos);
	float lightDist = length(lightVec);
	vec3 lightDir = lightVec / lightDist;
	float sqrLightDist = lightDist * lightDist;
	float invSqrAtt = 1.0 / max(sqrLightDist, 0.001);

	float attenuation = 1.0 / pow( lightDist / light.radius + 1.0, 1.2 ); 

	//float attenuation = invSqrAtt; //pow(saturate(1.0 - pow(lightDist / light.radius, 4.0)), 2.0) / (sqrLightDist + 1.0);

	float Beta = acos(dot(normal, lightDir));
    float H = lightDist;
    float h = H / light.radius;
    float x = sqrt(h * h - 1.0);
    float y = -x / tan(Beta);

    float illuminance = 0;
    if (h * cos(Beta) > 1){
		illuminance = cos(Beta) / (h * h);
    }
    else{
		illuminance = (1.0 / (M_PI * h * h)) *
		(cos(Beta) * acos(y) - x * sin(Beta) * sqrt(1.0 - y * y)) +
		atan(sin(Beta) * sqrt(1.0 - y * y) / x) / M_PI;
    }
    illuminance *= M_PI;

	res = vec4(light.color, 1.0) * albedo * lumiance * max(illuminance * attenuation, 0.0);

	return res;
}

vec4 computeSpotLight(vec3 pos, vec3 normal, vec4 albedo, SpotLight light){

	vec3 position = light.position;
    float radius = light.radius;
    float fov = light.fov;
    vec3 direction = light.direction;
    vec3 l = light.position - pos; //pos - light.position;
	vec3 lNorm = normalize(l);
	vec3 color = light.color;
	
	//Compute attenuation
	float distSquare = dot(l,l);
	float factor = distSquare / (radius * radius);
    float smoothFactor = saturate(1.0 - factor * factor);
	float distAttenuation = smoothFactor * smoothFactor / max(0.01 * 0.01, distSquare);
    float cosAngle = dot(lNorm, -direction);
    float linearAngle = (cosAngle - fov) / (1 - fov);
    float angleAtt = saturate(linearAngle);
    float attenuation = angleAtt * angleAtt * distAttenuation;

	//Compute diffuse
	float lambertian = max(0.0, dot(lNorm, normal));

	return albedo * lambertian * color * attenuation * light.intensity;

	//return applyLight(albedo, normal, roughness, metallness, ior, view, lNorm, color, attenuation) * light.intensity;

}

vec4 convRGBA8ToVec4( in uint val ){
    return vec4( float( (val&0x000000FF) ), float( (val&0x0000FF00)>>8U),
	             float( (val&0x00FF0000)>>16U), float( (val&0xFF000000)>>24U) );
}

uint convVec4ToRGBA8( in vec4 val ){
    return ( uint(val.w)&0x000000FF)<<24U | (uint(val.z)&0x000000FF)<<16U | (uint(val.y)&0x000000FF)<<8U | (uint(val.x)&0x000000FF);
}

/*void imageAtomicRGBA8Avg( vec4 val, ivec3 coord, layout(rgba8) image3D buf ){
	vec4 newVal = ( val );
	vec4 prev = vec4(0);
	vec4 cur;
	while( (cur = imageAtomicCompSwap( buf, coord, prev, newVal ) ) != prev ){
       prev = cur;
	   vec4 rval = cur; //convRGBA8ToVec4( cur );
	   rval.xyz = rval.xyz * rval.w;
	   vec4 curVal = rval + val;
	   curVal.xyz /= curVal.w;
	   newVal = convVec4ToRGBA8( curVal );
   }
     
}*/

void main(){

	uint ind = atomicCounterIncrement(voxelsCount);

	//uint ind = atomicAdd(atom.counter, 1);

	if(voxelState == 1){
		vec4 a = texture(albedo, vTexCordG);
		vec4 n = texture(normals, vTexCordG);
		vec4 r = texture(roughness, vTexCordG);
		vec4 m = texture(metallness, vTexCordG);
		int s = state;
		mat4 pm = projectionMatrix;
		mat4 vm = viewMatrix;
		mat4 mm = modelMatrix;
		vec3 bb = boundingBox;
		vec3 vp = viewPosition;
		float ir = ior;

		float zs = zoneSize;
		int gs = gridSize;

		ivec3 cp = ivec3(gl_FragCoord.x, gl_FragCoord.y, gl_FragCoord.z * gridSize);
		ivec3 fp;
		if(vAxis == 1){
			fp.x = gridSize - cp.z - 0;
			fp.y = cp.y;
			fp.z = cp.x;
		}else if(vAxis == 2){
			fp.x = cp.x;
			fp.y = gridSize - cp.z - 0;
			fp.z = cp.y;
		}else{
			fp = cp;
		}
		fp.z = gridSize - fp.z - 1;

		FragColor = vec4(0);

		if(type == 1){ 
			FragColor = vec4(0.0) * color.x; //vec4(color, 1.0) * vec4(vec3(1.0), 20.0) * 0.0;
		}else if(type == 2){
			
			vec3 normal = vNormalG;

			for(int i=0; i<rectangleLightCount; ++i)
				FragColor += vec4(computeRectangleLight2(vPositionG, normal, vec3(a), rectangleLights[i]), 1.0);

			for(int i=0; i<sphereLightCount; ++i)
				FragColor += computeSphereLight(vPositionG, normal, a, sphereLights[i]);

			for(int i=0; i<spotLightCount; ++i)
				FragColor += computeSpotLight(vPositionG, normal, a, spotLights[i]);

			/*for(int i=0; i<pointLightCount; ++i){
				pointLights[i];
			}*/

			//FragColor = vec4(0.1);

		}

		//imageStore(voxelPosTexture, int(ind), uvec4(fp, 0));
		//imageStore(voxelKdTexture, int(ind), vec4(FragColor));

		imageStore(voxelTexture, ivec3(fp), vec4(a));

		//imageStore(voxelTexture, ivec3(0), vec4(300.0));

		//imageAtomicRGBA8Avg(FragColor, fp, voxelTexture);

		//imageAtomicExchange(voxelTexture, ivec3(fp), convVec4ToRGBA8(FragColor));

	}

	//FragColor = vec4(imageLoad(voxelTexture, ivec3(0)));
}
