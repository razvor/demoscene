#version 440

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform vec3 viewPosition;
uniform int type;

uniform vec2 focalLen;
uniform vec2 uvToViewA;
uniform vec2 uvToViewB;

uniform sampler2D albedos;
uniform sampler2D normals;
uniform sampler2D positions;
uniform sampler2D depths;
uniform sampler2D types;
uniform sampler2D boundingBoxes;
uniform sampler2D finals;

/*in flat vec4 sourceVertex0, sourceVertex1, sourceVertex2;
in flat vec3 vFaceNormal;
in flat vec3 m0, m1, m2;
in flat int isBase;
in flat float area;*/

in vec2 coords;

layout(location=0) out vec4 FragColor;

float obscurance = 0.001;
float M_PI = 3.1428;
const float epsilon = 0.00001;
float falloffExponent = 6.0;

/*float computeFalloff
    (in  vec3 origin, 
     out vec3 p0,
     out vec3 p1, 
     out vec3 p2) {

	 float invMaxObscuranceDistance = 1.0 / obscurance;

    // Let pm[i] = p[i].dot(m[i]),
    // where p[i] is the polygon's vertex in tangent space
    // and m[i] is the normal to edge i.  
    //
    // pm[3] uses p[0] and m[3], which is the negative
    // normal to the entire occluding polygon.  That is,
    // pm[3] is the distance to the occluding polygon.
    vec4 pm;
    p0  = sourceVertex0.xyz - origin;

    // Always the top
    pm[3] = dot(p0, vFaceNormal);

    // Two early-out tests.
    //
    // Corectness: If distanceToPlane < 0, we're *behind* the entire volume.  We need to add a small offset
    // to ensure that we don't discard corners where a surface point is exactly
    // in the plane of the source triangle and might round off to "behind" it.
    //
    // Optimization: If area / distanceToPlane < smallConstant, then this is a small triangle relative to 
    // the point, so it will produce minimal occlusion that will round off to zero at the 
    // alpha blender.  Making the constant larger will start to abruptly truncate some occlusion.
    // Making the constant smaller will increase precision; the test can be eliminated entirely without
    // affecting correctness.
    if ((pm[3] < epsilon) || (area < pm[3] * 0.3)) {
        //discard;
    }

    pm[0] = dot(p0, m0);

    p1  = sourceVertex1.xyz - origin;
    pm[1] = dot(p1, m1);

    p2  = sourceVertex2.xyz - origin;
    pm[2] = dot(p2, m2);

    //// Let g[i] = max(0.0f, min(1.0f, 1.0f - pm[i] * invDelta));
    vec4 g = clamp(vec4(1.0) - pm * invMaxObscuranceDistance, vec4(0.0), vec4(1.0));

    g[3] = pow(g[3], falloffExponent);

    // Recall that meanCoverage is the average alpha value of the occluding polygon.
    float f = g[0] * g[1] * g[2] * g[3] * 1.0;

    // If falloffWeight is low, there's no point in computing AO        
    if (f <= 0.1) {
        //discard;
    }

    return f;
}

bool clipToPlane(const in vec3 n, inout vec3 v0, inout vec3 v1, inout vec3 v2, out vec3 v3) {

    // Distances to the plane (this is an array parallel to v[], stored as a vec3)
    vec3 dist = vec3(dot(v0, n), dot(v1, n), dot(v2, n));

    bool quad = false;

    // Perform this test conservatively since we want to eliminate
    // faces that are adjacent but below the point being shaded.
    // In order to be sure that two-sided surfaces don't slip and completly
    // occlude each other, we need a fairly large epsilon.  The same constant
    // appears in the ray tracer.

    if (! any(greaterThanEqual(dist, vec3(-0.0001)))) {
        // All clipped; no occlusion from this triangle
        discard;
    } else if (all(greaterThanEqual(dist, vec3(-epsilon)))) {
        // None clipped (original triangle vertices are unmodified)

		//discard;
    } else {
        bvec3 above = greaterThanEqual(dist, vec3(-0.0001));

        // There are either 1 or 2 vertices above the clipping plane.
        bool nextIsAbove;

        // Find the ccw-most vertex above the plane by cycling
        // the vertices in place.  There are three cases.
        if (above[1] && ! above[0]) {
            nextIsAbove = above[2];
            // Cycle once CCW.  Use v[3] as a temp
            v3 = v0; v0 = v1; v1 = v2; v2 = v3;
            dist = dist.yzx;
        } else if (above[2] && ! above[1]) {
            // Cycle once CW.  Use v3 as a temp.
            nextIsAbove = above[0];
            v3 = v2; v2 = v1; v1 = v0; v0 = v3;
            dist = dist.zxy;
        } else {
            nextIsAbove = above[1];
        }
        // Note: The above[] values are no longer in sync with v[] and dist[].

        // Both of the following branches require the same value, so we compute
        // it into v[3] and move it to v[2] if that was the required location.
        // This helps keep some more threads coherent.

        // Compute vertex 3 first so that we don't smash the data
        // we need to reuse in vertex 2 if this is a quad.
        v3 = mix(v0, v2, dist[0] / (dist[0] - dist[2]));

        if (nextIsAbove) {
            // There is a quad above the plane
            quad = true;

            //    i0---------i1
            //      \        |
            //   .....B......A...
            //          \    |
            //            \  |
            //              i2
            v2 = mix(v1, v2, dist[1] / (dist[1] - dist[2]));

        } else {
            // There is a triangle above the plane

            //            i0
            //           / |
            //         /   |
            //   .....B....A...
            //      /      |
            //    i2-------i1

            v2 = v3;
            v1 = mix(v0, v1, dist[0] / (dist[0] - dist[1]));
        }
    }

    // For triangle output, duplicate first vertex to avoid a branch
    // (and therefore, incoherence) later
    v3 = quad ? v3 : v0;

    return quad;
}

float projArea(const in vec3 a, const in vec3 b, const in vec3 n) {
    vec3 bXa = cross(b, a);
    float cosine = dot(a, b);
    float theta = acos(cosine * inversesqrt(dot(a, a) * dot(b, b)));
    return (theta * dot(n, bXa) * inversesqrt(dot(bXa, bXa)));
}

float computeFormFactor(vec3 n, vec3 p0, vec3 p1, vec3 p2){
	vec3 p3;
	bool quad = clipToPlane(n, p0, p1, p2, p3);

	float res = 0.0;

	if(quad){
		res += projArea(p3, p0, n);
	}
	res += projArea(p0, p1, n);
	res += projArea(p1, p2, n);
	res += projArea(p2, p3, n);

	res /= 2.0 * 3.14;
	//res = 1.0 - res;

	return res;
}

float radicalInverse_VdC(uint bits) {
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

vec2 hammersley(uint i, uint N) {
	return vec2( float(i) / float(N), radicalInverse_VdC(i) );
}

float SSAO(vec3 n, vec3 p){
	int N = 16;
	float radius = 0.2;

	vec3 t = normalize(cross( n, vec3(1, 0, 0) ));
	if(length(t) == 0) t = normalize(cross( n, vec3(0, 1, 0) ));
	vec3 b = normalize(cross(n, t));
	t = normalize(cross(n, b));

	mat4 screenMatrix = projectionMatrix * viewMatrix;

	float summ = 0.0;

	vec4 fragPos = vec4(p, 1.0); //screenMatrix * vec4(p, 1.0);
	//fragPos /= fragPos.w;

	for(int i=0; i<N; ++i){
		vec2 a = hammersley(i, N);
		float theta = a.x * M_PI;
		float phi = 2.0 * a.y * M_PI;

		vec3 sph = p;
		sph.x = sin(theta) * cos(phi);
		sph.z = sin(theta) * sin(phi);
		sph.y = cos(theta);

		vec3 x = p + mat3(t, b, n) * sph * radius;
		vec4 sp = (screenMatrix * vec4(x, 1.0));
		sp /= sp.w;
		sp = sp / 2.0 + 0.5;

		float sz = texture(positions, sp.xy).z;

		float sm = smoothstep(0.0, 1.0, radius / abs(fragPos.z - sz));
		//summ += (sz <= x.z + 0.01 ? 1.0 : 0.0 ) * sm;
		summ += sm;

	}

	return (summ / float(N));
}*/

float tanToSin(float x){
	return x * inversesqrt(x*x + 1.0);
}

vec3 uvToView(vec2 uv, float z){
	uv = uvToViewA * uv + uvToViewB;
	return vec3(uv * z, z);
}

vec3 getViewPos(vec2 uv){
	return uvToView(uv, (texture(boundingBoxes, uv).w * 2.0 - 1.0));
}

vec3 minDiff(vec3 p, vec3 pl, vec3 pr){
	vec3 d0 = (p - pl);
	vec3 d1 = (pr - p);
	return length(d0) < length(d1) ? d0 : d1;
}

float biasedTangent(vec3 V){
	float TanBias = 0.01;
	return V.z * 1.0 / length(V.xy) + TanBias;
}

float tangent(vec3 p, vec3 s){
	return -(p.z - s.z) * 1.0 / length(s.xy - p.xy);
}

float ao(vec3 n, vec3 p, vec3 pp){

	vec2 texSize = textureSize(boundingBoxes, 0);
	vec2 invSize = vec2(1.0) / texSize;

	const int N = 16;
	const int N_STEPS = 40;
	const float RADIUS = length(invSize * 40.0);

	vec3 viewSpaceNormal = n; //normalize(projectionMatrix * viewMatrix * vec4(n, 0.0)).xyz;

	//p.xy = round(p.xy * texSize) * invSize;
	vec2 uv = p.xy;

	vec3 p0 = getViewPos(uv);
	vec3 pr = getViewPos(uv + invSize * vec2(1, 0));
	vec3 pl = getViewPos(uv + invSize * vec2(-1, 0));
	vec3 pt = getViewPos(uv + invSize * vec2(0, 1));
	vec3 pb = getViewPos(uv + invSize * vec2(0, -1));

	vec3 dPdu = minDiff(p0, pl, pr);
	vec3 dPdv = minDiff(p0, pb, pt) * (texSize.y * invSize.x);

	//Theta offset:
	vec3 toffs = normalize(cross(p, n));
	float thetaOffset = 0; //toffs.x * M_PI * 2.0;
	
	float R2 = RADIUS * RADIUS;

	float alpha = 2.0 * M_PI / float(N);
	float ao = 0.0;
	for(int i=0; i<N; ++i){
		float theta = alpha * float(i) - M_PI + thetaOffset;

		vec3 viewSpaceDir = normalize(vec3( cos(theta), sin(theta), 0 ));
		float tangentAngle = acos( dot(viewSpaceNormal, viewSpaceDir) ); 
		tangentAngle = (M_PI / 2.0 - tangentAngle);

		vec3 tng = dPdu * viewSpaceDir.x + dPdv * viewSpaceDir.y;
		float tanH = -biasedTangent(tng);

		vec3 pi = p;
		pi.xy += invSize * viewSpaceDir;
		pi.xy = round(pi.xy * texSize) * invSize;

		float stepSize = RADIUS / float(N_STEPS);
		float maxAlpha = 0, maxDistance = 0;
		float d0 = texture(boundingBoxes, pi.xy).w * 2.0 - 1.0;
		
		vec3 P = getViewPos(pi.xy);

		float sinH, sinS, tanS = 0;

		//sinH = sin(tangentAngle); 
		sinH = tanToSin(tanH); 

		vec3 prevH;

		/*for(int j=0; j<N_STEPS; ++j){
			
			vec3 np = pi + j * stepSize * viewSpaceDir;
			np.xy = round(np.xy * texSize) * invSize;

			float dd = texture(boundingBoxes, np.xy).w * 2.0 - 1.0;
			vec3 D = vec3(np.xy, dd) - vec3(pi.xy, d0);

			float alpha = atan(-D.z / length(D.xy));
			float distance = length(D);
			
			if(alpha > maxAlpha ){maxAlpha = alpha; maxDistance = distance;}
			if(j > 0){
				ao += (dot(n, D) / length(D) - dot(n, prevH) / length(prevH)) * (1.0 - maxDistance / RADIUS);
			}

			prevH = D;
		}*/

		for(int j=1; j<N_STEPS; ++j){

			vec3 np = pi + j * stepSize * viewSpaceDir;
			np.xy = round(np.xy * texSize) * invSize;

			//float dd = texture(boundingBoxes, np.xy).w * 2.0 - 1.0;
			//vec3 D = vec3(np.xy, dd) - vec3(pi.xy, d0);
			//float tanS = -tangent(vec3(pi.xy, d0), vec3(np.xy, dd));

			vec3 S = getViewPos(np.xy);
			tanS = -tangent(P, S);

			//float alpha = atan(-D.z / length(D.xy));
			//float distance = length(D);

			//float distance = pow(length(vec3(pi.xy, d0) - vec3(np.xy, dd)), 2.0);
			float distance = pow( length(P - S), 2.0 );

			if(distance < R2 && tanS > tanH){
				//maxAlpha = alpha;
				//maxDistance = distance;

				///sinS = tanToSin(tan(maxAlpha));
				sinS = tanToSin(tanS);

				ao += (1.0 - pow(distance / (R2), 1.0)) * (sinS - sinH);
				sinH = sinS;
				tanH = tanS;
			}
			
		}
		float horizontalAngle = max(tangentAngle, maxAlpha);

	}

	//ao = 1.0 - ao / float(N) / (1.0 * M_PI);

	ao = 1.0 - ao / float(N) / (2.0 * M_PI);

	ao = pow(ao, 5.0);

	return ao;
}

void main(){

	ivec2 crd = ivec2(gl_FragCoord.xy); /// vec2(600);
	vec3 vp = viewPosition;
	
	vec4 x = texelFetch(positions, crd, 0);
	vec4 gNormal = texelFetch(normals, crd, 0) * 2.0 - 1.0;
	vec3 screenNormal = texture(boundingBoxes, coords).xyz * 2.0 - 1.0; //texelFetch(boundingBoxes, crd, 0).xyz * 2.0 - 1.0;
	float gDepth = texelFetch(boundingBoxes, crd, 0).w;
	vec4 type = texelFetch(types, crd, 0);
	vec4 final = texelFetch(finals, crd, 0);
	vec4 dp = texelFetch(depths, crd, 0);
	vec4 albedo = texelFetch(albedos, crd, 0);
	vec2 fl = focalLen;

	/*vec3 p0, p1, p2;
	
	//if(type == 1) return;

	float falloff = computeFalloff(x.xyz, p0, p1, p2);
	//falloff = 1.0;
	float formFactor = computeFormFactor(gNormal.xyz, p0, p1, p2);
	float ao = clamp(formFactor*falloff, 0.0, 1.0);
	FragColor = vec4(vec3(ao), 1.0);

	//FragColor = vec4(vec3(SSAO(gNormal.xyz, x.xyz)), 1.0);*/

	vec3 viewVec = normalize(x.xyz - viewPosition);

	//FragColor = vec4((projectionMatrix * viewMatrix * vec4(gNormal.xyz, 1.0)).z);
	//FragColor = vec4(gDepth);

	//FragColor = vec4((projectionMatrix * viewMatrix * vec4(x.xyz, 1.0)).xy, 0.0, 1.0);
	vec2 screenSpacePosition = coords; //vec2(crd) / textureSize(normals, 0).xy;

	FragColor = vec4( ao(screenNormal, vec3(screenSpacePosition, 0.0), x.xyz) );

	//FragColor = vec4( gDepth * 2.0 - 1.8 ) * 3.0;
	//FragColor = vec4(screenSpacePosition.y, 0.0, 0.0, 1.0);
	//FragColor = vec4(screenNormal, 1.0);

	//FragColor = vec4();

}