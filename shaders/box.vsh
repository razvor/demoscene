#version 410

layout(location=0) in vec3 aPosition;

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

out vec3 vPosition;

void main(){
	vPosition = aPosition;
	//vMat = projectionMatrix * viewMatrix * modelMatrix;

	vec4 oPos =  projectionMatrix * viewMatrix * modelMatrix * vec4( aPosition, 1.0 );

	gl_Position = oPos;
}
