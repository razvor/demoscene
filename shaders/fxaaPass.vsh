#version 440

layout(location = 0) in vec3 aPosition;
out vec2 cords;

void main() {
	cords = aPosition.xy / 2.0 + 0.5;
	vec4 oPos = vec4(aPosition, 1.0);
	oPos.z = -1.0;
	gl_Position = oPos;
}