//#version 310

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform vec3 viewPosition;
uniform vec3 lightPosition;
uniform int pointLightCount;
uniform int sphereLightCount;
uniform int rectangleLightCount;
uniform int state;

uniform sampler2D albedo;
uniform sampler2D normals;
uniform sampler2D roughness;
uniform sampler2D metallness;
//uniform sampler2D glossiness;

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCord;
in mat3 vTBN;
layout(location=0) out vec4 FragColor;

uniform DirLight directionalLights[1];
uniform PointLight pointLights[10];
uniform SphereLight sphereLights[10];
uniform RectangleLight rectangleLights[10];


vec3 calcLights(){
	vec3 r = vec3(0);

	r += calcDirectionalLight(directionalLights[0]) * 0.0;
	for(int i=0; i<pointLightCount; ++i){
		r += calcPointLight(pointLights[i]) * 1.0;
	}
	for(int i=0; i<sphereLightCount; ++i){
		r += calcSphereLight(sphereLights[i]) * 1.0;
	}
	for(int i=0; i<rectangleLightCount; ++i){
		r += calcRectangleLight(rectangleLights[i]);
	}

	return r;
}

float GetFresnel(float NdotI, float bias, float power)
{
  float facing = (1.0 - NdotI);
  return bias + (1-bias)*pow(facing, power);
}

void main(){

	vec3 ambient, diffuse, specular, final;

	globalData.fragPosition = vec3(modelMatrix * vec4(vPosition, 1.0));
	/*globalData.sNormal = 
	normalize(mat3(transpose(inverse(modelMatrix))) * vNormal);*/
	globalData.viewPosition = viewPosition;
	globalData.TBN = vTBN;

	vec3 nm = texture(normals, vTexCord).rgb;
	nm = normalize(nm * 2.0 - 1.0);
	globalData.normal = vTBN * nm;
	globalData.color = vec3(texture(albedo, vTexCord));
	//globalData.uwPosition = (projectionMatrix * viewMatrix * modelMatrix * vec4(vPosition, 1.0)).xyz;
	globalData.roughness = texture(roughness, vTexCord).r;
	globalData.metallness = texture(metallness, vTexCord).r;
	//globalData.glossiness = texture(glossiness, vTexCord).r;
	//globalData.f0 = 0.04;
	//globalData.state = state;
	//globalData.f0 = mix( globalData.f0, globalData.color.x, globalData.metallness );

	if(state == 1)
		final = calcLights() + vec3(0.0);
	else if(globalData.roughness < 0.55){
		vec3 origPos = vec3(1);
		vec3 cubemapPos = vec3(2.0, 0.2, -0.0);
		vec3 bBox = vec3( 12, 2, 12 );
		vec3 bBoxMin = origPos - bBox/2;
		vec3 bBoxMax = origPos + bBox/2;
		vec3 viewVec = globalData.viewPosition - globalData.fragPosition;
		vec3 normal = normalize(globalData.normal);
		vec3 vertPos = globalData.fragPosition;

		vec3 refDir = reflect(normalize(-viewVec), normal);
		vec3 invRef = 1.0 / refDir;
		vec3 intMin = (bBoxMin - vertPos) * invRef;
		vec3 intMax = (bBoxMax - vertPos) * invRef;
		vec3 invLar = max(intMin, intMax);
		float dist = min((invLar.x, invLar.y), invLar.z);

		vec3 intPos = vertPos + refDir * dist;	
		vec3 locVec = intPos - cubemapPos;
		//locVec = refDir;
		//locVec *= vec3(1, -1, 1);

		//final = texture(LCM, (locVec.xyz)).rgb * 1.0 + vec3(0.0);

		//final = vec3(vec2(globalData.roughness), 1);

	}else{
		final = calcLights() + vec3(0.2);
	}
	
	//if(globalData.roughness < 0.55) final = vec3(1.0);
	
	//final = vec3(globalData.roughness);

	/*if(state == 1){
		if((vMat * vec4(vPosition, 1.0)).z > 0.001)
			discard;
	}*/

	//final = vec3( globalData.roughness);

	FragColor = vec4(final, 1.0);
}
