#version 330

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform vec3 viewPosition;
uniform vec3 boundingBox;
uniform int state;
uniform float direction;
uniform vec3 color;
//uniform int pointLightCount;

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCord;
//in mat4 vTBN:
//uniform DirLight directionalLights[1];
//uniform PointLight pointLights[100];
layout(location=0) out vec4 FragColor;
layout(location=1) out vec4 nor;
layout(location=2) out vec4 pos;
layout(location=3) out vec4 bbox;
layout(location=4) out vec4 type;

void main(){
	float d = direction;
	vec3 vp = viewPosition * state;
	//mat4 mm = vMat * projectionMatrix * viewMatrix * modelMatrix;
	//vec3 vv = vPosition + vNormal;
	//vec3 vp = mat3(projectionMatrix * modelMatrix * viewMatrix) * viewPosition * state;
	//vec3 k = mat3( vMat ) * vTBN * (viewPosition + viewPosition + vNormal + vPosition ) * state;

	/*if(state == 1){
		if((vMat * vec4(vPosition, 1.0)).z > 0.001)
			discard;
	}*/

	bbox = vec4(boundingBox, 0);
	type = vec4(2);

	FragColor = vec4(color, 1.0);
}
