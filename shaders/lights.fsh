#version 440

#pragma once

struct SpotLight{
	vec3 position;
	vec3 color;
	float intensity;
	float fov;
	float radius;
	vec3 direction;
};

float saturate(float a){
	return clamp(a, 0.0, 1.0);
}

float brdfOrenNayarDiffuse(
	float LoV, float LoN, float VoN,
	float roughness
){

	/*float LoV = dot(lightDirection, viewDirection);
	float LoN = dot(surfaceNormal, lightDirection);
	float VoN = dot(viewDirection, surfaceNormal);*/

	/*float s = LdotV - NdotL * NdotV;
	float t = mix(1.0, max(NdotL, NdotV), step(0.0, s));

	float sigma2 = roughness * roughness;
	float A = 1.0 + sigma2 * (1.0 / (sigma2 + 0.13) + 0.5 / (sigma2 + 0.33));
	float B = 0.45 * sigma2 / (sigma2 + 0.09);

	return max(0.0, NdotL) * (A + B * s / t) / M_PI;*/

	float result = saturate(LdotN);
	float soft_rim = saturate(1-VdotN/2);
	float fakey = pow(1-result*soft_rim,2);
	float fakey_magic = 0.62;
	fakey = fakey_magic - fakey*fakey_magic;
	return mix(result, fakey, roughness);
}

float brdfDiffuse(float NoV, float NoL, float LoH, float VoH, float LoV, float roughness){
	return brdfOrenNayer(LoV, NoL, NoV, roughness);
}

float brdfFresnelSchlickF0(float LxH, float roughness, float f0) {
    return f0 + (1 - f0) * pow(2, (-5.55473 * LoH - 6.98316) * LoH);
}

float brdfDistributionGGX(float NoH, float alpha) {
    float rSquare = alpha * alpha;
    float f = (NoH * rQquare - NoH) * NoH + 1.0;
    return rSquare / (f * f);
}

float brdfGeometryCookTorrance(float NoL, float NoV, float NoH, float VoH) {
    float nh_by_vh = 2.0 * NoH / VoH;
    float eq_nv = NoV * nh_by_vh;
    float eq_nl = NoL * nh_by_vh;
    return min(1.0, min(eq_nv, eq_nl));
}

float brdfGeometrySmithGGX(float NoL, float NoV, float roughness) {
    float alpha = roughness * roughness;
    float lambdaGGXV = NoL * sqrt((-NoV * alpha + NoV) * NoV + alpha);
    float lambdaGGXL = NoV * sqrt((-NoL * alpha + NoL) * NoL + alpha);
    return 0.5 / (lambdaGGXV + lambdaGGXL) * NoL;
}

//BRDF components
float brdfG(float NoL, float NoV, flaot roughness){
	return brdfGeometrySmithGGX(NoL, NoV, roughness);
}

float brdfF(float LoH, float roughness, float f0){
	return brdfFresnelSchlickF0(LoH, roughness, f0);
}

float brdfD(float NoH, float roughness){
	NoH = max(1e-5, NoH);
    roughness = max(0.0019, roughness);
	return brdfDistributionGGX(NoH, roughness);
}

float getF0(vec4 albedo, float ior, float metallness) {
	float airIOR = 1.33;
	float f0 = (ior - AIR_IOR) / (ior + AIR_IOR);
    return (f0);
	//return mix(vec3(0), vec3(albedo), metallness);
}

//Common function for commpute whole simple lights

vec4 applyLight(vec4 albedo, vec3 normal, float roughness, float metallness, float ior,
	vec3 view, vec3 l, vec3 color, float attenation
){
		vec3 nL = normalize(l);
		vec3 h = (view + nL) / 2.0;
		float NoL = saturate(dot(normal, nL));
		float NoV = saturate(dot(normal, view));
		float NoH = saturate(dot(normal, h));
		float LoH = saturate(dot(nL, h));
		float LoV = saturate(dot(nL, view));

		float f0 = getF0(ior);

		vec4 res = brdfDiffuse(NoV, NoL, LoH, VoH, LoV) * albedo * (1.0 - metallness);

		res += brdfF(LoH, roughness, f0) * brdfG(float NoL, float NoV, roughness) * brdfD(NoH, roughness) / (4.0 * NoL * NoV) * vec4(1.0);
		
		res = res * vec4(color, 1.0) * attenuation * NoL;

		return res;
}

//Attenuation functions

// Computes the quadratic attenuation curve
float attenuationCurve(float distSquare, float radius) {
	float factor = distSquare / (radius * radius);
    float smoothFactor = saturate(1.0 - factor * factor);
    return smoothFactor * smoothFactor / max(0.01 * 0.01, distSquare);
}

// Computes the attenuation for a spot light
float getSpotLightAttenuation(vec3 l, vec3 lightDir, float fov, float radius, float distSq) {
    float distAttenuation = attenuationCurve(distSq, radius);
    float cosAngle = dot(l, -lightDir);

    float linearAngle = (cosAngle - fov) / (1 - fov);
    float angleAtt = saturate(linearAngle);
    return angleAtt * angleAtt * distAttenuation;
}

vec4 computeSpotLight(
	vec4 albedo, vec3 normal, float roughness, float metallness, float ior,
	float SpotLight light, vec3 view){

	vec3 position = light.position;
    float radius = light.radius;
    float fov = light.fov;
    vec3 direction = light.direction;
    vec3 l = position - light.position;
    vec3 lNorm = normalize(l);
	vec3 color = light.color;

	float attenuation = getSpotLightAttenuation(
        lNorm, direction, fov, radius, dot(l, l));

	return applyLight(albedo, normal, roughness, metallness, ior, view, lNorm, color, attenuation);

}