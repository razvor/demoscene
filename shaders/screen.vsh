#version 410

layout(location=0) in vec3 aPosition;
layout(location=1) in vec2 aTexCord;

uniform mat4 matrix;

out vec2 vTexCord;

void main(){
	
	vTexCord = aTexCord;

	gl_Position = matrix * vec4(aPosition, 1.0);
}
