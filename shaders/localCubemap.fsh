#version 410

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 basisMatrix;
uniform vec3 viewPosition;
uniform vec3 boundingBox;
uniform mat4 irradiance[3];
uniform int state; 
uniform samplerCube textureCube;
//uniform sampler2D frontSampler;
//uniform sampler2D backSampler;

in vec3 vPosition;
in vec3 vNormal;
in vec3 vTexCord;
layout(location=0) out vec4 FragColor;

const float PI = 3.14159265358979323846;

float 
	c1 = 0.429043,
	c2 = 0.511664,
	c3 = 0.743125, 
	c4 = 0.886227, 
	c5 = 0.247708;


float saturate(float s){
	return clamp(s, 0.0, 1.0);
}

float radicalInverse_VdC(uint bits) {
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

vec2 hammersley(uint i, uint N) {
	return vec2(float(i)/float(N), radicalInverse_VdC(i));
}

vec3 ImportanceSampleGGX(vec2 Xi, float Roughness, vec3 N){
	float a = Roughness * Roughness;

	float M_PI = 3.14;
	float Phi = 2 * M_PI * Xi.x;
	float CosTheta = sqrt((1 - Xi.y) / (1 + (a*a - 1) * Xi.y));
	float SinTheta = sqrt(1 - CosTheta * CosTheta);

	vec3 H;
	H.x = SinTheta * cos(Phi);
	H.y = SinTheta * sin(Phi);
	H.z = CosTheta;

	vec3 UpVector = abs(N.z) < 0.999 ? vec3(0, 0, 1) : vec3(1, 0, 0);
	vec3 TangentX = normalize(cross(UpVector, N));
	vec3 TangentY = cross(N, TangentX);

	// Tangent to world space
	return TangentX * H.x + TangentY * H.y + N * H.z;
}

vec3 importance_sample_lambert(vec2 Xi)
{
    float phi = 6.28 * Xi.x;
    float cos_theta = sqrt(Xi.y);
    float sin_theta = sqrt(1 - Xi.y);
    return vec3(sin_theta * cos(phi), sin_theta * sin(phi), cos_theta);
}

float D1(float NoH, float alpha){
	float CosSquared = NoH;
    float TanSquared = (1-CosSquared)/CosSquared;
    return (1.0/PI) * pow(alpha/(CosSquared * (alpha*alpha + TanSquared)), 2.0);
}

vec3 PrefilterEnvMap(float Roughness, vec3 R){
	vec3 N = R;
	vec3 V = R;
	float a = Roughness * Roughness;

	vec3 UpVector = abs(N.z) < 0.999 ? vec3(0, 0, 1) : vec3(1, 0, 0);
	vec3 TangentX = normalize(cross(UpVector, N));
	vec3 TangentY = cross(N, TangentX);
	vec3 H;
	
	vec3 PrefilteredColor = vec3(0);
	const uint NumSamples = 1024;
	float EnvMapSize = 256.0;
	float TotalWeight = 0;
	for (uint i = 0; i < NumSamples; i++){
		vec2 Xi = hammersley(i, NumSamples);
		//vec3 H = ImportanceSampleGGX(Xi, Roughness, N);
		
		float M_PI = 3.14;
		float Phi = 2 * M_PI * Xi.x;
		float CosTheta = sqrt((1 - Xi.y) / (1 + (a*a - 1) * Xi.y));
		float SinTheta = sqrt(1 - CosTheta * CosTheta);

		H.x = SinTheta * cos(Phi);
		H.y = SinTheta * sin(Phi);
		H.z = CosTheta;

		// Tangent to world space
		vec3 H = TangentX * H.x + TangentY * H.y + N * H.z;

		vec3 L = 2 * dot(V, H) * H - V;
		float NoL = saturate(dot(N, L));
  
		if (NoL > 0){
		  /*PrefilteredColor += texture( textureCube, L ).rgb * NoL; 
		  //EnvMap.SampleLevel(EnvMapSampler, L, 0).rgb * NoL;
		  TotalWeight += NoL;*/

		  /*float fNdotH = saturate(dot(N, H));
		  float fVdotH = saturate(dot(V, H));

		  // Probability Distribution Function
		  float fPdf = D1(fNdotH, a) * fNdotH / (4.0 * fVdotH);

		  // Solid angle represented by this sample
		  float fOmegaS = 1.0 / (NumSamples * fPdf);

		  // Solid angle covered by 1 pixel with 6 faces that are EnvMapSize X EnvMapSize
		  float fOmegaP = 4.0 * M_PI / (6.0 * EnvMapSize * EnvMapSize);
		  // Original paper suggest biasing the mip to improve the results
		  float fMipBias = 1.0;
		  float fMipLevel = max(0.5 * log2(fOmegaS / fOmegaP) + fMipBias, 0.0);*/

		  PrefilteredColor += textureLod(textureCube, L, 0).rgb * NoL;

		  TotalWeight += NoL;

		}
	}

	return PrefilteredColor / TotalWeight;
}


void main(){
	vec3 vp = viewPosition * state;

	vec3 final = vec3(0);
	vec3 fragPos = mat3(modelMatrix) * vPosition;
	vec3 normal = mat3(transpose(inverse(modelMatrix))) * vNormal;
	vec3 bbox = boundingBox;
	vec3 viewDir = viewPosition - fragPos;
	vec3 R = reflect( normalize(viewDir), normalize(normal) );
	//R = vec3( basisMatrix * vec4(R, 0) );

	float x,y;

	//vec3 ff = texture(textureCube, (vNormal) ).rgb;
	//vec3 RR = 2 * dot( viewDir, normal ) * viewDir - viewDir;
	//vec3 ff =  Prefilter3( 0.7, normal );
	//vec3 ff = vec3(0);

	mat4 M[3];

	vec4 nn = vec4(normal[0], normal[1], normal[2], 1.0);
	vec3 ff;
	for(int i=0; i<3; ++i){
		M[i] = irradiance[i];
		ff[i] = dot(nn, M[i] * nn);
	}

	//FragColor = vec4(texture(textureCube, normal).rgb, 1.0);
	//FragColor = vec4(ff, 1.0);
	FragColor = vec4(textureLod(textureCube, (normal), 1).rgb, 1.0);
}
