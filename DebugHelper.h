#ifndef DEBUG_HELPER
#define DEBUG_HELPER

#include "includer.h"

class DebugHelper{
private:

	//static DebugHelper *mInstance;
	DebugHelper();

public:

	~DebugHelper();

	static DebugHelper *getInstance();
	
	string getError();

	void printError(std::string p);

};

#endif
