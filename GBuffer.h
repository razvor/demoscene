#ifndef GBUFFER
#define GBUFFER

#include "includer.h"
#include "Texture.h"
#include "Framebuffer.h"

class GBuffer{
	private:

		const int mTexturesCount = 6;

		int mWidth, mHeight;

		//albedo
		//normals(rgb) + roughness(a)
		//position(rgb) + metal(a)
		//types(r:uint)

		std::string mTextureUnifNames[5]; //= 
		Framebuffer *mFramebuffer = NULL, 
					*mFinalFramebuffer = NULL;

		void initTextures();

	public:

		glm::uvec2 getSize();
		void setSize(int w, int h);
		Texture *getAlbedo();
		Texture *getNormals();
		Texture *getRMTD();
		Texture *getTypes();
		Texture *getFinal();

		void init();

		void beginCapture();
		void endCapture();

		void beginFinalCapture();
		void endFinalCapture();

		void show();
		void show(int n);
		void showFinal();

		int getPushCount(); //Find count of textures that pushes to shader
		void pushToShader(Program &p, int ind);

		GBuffer(int w, int h);
		~GBuffer();

};

#endif
