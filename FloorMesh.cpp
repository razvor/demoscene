#include "FloorMesh.h"

FloorMesh::FloorMesh(){}
FloorMesh::~FloorMesh(){}

void FloorMesh::init(){
	initGeometry();
	RenderMesh::init();
}

void FloorMesh::initGeometry(){
	
	float side = 10.f;

	std::vector<glm::vec3> verts = {
		glm::vec3(-1.0, -0.0, -1.0),
		glm::vec3(-1.0, -0.0, 1.0),
		glm::vec3(1.0, -0.0, 1.0),
		glm::vec3(1.0, -0.0, -1.0)
	};

	for(auto &v : verts){
	  v *= glm::vec3(side/2, 1, side/2);
	}

	std::vector<glm::vec3> norms = {
		glm::vec3(0, 1.0, 0),
		glm::vec3(0, 1.0, 0),
		glm::vec3(0, 1.0, 0),
		glm::vec3(0, 1.0, 0)
	};

	std::vector<int> inds = {
		0, 1, 2,
		2, 3, 0
	};

	glm::vec2 tt[4] = {
		glm::vec2(0, 0),
		glm::vec2(0, side),
		glm::vec2(side, side),
		glm::vec2(side, 0)
	};

	std::vector<glm::vec2> texCords = {
		tt[0], tt[1], 
		tt[2], tt[3]
	};
	
	//texCords.resize(verts.size(), glm::vec2(0));

	setVertices(verts);
	setNormals(norms);
	setIndices(inds);
	setTexCords(texCords);

	setPosition(glm::vec3(0, 0, 0));
	setRotation(glm::vec3(0));
	setScale(glm::vec3(1.0));

	/*Texture *tex = new Texture(
		"../models/Concrete_ChunkybrokenWall_Base_Color.png", 
		Texture::SizeMode::REPEAT
	);
	tex->setUniformName("tex");

	Texture *normalMap = new Texture(
		"../models/Concrete_ChunkybrokenWall_Normal.png", 
		Texture::SizeMode::REPEAT
	);
	normalMap->setUniformName("normalMap");

	Texture *roughnessMap = new Texture(
		"../models/Concrete_ChunkybrokenWall_Roughness.png",
		Texture::SizeMode::REPEAT
	);
	roughnessMap->setUniformName("roughnessMap");

	addTexture(tex);
	addTexture(normalMap);
	addTexture(roughnessMap);*/

	Material *material = new Material("../models/floor5");
	setMaterial(material);

}

void FloorMesh::initProgram(){
	mProgram = Program("mainVertex", "firstPass");
	if(!mProgram.init()){
		std::cerr << "Program init ERROR " << std::endl;
	}
}

void FloorMesh::initLocations(){
	RenderMesh::initLocations();
}

void FloorMesh::pushUniforms(){
	RenderMesh::pushUniforms();
}
