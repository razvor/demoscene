#ifndef LOCAL_CUBEMAP
#define LOCAL_CUBEMAP

#include "includer.h"
#include "Texture.h"
#include "RenderMesh.h"
#include "Framebuffer.h"
#include "FramebufferCube.h"
#include "TextureCube.h"

class LocalCubemap{
private:

	class Mesh : public RenderMesh{
	private:

		GLuint mTextureId;
		//Texture **mTextures;
		TextureCube *mTextureCube;
		glm::mat4 *irradiance;
		glm::mat4 *mViewMatrices;

		void initGeometry();

		struct UniformLocations{
		  int viewMatrix,
			  projectionMatrix,
			  modelMatrix,
			  viewPosition,
			  texCube,
			  basisMatrix;
		} unifLocations;

	public:

		void init();
		void initProgram();
		void initLocations();
		void setTextureId(GLuint textureId);
		void setTextureCube(TextureCube *tc);
		void setTextures(Texture **ts);
		void setViewMatrices(glm::mat4 *vms);
		void setIrradiance(glm::mat4 *ir);

		void pushUniforms();

	};

	Mesh mMesh;

	std::vector<RenderMesh *> mBindedMeshes;


	GLuint 
		mTextureId, 
		mDepthId;

	//Framebuffer *mFramebuffers[6];
	//Framebuffer *mFramebuffers[2];
	Texture **mTextures;
	TextureCube *textureCube;
	glm::mat4 irradiance[3];

	FramebufferCube *mPrefilterFramebuffer;
	VertexBuffer *mPrefilterVertexBuffer, *mPrefilterIndicesBuffer;
	TextureCube *mPrefTex;

	GLuint mFramebufferId;

	int 
		mBufferWidth = 128 * 2,
		mBufferHeight = 128 * 2;

	glm::vec3 mPosition;
	glm::mat4 mViewMatrices[6];

	Program mPrefilterProgram;
	GLuint texOut;

	void initTextures();
	void initBuffers();
	void initMatrices();

	void initMesh();

	void captureStart(int side);
	void captureEnd();
	void computeIrradiance();
	
public:

	TextureCube *mTextureCube;
	glm::mat4 vm, pm;

	void setPosition(glm::vec3 position);
	void setLights(Lights *lights);
	void bindMesh(RenderMesh *mesh);

	GLuint getTextureId();
	TextureCube *getTextureCube();
	glm::mat4 getViewMatrix(int ind);

	glm::vec3 getPosition();

	void init();
	void initPrefilter();

	void capture(std::function<void(glm::mat4 viewMatrix, float dir, Texture *t)> renderFunc);

	void renderMesh();
	void pushUniforms(Program &p);

	void pushToComponentShader(Program &p, int ui, int ti);

	void processIrradiance();
	void prefilter();

	LocalCubemap();
	~LocalCubemap();

};

#endif
