#include <iostream>
#include <vector>

//#include <OpenGL/gl.h>
//#include <GLUT/glut.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

using namespace std;

//#ifndef CHECKER
//#define CHECKER
/*GLenum glCheckError_(const char *file, int line)
{
    GLenum errorCode;
    while ((errorCode = glGetError()) != GL_NO_ERROR)
    {
        std::string error;
        switch (errorCode)
        {
            case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
            case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
            case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
            case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
            case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
            case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
            case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
        }
        std::cout << error << " | " << file << " (" << line << ")" << std::endl;
    }
    return errorCode;
}*/
//#define glCheckError() glCheckError_(__FILE__, __LINE__) 
//#endif

class VertexBuffer{

	GLuint handler;
	GLenum target;
	bool isTwice = false;

	void genBuffer(){
		glGenBuffers( 1, &handler );
	}
	
public:

	template< typename T >
	float *getLinear( vector< T > data, int stepSize ){
		int i,j,size;
		size = data.size();
		float *linearData = new float[ stepSize * size ];
		for( i = 0; i < size; i += stepSize )
			for( j = 0; j < stepSize; ++j )
				linearData[ i + j ] = data[ i ][ j ];

		return linearData;
	}
	
	void pushData( int type, vector< glm::vec3 > &data ){

		target = (type == 1) ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;

		glBindBuffer( target, handler );
		glBufferData( target, sizeof( glm::vec3 ) * data.size(), &data[0][0], GL_STATIC_DRAW );
		//glBindBuffer( target, 0 );

		//glCheckError_("vb", 67);

	}

	void pushData( int type, vector< int > &data ){

		target = (type == 1) ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;

		glBindBuffer( target, handler );
		glBufferData( target, sizeof( int ) * data.size(), &data[0], GL_STATIC_DRAW );
		//glBindBuffer( target, 0 );
	}

	void pushData( int type, vector< unsigned int > &data ){

		target = (type == 1) ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;

		/*glBindBuffer( target, handler );
		glBufferData( target, sizeof( size_t ) * data.size(), &data[0], GL_STATIC_DRAW );
		glBindBuffer( target, 0 );*/

		/*std::cout << "data: " << data.size() << " ";
		for(auto d : data){
			std::cout << d << " ";
		}
		std::cout << std::endl;*/

		//std::vector<size_t> ii; for(int i=0; i<36; ++i) ii.push_back(i);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handler);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * data.size(), &data[0], GL_STATIC_DRAW);

		//glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	}

	void pushData( int type, vector< glm::vec2 > data ){
		target = (type == 1) ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;
		isTwice = true;

		glBindBuffer( target, handler );
		glBufferData( target, sizeof( glm::vec2 ) * data.size(), &data[0][0], GL_STATIC_DRAW );
		//glBindBuffer( target, 0 );
	}

	void render( int attr  ){
		glBindBuffer(target, handler);
		if( target == GL_ARRAY_BUFFER ){
			glEnableVertexAttribArray(attr);
			glVertexAttribPointer(
				attr,
				isTwice ? 2 : 3,
				GL_FLOAT,
				GL_FALSE,
				0,
				//12,
				//(isTwice ? 2 : 3) * sizeof(float),
				(void*)0
			);
			glBindBuffer( target, 0 );
		}
		//glBindBuffer(target, 0);
	}


	VertexBuffer( int type, vector< glm::vec3 > &data ){
		genBuffer();
		pushData( type, data );
	}

	VertexBuffer( int type, vector< glm::vec2 > &data ){
		genBuffer();
		pushData( type, data );
	}

	VertexBuffer( int type, vector< int > &data ){
		genBuffer();
		pushData( type, data );
	}

	VertexBuffer( int type, vector< unsigned int > &data ){
		//std::cout << "sz" << std::endl;
		genBuffer();
		pushData( type, data );
	}

	int getInstance(){
		return handler;
	}

	VertexBuffer(){}

	~VertexBuffer(){
		glDeleteBuffers( 1, &handler );
	}

};
