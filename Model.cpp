#include "Model.h"

Model::Model(std::string path){
	mPath = path;

	mPath = "../models" + path;
}

Model::~Model() {

}

void Model::init(){
	Assimp::Importer importer;

	mScene = (aiScene*)importer.ReadFile(mPath, 
		aiProcess_Triangulate | 
		//aiProcess_GenNormals |
		//aiProcess_GenUVCoords |
		aiProcess_GenSmoothNormals |
		aiProcess_CalcTangentSpace
		//aiProcess_JoinIdenticalVertices
		//aiProcess_PreTransformVertices
	); 
	if(!mScene || mScene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !mScene->mRootNode) {
		std::cout << "ERROR::ASSIMP::" << importer.GetErrorString() << std::endl;
		getchar();
		exit(0);
    }

	std::cout << "Loaded" << std::endl;
	
	root = new ModelNode();
	processNode(mScene->mRootNode, root);

	postProcess();
}

void Model::postProcess() {
	std::cout << "PP Meshes count " << mMeshes.size() << std::endl;
}

void Model::processNode(aiNode *node, ModelNode *modelNode){
	auto toFace = getFaceNormalMeshes();
	int j = 0;

	for(GLuint i = 0; i < node->mNumMeshes; i++){
		aiMesh* mesh = mScene->mMeshes[node->mMeshes[i]]; 
		this->processMesh(mesh, modelNode);
	}
	for(GLuint i = 0; i < node->mNumChildren; i++){

		/*bool isToFace = false;
		while (j < toFace.size() && toFace[j] != i) ++j;
		if (j < toFace.size() && toFace[j] == i) isToFace = true;*/

		ModelNode *mn = new ModelNode();
		processNode(node->mChildren[i], mn);
		modelNode->children.push_back(mn);
	}
}

void Model::processMesh(aiMesh *mesh, ModelNode *modelNode){

	int ind = root->children.size();
	bool isToFace = false;
	auto toFace = getFaceNormalMeshes();
	if (std::find(toFace.begin(), toFace.end(), ind) != toFace.end()) {
		isToFace = true;
		std::cout << "to face " << ind << std::endl;
	}

	aiMaterial *material = mScene->mMaterials[mesh->mMaterialIndex];
	ObjMesh *om = new ObjMesh(mesh, material);
	if(isToFace) om->setFaceNormals();
	om->init();
	mMeshes.push_back(om);
	modelNode->meshes.push_back(om);
}

void Model::setMatrices(glm::mat4 view, glm::mat4 proj){
	mViewMatr = view;
	mProjMatr = proj;
}

void Model::setViewPosition(glm::vec3 pos){
	mViewPosition = pos;
}

void Model::setPosition(glm::vec3 p){
	for(auto mi : mMeshes){
		mi->setPosition(p);
	}
}

void Model::setScale(glm::vec3 p){
	for(auto mi : mMeshes){
		mi->setScale(p);
	}
}

void Model::setRotation(glm::vec3 r){
	for(auto mi : mMeshes){
		mi->setRotation(r);
	}
}

void Model::setMaterial(Material *m){
	for(auto mi : mMeshes){
		mi->setMaterial(m);
	}
}

ObjMesh* Model::getMesh(int ind) {
	if (mMeshes.size() > ind) return mMeshes[ind];
	else return NULL;
}

int Model::getMeshesCount() {
	return mMeshes.size();
}

void Model::render(){
	for(auto m : mMeshes){
		m->setProjectionMatrix(mProjMatr);
		m->setViewMatrix(mViewMatr);
		m->setViewPosition(mViewPosition);
		m->render();
	}
}

void Model::renderGeometry(Program &currentProgram){
	for(auto m : mMeshes){
		m->setProjectionMatrix(mProjMatr);
		m->setViewMatrix(mViewMatr);
		m->setViewPosition(mViewPosition);
		m->renderGeometry(currentProgram);
	}
}

std::vector<int> Model::getFaceNormalMeshes() {
	std::vector<int> r;
	return r;
}
