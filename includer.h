#ifndef INCLUDER
#define INCLUDER

#include <iostream>
#include <vector>

#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>

//#include <GL/glew.h>
#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <gli/gli.hpp>




#include "Program.cpp"
#include "VertexBuffer.cpp"


#endif
