#include "includer.h"
#include "Window.h"
#include "MainRenderer.h"

Program prog;
unsigned int VAO, VBO, EBO;

void test(){
	glEnable(GL_DEPTH_TEST);
	prog = Program("box", "box");
	prog.init();
    glGenVertexArrays(1, &VAO);
	std::vector<glm::vec3> vertices = {
		//front
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0),

		//back
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(-1.0, -1.0, 1.0),

		//top
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(-1.0, 1.0, -1.0),

		//bottom
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0),

		//right
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, -1.0, -1.0),

		//left
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0)
	};

	std::vector<unsigned int> indices;
	for(size_t i=0;i<36;++i) indices.push_back(i);
	glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	//glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(size_t) * indices.size(), &indices[0], GL_STATIC_DRAW);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

int main(){

	const int viewportWidth = 800 * 2;
	const int viewportHeight = 700 * 2;

	//Texture("../models/Body_OP.png");
	
	MainRenderer renderer;
	renderer.setViewportSize(viewportWidth, viewportHeight);
	renderer.setViewPosition(glm::vec3(0, 1, 0));

	Window window;
	window.setViewportSize(viewportWidth, viewportHeight);
	window.setDrawFunction(
		std::bind(&MainRenderer::render, &renderer)
		/*[&](){
			//std::cout << "a" << std::endl;

			glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			prog.use();

			glm::mat4 projection = glm::perspective(30.f, (float)600.f / (float)500.f, 0.1f, 1000.0f);
			glm::mat4 view = glm::lookAt(glm::vec3(5.0, 5.0, 5.0), glm::vec3(0), glm::vec3(0, 1, 0));

			int location = prog.uniform("projectionMatrix");
			glUniformMatrix4fv(location, 1, GL_FALSE, &projection[0][0]);

			location = prog.uniform("viewMatrix");
			glUniformMatrix4fv(location, 1, GL_FALSE, &view[0][0]);

			location = prog.uniform("modelMatrix");
			glUniformMatrix4fv(location, 1, GL_FALSE, &(glm::mat4(1))[0][0]);

			glBindVertexArray(VAO);
			//glDrawArrays(GL_TRIANGLES, 0, 36);
			glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
			glBindVertexArray(0);
			
		}*/
	);
	window.setEventFunction(
	 	std::bind(&MainRenderer::event, &renderer, std::placeholders::_1, std::placeholders::_2)
		/*[&](int a, int b){
		
		}*/
	);
	if(!window.init()) return 1;
	glGetError();

	renderer.setWindow(window);
	renderer.init();

	//Initing
	

	//End init

	window.start();
	window.end();

	return 0;
}
