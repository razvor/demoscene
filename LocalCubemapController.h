#include "includer.h"
#include "LocalCubemap.h"

#ifndef LOCAL_CUBEMAP_CONTROLLER
#define LOCAL_CUBEMAP_CONTROLLER

class LocalCubemapController{

private:
	
	std::vector<LocalCubemap*> mCubemaps;
	std::vector<glm::mat4> mMatrices;
	bool mUpdated = false;

	void packMaps();

public:

	void addCubemap(LocalCubemap *mCubemap);

	void pushToComponentShader();

	void update();
	
};

#endif

