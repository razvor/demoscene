#ifndef DIFFERRED_COMPONENT
#define DIFFERRED_COMPONENT

#include "includer.h"
#include "GBuffer.h"

class DifferredComponent{
private:

	GBuffer *mGBuffer;
	Texture *mTex;
	
	Program mProgram;
	VertexBuffer *mVerticesBuffer;
	VertexBuffer *mIndicesBuffer;

	GLuint mVAO;

	bool mRenderToBuffer;
	
	int mState;

	int mStateLocation;

	void initGeometry();


public:
	void initProgram();
	virtual void initLocations();
	virtual void pushAdditional();

	void setGBuffer(GBuffer *gb);
	void setProgram(Program p);
	void setState(int state);
	void setRenderToBuffer(bool rtb);

	GBuffer* getGBuffer();
	Program &getProgram();

	void init();
	void render();
	void render(int w, int h);

	DifferredComponent();
	~DifferredComponent();
};

#endif
