#ifndef CELLING_MESH
#define CELLING_MESH

#include "RenderMesh.h"

class CellingMesh : public RenderMesh{
private:

	void initGeometry();
	
public:

	void initProgram();
	void init();

	CellingMesh();
	~CellingMesh();
};

#endif
