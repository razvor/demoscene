﻿#include "Voxelizator.h"

Voxelizator::Voxelizator(){

}

Voxelizator::~Voxelizator(){

}

void Voxelizator::setMeshes(std::vector<RenderMesh*> &m){
	mMeshes = m;
}

void Voxelizator::setModels(std::vector<Model*> &m) {
	mModels = m;
}

void Voxelizator::init(){
	initShaders();
	initTexture();
	initBuffers();

	for(auto mi : mMeshes){
		mi->setGridSize(gridSize);
		mi->setZoneSize(zoneSize);
		mi->setVoxelTextureId(mVoxelTex);
		//mFirstPassProgram = mi->mProgram;
	}

	for(auto mo : mModels){
		for (auto mmi : mo->mMeshes) {
			mmi->setGridSize(gridSize);
			mmi->setZoneSize(zoneSize);
			mmi->setVoxelTextureId(mVoxelTex);
		}
	}

}

void Voxelizator::render(){

	glfwSetTime(0);

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	glViewport(0, 0, gridSize, gridSize);
	glClearColor(0.f, 0.f, 0.f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	genAtomicBuffer(&mCountAtomicBuffer);

	for(auto mi : mMeshes){
		mi->setGridSize(gridSize);
		mi->setVoxelState(0);
		mi->setVoxelTextureId(mVoxelTex);
		mi->setVoxelAtomicBuffer(mCountAtomicBuffer);
		mi->setProgram(mProgram);
		//mi->render();
	}

	for(auto mo : mModels){
		for (auto mmi : mo->mMeshes) {
			mmi->setGridSize(gridSize);
			mmi->setVoxelState(0);
			mmi->setVoxelTextureId(mVoxelTex);
			mmi->setVoxelAtomicBuffer(mCountAtomicBuffer);
			mmi->setProgram(mProgram);
			mmi->render();
		}
	}

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);

	glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, mCountAtomicBuffer);
	GLuint *count = (GLuint*)glMapBufferRange(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(GLuint), GL_MAP_READ_BIT);
	unsigned int voxelsCount = count[0];
	unsigned int bufferSize = sizeof(GLuint) * voxelsCount;
	glUnmapBuffer(GL_ATOMIC_COUNTER_BUFFER);
	glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);
	
	genLinearBuffer(4 * bufferSize, GL_RGBA16UI, &mVoxelPosTex, &mVoxelPosTexBuf);
	genLinearBuffer(4 * bufferSize, GL_RGBA8, &mVoxelKdTex, &mVoxelKdTexBuf);

	glDeleteBuffers(1, &mCountAtomicBuffer);
	mCountAtomicBuffer = 0;
	genAtomicBuffer(&mCountAtomicBuffer);

	//run voxelize second time to fill pull
	for(auto mi : mMeshes){
		mi->setVoxelState(1);
		mi->setVoxelKdTexId(mVoxelKdTex);
		mi->setVoxelPosTexId(mVoxelPosTex);
		mi->setVoxelTextureId(mVoxelTex);
		mi->setVoxelAtomicBuffer(mCountAtomicBuffer);
		mi->render();
	}

	for(auto mo : mModels){
		for (auto mmi : mo->mMeshes) {
			mmi->setVoxelState(1);
			mmi->setVoxelKdTexId(mVoxelKdTex);
			mmi->setVoxelPosTexId(mVoxelPosTex);
			mmi->setVoxelTextureId(mVoxelTex);
			mmi->setVoxelAtomicBuffer(mCountAtomicBuffer);
			mmi->render();
		}
	}
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);

	glDeleteBuffers(1, &mCountAtomicBuffer);
	mCountAtomicBuffer = 0;

	/*glBindBuffer(GL_TEXTURE_BUFFER, mVoxelKdTexBuf);
	std::vector<GLuint> dd(voxelsCount * 4, 0);
	glGetBufferSubData(GL_TEXTURE_BUFFER, 0, sizeof(GLuint) * voxelsCount * 4, &dd[0]);
	std::cout << std::endl << std::endl << std::endl;
	std::cout << "DIFF" << std::endl;
	for (int i = 0; i < 100; ++i) {
		std::cout << "( " << dd[i*4] << " " << dd[i*4+1] << " " << dd[i*4+2] << " ) ";
	}
	std::cout << std::endl << std::endl << std::endl;
	glBindBuffer(GL_TEXTURE_BUFFER, 0);*/

	/*for (int i = 0; i < 6; ++i) {
		mDownSampleProgram.use();
		glUniform1i(mOctreeFlagProgram.uniform("level"), i);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_3D, mVoxelTex);
		glUniform1i(mDownSampleProgram.uniform("inTexture"), 0);
		glBindImageTexture(0, mVoxelTex, i+1, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA16UI);
		int work = gridSize / (pow(2, i+1));
		glDispatchCompute(work, work, work);
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	}*/

	glActiveTexture(GL_TEXTURE0 + 40);
	glBindTexture(GL_TEXTURE_3D, mVoxelTex);
	glGenerateMipmap(GL_TEXTURE_3D);
	glBindTexture(GL_TEXTURE_3D, 0);

	//buildSVO(voxelsCount);

	turnBackState();

}

void Voxelizator::buildSVO(int voxelsCount) {
	

	GLuint allocCounter = 0;
	std::vector<int> allocList;
	allocList.push_back(1);
	int levelsCount = log2(gridSize);

	unsigned int maxNodes = 1;
	unsigned int tmp = 1;
	for (int i = 1; i <= levelsCount; ++i) {
		if(tmp <= voxelsCount ) tmp *= 8;
		maxNodes += tmp;
	}

	std::cout << "voxels count " << voxelsCount << std::endl;
	std::cout << "maxNodes " << maxNodes << std::endl;

	genLinearBuffer(sizeof(GLuint) * maxNodes, GL_R32UI, &mOctreeChildTex, &mOctreeChildBuf);
	genLinearBuffer(sizeof(GLuint) * maxNodes, GL_R32UI, &mOctreeDiffuseTex, &mOctreeDiffuseBuf);

	genAtomicBuffer(&allocCounter);

	int nodeOffset = 0;
	int allocOffset = 1;
	int dataWidth = 1024;
	int dataHeight = (voxelsCount + 1023) / dataWidth;
	int groupDimX = dataWidth / 8;
	int groupDimY = (dataHeight + 7) / 8;
	for (int i = 0; i < levelsCount; ++i) {
		mOctreeFlagProgram.use();
		glUniform1i(mOctreeFlagProgram.uniform("voxelsCount"), voxelsCount);
		glUniform1i(mOctreeFlagProgram.uniform("gridSize"), gridSize);
		glUniform1i(mOctreeFlagProgram.uniform("level"), i);
		glBindImageTexture(0, mVoxelPosTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA16UI);
		glBindImageTexture(1, mOctreeChildTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
		glDispatchCompute(groupDimX, groupDimY, 1);
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

		mOctreeAllocProgram.use();
		glUniform1i(mOctreeAllocProgram.uniform("indStart"), nodeOffset); //start index in pull to write new tiles
		glUniform1i(mOctreeAllocProgram.uniform("childStart"), allocOffset); //start index of tile
		glUniform1i(mOctreeAllocProgram.uniform("num"), allocList[i]); //num threads
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, allocCounter);
		glBindImageTexture(0, mOctreeChildTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);

		int allocGroupDim = (allocList[i] + 63) / 64;
		glDispatchCompute(allocGroupDim, 1, 1);
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_ATOMIC_COUNTER_BARRIER_BIT);

		GLuint tileAllocated;
		GLuint reset = 0;
		glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, allocCounter);
		glGetBufferSubData(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(GLuint), &tileAllocated);
		glBufferSubData(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(GLuint), &reset); //reset counter to zero
		glUnmapBuffer(GL_ATOMIC_COUNTER_BUFFER);
		glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);

		mOctreeInitProgram.use();
		int nodeAllocated = tileAllocated * 8;
		glUniform1i(mOctreeInitProgram.uniform("indStart"), allocOffset);
		glUniform1i(mOctreeInitProgram.uniform("num"), nodeAllocated); //num threads
		glBindImageTexture(0, mOctreeChildTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
		glBindImageTexture(1, mOctreeDiffuseTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);

		dataWidth = 1024;
		dataHeight = (nodeAllocated + 1023) / dataWidth;
		int initGroupDimX = dataWidth / 8;
		int initGroupDimY = (dataHeight + 7) / 8;
		glDispatchCompute(initGroupDimX, initGroupDimY, 1);
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

		allocList.push_back(nodeAllocated);
		nodeOffset += allocList[i];
		allocOffset += nodeAllocated;

		//Debug
		/*glBindBuffer(GL_TEXTURE_BUFFER, mOctreeChildBuf);
		std::vector<GLuint> dd(maxNodes, 0);
		glGetBufferSubData(GL_TEXTURE_BUFFER, 0, maxNodes, &dd[0]);
		std::cout << std::endl << std::endl << std::endl;
		std::cout << "dat" << std::endl;
		for (int i = 0; i < 201; ++i) {
			std::cout << std::hex << dd[i] << " ";
		}
		std::cout << std::endl << std::endl << std::endl;
		glBindBuffer(GL_TEXTURE_BUFFER, 0);*/
		//End Debug

		//if (i == 2) getchar();
		
	}
	//getchar();
	//return;*/

	mOctreeFlagProgram.use();
	glUniform1i(mOctreeFlagProgram.uniform("voxelsCount"), voxelsCount);
	glUniform1i(mOctreeFlagProgram.uniform("gridSize"), gridSize);
	glUniform1i(mOctreeFlagProgram.uniform("level"), levelsCount);
	glBindImageTexture(0, mVoxelPosTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA16UI);
	glBindImageTexture(1, mOctreeChildTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
	glDispatchCompute(groupDimX, groupDimY, 1);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	mOctreeLeafProgram.use();
	glUniform1i(mOctreeFlagProgram.uniform("voxelsCount"), voxelsCount);
	glUniform1i(mOctreeFlagProgram.uniform("gridSize"), gridSize);
	glUniform1i(mOctreeFlagProgram.uniform("level"), levelsCount);
	glBindImageTexture(0, mVoxelPosTex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA16UI);
	glBindImageTexture(1, mVoxelKdTex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8);
	glBindImageTexture(2, mOctreeChildTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
	glBindImageTexture(3, mOctreeDiffuseTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
	glDispatchCompute(groupDimX, groupDimY, 1);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	for (int i = levelsCount; i >= 1; --i) {
		mOctreeMipmapProgram.use();
		glUniform1i(mOctreeMipmapProgram.uniform("voxelsCount"), voxelsCount);
		glUniform1i(mOctreeMipmapProgram.uniform("gridSize"), gridSize);
		glUniform1i(mOctreeMipmapProgram.uniform("mip"), i);
		glBindImageTexture(0, mVoxelPosTex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA16UI);
		glBindImageTexture(1, mOctreeChildTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
		glBindImageTexture(2, mOctreeDiffuseTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
		glDispatchCompute(groupDimX, groupDimY, 1);
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	}

	glDeleteBuffers(1, &allocCounter);
	allocCounter = 0;

	//getchar();

	/*glBindBuffer(GL_TEXTURE_BUFFER, mVoxelKdTexBuf);
	std::vector<GLint> dd(voxelsCount * 2, 0);
	glGetBufferSubData(GL_TEXTURE_BUFFER, 0, maxNodes, &dd[0]);
	std::cout << std::endl << std::endl << std::endl;
	std::cout << "dat" << std::endl;
	for (int i = 0; i < 100; ++i) {
		std::cout << std::dec << " (" << dd[i*4] << " " << dd[i*4+1] << " " << dd[i*4+2] << ")  ";
	}
	std::cout << std::endl << std::endl << std::endl;
	glBindBuffer(GL_TEXTURE_BUFFER, 0);*/

	/*glBindBuffer(GL_TEXTURE_BUFFER, mOctreeDiffuseBuf);
	std::vector<GLuint> dd(maxNodes, 0);
	glGetBufferSubData(GL_TEXTURE_BUFFER, 0, maxNodes, &dd[0]);
	std::cout << std::endl << std::endl << std::endl;
	std::cout << "dat" << std::endl;
	for (int i = 0; i < 200; ++i) {
		std::cout << std::hex << dd[i] << " ";
	}
	std::cout << std::endl << std::endl << std::endl;
	glBindBuffer(GL_TEXTURE_BUFFER, 0);*/

	//getchar();

	std::cout << "Framerate " << 1.0 / glfwGetTime() << std::endl;

}

void Voxelizator::show(glm::mat4 v, glm::mat4 p, glm::vec3 viewPosition){
	
	glEnable(GL_DEPTH_TEST);

	//return;

	/*Debug*/
		/*GLubyte *read = new GLubyte[gridSize * gridSize * gridSize * 4];
		glBindTexture(GL_TEXTURE_3D, mVoxelTex);
		glGetTexImage(GL_TEXTURE_3D, 0, GL_RGBA, GL_UNSIGNED_BYTE, &read[0]);
		glBindTexture(GL_TEXTURE_3D, 0);
		//std::cout << "TEX IMAGE COLOR " << (unsigned int)read[0] << std::endl;
		delete[] read;*/
	/*End of debug*/

	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	
	mDebugProgram.use();

	glBindImageTexture(0, mOctreeDiffuseTex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32UI);
	glBindImageTexture(1, mOctreeChildTex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32UI);
	//glBindImageTexture(5, mVoxelTex, 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA8);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D, mVoxelTex);
	glUniform1i(mDebugProgram.uniform("voxelTexture1"), 0);

	glUniform1f(mDebugProgram.uniform("zoneSize"), zoneSize);
	glUniform1i(mDebugProgram.uniform("gridSize"), gridSize);
	glUniform1i(mDebugProgram.uniform("levels"), log2(gridSize));
	glUniform3f(mDebugProgram.uniform("viewPosition"), viewPosition.x, viewPosition.y, viewPosition.z);
	glUniformMatrix4fv(mDebugProgram.uniform("projectionMatrix"), 1, GL_FALSE, &p[0][0]);
	glUniformMatrix4fv(mDebugProgram.uniform("viewMatrix"), 1, GL_FALSE, &v[0][0]);

	/*glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D, mVoxelTex);
	glUniform1i(mDebugProgram.uniform("voxelTexture"), 0);*/
	
	glBindVertexArray(mDebugVAO);
	//glDrawArrays(GL_POINTS, 0, gridSize * gridSize * gridSize );
	glDrawArrays(GL_POINTS, 0, 1);
	glBindVertexArray(0);

	//getchar();
}

void Voxelizator::turnBackState(){
	for(auto mi : mMeshes){
		mi->setVoxelState(1);
		mi->setGridSize(0);
		mi->setProgram(mFirstPassProgram);
		mi->render();
	}

	for(auto mo : mModels){
		for (auto mmi : mo->mMeshes) {
			mmi->setVoxelState(1);
			mmi->setGridSize(0);
			mmi->setProgram(mFirstPassProgram);
			mmi->render();
		}
	}
}

void Voxelizator::initShaders(){
	mProgram = Program("voxelize", "voxelize1", "voxelization");	
	if (!mProgram.init()) {
		std::cerr << "Error vox program" << std::endl;
		getchar();
		exit(0);
	}

	mDebugProgram = Program("voxShow", "voxShow", "voxShow");
	if (!mDebugProgram.init()) {
		std::cerr << "Error deb vox program" << std::endl;
		getchar();
		exit(0);
	}

	mFirstPassProgram = Program("mainVertex", "firstPass");
	if (!mFirstPassProgram.init()) {
		std::cerr << "Error first pass program" << std::endl;
		getchar();
		exit(0);
	}

	mOctreeFlagProgram = Program("voxelOctreeBuild");
	if (!mOctreeFlagProgram.init()) {
		std::cerr << "Error octree flag program" << std::endl;
		getchar();
		exit(0);
	}

	mOctreeAllocProgram = Program("voxelOctreeAlloc");
	if (!mOctreeAllocProgram.init()) {
		std::cerr << "Error octree alloc program" << std::endl;
		getchar();
		exit(0);
	}

	mOctreeInitProgram = Program("voxelOctreeInit");
	if (!mOctreeInitProgram.init()) {
		std::cerr << "Error octree init program" << std::endl;
		getchar();
		exit(0);
	}

	mOctreeLeafProgram = Program("voxelOctreeLeaf");
	if (!mOctreeLeafProgram.init()) {
		std::cerr << "Error octree leaf program" << std::endl;
		getchar();
		exit(0);
	}

	mOctreeMipmapProgram = Program("voxelOctreeMipmap");
	if (!mOctreeMipmapProgram.init()) {
		std::cerr << "Error octree mipmap program" << std::endl;
		getchar();
		exit(0);
	}

	mDownSampleProgram = Program("textureMipMap");
	if (!mDownSampleProgram.init()) {
		std::cerr << "" << std::endl;
		getchar();
		exit(0);
	}
}

void Voxelizator::initTexture(){
	glEnable(GL_TEXTURE_3D);
	glGenTextures(1, &mVoxelTex);
	glBindTexture(GL_TEXTURE_3D, mVoxelTex);
	int voxelsCount = gridSize * gridSize * gridSize;
	std::vector<GLubyte> data;
	data.resize(voxelsCount * 4);
	for(int i=0; i<voxelsCount*4; ++i){
		data[i] = 0;
	}
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexStorage3D(GL_TEXTURE_3D, 7, GL_RGBA16F, gridSize, gridSize, gridSize);
	glBindTexture(GL_TEXTURE_3D, 0);

	//tetet

	std::cout << "int" << std::endl;

	/*glGenTextures(1, &mVoxelTex0);
	glBindTexture(GL_TEXTURE_2D, mVoxelTex0);
	std::vector<GLubyte> dd;
	dd.resize(gridSize * gridSize * 4);
	for(int i=0; i<gridSize * gridSize * 4; ++i){
		dd[i] = 0xFFFFFFFF;
	}
	std::cout << 1 << std::endl;
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, gridSize, gridSize, 0, GL_RGBA, GL_UNSIGNED_BYTE, &dd[0]);
	std::cout << 2 << std::endl;
	glBindTexture(GL_TEXTURE_2D, 0);*/

	std::cout << "0int" << std::endl;
}

void Voxelizator::initBuffers(){
	std::vector<glm::vec3> quad = { 
		glm::vec3(-1.0f, -1.0f, 0.0f),
		glm::vec3(1.0f, -1.0f, 0.0f),
		glm::vec3(-1.0f,  1.0f, 0.0f),
		glm::vec3(-1.0f,  1.0f, 0.0f),
		glm::vec3(1.0f, -1.0f, 0.0f),
		glm::vec3(1.0f,  1.0f, 0.0f),
	};

	VertexBuffer vb = VertexBuffer(1, quad);

	glGenVertexArrays(1, &mDebugVAO);
	glBindVertexArray(mDebugVAO);
	
	//vb.render(0);
	//glEnableVertexAttribArray(0);
	//glVertexAttribPointer(0, 3,	GL_FLOAT, GL_FALSE, 0, quad);
	glBindVertexArray(0);

	glGenBuffers(1, &mAtomicReadBuffer);
}

void Voxelizator::prepare(){
	for(auto mi : mMeshes){
		mi->setVoxelState(1);
		mi->setGridSize(0);
		mi->setProgram(mFirstPassProgram);
		mi->render();
	}

	for(auto mo : mModels){
		for (auto mmi : mo->mMeshes) {
			mmi->setVoxelState(1);
			mmi->setGridSize(0);
			mmi->setProgram(mFirstPassProgram);
			mmi->render();
		}
	}
}

GLuint Voxelizator::getVoxelTextureId() {
	return mVoxelTex;
}

GLuint Voxelizator::getSVOChildId() {
	return mOctreeChildTex;
}

GLuint Voxelizator::getSVODiffuseId() {
	return mOctreeDiffuseTex;
}

int Voxelizator::getVoxelGridSize() {
	return gridSize;
}

void Voxelizator::genLinearBuffer(unsigned int size, GLenum format, GLuint *tex, GLuint *buf){
	if ((*buf) > 0)
		glDeleteBuffers(1, buf);

	glGenBuffers(1, buf);
	glBindBuffer(GL_TEXTURE_BUFFER, *buf);
	glBufferData(GL_TEXTURE_BUFFER, size, 0, GL_STATIC_DRAW);
	//glBufferData()

	if ((*tex) > 0)
		glDeleteTextures(1, tex);

	glGenTextures(1, tex);
	glBindTexture(GL_TEXTURE_BUFFER, *tex);
	glTexBuffer(GL_TEXTURE_BUFFER, format, *buf);
	glBindBuffer(GL_TEXTURE_BUFFER, 0);
}

void Voxelizator::genAtomicBuffer(GLuint *buf) {
	if ((*buf) > 0)
		glDeleteBuffers(1, buf);

	int initVal = 0;
	glGenBuffers(1, buf);
	glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, *buf);
	glBufferData(GL_ATOMIC_COUNTER_BUFFER, sizeof(GLuint), &initVal, GL_STATIC_DRAW);
	glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);
}

void Voxelizator::genFastAtomicBuffer(GLuint *buf, GLuint *bufRead) {

	if ((*buf) > 0)
		glDeleteBuffers(1, buf);

	glGenBuffers(1, buf);
	int initVal = 0;
	glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, *buf);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, *buf);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(GLuint) * 1, &initVal, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}
