#ifndef TEXTURE_CUBE
#define TEXTURE_CUBE

#include "includer.h"

class TextureCube{
private:

	GLuint mId;
	GLint mInternalFormat;
	GLenum mFormat, mDataType;
	GLuint dataType;
	bool isAnisotropy;
	int mWidth, mHeight;
	GLuint mWrapT, mWrapW;
	std::string mUnifName;

	bool mIsInited = false;

	const int mTexturesCount = 6;

	void create();
	
public:

	GLint getId();
	glm::vec2 getSize();

	void setUnifName(std::string name);
	void setInternalFormat(GLint f);
	void setFormat(GLenum f);
	void setDataType(GLenum f);
	void setUniformName(std::string un);

	void init();
	void pushToShader(Program &p, int ind);

	TextureCube(int w, int h);
	~TextureCube();

};

#endif
