#include "BoxMesh.h"

BoxMesh::BoxMesh(){
}
BoxMesh::~BoxMesh(){
}

void BoxMesh::init(){
	initGeometry();
	initLights();
	RenderMesh::init();
}

void BoxMesh::initGeometry(){

	std::vector<glm::vec3> verts = {
		//front
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0),

		//back
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(-1.0, -1.0, 1.0),

		//top
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(-1.0, 1.0, -1.0),

		//bottom
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0),

		//right
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, -1.0, -1.0),

		//left
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0)

	};

	std::vector<unsigned int> inds;
	for(unsigned int i=0; i<36; ++i){
		inds.push_back(i);
	}

	glm::vec3 nds[] = {
		glm::vec3(0, 0, -1.0),
		glm::vec3(0, 0, 1.0),
		glm::vec3(0, 1.0, 0),
		glm::vec3(0, -1.0, 0),
		glm::vec3(1.0, 0, 0),
		glm::vec3(-1.0, 0, 0)
	};

	std::vector<glm::vec3> norms;
	for(int i=0; i<6; ++i){
		for(int j=0; j<6; ++j){
			norms.push_back(nds[i]);
		}
	}

	std::vector<glm::vec2> texCords;
	for(int i=0; i<6; ++i){
		texCords.push_back(glm::vec2(0, 0));
		texCords.push_back(glm::vec2(0, 1));
		texCords.push_back(glm::vec2(1, 1));
		texCords.push_back(glm::vec2(1, 1));
		texCords.push_back(glm::vec2(1, 0));
		texCords.push_back(glm::vec2(0, 0));
		/*texCords.push_back(glm::vec2(0, 0));
		texCords.push_back(glm::vec2(1, 0));
		texCords.push_back(glm::vec2(1, 1));
		texCords.push_back(glm::vec2(1, 1));
		texCords.push_back(glm::vec2(0, 1));
		texCords.push_back(glm::vec2(0, 0));*/
	}

	setVertices(verts);
	setNormals(norms);
	setIndices(inds);
	setTexCords(texCords);

	/*vector<glm::vec3> verts = {
		glm::vec3(-1.0, -1.0, 0.0),
		glm::vec3(1.0, -1.0, 0.0),
		glm::vec3(-1.0, -1.0, 0.0)
	};

	vector<glm::vec3> norms = {
		glm::vec3(0, 0, 1.0),
		glm::vec3(0, 0, 1.0),
		glm::vec3(0, 0, 1.0),
	};

	vector<glm::vec2> texCords(3, glm::vec2(0));
	vector<int> inds = { 0, 1, 2 };
	vector<glm::vec3> tns(3, glm::vec3(1, 0, 0));
	vector<glm::vec3> btns(3, glm::vec3(0, 1, 0));

	setVertices(verts);
	setNormals(norms);
	setIndices(inds);
	setTexCords(texCords);
	setTangents(tns);
	setBiTangents(btns);*/

	setPosition(glm::vec3(0, 0, 0));
	setRotation(glm::vec3(0));
	setScale(glm::vec3(2.0));

	//Texture *tex = new Texture("../models/flaking-limestone1-albedo.png");
	//Texture *tex = new Texture("../models/room_materials/sky/albedo.jpg");
	//Texture *tex = new Texture("../models/wall.jpg");
	Texture *tex = new Texture(glm::vec4(0.3, 0.3, 0.3, 1.0));
	tex->setUniformName("albedo");

	Texture *normalMap = new Texture("../models/flaking-limestone1-normal.png");
	normalMap->setUniformName("normals");

	Texture *roughnessMap = new Texture("../models/flaking-limestone1-roughness.png");
	roughnessMap->setUniformName("roughness");

	addTexture(tex);
	addTexture(normalMap);
	addTexture(roughnessMap);

	setType(3);

	/*Material *material = new Material("../models/deck1");
	setMaterial(material);*/
}

void BoxMesh::initLights(){
}

void BoxMesh::initProgram(){
	mProgram = Program("mainVertex", "firstPass");
	//mProgram = Program("mainVertex", "box");
	if(!mProgram.init()){
		std::cerr << "Program init ERROR " << std::endl;
	}
}

void BoxMesh::initLocations(){
	RenderMesh::initLocations();
}

void BoxMesh::pushUniforms(){
	RenderMesh::pushUniforms();
}

void BoxMesh::render(){
	RenderMesh::render();
	//r += 0.01;
	setRotation(glm::vec3(0,r,0));
}

void BoxMesh::setTTT(GLuint a){
  if (mTextures.size() > 0) {
	mTextures[0]->mId = a;
  }
}
