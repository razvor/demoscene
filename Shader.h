#ifndef SHADER
#define SHADER

#include <map>
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "Program.cpp"

class ShaderInput{
public:
	int inputType;
	int valueType;

	int location;
	string locationName;

	virtual void passToShader();
};

class ShaderVec3ArrInput : ShaderInput{
public:
  std::vector<glm::vec3> value;
  void passToShader();
};

class ShaderVec2ArrInput : ShaderInput{
public:
  std::vector<glm::vec2> value;
  void passToShader();
};

class ShaderVec3Input : ShaderInput{
public:
  std::vec3 value;
  void passToShader();
};

class ShaderIndicesInput : ShaderInput{
public:
  std::vector<int> value;
  void passToShader();
};

class ShaderMat4Input : ShaderInput{
public:
  glm::mat4 value;
  void passToShader();
};

class ShaderMat4Input : ShaderInput{
public:
  glm::mat4 value;
  void passToShader();
};

class ShaderTextureInput : ShaderInput{
public:
  //Texture value
  void passToShader();
};

class Shader{
private:

	std::vector<ShaderInput> mInputs;

	Program mProgram;

	
public:

	void addInput(ShaderInput inp);
	
	void init();
	
	void render();

	Shader( string vertex, string fragment );
	~Shader();
};

#endif
