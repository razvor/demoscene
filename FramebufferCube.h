#ifndef FRAMEBUFFER_CUBE
#define FRAMEBUFFER_CUBE

#include "includer.h"
#include "TextureCube.h"
#include "Texture.h"

class FramebufferCube{
private:

	GLuint mId;
	TextureCube *mTextureCube;
	Texture *mDepth;

	int mWidth, mHeight;

	const int mTexturesCount = 6;

	int mLevel = 0;

	void initDepth();
	void initBuffer();

	void prepare(int tex);
	
public:

	GLint getId();

	void init();

	void swapTexture(TextureCube *ntc);

	void setLevel(int);

	void beginCapture(int tex);
	void endCapture();
	
	FramebufferCube(TextureCube *tc);
	FramebufferCube(int w, int h);
	
	~FramebufferCube();
};

#endif
