#include "DifferredRenderer.h"
//#include "glMessageCallback.h"

void DifferredRenderer::setMatrices(glm::mat4 proj, glm::mat4 view){
	mProjectionMatrix = proj;
	mViewMatrix = view;
}

void DifferredRenderer::setLights(Lights *l){
	mLights = l;
}

void DifferredRenderer::setViewportSize(int w, int h){
	mViewportWidth = w;
	mViewportHeight = h;
}

void DifferredRenderer::setViewPosition(glm::vec3 vp){
	mViewPosition = vp;
}

void DifferredRenderer::setState(int s){
	mState = s;

	//if(mFirstLightComponent)
	mFirstLightComponent->setState(mState);

	switch(mState){
		case 1:
			mFirstLightComponent->setGBuffer(mLCMGBuffer);
			break;

		case 2:
			mFirstLightComponent->setGBuffer(mGBuffer);
			break;
	}
}

void DifferredRenderer::setTargetTexture(Texture *t){
	if(t != NULL){
		int w = t->getSize().x, h = t->getSize().y;
		mFramebuffer->swapTexture(0, t);
		mFramebuffer->setSize(w, h);
		mGBuffer->setSize(w, h);
		mToTexture = true;
	}else{
		mFramebuffer->setSize(mViewportWidth, mViewportHeight);
		mGBuffer->setSize(mViewportWidth, mViewportHeight);
		mToTexture = false;
	}
}

void DifferredRenderer::setVoxelTextureId(GLuint id) {
	mVoxelConeTracingComponent->setVoxelTextureId(id);
}

void DifferredRenderer::setVoxelWorldSize(float size) {
	mVoxelConeTracingComponent->setVoxelWorldSize(size);
}

void DifferredRenderer::addMesh(RenderMesh *m){
	mMeshes.push_back(m);
}

void DifferredRenderer::addModel(Model *m){
	mModels.push_back(m);
}

void DifferredRenderer::addLocalCubemap(LocalCubemap *c){
	mCubemaps.push_back(c);
	mReflectLightComponent->addLocalCubemap(c);
}

void DifferredRenderer::init(){

	mToTexture = false;

	mGBuffer = new GBuffer(mViewportWidth, mViewportHeight);
	mGBuffer->init();

	mLCMGBuffer = new GBuffer(256, 256);
	mLCMGBuffer->init();

	/*mReflectLightComponent = new ReflectLightComponent();
	mReflectLightComponent->setGBuffer(mGBuffer);
	mReflectLightComponent->setViewPosition(mViewPosition);
	mReflectLightComponent->init();

	mFirstLightComponent = new FirstLightComponent();
	mFirstLightComponent->setGBuffer(mGBuffer);
	mFirstLightComponent->setLights(mLights);
	mFirstLightComponent->setViewPosition(mViewPosition);
	mFirstLightComponent->init();*/

	mVoxelConeTracingComponent = new VoxelConeTracingComponent();
	mVoxelConeTracingComponent->setLights(mLights);
	mVoxelConeTracingComponent->setGBuffer(mGBuffer);
	mVoxelConeTracingComponent->setRenderToBuffer(true);
	mVoxelConeTracingComponent->setViewPosition(mViewPosition);
	mVoxelConeTracingComponent->init();

	mAntiAliasingComponent = new AntiAliasingComponent();
	mAntiAliasingComponent->setGBuffer(mGBuffer);
	mAntiAliasingComponent->setRenderToBuffer(false);
	mAntiAliasingComponent->init();
	
	/*auto d = new DifferredComponent();
	d->setGBuffer(mGBuffer);
	d->init();*/


	mTargetTexture = new Texture(mViewportWidth, mViewportHeight);
	mTargetTexture->setInternalFormat(GL_RGBA8);
	mTargetTexture->setFormat(GL_RGBA);
	mTargetTexture->setDataType(GL_UNSIGNED_BYTE);
	mTargetTexture->init();

	mFramebuffer = new Framebuffer(mViewportWidth, mViewportHeight);
	mFramebuffer->addTexture(mTargetTexture);
	mFramebuffer->init();

	mFramebufferCube = new FramebufferCube(256, 256);
	mFramebufferCube->init();

}

void DifferredRenderer::beginCapture(){
	if(mToTexture)
		mFramebuffer->beginCapture();
}

void DifferredRenderer::endCapture(){
	if(mToTexture)
		mFramebuffer->endCapture();
}

//Render gbuffer
void DifferredRenderer::firstPass(){
	if(mState == 1)
		mLCMGBuffer->beginCapture();
	else
		mGBuffer->beginCapture();

	for(auto &oi : mMeshes){
		oi->setMode(mState);
		//oi->setProjectionMatrix(mProjectionMatrix);
		//oi->setViewMatrix(mViewMatrix);
		//oi->setViewPosition(mViewPosition);
		oi->render();
	}

	for(auto &mi : mModels){
		mi->render();
	}

	if(mState == 1)
		mLCMGBuffer->endCapture();
	else
		mGBuffer->endCapture();

	//mGBuffer->show();
}

void DifferredRenderer::firstLightPass(){
	//glClearColor(1, 0, 1, 1);
	//glClear(GL_COLOR_BUFFER_BIT);
	//mGBuffer->show();

	mFirstLightComponent->setViewPosition(mViewPosition);
	mFirstLightComponent->render();
}

void DifferredRenderer::reflectLightPass(){

	/*mReflectLightComponent->setViewPosition(mViewPosition);
	mReflectLightComponent->render();*/

	mFirstLightComponent->setViewPosition(mViewPosition);
	mFirstLightComponent->render();
}

void DifferredRenderer::forwardPass(){
	glViewport(0, 0, mViewportWidth, mViewportHeight);
	for(auto ci : mCubemaps){
		ci->renderMesh();
	}
}

void DifferredRenderer::voxelGIPass() {
	mVoxelConeTracingComponent->setViewPosition(mViewPosition);
	mVoxelConeTracingComponent->render();
}

void DifferredRenderer::antiAliasingPass(){
	mAntiAliasingComponent->render();
}

void DifferredRenderer::renderFirstLight(glm::mat4 view){;
	//setState(2);
	firstPass();
	//mLCMGBuffer->show();

	beginCapture();
	firstLightPass();
	endCapture();
	//mFramebuffer->show(0, glm::vec4(0));
}

void DifferredRenderer::renderVoxelGI() {

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	firstPass();

	voxelGIPass();

	//mGBuffer->showFinal();
}

void DifferredRenderer::renderAA() {
	antiAliasingPass();
}

void DifferredRenderer::captureEnvMap(int ind){
	auto ci = mCubemaps[ind];
	for(int i=0; i<6; ++i){

		for(auto mi : mMeshes){
			mi->setViewMatrix(ci->getViewMatrix(i));
			mi->setViewPosition(ci->getPosition());
		}

		mFirstLightComponent->setViewPosition(ci->getPosition());

		firstPass();

		mFramebufferCube->swapTexture(ci->getTextureCube());
		mFramebufferCube->beginCapture(i);

		firstLightPass();

		mFramebufferCube->endCapture();
	}

	ci->processIrradiance();
}

void DifferredRenderer::captureEnvMaps(){
	for(int i=0; i<mCubemaps.size(); ++i){
		captureEnvMap(i);
	}
}

void DifferredRenderer::renderFinal(){
	firstPass();
	glDisable(GL_DEPTH_TEST);
	reflectLightPass();
	glEnable(GL_DEPTH_TEST);
	//forwardPass();
}

/*void DifferredRenderer::captureFirstLight(){
	setTargetTexture();
	renderFirstLight();
	setTargetTexture(NULL);
}*/

/*void DifferrendRenderer::render(){
	renderFirstLight(glm::mat4(0));
}*/

DifferredRenderer::DifferredRenderer(){

}

DifferredRenderer::~DifferredRenderer(){
}
