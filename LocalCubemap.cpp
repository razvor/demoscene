#include "LocalCubemap.h"
#include "glm/gtc/matrix_transform.hpp"
#include <chrono>

void LocalCubemap::Mesh::initGeometry(){
	float side = 1.f;

	/*std::vector<glm::vec3> verts = {
		//front
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0),

		//back
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(-1.0, -1.0, 1.0),

		//top
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(-1.0, 1.0, -1.0),

		//bottom
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0),

		//right
		glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, -1.0, -1.0),

		//left
		glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(-1.0, -1.0, -1.0)

	};

	std::vector<int> inds;
	for(int i=0; i<36; ++i){
		inds.push_back(i);
	}

	glm::vec3 nds[] = {
		glm::vec3(0, 0, -1.0),
		glm::vec3(0, 0, 1.0),
		glm::vec3(0, 1.0, 0),
		glm::vec3(0, -1.0, 0),
		glm::vec3(1.0, 0, 0),
		glm::vec3(-1.0, 0, 0)
	};

	std::vector<glm::vec3> norms;
	for(int i=0; i<6; ++i){
		for(int j=0; j<6; ++j){
			norms.push_back(nds[i]);
		}
	}

	std::vector<glm::vec2> texCords;
	for(int i=0; i<6; ++i){
		texCords.push_back(glm::vec2(0, 0));
		texCords.push_back(glm::vec2(0, 1));
		texCords.push_back(glm::vec2(1, 1));
		texCords.push_back(glm::vec2(1, 1));
		texCords.push_back(glm::vec2(1, 0));
		texCords.push_back(glm::vec2(0, 0));
	}*/

	const int density = 32;
	const float radius = 0.5f;

	std::vector<glm::vec3> vertices, normals;
	std::vector<glm::vec2> texCords; 
	std::vector<int> indices;

	float S = 2.f * M_PI / (float)(density-1);
	for(int i=0; i<density; ++i){
		for(int j=0; j<density; ++j){
			float r = cosf( i * S - M_PI / 2.f ) * radius;
			float y = sinf( i * S - M_PI / 2.f ) * radius;
			float x = cosf( j * S ) * r;
			float z = sinf( j * S ) * r;

			vertices.push_back(glm::vec3(x, y, z));
			normals.push_back(glm::normalize(glm::vec3(x, y, z)));
			texCords.push_back(glm::vec3(0));

			int a,b,c,d;
			a = i * density + j;
			b = i * density + j + 1;
			c = (i+1) * density + j + 1;
			d = (i+1) * density + j;

			indices.push_back( a );
			indices.push_back( b );
			indices.push_back( c );
			indices.push_back( c );
			indices.push_back( d );
			indices.push_back( a );
		}
	}

	setVertices(vertices);
	setNormals(normals);
	setIndices(indices);
	setTexCords(texCords);

	setRotation(glm::vec3(0));
	setScale(glm::vec3(side));

}

void LocalCubemap::Mesh::init(){
	
	initGeometry();
	RenderMesh::init();
}

void LocalCubemap::Mesh::initProgram(){
	mProgram = Program("localCubemap", "localCubemap");
	if(!mProgram.init()){
		std::cerr << "Program init ERROR " << std::endl;
	}
}

void LocalCubemap::Mesh::initLocations(){
	RenderMesh::initLocations();

	//unifLocations.irradiance = mProgram.uniform("irradiance");

	//unifLocations.texCube = mProgram.uniform("texCube");
	//unifLocations.basisMatrix = mProgram.uniform("basisMatrix");
}

void LocalCubemap::initPrefilter(){
	mPrefilterProgram = Program("envMapPrefilter", "envMapPrefilter");
	mPrefilterProgram.init();

	mPrefilterFramebuffer = new FramebufferCube(mBufferWidth, mBufferHeight);
	mPrefilterFramebuffer->init();

	std::vector<glm::vec3> mVertices = {
		glm::vec3(-1, -1, 0), 
		glm::vec3(-1, 1, 0), 
		glm::vec3(1, 1, 0), 
		glm::vec3(1,-1, 0)
	};

	std::vector<int> indices = {
		0, 1, 2, 2, 0, 3
	};

	mPrefilterVertexBuffer = new VertexBuffer(1, mVertices);
	mPrefilterIndicesBuffer = new VertexBuffer(2, indices);

	/*int tex_w = 256, tex_h = 256;
	glGenTextures(1, &texOut);
	//glActiveTexture(GL_TEXTURE0+10);
	glBindTexture(GL_TEXTURE_2D, texOut);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, tex_w, tex_h, 0, GL_RGBA, GL_FLOAT,
		 NULL);
	//glBindImageTexture(0, texOut, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);
	glBindTexture(GL_TEXTURE_2D, 0);*/
}

void LocalCubemap::Mesh::setTextureId(GLuint id){
	mTextureId = id;
}

/*void LocalCubemap::Mesh::setTextures(Texture **ts){
	mTextures = ts;
}*/

void LocalCubemap::Mesh::setTextureCube(TextureCube *tc){
	mTextureCube = tc;
}

void LocalCubemap::Mesh::pushUniforms(){
	RenderMesh::pushUniforms();

	/*glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, mTextureId);
	glUniform1i(unifLocations.texCube, 0);*/

	/*for(int i=0; i<2; ++i){
		glUniformMatrix4fv( unifLocations.basisMatrix, 1, GL_FALSE, &mViewMatrices[i][0][0] );
		mTextures[i]->setProgram(mProgram);
		mTextures[i]->pushToShader(i);
	}*/

	for(int i=0; i<3; ++i){
		int loc = mProgram.uniform(("irradiance["+std::to_string(i)+"]").c_str());
		glUniformMatrix4fv(loc, 1, GL_FALSE, &irradiance[i][0][0]);
	}

	mTextureCube->setUniformName("textureCube");
	mTextureCube->pushToShader(mProgram, 0);
}

void LocalCubemap::Mesh::setViewMatrices(glm::mat4 *vms){
	mViewMatrices = vms;
}

void LocalCubemap::Mesh::setIrradiance(glm::mat4 *irr){
	irradiance = irr;
}

LocalCubemap::LocalCubemap(){
	
}

LocalCubemap::~LocalCubemap(){

}

void LocalCubemap::initTextures(){
	/*glGenTextures(1, &mTextureId);
	glBindTexture(GL_TEXTURE_CUBE_MAP, mTextureId);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
 	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	int mWidth, mHeight;
	//unsigned char *img = SOIL_load_image("../wall.jpg", &mWidth, &mHeight, 0, SOIL_LOAD_RGBA);
	for(int i=0; i<6; ++i){
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, mBufferWidth, mBufferHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL );
		//glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA8, mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, img );
	}
	
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	glGenTextures(1, &mDepthId);
	glBindTexture(GL_TEXTURE_CUBE_MAP, mDepthId);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
 	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	for(int i=0; i<6; ++i){
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH24_STENCIL8, mBufferWidth, mBufferHeight, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0);
	}

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);*/

	/*
	 * Dual parabaloid
	 * std::string unifNames[] = {
		"frontSampler",
		"backSampler"
	};

	mTextures = new Texture*[2];
	for(int i=0; i<2; ++i){
		mTextures[i] = new Texture(mBufferWidth, mBufferWidth);
		mTextures[i]->setInternalFormat(GL_RGBA8);
		mTextures[i]->setFormat(GL_RGBA);
		mTextures[i]->setDataType(GL_UNSIGNED_BYTE);
		mTextures[i]->setUniformName(unifNames[i]);
		mTextures[i]->init();
	}*/

	mTextureCube = new TextureCube(mBufferWidth, mBufferHeight);
	mTextureCube->setInternalFormat(GL_RGBA8/*GL_RGBA16F*/);
	mTextureCube->setFormat(GL_RGBA);
	mTextureCube->setDataType(/*GL_UNSIGNED_BYTE*/ GL_FLOAT);
	mTextureCube->setUniformName("textureCube");
	mTextureCube->init();

	mPrefTex = new TextureCube(mBufferWidth, mBufferHeight);
	mPrefTex->setInternalFormat(GL_RGBA8/*GL_RGBA16F*/);
	mPrefTex->setFormat(GL_RGBA);
	mPrefTex->setDataType(/*GL_UNSIGNED_BYTE*/ GL_FLOAT);
	mPrefTex->setUniformName("textureCube");
	mPrefTex->init();

}

void LocalCubemap::initBuffers(){
	/*glGenFramebuffers(1, &mFramebufferId);
	glBindFramebuffer(GL_FRAMEBUFFER, mFramebufferId);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X, mTextureId, 0);

	if(glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT) != GL_FRAMEBUFFER_COMPLETE){
		std::cout << "LOCAL CUBEMAP ERROR: Framebuffer creating error" << std::endl;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);*/

	//New
	/*for(int i=0; i<6; ++i){
		mFramebuffers[i] = new Framebuffer(mWidth, mHeight);
		mFramebuffers[i]->setInternalFormat(GL_RGB);
		mFramebuffers[i]->setFormat(GL_RGB);
		mFramebuffers[i]->setChannel(0);
		mFramebuffers[i]->setDataFormat(GL_UNSIGNED_BYTE);
	}*/
	
	/*std::string unifNames[] = {
		"frontSampler",
		"backSampler"
	};

	for(int i=0; i<2; ++i){

		auto tex = new Texture(mBufferWidth, mBufferHeight);
		tex->setInternalFormat(GL_RGB8);
		tex->setFormat(GL_RGB);
		tex->setDataType(GL_UNSIGNED_BYTE);
		tex->setUniformName(unifNames[i]);
		tex->init();

		mFramebuffers[i] = new Framebuffer(mBufferWidth, mBufferHeight);
		mFramebuffers[i]->addTexture(tex);
		mFramebuffers[i]->init();
	}*/
}

void LocalCubemap::initMatrices(){

	glm::vec3 dirs[] = {
		/*glm::vec3(0.999, 0.001, 0),
		glm::vec3(-0.999, 0.001, 0),
		glm::vec3(0, 0.999, 0.001),
		glm::vec3(0.0, -0.999, 0.001),
		glm::vec3(0, 0.001, 0.999),
		glm::vec3(0, 0.001, -0.999)*/
		
		glm::vec3(0.1, 0, 0),
		glm::vec3(-0.1, 0, 0),
		glm::vec3(0, 0.1, 0),
		glm::vec3(0.0, -0.1, 0),
		glm::vec3(0.0, 0.0, 0.1),
		glm::vec3(0.0, 0.0, -0.1),
	};

	glm::vec3 ups[] = {
	    glm::vec3(0.0f, -1.0f, 0.0f),
	    glm::vec3(0.0f, -1.0f, 0.0f),
	    glm::vec3(0.0f, 0.0f, 1.0f),
	    glm::vec3(0.0f, 0.0f, -1.0f),
	    glm::vec3(0.0f, -1.0f, 0.0f),
	    glm::vec3(0.0f, -1.0f, 0.0f)
	};
	
	for(int i=0; i<6; ++i){
		mViewMatrices[i] = glm::lookAt(
			mPosition, 
			mPosition + dirs[i], 
			ups[i]
		);
	}
}

void LocalCubemap::initMesh(){
	mMesh.setPosition(mPosition);

	/*Texture **textures = new Texture*[2];
	for(int i=0; i<2; ++i)
		textures[i] = mFramebuffers[i]->getTexture(0);*/

	//mMesh.setTextures(mTextures);
	//mMesh.setTextureCube(mTextureCube);
	mMesh.setTextureCube(mPrefTex);

	glm::mat4 *vms = new glm::mat4[6];
	for(int i=0; i<6; ++i)
		vms[i] = mViewMatrices[i];

	mMesh.setViewMatrices(mViewMatrices);

	//mMesh.setTextureId(mTextureId);
	mMesh.init();
}

void LocalCubemap::init(){
	initTextures();
	//initBuffers();
	initMatrices();
	initMesh();
	initPrefilter();
}

void LocalCubemap::capture(std::function<void(glm::mat4, float dir, Texture *t)> renderFunc){
	/*
	 * Old
	 * for(int i=0; i<6; ++i){
		glBindFramebuffer(GL_FRAMEBUFFER, mFramebufferId);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, mTextureId, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, mDepthId, 0);

		glViewport(0, 0, mBufferWidth, mBufferHeight);
		renderFunc( mViewMatrices[i] );

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}*/
	float dir;
	for(int i=0; i<2; ++i){
		//mFramebuffers[i]->beginCapture();
		dir = !i ? -1.f : 1.f;
		renderFunc( mViewMatrices[i], dir, mTextures[i] );
		//mFramebuffers[i]->endCapture();
	}
}

float sinc(float x) {
	if (fabs(x) < 1.0e-4) return 1.0 ;
	else return(sin(x)/x) ;
}

void LocalCubemap::computeIrradiance(){
	const float 
		c1 = 0.429043,
		c2 = 0.511664,
		c3 = 0.743125, 
		c4 = 0.886227, 
		c5 = 0.247708;

	float coeffs[9][3];
	for(int i=0; i<9; ++i) for(int j=0; j<3; ++j) coeffs[i][j]=0.0;

	int scf = 8;
	int w = mBufferWidth;

	//int *vv = new int(1);
	/*int vv = 0;
	glBindTexture(GL_TEXTURE_CUBE_MAP, mTextureCube->getId());
	glGetTexLevelParameteriv(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &vv);
	std::cout << "Texture size " << vv << std::endl;
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	return;*/

	/*using namespace std::chrono;
	std::chrono::milliseconds ms = duration_cast< milliseconds >(
		    system_clock::now().time_since_epoch()
		).count();*/


	/*std::chrono::milliseconds ms2 = duration_cast< milliseconds >(
		    system_clock::now().time_since_epoch()
		).count();

	std::cout << ms2 - ms << std::endl;*/

	//return;
	
	float *read[6];	
	//float *read2[6];	
	for(int i=0; i<6; ++i){
		read[i] = new float[ mBufferWidth * mBufferHeight * 4 ];
		//read2[i] = new float[ mBufferWidth * mBufferHeight * 4 ];

		glBindTexture(GL_TEXTURE_CUBE_MAP, mTextureCube->getId());
		glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, GL_FLOAT, &read[i][0]);
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
		
		/*for(int j=0; j<mBufferWidth * mBufferHeight * sizeof(float); ++j){
			//if(j/(256*4) % 20 == 1)
			read2[i][j] = 0.0;
		}*/
	}
	//std::cout << "size is " << sizeof(float) << std::endl;

	float phi, theta, dval;
	w = 255;
	int passes = 300;
	dval = M_PI / (float)passes;
	int count = passes * passes * 2;
	float lp = 0, lt = 0;
	for(phi = 0; phi <= 2 * M_PI; phi += dval){
		for(theta = 0; theta <= M_PI; theta += dval){
			float cx = sinf(theta) * cosf(phi);
			float cz = sinf(theta) * sinf(phi);
			float cy = cosf(theta);

			float xx = cx;
			float yy = cy;
			float zz = cz;

			float tx = cx / 2.0 + 0.5;
			float tz = cz / 2.0 + 0.5;
			float ty = cy / 2.0 + 0.5;

			int side = -1;
			int x, y;


			/*if(face == 0) N = vec3(1.0, -vp.y, -vp.x);
			if(face == 1) N = vec3(-1.0, -vp.y, vp.x);
			if(face == 2) N = vec3(vp.x, 1.0, vp.y);
			if(face == 3) N = vec3(vp.x, -1.0, -vp.y);
			if(face == 4) N = vec3(vp.x, -vp.y, 1.0);
			if(face == 5) N = vec3(-vp.x, -vp.y, -1.0);*/

			if(fabs(yy) <= fabs(xx) && fabs(zz) <= fabs(xx)){
				side = (xx > 0) ? 0 : 1;
				x = tz * w;
				y = ty * w;
				//if(side == 1) x = x;

			}
			if(fabs(xx) <= fabs(yy) && fabs(zz) <= fabs(yy)){
				side = (yy > 0) ? 2 : 3;
				x = tz * w;
				y = tx * w;
				//if(side == 3) x = x;
			}
			if(fabs(xx) <= fabs(zz) && fabs(yy) <= fabs(zz)){
				side = (zz > 0) ? 4 : 5;
				x = tx * w;
				y = ty * w;
				//if(side == 4) x = x;
			}

			float hdr[3] = {0, 0, 0};
			int offset = (y*(256)+x)*4;
			hdr[0] = read[side][offset];
			hdr[1] = read[side][offset+1];
			hdr[2] = read[side][offset+2];

			/*read2[side][offset] = 1.0; //theta/M_PI;
			read2[side][offset+1] = 1.0; //theta/M_PI;			
			read2[side][offset+2] = 1.0; //theta/M_PI;*/

			float dt = fabs(theta - lt);
			float dp = fabs(phi - lp);
			lp = phi;
			lt = theta;
			//float domega = 2 * M_PI * dt * dp * sinc(theta); 
			float domega = (2*M_PI)*sinc(theta) / passes / passes / 2;

			//float x = cx, y = cy, z = cz;
			
			for (int col = 0 ; col < 3 ; col++) {
				float c ; /* A different constant for each coefficient */

				/* L_{00}.  Note that Y_{00} = 0.282095 */
				c = 0.282095 ;
				coeffs[0][col] += hdr[col]*c*domega ;

				/* L_{1m}. -1 <= m <= 1.  The linear terms */
				c = 0.488603 ;
				coeffs[1][col] += hdr[col]*(c*cy)*domega ;   /* Y_{1-1} = 0.488603 y  */
				coeffs[2][col] += hdr[col]*(c*cz)*domega ;   /* Y_{10}  = 0.488603 z  */
				coeffs[3][col] += hdr[col]*(c*cx)*domega ;   /* Y_{11}  = 0.488603 x  */

				/* The Quadratic terms, L_{2m} -2 <= m <= 2 */

				/* First, L_{2-2}, L_{2-1}, L_{21} corresponding to xy,yz,xz */
				c = 1.092548 ;
				coeffs[4][col] += hdr[col]*(c*cx*cy)*domega ; /* Y_{2-2} = 1.092548 xy */ 
				coeffs[5][col] += hdr[col]*(c*cy*cz)*domega ; /* Y_{2-1} = 1.092548 yz */ 
				coeffs[7][col] += hdr[col]*(c*cx*cz)*domega ; /* Y_{21}  = 1.092548 xz */ 

				/* L_{20}.  Note that Y_{20} = 0.315392 (3z^2 - 1) */
				c = 0.315392 ;
				coeffs[6][col] += hdr[col]*(c*(3*cz*cz-1))*domega ; 

				/* L_{22}.  Note that Y_{22} = 0.546274 (x^2 - y^2) */
				c = 0.546274 ;
				coeffs[8][col] += hdr[col]*(c*(cx*cx-cy*cy))*domega ;

			}

		}
	}

	for(int i=0; i<6; ++i){
		delete read[i];
	}

	int col ;
	for (col = 0 ; col < 3 ; col++) { /* Equation 12 */

		irradiance[col][0][0] = c1*coeffs[8][col] ; /* c1 L_{22}  */
		irradiance[col][0][1] = c1*coeffs[4][col] ; /* c1 L_{2-2} */
		irradiance[col][0][2] = c1*coeffs[7][col] ; /* c1 L_{21}  */
		irradiance[col][0][3] = c2*coeffs[3][col] ; /* c2 L_{11}  */

		irradiance[col][1][0] = c1*coeffs[4][col] ; /* c1 L_{2-2} */
		irradiance[col][1][1] = -c1*coeffs[8][col]; /*-c1 L_{22}  */
		irradiance[col][1][2] = c1*coeffs[5][col] ; /* c1 L_{2-1} */
		irradiance[col][1][3] = c2*coeffs[1][col] ; /* c2 L_{1-1} */

		irradiance[col][2][0] = c1*coeffs[7][col] ; /* c1 L_{21}  */
		irradiance[col][2][1] = c1*coeffs[5][col] ; /* c1 L_{2-1} */
		irradiance[col][2][2] = c3*coeffs[6][col] ; /* c3 L_{20}  */
		irradiance[col][2][3] = c2*coeffs[2][col] ; /* c2 L_{10}  */

		irradiance[col][3][0] = c2*coeffs[3][col] ; /* c2 L_{11}  */
		irradiance[col][3][1] = c2*coeffs[1][col] ; /* c2 L_{1-1} */
		irradiance[col][3][2] = c2*coeffs[2][col] ; /* c2 L_{10}  */
		irradiance[col][3][3] = c4*coeffs[0][col] - c5*coeffs[6][col] ; /* c4 L_{00} - c5 L_{20} */
	}

	/*for(int i=0;i<6; ++i){
		glBindTexture(GL_TEXTURE_CUBE_MAP, mTextureCube->getId());
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA8, mBufferWidth, mBufferWidth, 0, GL_RGBA, GL_FLOAT, &read[i][0]);
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	}*/

	//test
	
	/*irradiance[0] = glm::mat4(
		0.009098, -0.004780,  0.024033, -0.014947,
		-0.004780, -0.009098, -0.011258,  0.020210,
		0.024033, -0.011258, -0.011570, -0.017383, 
		-0.014947,  0.020210, -0.017383,  0.073787
	);

	irradiance[1] = glm::mat4(
		-0.002331, -0.002184,  0.009201, -0.002846 ,
		-0.002184,  0.002331, -0.009611,  0.017903, 
		0.009201, -0.009611, -0.007038, -0.009331, 
		-0.002846,  0.017903, -0.009331,  0.041083 
	);

	irradiance[2] = glm::mat4(
		-0.013032, -0.005248,  0.005970,  0.000483, 
		-0.005248,  0.013032, -0.020370,  0.030949, 
		0.005970, -0.020370, -0.010948, -0.013784, 
		0.000483,  0.030949, -0.013784,  0.051648 
	);*/
 
}

void LocalCubemap::renderMesh(){
	mMesh.setProjectionMatrix(pm);
	mMesh.setViewMatrix(vm);
	mMesh.setViewPosition(glm::vec3(0));
	mMesh.setIrradiance(&irradiance[0]);
	mMesh.render();
}

void LocalCubemap::setPosition(glm::vec3 position){
	mPosition = position;
}

void LocalCubemap::setLights(Lights *lights){
	mMesh.setLights(lights);
}

TextureCube* LocalCubemap::getTextureCube(){
	return mTextureCube;
}

GLuint LocalCubemap::getTextureId(){
	return mTextureId;
}

glm::vec3 LocalCubemap::getPosition(){
	return mPosition;
}

glm::mat4 LocalCubemap::getViewMatrix(int ind){
	return mViewMatrices[ind];
}

void LocalCubemap::bindMesh(RenderMesh *rm){
	mBindedMeshes.push_back(rm);
}

void LocalCubemap::pushUniforms(Program &p){
	
}

void LocalCubemap::pushToComponentShader(Program &p, int ui, int ti){
	mPrefTex->setUniformName("envMaps[" + std::to_string(ui) + "]");
	mPrefTex->pushToShader(p, ti);

	int positionLocation = p.uniform(("envMapPosition[" + std::to_string(ui) + "]").c_str());
	glUniform3f( positionLocation, mPosition.x, mPosition.y, mPosition.z );

	glm::mat4 tmat =
	  glm::translate(glm::mat4(1), glm::vec3(0, 1.0, 0.0)) * 
	  glm::scale(glm::mat4(1), glm::vec3(5.0, 2.0, 5.0));

	int matrixLocation = p.uniform(("envMapMatrices[" + std::to_string(ui) + "]").c_str());
	glUniformMatrix4fv(matrixLocation, 1, GL_FALSE, &tmat[0][0]);

	//Irradiance matrices(red, green, blue)
	for(int i=0; i<3; ++i){
		int irradianceLocation = p.uniform(("irradiance[" + std::to_string( ui * 3 + i ) + "]").c_str());
		glUniformMatrix4fv(irradianceLocation, 1, GL_FALSE, &irradiance[i][0][0]);
	}
}

void LocalCubemap::prefilter(){

	glm::mat4 mats[] = {
		glm::rotate(glm::mat4(1), -90.f, glm::vec3(0,0,1)),
		glm::rotate(glm::mat4(1), 90.f, glm::vec3(0,0,1)),
		glm::rotate(glm::mat4(1), -180.f, glm::vec3(1,0,0)),
		glm::rotate(glm::mat4(1), 0.f, glm::vec3(1,0,0)),
		glm::rotate(glm::mat4(1), -90.f, glm::vec3(1,0,0)),
		glm::rotate(glm::mat4(1), 90.f, glm::vec3(1,0,0))
	};

	int levelsCount = (int)log2(mBufferWidth)-1;
	//float roughness = 0.5;
	mPrefilterFramebuffer->swapTexture(mPrefTex);
	for(int l=0; l<levelsCount; ++l){

		mPrefilterFramebuffer->setLevel(l);
		float roughness = (float)l / levelsCount * 0.99 + 0.01;

		for(int i=0; i<6; ++i){
			mPrefilterFramebuffer->beginCapture(i);

			mPrefilterProgram.use();
			//glClearColor(0, 0, 0, 1);
			//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			mTextureCube->setUniformName("envMap");
			mTextureCube->pushToShader(mPrefilterProgram, 0);

			int roughnessLocation = mPrefilterProgram.uniform("roughness");
			glUniform1f( roughnessLocation, roughness );

			/*int transLocation = mPrefilterProgram.uniform("trans");
			glUniformMatrix4fv( transLocation, 1, GL_FALSE, &mats[i][0][0] );*/
			int faceLoc = mPrefilterProgram.uniform("face");
			glUniform1i(faceLoc, i);

			mPrefilterVertexBuffer->render(0);
			glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mPrefilterIndicesBuffer->getInstance() );
			glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL );
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			mPrefilterFramebuffer->endCapture();

		}
	}


	/*mPrefilterProgram.use();

	//glBindImageTexture(0, texOut, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texOut);
	//glBindImageTexture(0, texOut, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);
	glUniform1i(0, 0);
	
	//mTextureCube->setUniformName("input");
	//mTextureCube->pushToShader(mPrefilterProgram, 0);

	//glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	glDispatchCompute(1, 1, 1);*/

}

void LocalCubemap::processIrradiance(){
	this->computeIrradiance();
	this->prefilter();
}
