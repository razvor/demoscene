#ifndef WALLS_MESH
#define WALLS_MESH

#include "includer.h"
#include "RenderMesh.h"

class WallsMesh : public RenderMesh{
private:
	void initGeometry();
	void initMaterial();
public:

	void initProgram();
	void init();

	WallsMesh();
	~WallsMesh();
};

#endif
