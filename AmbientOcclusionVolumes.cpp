#include "AmbientOcclusionVolumes.h"

AmbientOcclusionVolumes::AmbientOcclusionVolumes() {

}

AmbientOcclusionVolumes::~AmbientOcclusionVolumes() {

}

void AmbientOcclusionVolumes::setMeshes(std::vector<RenderMesh*> ms) {
	mMeshes = ms;
}

void AmbientOcclusionVolumes::setModels(std::vector<Model*> ms){
	mModels = ms;
}

void AmbientOcclusionVolumes::setSize(int w, int h) {
	mWidth = w;
	mHeight = h;
}

void AmbientOcclusionVolumes::init() {
	initConsts();
	initPrograms();
	initQuad();
	initFramebuffers();
}

void AmbientOcclusionVolumes::initConsts() {
	float fov = glm::radians(-30.f);
	mFocalLen[0] = 1.0f / tanf(fov * 0.5f) * ((float)mHeight / (float)mWidth);
	mFocalLen[1] = 1.0f / tanf(fov * 0.5f);

	mInvFocalLen[0] = 1.0f / mFocalLen[0];
	mInvFocalLen[1] = 1.0f / mFocalLen[1];

	mUVToViewA[0] = -2.0f * mInvFocalLen[0];
	mUVToViewA[1] = -2.0f * mInvFocalLen[1];
	mUVToViewB[0] = 1.0f * mInvFocalLen[0];
	mUVToViewB[1] = 1.0f * mInvFocalLen[1];
}

void AmbientOcclusionVolumes::initFramebuffers() {

	mOutTexture = new Texture(mWidth, mHeight);
	mOutTexture->setInternalFormat(GL_RGBA8);
	mOutTexture->setFormat(GL_RGBA);
	mOutTexture->setDataType(GL_UNSIGNED_BYTE);
	mOutTexture->init();

	mOutFramebuffer = new Framebuffer(mWidth, mHeight);
	mOutFramebuffer->addTexture(mOutTexture);
	mOutFramebuffer->swapTexture(0, mOutTexture);
	mOutFramebuffer->setSize(mWidth, mHeight);
	mOutFramebuffer->init();

	mGBuffer = new GBuffer(mWidth, mHeight);
	mGBuffer->init();

}

Texture* AmbientOcclusionVolumes::getOutTexture() {
	return mOutTexture;
}

void AmbientOcclusionVolumes::initPrograms() {
	mFirstPassProgram = Program("mainVertex", "firstPass");
	if (!mFirstPassProgram.init()) {
		std::cout << "Cant init first pass program" << std::endl;
		getchar();
	}

	mAOProgram = Program("AOV", "AOV");
	if (!mAOProgram.init()) {
		std::cout << "Cant init AOV" << std::endl;
		getchar();
	}
	std::cout << "Programs inited" << std::endl;
}

void AmbientOcclusionVolumes::initQuad() {
	std::cout << "init quad" << std::endl;

	mAOProgram.use();

	std::vector<glm::vec3> vertices = {
		glm::vec3(-1, -1, 0), 
		glm::vec3(-1, 1, 0), 
		glm::vec3(1, 1, 0), 
		glm::vec3(1,-1, 0)
	};
	std::vector<unsigned int> indices = {
		0, 1, 2, 2, 0, 3
	};

	mVerticesBuffer = new VertexBuffer(1, vertices);
	auto mIndicesBuffer = new VertexBuffer(2, indices);

	glGenVertexArrays(1, &mQuadVAO);
	glBindVertexArray(mQuadVAO);
	mVerticesBuffer->render(0);
	mIndicesBuffer->render(0);
	glBindVertexArray(0);
}

void AmbientOcclusionVolumes::firstPass() {
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	//glEnable(GL_DEPTH_CLAMP);
	glEnable(GL_DEPTH_TEST);
	mGBuffer->beginCapture();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	for (auto m : mMeshes) {
		m->setProgram(mFirstPassProgram);
		m->render();
	}
	for (auto mi : mModels) {
		for (auto m : mi->mMeshes) {
			m->setProgram(mFirstPassProgram);
		}
		mi->render();
	}
	mGBuffer->endCapture();
	//mGBuffer->show(2);
}

void AmbientOcclusionVolumes::aoPass() {
	mOutFramebuffer->beginCapture();

	glEnable(GL_DEPTH_TEST);
	//glDepthMask(GL_FALSE);
	/*glEnable(GL_ALPHA);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);*/

	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mAOProgram.use();
	mVerticesBuffer->render(0);
	mGBuffer->pushToShader(mAOProgram, 1);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	/*for (auto m : mMeshes) {
		//m->render();
		m->renderGeometry(mAOProgram);
	}
	for (auto mi : mModels) {
		mi->renderGeometry(mAOProgram);
		//mi->render();
	}*/

	glUniform2f(mAOProgram.uniform("focalLen"), mFocalLen[0], mFocalLen[1]);
	glUniform2f(mAOProgram.uniform("uvToViewA"), mUVToViewA[0], mUVToViewA[1]);
	glUniform2f(mAOProgram.uniform("uvToViewB"), mUVToViewB[0], mUVToViewB[1]);

	glBindVertexArray(mQuadVAO);
	glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL );
	glBindVertexArray(0);

	mOutFramebuffer->endCapture();

	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//mOutFramebuffer->show(0, glm::vec4(0));

	//glDisable(GL_BLEND);
}

void AmbientOcclusionVolumes::render(){
	firstPass();
	aoPass();
}