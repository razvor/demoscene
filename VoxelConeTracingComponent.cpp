#include "VoxelConeTracingComponent.h"

VoxelConeTracingComponent::VoxelConeTracingComponent() {}

VoxelConeTracingComponent::~VoxelConeTracingComponent() {}

void VoxelConeTracingComponent::init() {
	this->initProgram();
	DifferredComponent::init();
	mLights->init(getProgram());
}

void VoxelConeTracingComponent::initProgram() {
	setProgram(Program("voxelGIPass", "voxelGIPass"));
}

void VoxelConeTracingComponent::initLocations() {
	DifferredComponent::initLocations();
	mViewPositionLocation = getProgram().uniform("viewPosition");
	mVoxelTextureLocation = getProgram().uniform("voxelTexture");
	mVoxelWorldSizeLocation = getProgram().uniform("voxelWorldSize");
	mVoxelGridSizeLocation = getProgram().uniform("gridSize");
}

void VoxelConeTracingComponent::setVoxelTextureId(int voxelTextureId) {
	mVoxelTextureId = voxelTextureId;
}

void VoxelConeTracingComponent::setVoxelWorldSize(float size) {
	mVoxelWorldSize = size;
}

void VoxelConeTracingComponent::setViewPosition(glm::vec3 vp) {
	mViewPosition= vp;
}

void VoxelConeTracingComponent::setVoxelGridSize(int size){
	mVoxelGridSize = size;
}

void VoxelConeTracingComponent::setSVOChildId(GLuint id) {
	mSVOChildId = id;
}

void VoxelConeTracingComponent::setSVODiffuseId(GLuint id) {
	mSVODiffuseId = id;
}

void VoxelConeTracingComponent::pushAdditional() {
	glUniform3f(mViewPositionLocation, 
		mViewPosition.x, mViewPosition.y, mViewPosition.z);

	glUniform1f(mVoxelWorldSizeLocation, mVoxelWorldSize);
	//std::cout << "grid " << mVoxelGridSizeLocation << " " << mVoxelGridSize << std::endl;
	glUniform1i(mVoxelGridSizeLocation, mVoxelGridSize);

	//glBindImageTexture(5, mVoxelTextureId, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA8);

	//glBindImageTexture(5, 0, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA8);

	glBindImageTexture(0, mSVOChildId, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32UI);
	glBindImageTexture(1, mSVODiffuseId, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32UI);
	
	mVoxelTextureLocation = getProgram().uniform("voxelTexture");
	//std::cout << "voxt " << mVoxelTextureLocation << " " << mVoxelTextureId << std::endl;
	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_3D, mVoxelTextureId);
	glUniform1i(mVoxelTextureLocation, 0);

	mLights->pushToShader();

	mAOTexture->setProgram(getProgram());
	mAOTexture->setUniformName("aoTexture");
	mAOTexture->pushToShader(20);

	DifferredComponent::pushAdditional();
}

void VoxelConeTracingComponent::setLights(Lights *l){
	mLights = new Lights(*l);
}

void VoxelConeTracingComponent::setAOTexture(Texture *t) {
	mAOTexture = t;
}
