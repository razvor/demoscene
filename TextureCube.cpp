#include "TextureCube.h"

TextureCube::TextureCube(int w, int h){
	mWidth = w;
	mHeight = h;
}

TextureCube::~TextureCube(){

}

GLint TextureCube::getId(){
	return mId;
}

glm::vec2 TextureCube::getSize(){
	return glm::vec2(mWidth, mHeight); 
}

void TextureCube::setInternalFormat(GLint f){
	mInternalFormat = f;
}

void TextureCube::setFormat(GLenum f){
	mFormat = f;
}

void TextureCube::setDataType(GLenum f){
	mDataType = f;
}

void TextureCube::setUniformName(std::string un){
	mUnifName = un;
}

void TextureCube::create(){
	glGenTextures(1, &mId);
	glBindTexture(GL_TEXTURE_CUBE_MAP, mId);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
 	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	//glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_GENERATE_MIPMAP, GL_TRUE);

	/*for(int i=0; i<mTexturesCount; ++i){
		for(int l=0; l<2; ++l){
			glTexImage2D(
				GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 
				l, 
				mInternalFormat, 
				mWidth / pow(2, l), 
				mHeight / pow(2, l), 
				0, 
				mFormat, 
				mDataType, 
				NULL
			);
		}
	}*/

	glTexStorage2D(GL_TEXTURE_CUBE_MAP, (int)log2(mWidth), mInternalFormat, mWidth, mHeight);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

}

void TextureCube::init(){
	if(!mIsInited){
		mIsInited = true;
		create();
	}
}

void TextureCube::pushToShader(Program &program, int ind){
	
	int mUnifLocation = program.uniform(mUnifName.c_str());

	//std::cout << "Push to shader " << mUnifLocation << " " << ind << " " << mId << std::endl;

	glActiveTexture(GL_TEXTURE0 + ind);
	glBindTexture(GL_TEXTURE_CUBE_MAP, mId);
	glUniform1i(mUnifLocation, ind);

}

