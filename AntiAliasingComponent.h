#include "DifferredComponent.h"

#pragma once

class AntiAliasingComponent : public DifferredComponent {
private:
	
public:

	void init();
	void initProgram();
	void initLocations();
	void pushAdditional();

};