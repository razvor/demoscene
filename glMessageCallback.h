#ifndef GL_MESSAGE
#define GL_MESSAGE

#include "includer.h"

void GLDebugMessageCallback(GLenum source, GLenum type, GLuint id,
            GLenum severity, GLsizei length,
			const GLchar *msg, const void *data)
{
	char* _source;
	char* _type;
	char* _severity;
	switch (source) {
		case GL_DEBUG_SOURCE_API:
				_source = "API";
		break;
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
				_source = "WINDOW SYSTEM";
		break;
		case GL_DEBUG_SOURCE_SHADER_COMPILER:
				_source = "SHADER COMPILER";
		break;
		case GL_DEBUG_SOURCE_THIRD_PARTY:
				_source = "THIRD PARTY";
		break;
		case GL_DEBUG_SOURCE_APPLICATION:
				_source = "APPLICATION";
		break;
		case GL_DEBUG_SOURCE_OTHER:
				_source = "UNKNOWN";
		break;
		default:
				_source = "UNKNOWN";
		break;
	}
	switch (type) {
		case GL_DEBUG_TYPE_ERROR:
				_type = "ERROR";
		break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
				_type = "DEPRECATED BEHAVIOR";
		break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
				_type = "UDEFINED BEHAVIOR";
		break;
		case GL_DEBUG_TYPE_PORTABILITY:
				_type = "PORTABILITY";
		break;
		case GL_DEBUG_TYPE_PERFORMANCE:
				_type = "PERFORMANCE";
		break;
		case GL_DEBUG_TYPE_OTHER:
				_type = "OTHER";
		break;
		case GL_DEBUG_TYPE_MARKER:
				_type = "MARKER";
		break;
		default:
				_type = "UNKNOWN";
		break;
	}
	switch (severity) {
		case GL_DEBUG_SEVERITY_HIGH:
				_severity = "HIGH";
		break;
		case GL_DEBUG_SEVERITY_MEDIUM:
				_severity = "MEDIUM";
		break;
		case GL_DEBUG_SEVERITY_LOW:
				_severity = "LOW";
		break;
		case GL_DEBUG_SEVERITY_NOTIFICATION:
				_severity = "NOTIFICATION";
		break;
		default:
				_severity = "UNKNOWN";
		break;
	}

	printf("%d: %s of %s severity, raised from %s: %s\n",
				id, _type, _severity, _source, msg);

}

void glCheckError(){

	GLenum e = glGetError();
	switch(e){
		case GL_NO_ERROR:
			//std::cout << "no error " << std::endl;
			break;
		case GL_INVALID_OPERATION:
			std::cout << "invalid operation " << std::endl;
			break;
		case GL_INVALID_ENUM:
			std::cout << "invalid enum" << std::endl;
			break;
		case GL_INVALID_VALUE:
			std::cout << "invalid value" << std::endl;
			break;
		case GL_OUT_OF_MEMORY:
			std::cout << "out of memory" << std::endl;
			break;

		default:
			std::cout << "another error" << e << std::endl;
			break;
	}

}

#endif
