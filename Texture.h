#ifndef TEXTURE
#define TEXTURE

#include "includer.h"
//#include "stb/stb_image.h"
//#include "ImageLoader.h"

class Texture{
private:

	bool isInited = false;

	bool isGeneric = false;
	glm::vec4 mGen;

	unsigned char *mImage;

	std::string file;
	std::string mUnifName;
	int mUnifLocation;

	int mWidth, mHeight;
	GLint mInternalFormat;
	GLenum mFormat, mDataType;
	
	void load(std::string file);
	void create();
	void initLocations(Program &program);


public:
	GLuint mId;

	enum SizeMode{
		CLAMP_EDGE,
		REPEAT
	};
	SizeMode mSizeMode;

	//void load(std::string file, int format);
	
	GLuint getId();
	glm::vec2 getSize();
	void setUniformName(std::string un);
	void setSizeMode(SizeMode sm);

	void onLoad(unsigned char* img, int w, int h);

	void init(Program &program);
	void init();
	void setProgram(Program &program);
	void pushToShader(int ind);

	void setInternalFormat(GLint format);
	void setFormat(GLenum format);
	void setDataType(GLenum type);
	  
	Texture(int width, int height);
	Texture(glm::vec4 clr);
	Texture(std::string file);
	Texture(std::string file, SizeMode sm);
	~Texture();
};

#endif
