#ifndef LIGHT_MESH
#define LIGHT_MESH

#include "includer.h"
#include "RenderMesh.h"
#include "Light.h"
#include "Texture.h"

class LightMesh: public RenderMesh{
private:

	enum LightType{
		Point,
		Sphere,
		Rectangle,
		Spot
	};

	LightType mLightType;

	glm::vec3 mColor;
	RectangleLight *mRl;
	SphereLight *mSl;
	SpotLight *mSp;
	Texture *mGDepth;

	struct UniformLocations{
	  int color;
	} unifLocations;
	
	void initGeometry();
	void initSphereGeometry(float radius);
	void initRectGeometry();
	void initSpotGeometry();

public:

	void setGDepth(Texture *t);

	void initProgram();
	void init();

	void initLocations();
	void pushUniforms();

	LightMesh(glm::vec3 pos, glm::vec3 color);

	LightMesh(SpotLight *sp);
	LightMesh(SphereLight *sl);
	LightMesh(RectangleLight *rl);

	~LightMesh();
};

#endif
