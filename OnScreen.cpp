#include "OnScreen.h"

OnScreen::OnScreen(){

}

OnScreen::~OnScreen(){

}

void OnScreen::addTexture(GLuint id){
	mTextureIds.push_back(id);
}

void OnScreen::setShaderFile(string name){
	mShaderFile = name;
}

void OnScreen::init(){
	
	mMatrix = glm::mat4(1.0);

	mProgram = Program(mShaderFile, mShaderFile);
	mProgram.init();

	matrixLocation = mProgram.uniform("matrix");

	for(int i = 0; i < mTextureIds.size(); ++i){
		texturesLocations.push_back( mProgram.uniform("tex") );
	}

	positions = {
		glm::vec3(-1.0, -1.0, 0),
		glm::vec3(-1.0, 1.0, 0),
		glm::vec3(1.0, 1.0, 0),
		glm::vec3(1.0, -1.0, 0)
	};
	indices = {
		0, 1, 2,
		2, 3, 0
	};
	texCords = {
		glm::vec2(0.0, 0.0),
		glm::vec2(0.0, 1.0),
		glm::vec2(1.0, 1.0),
		glm::vec2(1.0, 0.0)
	};

	verticesBuffer = new VertexBuffer(1, positions);
	indicesBuffer = new VertexBuffer(2, indices);
	texCordsBuffer = new VertexBuffer(1, texCords);

}

void OnScreen::render(){
	mProgram.use();

	verticesBuffer->render(0);
	texCordsBuffer->render(1);

	glUniformMatrix4fv( matrixLocation, 1, GL_FALSE, &mMatrix[0][0] );

	//for(int i = 0; i < mTextureIds.size(); ++i){
		glActiveTexture(GL_TEXTURE0 + 0);
		glBindTexture(GL_TEXTURE_2D, mTextureIds[0]);
		glUniform1i(texturesLocations[0], 0);
		//glActiveTexture(0);
	//}

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, indicesBuffer->getInstance() );
	glDrawElements( GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, NULL );
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
