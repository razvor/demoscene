#ifndef FIRST_LIGHT_COMPONENT
#define FIRST_LIGHT_COMPONENT

#include "includer.h"
#include "DifferredComponent.h"
#include "Light.h"

class FirstLightComponent : public DifferredComponent{
private:

	glm::vec3 mViewPosition;
	Lights *mLights = NULL;

	int mViewPositionLocation;

	void pushAdditional();

	void initProgram();
	
public:
	void initLocations();

	void setLights(Lights *l);
	void setViewPosition(glm::vec3 vp);

	void init();

	FirstLightComponent();
	~FirstLightComponent();
		
};

#endif
