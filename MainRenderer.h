#ifndef MAIN_RENDERER
#define MAIN_RENDERER

#include "includer.h"
#include "RenderMesh.h"
#include "BoxMesh.h"
#include "FloorMesh.h"
#include "Camera.h"
#include "Texture.h"
#include "OnScreen.h"
#include "LocalCubemap.h"
#include "DifferredRenderer.h"
#include "Window.h"
#include "Model.h"
#include "Voxelizator.h"
#include "VoxelConeTracingComponent.h"
#include "AmbientOcclusionVolumes.h"
#include "RoomModel.h"

class MainRenderer{
private:
	
	Window mWindow;

	int viewportWidth, viewportHeight;
	glm::vec3 mViewPosition;

	glm::mat4 
	  mProjectionMatrix,
	  mViewMatrix;

	DifferredRenderer *mDifferredRenderer;

	Program mProgram;

	int mMode;

	Lights *lights;
	Camera camera;
	OnScreen onScreen;

	GLuint framebuffer;
	GLuint renderbuffer;
	GLuint bufferTexture;

	float rr = 0;

	std::vector<RenderMesh *> meshes; 
	std::vector<Model *> models; 
	std::vector<LocalCubemap *> localCubemaps;

	Voxelizator *mVoxelizator;
	AmbientOcclusionVolumes *mAmbientOcclusionVolumes;

	RoomModel *mRoomModel;

	//std::vector<GL>
	//LocalCubemap lcm;

	void initParams();

	void initGL();
	void initDifferredRenderer();
	void initFrameBuffers();
	void initCamera();
	void initObjects();
	void initLightsObjects();
	void initLocalCubemaps();

	void clear();

	void renderShadows();
	void renderOclusion();
	void renderObjects();
	void renderLights();

	void renderLocalCubemaps();
	void renderLocalCubemapsMeshes();

public:

	void setWindow(Window &w);
	void setViewportSize(int w, int h);
	void setViewPosition(glm::vec3 vp);

	void init();
	void renderToCubemap(glm::mat4 matr, float dir, Texture *t);
	void render();
	void event(int key, int act);

	MainRenderer();
	~MainRenderer();

};

#endif
