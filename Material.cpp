#include "Material.h"

Material::Material(std::string p, float IOR){
	mPath = p;
	load();

	mIOR = IOR;

	/*mIOR = new Texture(glm::vec4(IOR));
	mIOR->setUniformName("ior");*/
}

Material::Material(glm::vec4 albedo, float rough, float metal, float IOR) {
	mAlbedo = new Texture(albedo);
	mAlbedo->setUniformName("albedo");

	mNormals = new Texture(glm::vec4(0.5, 0.5, 1.0, 0));
	mNormals->setUniformName("normals");

	mRoughness = new Texture(glm::vec4(rough, 0, 0, 0));	
	mRoughness->setUniformName("roughness");

	mMetallness = new Texture(glm::vec4(metal, 0, 0, 0));
	mMetallness->setUniformName("metallness");

	/*mIOR = new Texture(glm::vec4(IOR));
	mIOR->setUniformName("ior");*/

	mIOR = IOR;

	/*mOcclusion = new Texture(glm::vec4(1.0));
	mOcclusion->setUniformName("occlusion");*/

}

Material::~Material(){
	if(mAlbedo) delete mAlbedo;
	if(mNormals) delete mNormals;
	if(mRoughness) delete mRoughness;
	if(mMetallness) delete mMetallness;
}

void Material::load(){
	std::string path = mPath + "/";
	mAlbedo = new Texture(path + "albedo.jpg",
		Texture::SizeMode::REPEAT
		);
	//mAlbedo->load(path + "albedo.tga", 1);
	mAlbedo->setUniformName("albedo");

	mNormals = new Texture(path + "normals.jpg", 
		Texture::SizeMode::REPEAT
		);
	mNormals->setUniformName("normals");

	mRoughness = new Texture(path + "roughness.jpg",
		Texture::SizeMode::REPEAT
		);
	mRoughness->setUniformName("roughness");

	mMetallness = new Texture(path + "metallness.jpg",
		Texture::SizeMode::REPEAT
		);
	mMetallness->setUniformName("metallness");

	//mIOR = new Texture(glm::vec4(IOR));
	//mIOR->setUniformName("ior");

	/*mOcclusion = new Texture(path + "roughness.jpg",
		Texture::SizeMode::REPEAT
	);
	mOcclusion->setUniformName("roughness");*/

	/*mGlossiness = new Texture(path + "glossiness.jpeg",
		Texture::SizeMode::REPEAT
		);
	mMetallness->setUniformName("glossiness");*/
}

void Material::init(Program program){
	mAlbedo->init(program);
	mRoughness->init(program);
	mMetallness->init(program);
	mNormals->init(program);
	//mIOR->init(program);

	glUniform1f(program.uniform("ior"), mIOR);

	//mOcclusion->init(program);
	//mGlossiness->init(program);
}

void Material::pushToShader(int i){
	mAlbedo->pushToShader(i+0);
	mNormals->pushToShader(i+1);
	mRoughness->pushToShader(i+2);
	mMetallness->pushToShader(i+3);
	//mIOR->pushToShader(i+4);

	

	//mOcclusion->pushToShader(i + 4);
	//mGlossiness->pushToShader(i+4);

}
