#ifndef ON_SCREEN
#define ON_SCREEN

#include "includer.h"

class OnScreen{
private:

	std::vector<glm::vec3> positions;
	std::vector<glm::vec2> texCords;
	std::vector<int> indices;

	string mShaderFile;

	VertexBuffer *verticesBuffer, *texCordsBuffer, *indicesBuffer;

	glm::mat4 mMatrix;

	std::vector<GLuint> mTextureIds;

	int positionLocation, texCordLocation, matrixLocation;
	std::vector<int> texturesLocations;

public:
	
	Program mProgram;
	
	void addTexture(GLuint id);
	void setShaderFile(string a);

	void setMatrix(glm::mat4 matr);
	void init();
	void render();

	OnScreen();
	~OnScreen();

};

#endif
