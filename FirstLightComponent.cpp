#include "FirstLightComponent.h"

FirstLightComponent::FirstLightComponent(){

}

FirstLightComponent::~FirstLightComponent(){

}

void FirstLightComponent::setLights(Lights *l){
	mLights = new Lights(*l);
}

void FirstLightComponent::setViewPosition(glm::vec3 vp){
	mViewPosition = vp;
}

void FirstLightComponent::init(){
	this->initProgram();
	DifferredComponent::init();
	mLights->init(getProgram());
}

void FirstLightComponent::initProgram(){
	setProgram(Program("lightFirstPass", "lightFirstPass"));
}

void FirstLightComponent::initLocations(){
	DifferredComponent::initLocations();
	mViewPositionLocation = getProgram().uniform("viewPosition");
}

void FirstLightComponent::pushAdditional(){
	glUniform3f(mViewPositionLocation, 
		mViewPosition.x, mViewPosition.y, mViewPosition.z);

	mLights->pushToShader();

	DifferredComponent::pushAdditional();
}
