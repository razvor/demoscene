#include "CellingMesh.h"

CellingMesh::CellingMesh(){

}

CellingMesh::~CellingMesh(){

}

void CellingMesh::init(){
	std::cout << "CELLING MESH" << std::endl;
	initGeometry();
	RenderMesh::init();
}

void CellingMesh::initProgram(){
	mProgram = Program("mainVertex", "firstPass");
	if(!mProgram.init())
		std::cout << "Cant init prorgam of celling mesh" << std::endl;
}

void CellingMesh::initGeometry(){
	float side = 10.f;

	std::vector<glm::vec3> verts = {
		glm::vec3(-1.0, 0.0, -1.0),
		glm::vec3(-1.0, 0.0, 1.0),
		glm::vec3(1.0, 0.0, 1.0),
		glm::vec3(1.0, 0.0, -1.0)
	};
	for(auto &v : verts)
	  v *= side/2;

	std::vector<glm::vec3> norms = {
		glm::vec3(0, -1.0, 0),
		glm::vec3(0, -1.0, 0),
		glm::vec3(0, -1.0, 0),
		glm::vec3(0, -1.0, 0)
	};

	std::vector<int> inds = {
		0, 1, 2,
		2, 3, 0
	};

	glm::vec2 tt[4] = {
		glm::vec2(0, 0),
		glm::vec2(0, side),
		glm::vec2(side, side),
		glm::vec2(side, 0)
	};

	std::vector<glm::vec2> texCords = {
		tt[0], tt[1], 
		tt[2], tt[3]
	};

	setVertices(verts);
	setNormals(norms);
	setIndices(inds);
	setTexCords(texCords);

	setPosition(glm::vec3(0, 3, 0));
	setRotation(glm::vec3(0));
	setScale(glm::vec3(1.0));

	Material *material = new Material("../models/celling1");
	setMaterial(material);
}


