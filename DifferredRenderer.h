#ifndef DIFFERRED_RENDERER
#define DIFFERRED_RENDERER

#include "includer.h"
#include "RenderMesh.h"
#include "Light.h"
#include "Camera.h"
#include "LocalCubemap.h"
#include "GBuffer.h"
#include "DifferredComponent.h"
#include "FirstLightComponent.h"
#include "ReflectLightComponent.h"
#include "TextureCube.h"
#include "FramebufferCube.h"
#include "VoxelConeTracingComponent.h"
#include "AntiAliasingComponent.h"
#include "Model.h"
//#include "glMessageCallback.h"

#include "DebugHelper.h"

class DifferredRenderer{
private:
	
	GBuffer *mGBuffer;
	GBuffer *mLCMGBuffer;
	GBuffer *mShadowBuffer;
	Texture *mTargetTexture;
	TextureCube *mTargetTextureCube;
	Framebuffer *mFramebuffer;
	FramebufferCube *mFramebufferCube;
	FirstLightComponent *mFirstLightComponent;
	ReflectLightComponent *mReflectLightComponent;
	AntiAliasingComponent *mAntiAliasingComponent;

	std::vector<RenderMesh*> mMeshes;
	std::vector<Model*> mModels;
	std::vector<LocalCubemap*> mCubemaps;
	Lights *mLights;
	glm::mat4 mProjectionMatrix;
	glm::mat4 mViewMatrix;
	glm::vec3 mViewPosition;
	int mViewportWidth, mViewportHeight;
	int mState;
	bool mToTexture;

	void beginCapture();
	void endCapture();

	void firstPass();
	
	void firstLightPass();

	void forwardPass();
	void voxelGIPass();

	void reflectLightPass();
	void antiAliasingPass();

public:

	VoxelConeTracingComponent *mVoxelConeTracingComponent;

	void addMesh(RenderMesh *mesh);
	void addModel(Model *model);
	void addLocalCubemap(LocalCubemap *c);
	void setLights(Lights *l);
	void setMatrices(glm::mat4 proj, glm::mat4 view);
	void setViewportSize(int w, int h);
	void setViewPosition(glm::vec3 vp);
	void setState(int s);
	void setTargetTexture(Texture *t);
	void setVoxelTextureId(GLuint id);
	void setVoxelWorldSize(float size);

	void init();

	void renderFirstLight(glm::mat4 mat);
	void renderVoxelGI();
	void renderAA();
	void captureLocalCubemaps();
	void renderFinal();

	void captureEnvMap(int i);
	void captureEnvMaps();

	DifferredRenderer();
	~DifferredRenderer();

};

#endif
